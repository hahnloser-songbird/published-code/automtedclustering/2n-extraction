function        [spec_plot,Bz,Bz_mask]=func_extract_spec(Flat,winl,winr,spec_plot_IDs,reduced_nfft)

if nargin<5 | ~isfield(Flat.p,'nfft_iDisp')
    reduced_nfft=0;
end
if isfield(Flat.v,'spec_plot')
    spec_plot=Flat.v.spec_plot;
else
    spec_plot=[];
end
if isfield(Flat.v,'spec_mask_display') &&  ~isempty(Flat.v.spec_mask_display) &&  isfield(Flat.DAT,Flat.v.spec_mask_display)
    do_mask=true; %% XXX
    multiplier=0.25;
else
    do_mask=false;
    multiplier=0;
end
Flat.p.spec_num=length(spec_plot_IDs);


%% fullspec from memory
if isfield(Flat.p,'spec_full_mode') && Flat.p.spec_full_mode==1
    l=length(Flat.p.nfft_i);
    if isfield(Flat.p,'nfft_iDisp') & reduced_nfft
        l=length(Flat.p.nfft_iDisp);
    end
    Ldisp=length(Flat.p.channel_mic_display);
    if length(Flat.p.CHANNEL_MIC)>1 && Ldisp>1
        % diff from below
        % sampling rate of 44100 leads to eventual errors
        Bz=zeros(Flat.p.spec_num*l*Ldisp,ceil(winl/Flat.p.nonoverlap)+floor(winr/Flat.p.nonoverlap));
        Bz_mask=multiplier*ones(Flat.p.spec_num*l*Ldisp,ceil(winl/Flat.p.nonoverlap)+ceil(winr/Flat.p.nonoverlap));
        % same as below
        leave_in=~(spec_plot_IDs==0);
        a_sp=max(1,abs(spec_plot_IDs));
        % diff from below
        %   BBh=zeros(Ldisp*l,size(Flat.DAT.data{Flat.X.DATindex(a_sp)}{1},2));
        BBh=Flat.DAT.datas(Flat.X.DATindex(a_sp));
        % same as below
        lbuff=ceil(winl/Flat.p.nonoverlap);
        rbuff=floor(winr/Flat.p.nonoverlap);
        hh=find(leave_in); if size(hh,1)>size(hh,2); hh=hh'; end
        m=Flat.p.CHANNEL_MIC(Flat.p.channel_mic_display);
        h=length(hh);
        for i=hh
            %   i0=ceil(Flat.X.indices_all(abs(spec_plot_IDs(i)))/Flat.p.nonoverlap);
            % formula corresponding to func_stack_which_buffer
            indices_all=Flat.X.indices_all(a_sp(i));
            i0=max(1,ceil((indices_all-Flat.p.nfft)/Flat.p.nonoverlap)+1);
            on=min(lbuff,i0);
            % NEW
            %off=min(rbuff,size(BBh{1}{1},1)-i0); %off=min(rbuff,size(a,2)-i0);
            BBhi=BBh{i};
            for k=1:length(Flat.p.channel_mic_display)
                off=min(rbuff,size(BBhi{1},1)-i0);
                if  Flat.v.display_multi==0
                    Bz((i-1)*l*Ldisp+(k-1)*l+1:(i-1)*l*Ldisp+k*l,lbuff-on+1:lbuff+off)=BBhi{m(k)}(i0-on+1:i0+off,end:-1:1)';
                else
                    Bz((k-1)*l*h+(i-1)*l+1:(k-1)*l*h+i*l,lbuff-on+1:lbuff+off)=BBhi{m(k)}(i0-on+1:i0+off,end:-1:1)';
                end
            end
            % OLD
            %             a=[BBh{i}{Flat.p.CHANNEL_MIC(Flat.p.channel_mic_display)}];
            %             off=min(rbuff,size(a,1)-i0);
            %           %  for j=1:Ldisp
            %                 %  BBh((j-1)*Ldisp+1:j*Ldisp)=Flat.DAT.data(Flat.X.DATindex(a_sp));
            %                 Bz((i-1)*l*Ldisp+1:(i-1)*l*Ldisp+Ldisp*l,lbuff-on+1:lbuff+off)=a(i0-on+1:i0+off,end:-1:1)';
            %            % end
            %
        end
    else
        
        %Bz=double(min(min(Flat.DAT.data{1})))*ones(Flat.p.spec_num*l,ceil(winl/Flat.p.nonoverlap)+floor(winr/Flat.p.nonoverlap));
        %Bz=zeros(Flat.p.spec_num*l,ceil(winl/Flat.p.nonoverlap)+floor(winr/Flat.p.nonoverlap));
        Bz=NaN(Flat.p.spec_num*l,floor(winl/Flat.p.nonoverlap)+ceil(winr/Flat.p.nonoverlap));
        Bz_mask=multiplier*ones(Flat.p.spec_num*l,ceil(winl/Flat.p.nonoverlap)+floor(winr/Flat.p.nonoverlap));
        leave_in=~(spec_plot_IDs==0);
        a_sp=min(Flat.num_Tags,max(1,abs(spec_plot_IDs)));
        BBh=Flat.DAT.data(Flat.X.DATindex(a_sp));
        lbuff=floor(winl/Flat.p.nonoverlap);
        rbuff=ceil(winr/Flat.p.nonoverlap);
        hh=find(leave_in); if size(hh,1)>size(hh,2); hh=hh'; end
        for i=hh
            %   i0=ceil(Flat.X.indices_all(abs(spec_plot_IDs(i)))/Flat.p.nonoverlap);
            % formula corresponding to func_stack_which_buffer
            indices_all=Flat.X.indices_all(a_sp(i));
            i0=max(1,ceil((indices_all-Flat.p.nfft)/Flat.p.nonoverlap)+1);
            on=min(lbuff,i0);
            a=BBh{i};
            off=min(rbuff,size(a,2)-i0);
            if reduced_nfft
                Bz((i-1)*l+1:i*l,lbuff-on+1:lbuff+off)=a(Flat.p.nfft_iDisp(end:-1:1),i0-on+1:i0+off);
            else
                Bz((i-1)*l+1:i*l,lbuff-on+1:lbuff+off)=a(end:-1:1,i0-on+1:i0+off);
            end
            %% mask stuff
            if ~do_mask
                continue
            end
            which_file=Flat.X.DATindex(a_sp(i));
            on_off=Flat.DAT.(Flat.v.spec_mask_display){which_file};
            if ~isempty(on_off)
                for j=1:size(on_off,2)
                    on_off_buff=func_numbuffs(on_off(:,j),Flat.p.nfft,Flat.p.nonoverlap);
                    
                    do_mask_this=false;
                    if on_off_buff(1)<i0+off-1
                        on_mask=min(i0-on_off_buff(1),on);
                        do_mask_this=true;
                    else
                        continue
                    end
                    
                    if on_off_buff(2)>i0-on-1
                        off_mask=min(on_off_buff(2)-i0,off);
                        do_mask_this=true;
                    else
                        continue
                    end
                    if do_mask_this
                        Bz_mask((i-1)*l+1:i*l,lbuff-on_mask+1:lbuff+off_mask)=0;
                    end
                end
            end
        end
        
    end
    
    %% XXX ugly
    spec_plot.xs=(1:size(Bz,2))/size(Bz,2)*(Flat.p.spec_winls+Flat.p.spec_winrs);
    %    Ys=0:Flat.scanrate/Flat.p.nfft:Flat.p.spec_num*Flat.scanrate/4;
    Ys=0:Flat.scanrate/Flat.p.nfft:Flat.p.spec_num*8000;%SCANRATEFIX
%    Ys=Flat.p.nfft_iDisp/128*8000*Flat.p.spec_num;
    spec_plot.Ys=Ys(2:end);
    
    %         set(fcH.ha_spcgrm,'YDir','normal');
else
    IBz=zeros(length(Flat.v.spec_plot.Ys),length(Flat.v.spec_plot.xs));
    Bz_mask=zeros(length(Flat.v.spec_plot.Ys),length(Flat.v.spec_plot.xs));
    BB=Flat.X.specdata(abs(Flat.v.spec_plot_IDs));
    %       set(fcH.ha_spcgrm,'YDir','reverse');
    BB=[BB{:}]';
    Bz(1:size(BB,1),1:size(BB,2))=BB;
end
if isfield(Flat.v,'spec_cap')
    Bz=min(Bz,Flat.v.spec_cap);
end
if isfield(Flat.v,'spec_floor')
    Bz=max(Bz,Flat.v.spec_floor);
end
end