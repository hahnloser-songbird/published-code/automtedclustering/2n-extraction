function [p,q] = func_mouseclick_element(Flat, handle)

p = [];
q = [];

% return
    fcH=evalin('base','fcH');

    ha=fcH.(handle);
    p=get(ha,'currentpoint');
    q=p(1,1);
    if strcmp(handle,'ha_spcgrm')
        % make sure scanrate is populated with some data
        if ~isempty(Flat.scanrate)
%            p = ceil(p(1,2)/(Flat.scanrate/4)); % Flat.scanrate/4 might not be optimal
            p = ceil(p(1,2)/8000); %SCANRATEFIX % Flat.scanrate/4 might not be optimal
        else
            p = nan;
        end

    elseif strcmp(handle,'ha_clust')
        p = ceil(p(1,1)/Flat.v.numbuffs_mouseclick);
        if length(Flat.v.order_sequence) < p || p<1
            fprintf('click an element in figure 20\n');
            p=nan;
         end

    elseif strcmp(handle,'ha_templ') || strcmp(handle,'ha_grow')
        p = ceil(p(1,1)/Flat.v.numbuffs);
        
    elseif strcmp(handle,'ha_import2') 
         p = ceil(p(1,1));
         
    end
    
end
%% EOF
