function [Flat,fcH] = func_spec_prepare_plot(Flat,fcH)

    Flat.v.spec_plot_IDs = func_get_spec_plot_IDs(Flat);
    [Flat,fcH] = func_get_specgrams(Flat,fcH,abs(Flat.v.spec_plot_IDs),0);
    
    if isfield(Flat,'umap') && isfield(Flat.umap,'brute_force') && isfield(Flat.umap.brute_force,'specID_examples')% && ~isempty(Flat.BOARD.tagboard{4}) && strcmp(Flat.BOARD.tagboard{4}{1}(1),'C')
        Flat.v.spec_plot_IDs(1:2)=Flat.umap.brute_force.specID_examples;
        Flat.umap.brute_force=rmfield(Flat.umap.brute_force,'specID_examples');
    end

end
%% EOF