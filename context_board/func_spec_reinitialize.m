function [Flat,fcH]=func_spec_reinitialize(Flat,fcH)
if Flat.num_Tags>0
    %fcH=evalin('base','fcH');
    
    Flat.X.specdata=cell(1,Flat.num_Tags);
    Flat.v.spec_resize=0;
    
    winsize=get(fcH.hf_spcgrm,'position');
    Flat.v.spec_plot.winsize=winsize(3);
    Flat.v.spec_plot.nfft=2.^(ceil(log2(winsize(4)/Flat.p.spec_num*4)));
    Flat.v.spec_plot.ovlp=max(0,Flat.v.spec_plot.nfft-2^ceil(-.5+log2((Flat.p.spec_winls+Flat.p.spec_winrs)*Flat.scanrate/winsize(3))));
    
    xs=0:(Flat.v.spec_plot.nfft-Flat.v.spec_plot.ovlp)/Flat.scanrate:(Flat.p.spec_winls+Flat.p.spec_winrs);
    Flat.v.spec_plot.xs=xs(1:end-2);
    
    ys=0:Flat.scanrate/Flat.v.spec_plot.nfft:8000;%SCANRATEFIX Flat.scanrate/4;
    Flat.v.spec_plot.ys=ys(2:end);
    Ys=0:Flat.scanrate/Flat.v.spec_plot.nfft:Flat.p.spec_num*8000;%SCANRATEFIX Flat.scanrate/4;
    Flat.v.spec_plot.Ys=Ys(2:end);
    Flat.Bz=zeros(length(Flat.v.spec_plot_IDs)*length(Flat.p.nfft_i),ceil(Flat.p.spec_winls*Flat.scanrate/Flat.p.nonoverlap)+ceil(Flat.p.spec_winrs*Flat.scanrate/Flat.p.nonoverlap));
    set(fcH.ha_spcgrm,'YDir','reverse','XLim',[0 Flat.v.spec_plot.xs(end)],'YLim',[0 Flat.v.spec_plot.Ys(end)]); % update the axes
 

end
 

fprintf('Window 24 reinitialized\n');

end
