function             [Flat,fcH]=func_help_evalkey_tab(Flat,fcH)

Flat.v.run_select=0;
Flat.v.run_select_clust=0;
Flat.v.run_set_consistency=0;
Flat.v.set_spec_consistency=1;

%% remove white lines in spec fig
for i=1:Flat.p.spec_num
    set(fcH.hl_spcgrm_wline{i},'visible','off');
end

%% remove green lines in DAT fig
% check if figure is open.
if isfield(fcH,'hf_dat_lines') && ishandle(fcH.hf_dat_lines{1})
    for i=1:length(fcH.hf_dat_lines)
        set(fcH.hf_dat_lines{i},'visible','off');
    end
end
Flat.v.plot_clust_white_lines=1;
end