function func_mousewheel(src, evnt)
% % handle mouse wheel event 
% % only for figure 24 at the moment
% % <ah> 12/04/12

%% read in from workspace
Flat = evalin('base', 'Flat');
fcH = evalin('base', 'fcH');

%% per default do everything, for speed remove some todo's
Flat.v.spec_show_str=0;
Flat.v.run_set_consistency=1;
Flat.v.run_select=1;
Flat.v.run_select_clust=1;
Flat.v.plot_clust=1;
Flat.v.plot_spec=1;
Flat.v.plot_clust_fast=0;
Flat.v.plot_templates=0;
Flat.v.prepare_plot=0;
Flat.v.set_spec_consistency=0;
Flat.v.spec_skip_plot=0;
spec_keypress = 1;

if src==24  % context figure    
   
    
    %$ Displays elements to the left within the current cluster.  $%
    %* The elements the left are displayed. Which elements these are depends on your sorting mode. *%
    if (evnt.VerticalScrollCount<0)  %     case 'leftarrow'
        if Flat.v.zoom_level>0
            Flat.v.leftmost_element=max(1,Flat.v.leftmost_element-Flat.p.spec_num);
            Flat.v.prepare_plot=1;
            % [Flat,fcH]=func_spec_prepare_plot(Flat,fcH); %%
            Flat.v.run_set_consistency=0;
            Flat.v.run_select=0;
            Flat.v.run_select_clust=0;
            Flat.v.plot_clust=0;
        end
    else  %case 'rightarrow'
        %$ Displays elements to the right within the current cluster.  $%
        %* The elements the r are displayed. Which elements these are depends on your sorting mode. *%
        if Flat.v.zoom_level>0
            Flat.v.leftmost_element=max(1,min(1+length(Flat.v.select_clust)-Flat.p.spec_num,Flat.v.leftmost_element+Flat.p.spec_num));
            Flat.v.prepare_plot=1;
            %[Flat,fcH]=func_spec_prepare_plot(Flat,fcH);
            Flat.v.run_set_consistency=0;
            Flat.v.run_select=0;
            Flat.v.run_select_clust=0;
            %                Flat.v.plot_clust_fast=1;
            Flat.v.plot_clust=0;
        end
        
    end
    

    % mimik space pressed...
    Flat.v.spec_skip_plot=0; 
    Flat.v.plot_clust=1;
    Flat.v.run_set_consistency=0;
    
    %%
    set(src, 'UserData', spec_keypress);
    
    %% selection
  
    [Flat, Flat.v.select, Flat.v.select_clust]=func_select_mode(Flat);
    
    
    %% consistency
    if  Flat.v.set_spec_consistency
        [Flat,fcH]=func_set_spec_consistency(Flat,fcH);
    end
    
    if Flat.v.run_set_consistency
        Flat=func_set_consistencies(Flat);
    end
    
    [Flat,fcH] = plot_figs(Flat, fcH);


elseif src==32
    
    Flat.p.umap.time=Flat.p.umap.time+evnt.VerticalScrollCount;
    
    [fcH,Flat]=func_help_umap_map_to_I(fcH,Flat);
    [fcH,Flat]=func_umap_select(fcH,Flat,Flat.umap.X);
    
end


%% write back to workspace
assignin('base', 'Flat', Flat);
assignin('base', 'fcH', fcH);



end
