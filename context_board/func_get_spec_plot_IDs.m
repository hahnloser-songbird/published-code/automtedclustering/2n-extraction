function spec_plot_IDs=func_get_spec_plot_IDs(Flat)

[Flat,order_sequence,dummy]=func_update_order_sequence(Flat,Flat.v.plot_ordered,Flat.v.curr_clust);
pps=1:min(Flat.p.spec_num,length(order_sequence));
spec_plot_IDs=int32(order_sequence(pps));
