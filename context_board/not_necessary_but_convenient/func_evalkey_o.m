function [plot_ordered,status]=func_evalkey_o(Flat)
% 1  = Nearest Neighbor
% 2  = Timestamp
% 3  = Duration
% 4  = Left context
% 5  = Right context
% 6  = Template similarity (c)
% 7  = Random
% 8  = custom
% 9  = Tag Pointer
% 10 = Localpeak
% 11 = FR
% 12 = inverse Timestamp
% 13 = Timestamp+tagpoint
% 14 = pitch (activator_pitch_shift)
% 15 = syllable duration (activator_syllable_stretch)
% 16 = syllable-gap duration (activator_syllable_gap_stretch)
% 17 = Smart move 
% 18 = stack_win1

% next edit  func_update_order_sequence.m
Lstring={'Timestamp','Duration', ...
                        'Random'};
                    
                    
    [which,status] = listdlg('PromptString','Order Elements', ...
        'SelectionMode', 'single', ...
        'ListString', Lstring);
    
    drawnow();    % Bugfix to prevent blocking on Windows; Important!
    if status
        plot_ordered=which;
    else
        plot_ordered=Flat.v.plot_ordered;
    end
    
end
%% EOF
