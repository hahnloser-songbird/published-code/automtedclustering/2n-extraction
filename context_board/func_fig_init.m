function fcH=func_fig_init(fcH,Flat,scanrate,init)

if nargin<4
    init=0;
end
marktext_color= [1 0.7 1];
text_color = [1 0 0];
fsize=ceil(12-Flat.p.spec_num/10);



%% spectrogram figure

%figure(24);
set(fcH.hi_spcgrm,'XData', 1:1000, 'YData', zeros(250, 1), 'CData', zeros(250, 1000));
set(fcH.hl_spcgrm{1},'XData',[0 0],'YData',[0 1e4],'Color','r','LineStyle','-.','Visible','off');
set(fcH.hl_spcgrm{2},'XData',[0 0],'YData',[0 1e4],'Color','r','LineStyle',':','Visible','off');
set(fcH.hl_spcgrm{3},'XData',[0 0],'YData',[0 1e4],'Color','r','LineStyle',':','Visible','off');
%axis ij

[Flat,fcH]=func_spec_reinitialize(Flat,fcH);

hold on;
set(fcH.hbig_spcgrm,'position',[Flat.p.spec_winls*.666 scanrate/8*Flat.p.spec_num],'string','','color','w','fontsize',36);
figure(24);
for i=1:Flat.p.spec_num
    if i>length(fcH.hf_spcgrm_text)
        fcH.hf_spcgrm_text{i}=text(0,0,'','color','w','BackgroundColor', 'k', 'FontWeight', 'bold','linewidth',1);
    end
    set(fcH.hf_spcgrm_text{i},'position',[0.01,(i-1)*8000+6000,0],'String',num2str(Flat.p.spec_num+1-i),'color','w','fontsize',fsize, 'BackgroundColor', 'k', 'FontWeight', 'bold','linewidth',1);
    
    if i>length(fcH.hf_spcgrm_textL)
        fcH.hf_spcgrm_textL{i}=text(0.01,(i-1)*8000+2000,0,num2str(i),'color','g','fontsize',9,'BackgroundColor', 'k', 'linewidth',1,'fontweight','bold');
    end
    set(fcH.hf_spcgrm_textL{i},'position',[0.01,(i-1)*8000+2000,0],'String',num2str(Flat.p.spec_num+1-i),'color','g','fontsize',9,'BackgroundColor', 'k', 'linewidth',1,'fontweight','bold');
    
    if i>length(fcH.hf_spcgrm_textR)
        fcH.hf_spcgrm_textR{i}=text(0,0,'','color','g','fontsize',9,'BackgroundColor', 'k','linewidth',1,'fontweight','bold');
    end
    set(fcH.hf_spcgrm_textR{i},'position',[0.95*(Flat.p.spec_winls+Flat.p.spec_winrs),(i-1)*8000+2000,0],'String',num2str(Flat.p.spec_num+1-i),'color','g','fontsize',9,'BackgroundColor', 'k','linewidth',1,'fontweight','bold');
    
    if i>length(fcH.hf_spcgrm_marktext)
        fcH.hf_spcgrm_marktext{i}=text(0,0,'','color',marktext_color,'fontsize',fsize,'BackgroundColor', 'k','fontweight','bold','linewidth',1,'visible','off');
    end
    set(fcH.hf_spcgrm_marktext{i},'position',[0.93*(Flat.p.spec_winls+Flat.p.spec_winrs),(i-1)*8000+6000,0],'String',num2str(Flat.p.spec_num+1-i),'color',marktext_color,'fontsize',fsize,'BackgroundColor', 'k','fontweight','bold','linewidth',1,'visible','off');
    
    if i>length(fcH.hl_spcgrm_wline)
        fcH.hl_spcgrm_wline{i}=plot([0 0],[0 0],'color','w','visible','on','linewidth',1);
    end
    set(fcH.hl_spcgrm_wline{i},'XData',Flat.p.spec_winls*[1 1],'YData',[(i-1)*8000 i*8000],'color','w','visible','on','linewidth',1);
end

for i=Flat.p.spec_num+1:length(fcH.hl_spcgrm_wline)
    set(fcH.hl_spcgrm_wline{i},'visible','off');
end
fcH.hl_spcgrm_wline=fcH.hl_spcgrm_wline(1:Flat.p.spec_num);

for i=Flat.p.spec_num+1:length(fcH.hf_spcgrm_text)
    set(fcH.hf_spcgrm_text{i},'visible','off');
end
fcH.hf_spcgrm_text=fcH.hf_spcgrm_text(1:Flat.p.spec_num);

for i=Flat.p.spec_num+1:length(fcH.hf_spcgrm_textL)
    set(fcH.hf_spcgrm_textL{i},'visible','off');
end
fcH.hf_spcgrm_textL=fcH.hf_spcgrm_textL(1:Flat.p.spec_num);

for i=Flat.p.spec_num+1:length(fcH.hf_spcgrm_textR)
    set(fcH.hf_spcgrm_textR{i},'visible','off');
end
fcH.hf_spcgrm_textR=fcH.hf_spcgrm_textR(1:Flat.p.spec_num);

for i=Flat.p.spec_num+1:length(fcH.hf_spcgrm_marktext)
    set(fcH.hf_spcgrm_marktext{i},'visible','off');
end

fcH.hf_spcgrm_marktext=fcH.hf_spcgrm_marktext(1:Flat.p.spec_num);

if evalin('base','exist(''ProjectInfo'',''var'')') && ~isempty(Flat.birdname)
    dd=evalin('base','directoryDelimiter');
    ProjectInfo=evalin('base','ProjectInfo');
    paths = create_path_structure(ProjectInfo.Data_Path, ProjectInfo.Data_Path, dd, Flat.birdname, ProjectInfo.homedir, [],[]);
    assignin('base','paths',paths);
end
end