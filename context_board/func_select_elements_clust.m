function select_clust=func_select_elements_clust(Flat,clust_IDs,select)


%% clust_IDs negative ?
if length(clust_IDs)==1 && clust_IDs(1)<0
    %% do complement
    u=unique(Flat.X.clust_ID); u(u==abs(clust_IDs))=[];
    clust_IDs=u;
end

if length(clust_IDs)==1
    %select_clust=find(Flat.X.clust_ID==clust_IDs);
    select_clust=select(Flat.X.clust_ID(select)==clust_IDs);
else
    %select_clust=find(ismember(Flat.X.clust_ID,clust_IDs));
    select_clust=select(ismember(Flat.X.clust_ID(select),clust_IDs));
end

%if Flat.v.sequence_redo
%    Flat.v.sequence_redo=0;
%end
%order=Flat.v.sequence{Flat.v.plot_ordered,select_clust};
%[order sorted]=sort(order,'ascend');
%select_clust=select_clust(sortex);
end
%select_clust=select_clust(Flat.
%select_clust=intersect(select,select_clust);
