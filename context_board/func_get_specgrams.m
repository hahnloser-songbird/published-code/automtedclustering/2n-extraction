function [Flat,fcH] = func_get_specgrams(Flat,fcH, which_elements, do_pause)


if isfield(Flat.p,'spec_full_mode') && Flat.p.spec_full_mode==1 &&  ~any(cellfun(@isempty,Flat.DAT.data(Flat.X.DATindex(which_elements))))
return
end
IFDEBUG_SPEED = false;

if IFDEBUG_SPEED
    tstart = tic;
end

paths = evalin('base', 'paths');
cfg = evalin('base', 'cfg');
dd = evalin('base','directoryDelimiter');
PBdir = GUI_filetype_filter_setup(); % check this ---------------
if Flat.v.spec_resize || ~isfield(Flat.v,'spec_plot')
    [Flat,fcH]=func_spec_reinitialize(Flat,fcH);
end

% temporary fix
if isfield(Flat.v,'spec_plot') &&  ~isfield(Flat.v.spec_plot,'nfft')
        [Flat,fcH]=func_spec_reinitialize(Flat,fcH);
end

%% params
scanrate = Flat.scanrate;
winl=ceil(Flat.p.spec_winls*scanrate);
winr=ceil(Flat.p.spec_winrs*scanrate);
twin=Flat.v.spec_plot.nfft;

% if isfield(Flat.p,'spec_full_mode') && Flat.p.spec_full_mode==1
%     full_mode=1;
%     twin=Flat.p.
% else
%     full_mode=0;
% end

%% check which elements need to be loaded
elements_to_load = cellfun(@(x) isempty(x), {Flat.X.specdata{which_elements}});
which_elements = which_elements(elements_to_load);
if isempty(which_elements)
    return;
end

%% prepare data holding array

nbr_elements = size(which_elements, 2);
data_loop = cell(1, nbr_elements);

%% prepare parameters and variables fro the parfor loop

data_default = zeros(1,winl+winr);
sudirectories = {Flat.DAT.subdir{Flat.X.DATindex(which_elements)}};
filenames = {Flat.DAT.filename{Flat.X.DATindex(which_elements)}};
indices = Flat.X.indices_all(which_elements);
eofs=Flat.DAT.eof(Flat.X.DATindex(which_elements));
sparseFlat.p = Flat.p;
sparseFlat.v = Flat.v;
%sparseFlat.do_raw = Flat.p.do_raw;
sparseFlat.dynamic_range = Flat.dynamic_range;
fromDATchannels=zeros(1,nbr_elements);
mic_channel=cell(1,nbr_elements);

%% Determine whether we can generate the spectrograms from rawdata in memory
try
    mic_channel=Flat.DAT.(['data' num2str(Flat.p.CHANNEL_MIC)])(Flat.X.DATindex(which_elements));
    reloadFromMemory=~any(cellfun(@isempty,mic_channel));
catch
    reloadFromMemory = false;
end

%% If relaodFromMemory: get correct channel IDs if Parser was used
if reloadFromMemory
    fprintf('Generating %i spectrograms from DAT.%s.\n', nbr_elements, Flat.p.Channel_names{Flat.p.CHANNEL_MIC});
else
    if nbr_elements > 1
        fprintf(['Generating ' num2str(nbr_elements) ' spectrograms from ']);
    end
    
    try
      fromDATchannels=Flat.DAT.CHANNELS_SAVED(Flat.p.CHANNEL_MIC,Flat.X.DATindex(which_elements));
        fprintf('%s channel on disk.\n',Flat.p.Channel_names{Flat.p.CHANNEL_MIC});
    catch
        fprintf('channel %d in raw files.\n',Flat.p.CHANNEL_MIC);
    end
end


if IFDEBUG_SPEED
    fprintf('%20s', 'init:');
    toc(tstart);
    tstart = tic;
end

%% Generate the spectrograms

for i = 1 : nbr_elements
    % extract correct information for this file
    subdir = sudirectories{i};
    filename = filenames{i};
    index = indices(i);
    
    if reloadFromMemory
%         if full_mode
%             data_full= sparseFlat.p.spec_raw_scale*double(mic_channel{i})/2^15*sparseFlat.dynamic_range+ sparseFlat.p.spec_raw_offset;
%             data = func_data_to_specgram(data_full, twin, scanrate, twin, sparseFlat.v.spec_plot.ovlp);
%         else
            lsamp = floor(min(winl,index));
            rsamp = ceil(min(winr,length(mic_channel{i})-index));
            data_full = data_default;
            data_full(winl-lsamp+1:winl+rsamp) =  mic_channel{i}(index-lsamp+1:index+rsamp);
            data_full= sparseFlat.p.spec_raw_scale*double(data_full)/2^15*sparseFlat.dynamic_range+ sparseFlat.p.spec_raw_offset;
            data = func_data_to_specgram(data_full, twin, scanrate, twin, sparseFlat.v.spec_plot.ovlp);
%         end
    else
%         if full_mode
%           data = func_load_and_generate_spectrogram(paths, cfg, dd, PBdir, subdir, filename,...
%             1, sparseFlat.p, sparseFlat.v.spec_plot.ovlp, 1, eofs(i), twin, scanrate, data_default, fromDATchannels(i));
%       else
        data = func_load_and_generate_spectrogram(paths, cfg, dd, PBdir, subdir, filename,...
            index, sparseFlat.p, sparseFlat.v.spec_plot.ovlp, winl, winr, twin, scanrate, data_default, fromDATchannels(i));
%         end
    end
    
    % store data
    data_loop{i} = data;
end

if IFDEBUG_SPEED
    fprintf('%20s', 'parfor:');
    toc(tstart);
    tstart = tic;
end



%% Merging: copy data back to main structure
for i = 1 : nbr_elements
    this_element = which_elements(i);
    Flat.X.specdata{this_element} = data_loop{i};
end

if nbr_elements > 1
    %disp(' ... done.');
end

if IFDEBUG_SPEED
    fprintf('%20s', 'rest:');
    toc(tstart)
end

end

%% EOF