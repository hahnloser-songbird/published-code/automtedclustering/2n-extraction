function func_spec_keyrelease()

Flat = evalin('base', 'Flat');
fcH = evalin('base', 'fcH');
%%
if ~Flat.v.plot_clust_fast
[Flat,fcH]=func_spec_keyrelease2(Flat,fcH);
end

%%
assignin('base', 'Flat', Flat);
assignin('base', 'fcH', fcH);

end
%% EOF
