function [Flat,sels]=func_syl_check_overlap(Flat,sels)

return
h=Flat.X.DATindex(sels(1:end-1))==Flat.X.DATindex(sels(2:end)) & (Flat.X.data_off(sels(2:end))<0 |  Flat.X.indices_all(sels(1:end-1))+Flat.X.data_off(sels(1:end-1))>=Flat.X.indices_all(sels(2:end)));
while any(h)
    % simple manual fix, in command window run
    % [Flat,sels,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS});
    % Flat.X.data_off(sels(h))=Flat.X.data_off(sels(h))-Flat.p.nonoverlap;
    fprintf('Overlap in %d vocalizations\n',sum(h));
    if isfield(Flat.v,'ask_overlap') & Flat.v.ask_overlap==0
        return
    else
        y=input('Fix (yes=1, but better delete manually) ? ');
        if y~=1
            Flat.v.ask_overlap=0;
            fprintf('Elements %d\n',sels(find(h)));
            return
        end
        Flat=func_remove_elements(Flat,sels(find(h)+1),0,1);
        [Flat,sels,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS});
        disp('Fixed');
        h=Flat.X.DATindex(sels(1:end-1))==Flat.X.DATindex(sels(2:end)) & (Flat.X.data_off(sels(2:end))<=0 |  Flat.X.indices_all(sels(1:end-1))+Flat.X.data_off(sels(1:end-1))>=Flat.X.indices_all(sels(2:end)));
    end
end
end