function Flat=func_set_consistencies(Flat)

Flat.v.run_set_consistency=0;

%% zoom level and Flat.v.max_elements_on_screen
% if  Flat.v.spec_data_mismatch
%     Flat.v.max_elements_on_screen=floor(2^Flat.v.zoom_level*Flat.v.Screensize(1)/Flat.v.numbuffs/8);
if Flat.v.zoom_level==0;
    Flat.v.max_elements_on_screen=2;
else
    Flat.v.max_elements_on_screen=floor(2^Flat.v.zoom_level*Flat.v.Screensize(1)/Flat.spec.numbuffs{Flat.spec_curr_ID}(Flat.v.curr_clust)/8);
end
Flat.p.num_grow=max(24,round(Flat.v.max_elements_on_screen/2));


%% is_classified
%Flat.is_classified(Flat.X.clust_ID<Flat.num_classified+1)=1;
%    indices=Flat.X.clust_ID<Flat.num_classified+1;
%     Flat.Tags(1,indices)=cellfun(@(x) '1',Flat.Tags(1,indices),'uniformoutput',false);
Flat.Tags(1,Flat.X.clust_ID<Flat.num_classified+1)={'1'};

%Flat.is_classified(Flat.X.clust_ID>Flat.num_classified)=0;
%    indices=Flat.X.clust_ID>Flat.num_classified;
%     Flat.Tags(1,indices)=cellfun(@(x) '0',Flat.Tags(1,indices),'uniformoutput',false);
Flat.Tags(1,Flat.X.clust_ID>Flat.num_classified)={'0'};

Flat.num_classified=min(max(Flat.X.clust_ID),Flat.num_classified);

%% check cardinality equal to Flat.num_Tags
%fields={'data','data_eof','data_off','indices_all','timestamp','filenames','subdir','specdata','clust_ID','PCcoeffs','tags','tag_threshold'};
fieldsv={'post','pre'};

%check_length(Flat,'Flat',fields,Flat.num_Tags);
check_length(Flat.v,'Flat.v',fieldsv,Flat.num_Tags);
fn={'STACK', 'data','subdir','timestamp','eof','file_imported','CHANNELS_SAVED','GAIN','thresh','spk','spk_del','spk_esc','Song_on_off','Playback_on_off'};
check_length(Flat.DAT,'Flat.DAT',fn, get_number_of_recordings(Flat));

if isfield(Flat,'user')
    fieldsUser={'data'};
    check_length(Flat.user,'Flat.user',fieldsUser,Flat.num_Tags);
end

if Flat.templates(Flat.v.curr_clust)==0
%     Flat=func_update_templates(Flat);
end
%
% if length(Flat.templates)>Flat.p.num_clust
%     Flat.templates=Flat.templates(1:Flat.p.num_clust);
% end
% if length(Flat.v.templates)>Flat.p.num_clust
%     Flat.v.templates=Flat.v.templates(1:Flat.p.num_clust);
% end

%% remove empty clusters except at end
syl_ids=unique(Flat.X.clust_ID);
if ~isempty(syl_ids) && ~Flat.p.allow_empty_clusters
    syl_empty=setdiff(1:max(syl_ids),syl_ids);
    if ~isempty(syl_empty)
        disp('Flat.p.allow_empty_clusters is set to 0, therefore your clusters will be moved.');
    end
    while ~isempty(syl_empty)
        Flat.X.clust_ID(Flat.X.clust_ID>syl_empty(1))=Flat.X.clust_ID(Flat.X.clust_ID>syl_empty(1))-1;
        syl_ids=unique(Flat.X.clust_ID);
        syl_empty=setdiff(1:max(syl_ids),syl_ids);
    end
end

%% emtpy Flat?
if Flat.num_Tags==0
    Flat.v.curr_clust=1;
    Flat.v.sequence=cell(25,Flat.p.num_clust);
end

%% grow examples
if max(Flat.grow_examples)>Flat.num_Tags
    Flat.grow_examples=[];
end

if size(Flat.Tags,2)~=Flat.num_Tags
    disp('Flat.Tags is of wrong size - archive is corrupt');
    keyboard
end

%% RMS Offset
if xor(Flat.v.rms_offset_done,Flat.v.rms_offset)
    if ~Flat.v.rms_offset_done && Flat.v.rms_offset
        [Flat,rms_select]=func_select_tagboard(Flat,[],Flat.p.tagnames{Flat.v.tags.activator},{Flat.v.RMS});
        Flat.X.indices_all(rms_select)=Flat.X.indices_all(rms_select)+Flat.X.data_off(rms_select);
        %         Flat.X.data_off_backup=Flat.X.data_off;
        %         Flat.X.data_off(rms_select)=0;
        Flat.X.data_off(rms_select)=- Flat.X.data_off(rms_select);
        Flat.v.rms_offset_done =1;
        
    elseif Flat.v.rms_offset_done && ~Flat.v.rms_offset
        [Flat,rms_select]=func_select_tagboard(Flat,[],Flat.p.tagnames{Flat.v.tags.activator},{Flat.v.RMS});
        %       Flat.X.indices_all(rms_select)=Flat.X.indices_all(rms_select)-Flat.X.data_off_backup(rms_select);
        Flat.X.indices_all(rms_select)=Flat.X.indices_all(rms_select)+Flat.X.data_off(rms_select);
        Flat.X.data_off(rms_select)=- Flat.X.data_off(rms_select);
        %         Flat.X.data_off=Flat.X.data_off_backup;
        %         Flat.X=rmfield(Flat.X,'data_off_backup');
        Flat.v.rms_offset_done =0;
    end
    Flat.v.DATspk_done=zeros(1,Flat.num_Tags);
    %     Flat=func_reorder_by_time_and_trimm2(Flat);
    Flat=func_reorder_by_time(Flat);
    
end

%% return if speedy mode
if Flat.v.plot_clust_fast
    return
end

%% tagview
% if Flat.v.tagview
%     do_correct=0;
%     if xor(isnumeric(Flat.Tags{Flat.v.tags_curr_i}),isnumeric(Flat.v.tags_curr_ij{Flat.v.tags_curr_i}))
%         do_correct=1;
%  %   elseif isempty(find(ismember(Flat.Tags{Flat.v.tags_curr_i},Flat.v.tags_curr_ij{Flat.v.tags_curr_i})))
%   %      do_correct=1;
%     end
%     if do_correct
%             Flat.v.tags_curr_ij{Flat.v.tags_curr_i}=Flat.Tags{Flat.v.tags_curr_i}(1);
%             fprintf('Corrected nonexistent Tag\n');
%     end
% end



%% DAT consistency check
% if isempty(Flat.v.DATcurr_chan) && ~isempty(Flat.v.CHANNELS_LOAD)
%     Flat.v.DATcurr_chan=Flat.v.CHANNELS_LOAD(1);
% end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% must come last
%% check if data not loaded
if  ~Flat.p.spec_full_mode
    not_loaded=func_not_loaded(Flat);
    if ~isempty(not_loaded)
        Flat=func_get_data_parallel_cl(Flat,not_loaded,Flat.v.import);
        [Flat Flat.v.select Flat.v.select_clust]=func_select_mode(Flat);
        Flat.v.plot_clust=1;
    end
end


end
%%EOF


function check_length(X,Xstring,fields,num_Tags)

for i=1:length(fields)
    if isfield(X,fields{i})
        if ~(num_Tags==size(X.(fields{i}),2))
            fprintf('\n %s.%s is %d long, whereas Flat.num_Tags=%d\n',Xstring,fields{i},size(X.(fields{i}),2),num_Tags);
            %             y=input('correct ? (yes=1)');
            %             if y==1
            %
            %             end
        end
    end
end
end