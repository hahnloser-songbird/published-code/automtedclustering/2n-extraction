function [Flat,fcH]=plot_spec(Flat,fcH)

% check whether figure 24 is open.
if ishandle(fcH.hi_spcgrm) == 0
    disp('Please open figure 24 again.');
    return;
end
Flat.spec_curr_ID=2;


if ~isempty(Flat.v.spec_plot_IDs)
    ma=int32(Flat.num_Tags);
    Flat.v.spec_plot_IDs=max(-ma,min(ma,Flat.v.spec_plot_IDs));
    
    scanrate=Flat.scanrate;
    if Flat.v.spec_plot_IDs==0
        pps=Flat.v.leftmost_element:min(Flat.v.leftmost_element+Flat.p.spec_num,length(Flat.v.order_sequence));
        Flat.v.spec_plot_IDs=Flat.v.order_sequence(pps);
    end
    % [Flat.V.pre Flat.V.post]=func_sequence_context(Flat,abs(Flat.v.spec_plot_IDs));
    winl=ceil(Flat.p.spec_winls*scanrate);    winr=ceil(Flat.p.spec_winrs*scanrate);
    fsize=ceil(12-Flat.p.spec_num/10);
    
    
    
    if  ~(Flat.v.spec_skip_plot==1) || (isfield(Flat.v.PLOT,'spec') && Flat.v.PLOT.spec==1 ) % only plot if not =1
        
        [Flat.v.spec_plot,Flat.Bz,Flat.Bz_mask]=func_extract_spec(Flat,winl,winr,Flat.v.spec_plot_IDs,1);
        %% plot data
        set(fcH.hi_spcgrm,'XData', Flat.v.spec_plot.xs', 'YData', Flat.v.spec_plot.Ys', 'CData', Flat.Bz); % update the spectrogram
        % set(fcH.hi_spcgrm,'CData', Bz); % update the spectrogram
        %   set(fcH.ha_spcgrm,'YDir','reverse','XLim',[0 Flat.v.spec_plot.xs(end)],'YLim',[0 Flat.v.spec_plot.Ys(end)]); % update the axes
        set(0,'CurrentFigure',fcH.hf_spcgrm);
        set(fcH.hl_spcgrm{1},'XData',winl/scanrate*[1 1],'YData',[0 Flat.v.spec_plot.Ys(end)],'Visible','on'); % update the red line
        
        %% redraw markings
        if Flat.v.plot_clust_white_lines
            fcH=func_redraw_markings(Flat,fcH,Flat.spec.numbuffs{Flat.spec_curr_ID}(Flat.v.curr_clust));
            Flat.v.plot_clust_white_lines=0;
        end
        
    end
    
    
    %     if Flat.v.spec_skip_plot
    
    firstbuff=Flat.spec.firstbuff{Flat.spec_curr_ID}(Flat.v.curr_clust);
    numbuffs=Flat.spec.numbuffs{Flat.spec_curr_ID}(Flat.v.curr_clust);
    
    
    if Flat.spec_curr_ID == 2
        title_str=['W' num2str(Flat.spec_curr_ID) ' || ' Flat.spec_names{Flat.spec_curr_ID} ];
    else
        title_str=['W' num2str(Flat.spec_curr_ID) ' || ' Flat.spec_names{Flat.spec_curr_ID} ' (' num2str(firstbuff) '+' num2str(numbuffs) ')'];
    end
    
    
    % plot red lines
    if Flat.v.spec_coupled
        % title_str=[title_str ' || C'];
        set(fcH.hl_spcgrm{2},'XData',(winl-Flat.spec.samp_left{Flat.spec_curr_ID}(Flat.v.curr_clust))/scanrate*[1 1],'YData',[0 Flat.v.spec_plot.Ys(end)],'Visible','on','color','r'); % update the red line
        set(fcH.hl_spcgrm{3},'XData',(winl+Flat.spec.samp_right{Flat.spec_curr_ID}(Flat.v.curr_clust))/scanrate*[1 1],'YData',[0 Flat.v.spec_plot.Ys(end)],'Visible','on','color','r'); % update the red line
        
    else
        % title_str=[title_str ' || U'];
        set(fcH.hl_spcgrm{2},'XData',(winl-Flat.v.spec_rlines(1))/scanrate*[1 1],'YData',[0 Flat.v.spec_plot.Ys(end)],'Visible','on','color','g'); % update the red line
        set(fcH.hl_spcgrm{3},'XData',(winl+Flat.v.spec_rlines(2))/scanrate*[1 1],'YData',[0 Flat.v.spec_plot.Ys(end)],'Visible','on','color','g'); % update the red line
    end
    if Flat.v.PLOT.spec
        title_str=['! ' title_str];
    end
    if (Flat.v.plot_ordered==4 || Flat.v.plot_ordered==5) && isfield(Flat.v,'spec_context_num')
        title_str=[title_str ' || ' num2str(Flat.v.spec_context_num)];
        Flat.v=rmfield(Flat.v,'spec_context_num')';
    end
    
    if ~Flat.v.all_clust_mode
        title_str=[title_str ' || cluster #' num2str(Flat.v.curr_clust)];
    end
    
    if isempty(Flat.v.select_clust)
        title_str=[title_str ' || no elements'];
    else
        title_str=[title_str ' || ' num2str(length(Flat.v.select_clust)) ' elements'];
    end
    
    
    set(fcH.hf_spcgrm,'name',title_str);
    %     end
    
    %% pot white lines
    %% NNet
    if Flat.spec_curr_ID==3 && length(Flat.net)>=Flat.v.curr_clust && length(Flat.net{Flat.v.curr_clust})>0 && isfield(Flat,'net_samp')
        for i=1:length(Flat.v.spec_plot_IDs)
            set(fcH.hl_spcgrm_wline{i},'XData',(winl+Flat.net_samp(abs(Flat.v.spec_plot_IDs(i))))/scanrate*[1 1],'visible','on');
        end
        for i=length(Flat.v.spec_plot_IDs)+1:Flat.p.spec_num
            set(fcH.hl_spcgrm_wline{i},'visible','off');
        end
        
        %% End of syllable
    elseif Flat.spec_curr_ID==2 || Flat.p.spec_syll_dur==1 % && isfield(Flat.X,'data_off')
        l=min(length(Flat.v.spec_plot_IDs),length(fcH.hl_spcgrm_wline));
        for i=1:l
            set(fcH.hl_spcgrm_wline{i},'XData',(winl+Flat.X.data_off(min(Flat.num_Tags,abs(Flat.v.spec_plot_IDs(i)))))/scanrate*[1 1],'visible','on');
        end
        for i=length(Flat.v.spec_plot_IDs)+1:Flat.p.spec_num
            set(fcH.hl_spcgrm_wline{i},'visible','off');
        end
        %    else
        %         for i=1:Flat.p.spec_num
        %             set(fcH.hl_spcgrm_wline{i},'visible','off');
        %         end
    end
    
    if Flat.p.display_marks
        fcH=func_display_marks(fcH,Flat.Tags,abs(Flat.v.spec_plot_IDs),Flat.p.spec_num,'hf_spcgrm_marktext',-1,-1,1*(Flat.p.spec_winls+Flat.p.spec_winrs),scanrate,4);
    end
    
    %% plot big cluster name
    if Flat.v.spec_show_str
        set(fcH.hbig_spcgrm,'string',Flat.p.clust_names{Flat.v.curr_clust});
        %pause(.1)
        set(fcH.hbig_spcgrm,'string','');
    end
    
    pic=Flat.Bz_mask;
    set(fcH.hi_spcgrm_mask,'CData',64*ones(size(Flat.Bz_mask)),'XData', Flat.v.spec_plot.xs', 'YData', Flat.v.spec_plot.Ys');
    %,'XData',[-Flat.v.DATwinl Flat.v.DATwinr],'YData',[1 lDat]);
    set(fcH.hi_spcgrm_mask,'AlphaData',pic,'visible','on');
    
    
    %% scrollbar
    if Flat.p.draw_scrollbars && ~isempty(Flat.v.order_sequence_DAT)
        
        barLength = double(length(Flat.v.spec_plot_IDs))/(eps+length(Flat.v.select_clust));
        barLeft = double(Flat.v.leftmost_element-1)/(eps+length(Flat.v.select_clust));
        xlims = [Flat.v.spec_plot.xs(1), Flat.v.spec_plot.xs(end)];
        ylims = [Flat.v.spec_plot.Ys(1), Flat.v.spec_plot.Ys(end)];
        yExtend = diff(ylims);
        xStep = double(diff(xlims))*0.02;
        
        if Flat.p.spec_num < size(Flat.v.select_clust, 2) % size(Flat.v.spec_plot_IDs, 2)
            set(fcH.hf_spcgrm_scrollbar_border, 'Position', [xlims(2)-xStep, ylims(1), xStep, yExtend]);
            set(fcH.hf_spcgrm_scrollbar_interior, 'Position', [xlims(2)-xStep, round(barLeft*yExtend+ylims(1)), xStep, barLength*yExtend]);
        end
    end
    
else
    set(fcH.hi_spcgrm,'XData', Flat.v.spec_plot.xs', 'YData', Flat.v.spec_plot.Ys', 'CData', []); % update the spectrogram
    for i=1:Flat.p.spec_num
        set(fcH.hl_spcgrm_wline{i},'visible','off');
    end
    
    if ~Flat.v.all_clust_mode
        title_str=[Flat.spec_names{Flat.spec_curr_ID} ' || cluster #' num2str(Flat.v.curr_clust) ' || no elements'];
    else
        title_str=[Flat.spec_names{Flat.spec_curr_ID} ' || all clust mode || no elements'];
    end
    set(fcH.hf_spcgrm,'name',title_str);
end
%     if evalin('base','spec_keypress')==1
%     func_spec_keyrelease
%     end
end
%% EOF
