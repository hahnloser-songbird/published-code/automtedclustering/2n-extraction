function fcH = evalkey_spec(src, evnt)
% FIGURE 24
%try
% which modifier got pressed?
modifier = evnt.Modifier;

%% read in from workspace
Flat = evalin('base', 'Flat');
fcH = evalin('base', 'fcH');

%Flat=func_reorder_by_indices_all(Flat);

%% per default do everything, for speed remove some todo's
Flat.v.spec_show_str=0;
Flat.v.run_set_consistency=1;
Flat.v.run_select=1;
Flat.v.run_select_clust=1;
Flat.v.plot_clust=1;
Flat.v.plot_spec=1;
Flat.v.plot_clust_fast=0;
Flat.v.plot_templates=0;
Flat.v.prepare_plot=0;
Flat.v.set_spec_consistency=0;
Flat.v.spec_skip_plot=0;
%Flat.v.PLOT.eotFigure=1;
spec_keypress = 0;
Flat.v.key_mod_spec=modifier;
if isfield(Flat,'umap') & isfield(Flat.umap,'brute_force')
    if ~isfield(Flat.umap.brute_force,'Gpress')
        Flat.umap.brute_force.Gpress=0;
    end
    Flat.umap.brute_force.Gpress=max(0,Flat.umap.brute_force.Gpress-1);
end
switch evnt.Key %
    
    case 'm' % move
        p = func_mouseclick_element(Flat, 'ha_spcgrm');
        Flat = func_evalkey_m(Flat, modifier, p);
        
        %         %% smart move: move additional elements
        %         if Flat.p.do_smart_move
        %             Flat=func_smart_move(Flat,Flat.v.order_sequence(p));
        %             Flat=func_clust_scal_prod_all_move(Flat);
        %         end
        %
        Flat.v.prepare_plot = 1;
        %   [Flat,fcH]=func_spec_prepare_plot(Flat,fcH);
        Flat.v.plot_clust_fast = 1;
        Flat.v.run_select = 0;
        Flat.v.run_set_consistency = 0;
        
        % the following is a dummy placeholder code for the help window
        if isempty(modifier)
            
            %$ move to cluster number. $%
            %* moves the elements to a cluster number of choice.*%
        elseif strcmp(modifier, 'shift')
            
        end
        
        %$ Moves one cluster down.  $%
        %* The elements of the next cluster (within the range 1 and Flat.p.num_clust) are displayed. In ALL Clust mode, this key has no effect. *%
    case 'downarrow'
        if isempty(modifier)
            Flat=func_evalkey_downarrow(Flat,Flat.v.select_clust,src);
            Flat.v.prepare_plot=1;
            %[Flat,fcH]=func_spec_prepare_plot(Flat,fcH);
            Flat.v.spec_show_str=1;
            Flat.v.run_set_consistency=0;
            Flat.v.run_select=0;
            Flat.v.run_select_clust=0;
            if ~isempty(Flat.grow_examples)
                Flat.grow_examples=[]; Flat.v.redtext.grow_examples=[];
                Flat.v.plot_grow = 1;
            end
            %  assignin('base', 'Flat', Flat);
            
            %$ Show more context spectrograms.  $%
            %* The numer of elements for which the context spectrograms is shown increases by roughly 25 percent.*%
        elseif  strcmp(modifier,'shift')
            Flat.p.spec_num=ceil(1.25*Flat.p.spec_num);
            if Flat.p.spec_num>Flat.v.max_elements_on_screen
                Flat.v.zoom_level=min(7,Flat.v.zoom_level+1);
                Flat.v.plot_clust=1;
                Flat.v.plot_clust_fast=0;
                fprintf('Zoom level %d\n',Flat.v.zoom_level);
            end
            Flat.X.specdata=cell(1,Flat.num_Tags);
            fcH=func_fig_init(fcH,Flat,Flat.scanrate);
            [Flat,fcH]=func_help_evalkey_tab(Flat,fcH);
            Flat.v.run_set_consistency=1;
            Flat.v.spec_resize=1;
            
            Flat.v.prepare_plot=1;
            Flat.v.run_select=0;
            Flat.v.run_select_clust=0;
            Flat.v.spec_skip_plot=2; % plot all
            
        end
        
        %$ Moves one cluster up.  $%
        %* The elements of the previous cluster (within the range 1 and Flat.p.num_clust) are displayed. In ALL Clust mode, this key has no effect. *%
    case 'uparrow'
        if isempty(modifier)
            Flat=func_evalkey_uparrow(Flat,Flat.v.select_clust,src);
            Flat.v.prepare_plot=1;
            %[Flat,fcH]=func_spec_prepare_plot(Flat,fcH);
            Flat.v.spec_show_str=1;
            Flat.v.run_set_consistency=0;
            Flat.v.run_select=0;
            Flat.v.run_select_clust=0;
            
            %$ Show fewer context spectrograms.  $%
            %* The numer of elements for which the context spectrograms is shown decreases by roughly 20 percent.*%
        elseif  strcmp(modifier,'shift')
            Flat.p.spec_num=ceil(.8*Flat.p.spec_num);
            Flat.X.specdata=cell(1,Flat.num_Tags);
            fcH=func_fig_init(fcH,Flat,Flat.scanrate);
            [Flat,fcH]=func_help_evalkey_tab(Flat,fcH);
            Flat.v.spec_resize=1;
            Flat.v.prepare_plot=1;
            Flat.v.run_select=0;
            Flat.v.run_select_clust=0;
            Flat.v.spec_skip_plot=2; % plot all
        end
        
        
        %$ Displays elements to the left within the current cluster.  $%
        %* The elements the left are displayed. Which elements these are depends on your sorting mode. *%
    case 'leftarrow'
        if isempty(modifier) || (~isempty(modifier) && sum(strcmp(modifier, {'shift', 'control'})) == 2)
            if (~isempty(modifier) && sum(strcmp(modifier, {'shift', 'control'})) == 2)
                spec_keypress = 0;
            else
                spec_keypress = 1;
            end
            if Flat.v.zoom_level>0
                Flat.v.leftmost_element=max(1,Flat.v.leftmost_element-Flat.p.spec_num);
                Flat.v.prepare_plot=1;
                %[Flat,fcH]=func_spec_prepare_plot(Flat,fcH);
                Flat.v.run_set_consistency=0;
                Flat.v.run_select=0;
                Flat.v.run_select_clust=0;
                Flat.v.plot_clust=0;
                %             Flat.v.plot_clust_fast=1;
            end
            
            %$ Decrease syllable context.  $%
            %* This function only works in left- or right context sorting mode (type 'o' to select that mode). The elements in the current cluster following (left context) or preceding (right context) elements in another chosen cluster are displayed. That chosen cluster number id decreased by one.*%
        elseif strcmp(modifier,'shift')
            if Flat.v.plot_ordered==4  % Left context sequence
                [Flat.v.leftmost_element,l]=func_sequence_context_leftmost(Flat,Flat.v.leftmost_element,'pre',-1);
                Flat.v.prepare_plot=1;
                Flat.v.spec_context_num=l;
                Flat.v.spec_skip_plot=2;
                %[Flat,fcH]=func_spec_prepare_plot(Flat,fcH);
                
            elseif Flat.v.plot_ordered==5 % Right context sequence
                [Flat.v.leftmost_element,l]=func_sequence_context_leftmost(Flat,Flat.v.leftmost_element,'post',-1);
                Flat.v.prepare_plot=1;
                Flat.v.spec_context_num=l;
                Flat.v.spec_skip_plot=2;
                % [Flat,fcH]=func_spec_prepare_plot(Flat,fcH);
            end
            %fprintf('%d elements\n',l);
            
            %$ Show more left context. $%
            %* Increases the window size to the left. *%
        elseif strcmp(modifier,'control')
            Flat.p.spec_winls=Flat.p.spec_winls*1.1;
            Flat.v.spec_skip_plot=2;
            [Flat,fcH]=func_spec_reinitialize(Flat,fcH);
            
            %$ Show less right context. $%
            %* Decreases the window size to the right. *%
        elseif strcmp(modifier,'alt')
            Flat.p.spec_winrs=Flat.p.spec_winrs/1.1;
            Flat.v.spec_skip_plot=2;
            [Flat,fcH]=func_spec_reinitialize(Flat,fcH);
        end
        
        
        %$ Displays elements to the right within the current cluster.  $%
        %* The elements the r are displayed. Which elements these are depends on your sorting mode. *%
    case 'rightarrow'
        if isempty(modifier) || (~isempty(modifier) && sum(strcmp(modifier, {'shift', 'control'})) == 2)
            if (~isempty(modifier) && sum(strcmp(modifier, {'shift', 'control'})) == 2)
                spec_keypress = 0;
            else
                spec_keypress = 1;
            end
            if Flat.v.zoom_level>0
                Flat.v.leftmost_element=max(1,min(1+length(Flat.v.select_clust)-Flat.p.spec_num,Flat.v.leftmost_element+Flat.p.spec_num));
                Flat.v.prepare_plot=1;
                %[Flat,fcH]=func_spec_prepare_plot(Flat,fcH);
                Flat.v.run_set_consistency=0;
                Flat.v.run_select=0;
                Flat.v.run_select_clust=0;
                Flat.v.plot_clust=0;
            end        %$ Increase syllable context.  $%
            %* This function only works in left- or right context sorting mode (type 'o' to select that mode). The elements in the current cluster following (left context) or preceding (right context) elements in another chosen cluster are displayed. That chosen cluster number id increased by one.*%
        elseif strcmp(modifier,'shift')
            if Flat.v.plot_ordered==4  % Left context sequence
                [Flat.v.leftmost_element,l]=func_sequence_context_leftmost(Flat,Flat.v.leftmost_element,'pre',1);
                Flat.v.prepare_plot=1;
                Flat.v.spec_context_num=l;
                Flat.v.spec_skip_plot=2;
                % [Flat,fcH]=func_spec_prepare_plot(Flat,fcH);
                
            elseif Flat.v.plot_ordered==5 % Right context sequence
                [Flat.v.leftmost_element,l]=func_sequence_context_leftmost(Flat,Flat.v.leftmost_element,'post',1);
                Flat.v.prepare_plot=1;
                Flat.v.spec_context_num=l;
                Flat.v.spec_skip_plot=2;
                %[Flat,fcH]=func_spec_prepare_plot(Flat,fcH);
            end
            %fprintf('%d elements\n',l);
            
            %$ Show less left context. $%
            %* Decreases the window size to the left. *%
        elseif strcmp(modifier,'control')
            Flat.p.spec_winls=Flat.p.spec_winls/1.1;
            Flat.v.spec_skip_plot=2;
            [Flat,fcH]=func_spec_reinitialize(Flat,fcH);
            
            %$ Show more right context. $%
            %* Increases the window size to the right. *%
        elseif strcmp(modifier,'alt')
            Flat.p.spec_winrs=Flat.p.spec_winrs*1.1;
            Flat.v.spec_skip_plot=2;
            [Flat,fcH]=func_spec_reinitialize(Flat,fcH);
        end
        
        %         %@$ Switch between coupled or uncoupled red delimiters. $%
        %         %* The delimiting lines switch color between constrained (red) and unconstrained (green). *%
        %     case 'a'% switch between coupled and uncoupled red dashed lines
        %         if isempty(modifier)
        %             Flat.v.spec_coupled=~Flat.v.spec_coupled;
        %             Flat.v.spec_skip_plot=1;
        %             Flat.v.run_set_consistency=0;
        %             Flat.v.run_select=0;
        %             Flat.v.run_select_clust=0;
        %             Flat.v.plot_clust=0;
        %         end
        
        %$ Set element to top of Fig. 24 $%
        %* Shows selected element on the left in Fig 20 and displays context in Fig. 24 on top. *%
    case 'q'
        Flat.v.plot_spec=1;
        Flat.v.plot_grow = 1;
        Flat.v.spec_skip_plot=2; % plot all
        [Flat,fcH] = func_evalkey_q(Flat,fcH, src, modifier);
        Flat.v.run_select=0;
        Flat.v.run_set_consistency=0;
        Flat.v.set_spec_consistency=1;
        Flat.v.run_select_clust=0;
        
        
    case 's'
        %$  Show scalebar. $%
        %* *%
        if strcmp(modifier,'shift')
            if isfield(fcH,'hf_spcgrm_scalebar') && ishandle(fcH.hf_spcgrm_scalebar)
                delete(fcH.hf_spcgrm_scalebar);
                delete(fcH.hf_spcgrm_scalebar_text);
            else
                x=10^floor(log10(Flat.p.spec_winrs/4));
                y=get(fcH.ha_spcgrm,'ylim');
                fcH.hf_spcgrm_scalebar=plot(fcH.ha_spcgrm,[Flat.p.spec_winls Flat.p.spec_winls+x],y(2)/2*[1 1],'w','linewidth',3);
                figure(fcH.hf_spcgrm);
                fcH.hf_spcgrm_scalebar_text=text(Flat.p.spec_winls,1.1*y(2)/2,[num2str(1000*x) ' ms'],'fontsize',24,'color','w');
            end
        end
        
        
        %$  Go to beginning of cluster. $%
        %* Displays the first elements in current cluster. *%
    case 'b'
        if isempty(modifier)
            Flat.v.leftmost_element=1;
            Flat.v.run_select=0;
            Flat.v.run_select_clust=0;
            Flat.v.plot_clust_fast=1;
            Flat.v.run_set_consistency=0;
            Flat.v.prepare_plot=1;
            %[Flat,fcH]=func_spec_prepare_plot(Flat,fcH);
        end
        
        %$  Go to end of cluster. $%
        %* Displays the last elements in current cluster. *%
    case 'e'
        if isempty(modifier)
            Flat.v.leftmost_element=max(1,1+length(Flat.v.select_clust)-Flat.p.spec_num);
            Flat.v.run_select=0;
            Flat.v.run_select_clust=0;
            Flat.v.plot_clust_fast=1;
            Flat.v.run_set_consistency=0;
            Flat.v.prepare_plot=1;
            %[Flat,fcH]=func_spec_prepare_plot(Flat,fcH);
        end
        
        %$  Change sorting mode of current cluster. $%
        %* Opens exhaustive list of choices how to sort elements in current cluster. *%
    case 'o' % order cluster sequence, press shift to recompute ordering
        [Flat.v.plot_ordered,status]=func_evalkey_o(Flat);
        if ~status
            return
        end
        Flat.v.sequence{Flat.v.plot_ordered,Flat.v.curr_clust}=[];
        
        Flat.v.prepare_plot=1;
        Flat.v.run_select=0;
        Flat.v.run_select_clust=0;
        Flat.v.run_set_consistency=0;
        
        
        %$  Display help.$%
        %* You're looking at it. *%
    case 'h'
        if isempty(modifier)% strcmp(modifier,'shift')
            %% show help
            currentPath = [mfilename('fullpath') '.m'];
            func_show_help(currentPath);
            
        end
        
        
        %$ Jump to clusters 1, 2, 3, to 10. $%
        %* Quick way to switch cluster. *%
    case {'1','2','3','4','5','6','7','8','9','0'} %group it %^ 0 - 9 ^%
        if isempty(modifier)
            Flat.v.curr_clust=uint8(str2double(evnt.Key));
            if Flat.v.curr_clust==0
                Flat.v.curr_clust=10;
            end
            Flat.v.run_select_clust=1;
            [Flat Flat.v.select Flat.v.select_clust]=func_select_mode(Flat);
            if (Flat.v.leftmost_element~=1 && Flat.v.leftmost_element==1+length(Flat.v.select_clust)-Flat.v.max_elements_on_screen)
                Flat.v.leftmost_element=max(1,1+length(Flat.v.select_clust)-Flat.v.max_elements_on_screen);
            else
                Flat.v.leftmost_element=1;
            end
            Flat.v.run_set_consistency=0;
            Flat.v.run_select=0;
            if ~isempty(Flat.grow_examples)
                Flat.grow_examples=[]; Flat.v.redtext.grow_examples=[];
                Flat.v.plot_grow = 1;
            end
            
            Flat.v.plot_spec=1;
            Flat.v.prepare_plot = 1;
            
            
            %$ Jump to clusters 11, 12, 13, to 20. $%
            %* Quick way to switch cluster. *%
        elseif strcmp(modifier,'shift')
            Flat.v.curr_clust=uint8(str2double(evnt.Key))+10;
            if Flat.v.curr_clust==10
                Flat.v.curr_clust=20;
            end
            Flat.v.run_select_clust=1;
            [Flat Flat.v.select Flat.v.select_clust]=func_select_mode(Flat);
            if (Flat.v.leftmost_element~=1 && Flat.v.leftmost_element==1+length(Flat.v.select_clust)-Flat.v.max_elements_on_screen)
                Flat.v.leftmost_element=max(1,1+length(Flat.v.select_clust)-Flat.v.max_elements_on_screen);
            else
                Flat.v.leftmost_element=1;
            end
            Flat.v.run_set_consistency=0;
            Flat.v.run_select=0;
            if ~isempty(Flat.grow_examples)
                Flat.grow_examples=[]; Flat.v.redtext.grow_examples=[];
                Flat.v.plot_grow = 1;
            end
            
            Flat.v.spec_skip_plot = 1;
            Flat.v.plot_spec=1;
            Flat.v.prepare_plot = 1;
            
        end
        
    case 'rightbracket'  %^ ] ^%
        p=func_mouseclick_element(Flat,'ha_spcgrm');
        if p>length(Flat.v.spec_plot_IDs) || p<1
            fprintf('Press a spectrogram in figure 24! \n');
            return
        end
        
        i=abs(Flat.v.spec_plot_IDs(p));
        sel=func_select_mic(Flat,Flat.p.channel_mic_display);
        
        if isfield(Flat.v,'fig24_range') && ~isempty(Flat.v.fig24_range)
            i=Flat.v.fig24_range;
            %   ii=find(ismember(sel,i));
            [lia,seli]=ismember(i,sel);
            ii=sel(min(seli+1,end));
            
            p=find(ismember(abs(Flat.v.spec_plot_IDs),i));
            sel_el=Flat.v.order_sequence(p);
            %pii=find(ismember(sel,sel_el));
            [lia,pii]=ismember(abs(Flat.v.spec_plot_IDs(p)),sel); % tht
            
            pii=sel(min(pii+1,end));
            %  pii=sel(min(abs(Flat.v.spec_plot_IDs(p))+1,end));
        else
            seli= find(sel==i);
            ii=sel(min(seli+1,end));
            sel_el=Flat.v.order_sequence(p);
            pii=ii;
        end
        
        Flat.v.spec_plot_IDs=int32(Flat.v.spec_plot_IDs);
        if isempty(pii) && isempty(modifier)
            disp('Problem - not a syllable ? aborting');
            return
        end
        
        
        %$  Move syllable onset to the right. $%
        %* This function allows for fine-tuning the syllable onset of the selected element. *%
        if strcmp(modifier,'shift')
            brute_force_not_clust=isfield(Flat,'umap') && isfield(Flat.umap,'brute_force') && isfield(Flat.p,'umap') && isfield(Flat.p.umap,'brute_force') && isfield(Flat.p.umap.brute_force,'clusts') && ~any(ismember(Flat.X.clust_ID(i),Flat.p.umap.brute_force.clusts));
            umap_no_brute_force=isfield(Flat,'Z') && isfield(Flat,'umap') && ~isfield(Flat.umap,'brute_force');
            if umap_no_brute_force | brute_force_not_clust
                %if isfield(Flat,'umap') & isfield(Flat.umap,'brute_force') & ~any(ismember(Flat.X.clust_ID(i),Flat.p.umap.brute_force.clusts))
                h=1:length(i);%ones(size(i));%find(~Flat.Z.stencil(Flat.X.custom(1,i)+1))
                Flat.Z.stencil(Flat.X.custom(1,i(h)))=0;
                Flat.X.custom(1,i(h))=Flat.X.custom(1,i(h))+1;
                Flat.X.indices_all(i(h))=Flat.Z.indices_all(Flat.X.custom(1,i(h)));
                Flat.X.data_off(i(h))=Flat.Z.indices_all(Flat.X.custom(2,i(h)))-Flat.Z.indices_all(Flat.X.custom(1,i(h))); % shift data_off
                
                
            elseif isfield(Flat,'umap') & isfield(Flat.umap,'brute_force') & any(ismember(Flat.X.clust_ID(i),Flat.p.umap.brute_force.clusts))
                disp('Currently not allowed after running brute force - aborting');
                return
            else
                Flat.X.indices_all(i)=Flat.X.indices_all(i)+Flat.p.nonoverlap;
                Flat.X.data_off(i)=max(0,Flat.X.data_off(i)-Flat.p.nonoverlap); % keep the offset at the same absolute position
            end
            Flat.v.run_set_consistency=0;
            Flat.v.run_select=0;
            Flat.v.run_select_clust=0;
            Flat.v.plot_spec=1;
            
            %$  Move syllable offset to the right. $%
            %* This function allows for fine-tuning the syllable offset of the selected element. *%
        elseif strcmp(modifier,'alt')
            %   sel=func_select_mic(Flat,Flat.p.channel_mic_display);
            brute_force_not_clust=isfield(Flat,'umap') && isfield(Flat.umap,'brute_force') && isfield(Flat.p,'umap') && isfield(Flat.p.umap,'brute_force') && isfield(Flat.p.umap.brute_force,'clusts') && ~any(ismember(Flat.X.clust_ID(i),Flat.p.umap.brute_force.clusts));
            umap_no_brute_force=isfield(Flat,'Z') && isfield(Flat,'umap') && ~isfield(Flat.umap,'brute_force');
            if umap_no_brute_force | brute_force_not_clust
                %if isfield(Flat,'umap') & isfield(Flat.umap,'brute_force') & ~any(ismember(Flat.X.clust_ID(i),Flat.p.umap.brute_force.clusts))
                if isfield(Flat.umap,'brute_force') & isfield(Flat.p.umap.brute_force,'buff_span')
                    buff_span=Flat.p.umap.brute_force.buff_span;
                else
                    buff_span=inf;
                end
                i0=i;
                for j=1:length(i0)
                    i=i0(j);
                    % h=find(~Flat.Z.stencil(Flat.X.custom(1,i)-1)) & diff(Flat.Z.buffer(Flat.X.custom(1,i)-1:Flat.X.custom(1,i)))<2;
%                     is_gap=diff(Flat.Z.buffer(Flat.X.custom(2,i):Flat.X.custom(2,i)+1))>=buff_span;
%                     if is_gap
%                         disp('gap blocking, consider changing Flat.p.umap.brute_force.buff_span');
%                         continue
%                     end
%                     syl_blocking=Flat.Z.stencil(Flat.X.custom(2,i)+1);
%                     if syl_blocking
%                         if ~isfield(Flat.p,'force_boundary_move') || Flat.p.force_boundary_move==0
%                             disp('syllable blocking, try setting Flat.p.force_boundary_move=1');
%                             continue
%                         end
%                     end
                    
%                     Flat.X.custom(2,i)=Flat.X.custom(2,i)+1;}
%                     Flat.X.data_off(i)=Flat.Z.indices_all(Flat.X.custom(2,i))-Flat.Z.indices_all(Flat.X.custom(1,i)); % shift data_off
%                     Flat.Z.stencil(Flat.X.custom(2,i))=Flat.Z.stencil(Flat.X.custom(2,i)-1);
%                     Flat=func_help_shift_syls_right(Flat,seli(j),i,sel);
                    
              
                    Flat.X.data_off(i)=max(0,Flat.X.data_off(i)+Flat.p.nonoverlap); % keep the offset at the same absolute position

                end
            elseif isfield(Flat,'umap') & isfield(Flat.umap,'brute_force') & any(ismember(Flat.X.clust_ID(i),Flat.p.umap.brute_force.clusts))
                disp('Currently not allowed after running brute force - aborting');
                return
            else
                Flat.X.data_off(i)=Flat.X.data_off(i)+Flat.p.nonoverlap; % shift data_off
                for iii=1:length(seli)
                    Flat=func_help_shift_syls_right(Flat,seli(iii),i(iii),sel);
                end
                
            end
            Flat.v.prepare_plot = 1;
            Flat.v.plot_clust_fast = 1;
            Flat.v.run_select = 0;
            Flat.v.run_set_consistency = 0;
            Flat.v.spec_skip_plot=1;
            Flat.v.spec_keyrelease=1;
        end
        
    case 'leftbracket'
        p=func_mouseclick_element(Flat,'ha_spcgrm');
        if p>length(Flat.v.spec_plot_IDs) || p<1
            fprintf('Press a spectrogram in figure 24! \n');
            return
        end
        i=abs(Flat.v.spec_plot_IDs(p));
        sel=func_select_mic(Flat,Flat.p.channel_mic_display);

        
        if isfield(Flat.v,'fig24_range') && ~isempty(Flat.v.fig24_range)
            i=Flat.v.fig24_range;
            [lia,seli]=ismember(i,sel);
            ii=sel(max(seli-1,1));
            
            p=find(ismember(abs(Flat.v.spec_plot_IDs),i));
            sel_el=Flat.v.order_sequence(p);
            [lia,pii]=ismember(abs(Flat.v.spec_plot_IDs(p)),sel); %tht
            pii=sel(max(pii-1,1));
            
        else
            seli= find(sel==i);
            ii=sel(max(seli-1,1));
            sel_el=Flat.v.order_sequence(p);
            pii=ii;
        end
        
        
        Flat.v.spec_plot_IDs=int32(Flat.v.spec_plot_IDs);
        
        if isempty(pii) && isempty(modifier)
            disp('Problem - not a syllable ? aborting');
            return
        end
        
        %$  Move syllable onset to the left. $%
        %* This function allows for fine-tuning the syllable onset of the selected element. *%
        if strcmp(modifier,'shift') % shift indices_all (onset)
            umap_no_brute_force=isfield(Flat,'Z') && isfield(Flat,'umap') && ~isfield(Flat.umap,'brute_force');
            brute_force_not_clust=isfield(Flat,'umap') && isfield(Flat.umap,'brute_force') && isfield(Flat.p,'umap') && isfield(Flat.p.umap,'brute_force') && isfield(Flat.p.umap.brute_force,'clusts') && ~any(ismember(Flat.X.clust_ID(i),Flat.p.umap.brute_force.clusts));
%             if umap_no_brute_force | brute_force_not_clust
%                 if isfield(Flat.p.umap,'brute_force') & isfield(Flat.p.umap.brute_force,'buff_span')
%                     buff_span=Flat.p.umap.brute_force.buff_span;
%                 else
%                     buff_span=2;
%                 end
%                 i0=i;
%                 for j=1:length(i0)
%                     i=i0(j);
%                     if Flat.X.custom(1,i)==1
%                         continue
%                     end
%                     
%                     is_gap=diff(Flat.Z.buffer(Flat.X.custom(1,i)-1:Flat.X.custom(1,i)))>=buff_span;
%                     if is_gap
%                         %disp('gap blocking, consider changing Flat.p.umap.brute_force.buff_span');
%                         continue
%                     end
%                     syl_blocking=Flat.Z.stencil(Flat.X.custom(2,i)+1);
%                     if syl_blocking
%                         if ~isfield(Flat.p,'force_boundary_move') || Flat.p.force_boundary_move==0
%                             disp('syllable blocking, try setting Flat.p.force_boundary_move=1');
%                             continue
%                         end
%                     end
%                     
%                     Flat.Z.stencil(Flat.X.custom(1,i))=Flat.Z.stencil(Flat.X.custom(1,i)+1);%Flat.X.clust_ID(i(h));
%                     Flat.X.indices_all(i)=Flat.Z.indices_all(Flat.X.custom(1,i));
%                     Flat.X.data_off(i)=max(0,Flat.Z.indices_all(Flat.X.custom(2,i))-Flat.Z.indices_all(Flat.X.custom(1,i))); % shift data_off
%                     
%                     if i==1
%                         continue
%                     end
%                     Flat=func_help_shift_syls_left(Flat,seli(j),i,sel);
%                     
%                 end
%                                 
%             elseif isfield(Flat,'umap') & isfield(Flat.umap,'brute_force') & any(ismember(Flat.X.clust_ID(i),Flat.p.umap.brute_force.clusts))
%                 disp('Currently not allowed after running brute force - aborting');
%                 return
%             else
                h=Flat.X.indices_all(i);
                Flat.X.indices_all(i)=max(Flat.p.nfft,Flat.X.indices_all(i)-Flat.p.nonoverlap);
                Flat.X.data_off(i)=max(0,Flat.X.data_off(i)+h-Flat.X.indices_all(i)); % keep the offset at the same absolute position
                for iii=1:length(i)
                    Flat=func_help_shift_syls_left(Flat,seli(iii),i(iii),sel);
%                 end
                
            end
            
            Flat.v.run_set_consistency=0;
            Flat.v.run_select=0;
            Flat.v.run_select_clust=0;
            Flat.v.plot_spec=1;
            
            %$  Move syllable offset to the left. $%
            %* This function allows for fine-tuning the syllable offset of the selected element. *%
        elseif strcmp(modifier,'alt') % shift data_off (offset)
            brute_force_not_clust=isfield(Flat,'umap') && isfield(Flat.umap,'brute_force') && isfield(Flat.p,'umap') && isfield(Flat.p.umap,'brute_force') && isfield(Flat.p.umap.brute_force,'clusts') && ~any(ismember(Flat.X.clust_ID(i),Flat.p.umap.brute_force.clusts));
            umap_no_brute_force=isfield(Flat,'Z') && isfield(Flat,'umap') && ~isfield(Flat.umap,'brute_force');
            if umap_no_brute_force | brute_force_not_clust
                h=1:length(i);%ones(size(i));
                Flat.Z.stencil(Flat.X.custom(2,i(h)))=0;
                Flat.X.custom(2,i(h))=Flat.X.custom(2,i(h))-1;
                Flat.X.data_off(i(h))=Flat.Z.indices_all(Flat.X.custom(2,i(h)))-Flat.Z.indices_all(Flat.X.custom(1,i(h))); % shift data_off
            elseif isfield(Flat,'umap') & isfield(Flat.umap,'brute_force') & any(ismember(Flat.X.clust_ID(i),Flat.p.umap.brute_force.clusts))
                disp('Currently not allowed after running brute force - aborting');
                return
            else
                Flat.X.data_off(i)=Flat.X.data_off(i)-Flat.p.nonoverlap;
            end
            
            Flat.v.prepare_plot = 1;
            Flat.v.plot_clust_fast = 1;
            Flat.v.run_select = 0;
            Flat.v.run_set_consistency = 0;
            Flat.v.spec_skip_plot=1;
            Flat.v.spec_keyrelease=1;
            
         
        end
        
        
    otherwise
        assignin('base', 'Flat', Flat);
        return;
end

%%
set(src, 'UserData', spec_keypress);

%% selection
%     if Flat.v.run_select
%         Flat.v.select=func_select_tagboard(Flat,Flat.v.tagboard);
%     end
%
%     if Flat.v.run_select_clust
%         Flat.v.select_clust=func_select_elements_clust(Flat,Flat.v.curr_clust,Flat.v.select);
%     end
%if    Flat.v.run_select_clust
[Flat Flat.v.select Flat.v.select_clust]=func_select_mode(Flat);
%end

%% consistency
if  Flat.v.set_spec_consistency
    [Flat,fcH]=func_set_spec_consistency(Flat,fcH);
end


if Flat.v.run_set_consistency
    Flat=func_set_consistencies(Flat);
end


%% write back to workspace
[Flat,sels,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS});

[Flat,sels]=func_syl_check_overlap(Flat,sels);



[Flat,fcH] = plot_figs(Flat, fcH);
func_spec_keyrelease();
[Flat,fcH] = plot_figs(Flat, fcH);

assignin('base', 'Flat', Flat);
assignin('base', 'fcH', fcH);


end
%% EOF
