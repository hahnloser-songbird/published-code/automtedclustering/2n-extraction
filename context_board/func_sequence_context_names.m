function str=func_sequence_context_names(which)
str=cell(1,length(which));
for i=1:length(which)
    switch which(i)
        case -1
            str{i}='bf';
        case -2
            str{i}='ef';
        case -3
            str{i}='bs';
        case -4
            str{i}='es';
        otherwise
            str{i}=num2str(which(i));
    end
end