function fcH = func_init_fig_spec(Flat,fcH)

scanrate = Flat.scanrate;
text_color = [1 0 0];
marktext_color= [1 0.7 1];
Flat.v.color_scrollbar_interior = [0 0 0];%[1, 0, 0]*0.8;
Flat.v.color_scrollbar_border = [1 1 1];%[1, 1, 1]*0.5;
fsize = 12;

%% Spectrogram fig
fcH.hf_spcgrm=figure(24);clf; hold on
set(fcH.hf_spcgrm,'ResizeFcn','Flat.v.spec_resize=1;', 'MenuBar', 'none', ...
    'WindowScrollWheelFcn',{@func_mousewheel,}, ...
    'WindowButtonDownFcn',{@func_mousepress, }, ...
    'keyReleaseFcn','func_spec_keyrelease;');
fcH.ha_spcgrm=axes;
set(fcH.ha_spcgrm,'position',[0 0 1 1]);
%set(fcH.ha_spcgrm,'position',[0 0 1 1], 'DrawMode', 'fast');
set(fcH.hf_spcgrm, 'BusyAction', 'cancel', ...
    'Interruptible', 'off', 'position', [Flat.v.Screensize(1)-440 40 400 Flat.v.Screensize(2)-100],'ToolBar','none');  % 'MenuBar','none'
set(fcH.hf_spcgrm,'name','Selected sample, press q to update');
set(fcH.hf_spcgrm, 'KeyPressFcn', {@evalkey_spec, }, 'UserData', 0);

fcH.hi_spcgrm=imagesc('XData', 1:1000, 'YData', zeros(250, 1), 'CData', zeros(250, 1000));
fcH.hl_spcgrm{1}=line([0 0],[0 1e4],'Color','r','LineStyle','-.','Visible','off');
fcH.hl_spcgrm{2}=line([0 0],[0 1e4],'Color','r','LineStyle',':','Visible','off');
fcH.hl_spcgrm{3}=line([0 0],[0 1e4],'Color','r','LineStyle',':','Visible','off');
%ht_spcgrm=text('String','');
axis xy
hold on;
fcH.hi_spcgrm_mask=image(0);

fcH.hbig_spcgrm=text('position',[Flat.p.spec_winls*.666 scanrate/8*Flat.p.spec_num],'string','','color','w','fontsize',36);
for i=1:Flat.p.spec_num
    fcH.hf_spcgrm_text{i}=text(0.01,(i-1)*scanrate/4+6000,num2str(Flat.p.spec_num+1-i),'color','w','fontsize',11, 'BackgroundColor', 'k', 'FontWeight', 'bold','linewidth',1);
    fcH.hf_spcgrm_textL{i}=text(0.01,(i-1)*scanrate/4+2000,num2str(Flat.p.spec_num+1-i),'color','g','fontsize',9,'BackgroundColor', 'k', 'linewidth',1,'fontweight','bold');
    fcH.hf_spcgrm_textR{i}=text(0.95,(i-1)*scanrate/4+2000,num2str(Flat.p.spec_num+1-i),'color','g','fontsize',9,'BackgroundColor', 'k','linewidth',1,'fontweight','bold');
    fcH.hf_spcgrm_marktext{i}=text(0.93*(Flat.p.spec_winls+Flat.p.spec_winrs),(i-1)*scanrate/4+6000,num2str(Flat.p.spec_num+1-i),'color',marktext_color,'fontsize',11,'BackgroundColor', 'k','fontweight','bold','linewidth',1,'visible','off');
    fcH.hl_spcgrm_wline{i}=plot(Flat.p.spec_winls*[1 1],[(i-1)*scanrate/4 (i-1)*scanrate/4+8000],'color','w','visible','on','linewidth',1);
    % for j=1:10
    %  hl_spcgrm_taglines{i,j}=plot(Flat.p.spec_winls*[1 1],[(i-1)*scanrate/4 (i-1)*scanrate/4+8000],'color','y','visible','off','linewidth',1);
    % end
end

if Flat.p.draw_scrollbars
    figure(24); 
    fcH.hf_spcgrm_scrollbar_border = rectangle('Position', [0, 0, 1, 1], 'FaceColor', Flat.v.color_scrollbar_border, 'EdgeColor', Flat.v.color_scrollbar_border);
    fcH.hf_spcgrm_scrollbar_interior = rectangle('Position', [0, 0, 1, 1], 'FaceColor', Flat.v.color_scrollbar_interior, 'EdgeColor', Flat.v.color_scrollbar_interior);
end

%%

% [Flat,fcH]=func_spec_reinitialize(Flat,fcH);

hold on;
% set(fcH.hbig_spcgrm,'position',[Flat.p.spec_winls*.666 scanrate/8*Flat.p.spec_num],'string','','color','w','fontsize',36);
figure(24);
for i=1:Flat.p.spec_num
    if i>length(fcH.hf_spcgrm_text)
        fcH.hf_spcgrm_text{i}=text(0,0,'','color','w','BackgroundColor', 'k', 'FontWeight', 'bold','linewidth',1);
    end
    set(fcH.hf_spcgrm_text{i},'position',[0.01,(i-1)*8000+6000,0],'String',num2str(Flat.p.spec_num+1-i),'color','w','fontsize',fsize, 'BackgroundColor', 'k', 'FontWeight', 'bold','linewidth',1);
    
    if i>length(fcH.hf_spcgrm_textL)
        fcH.hf_spcgrm_textL{i}=text(0.01,(i-1)*8000+2000,0,num2str(i),'color','g','fontsize',9,'BackgroundColor', 'k', 'linewidth',1,'fontweight','bold');
    end
    set(fcH.hf_spcgrm_textL{i},'position',[0.01,(i-1)*8000+2000,0],'String',num2str(Flat.p.spec_num+1-i),'color','g','fontsize',9,'BackgroundColor', 'k', 'linewidth',1,'fontweight','bold');
    
    if i>length(fcH.hf_spcgrm_textR)
        fcH.hf_spcgrm_textR{i}=text(0,0,'','color','g','fontsize',9,'BackgroundColor', 'k','linewidth',1,'fontweight','bold');
    end
    set(fcH.hf_spcgrm_textR{i},'position',[0.95*(Flat.p.spec_winls+Flat.p.spec_winrs),(i-1)*8000+2000,0],'String',num2str(Flat.p.spec_num+1-i),'color','g','fontsize',9,'BackgroundColor', 'k','linewidth',1,'fontweight','bold');
    
    if i>length(fcH.hf_spcgrm_marktext)
        fcH.hf_spcgrm_marktext{i}=text(0,0,'','color',marktext_color,'fontsize',fsize,'BackgroundColor', 'k','fontweight','bold','linewidth',1,'visible','off');
    end
    set(fcH.hf_spcgrm_marktext{i},'position',[0.93*(Flat.p.spec_winls+Flat.p.spec_winrs),(i-1)*8000+6000,0],'String',num2str(Flat.p.spec_num+1-i),'color',marktext_color,'fontsize',fsize,'BackgroundColor', 'k','fontweight','bold','linewidth',1,'visible','off');
    
    if i>length(fcH.hl_spcgrm_wline)
        fcH.hl_spcgrm_wline{i}=plot([0 0],[0 0],'color','w','visible','on','linewidth',1);
    end
    set(fcH.hl_spcgrm_wline{i},'XData',Flat.p.spec_winls*[1 1],'YData',[(i-1)*8000 i*8000],'color','w','visible','on','linewidth',1);
    % for j=1:10
    %  hl_spcgrm_taglines{i,j}=plot(Flat.p.spec_winls*[1 1],[(i-1)*scanrate/4 (i-1)*scanrate/4+8000],'color','y','visible','off','linewidth',1);
    % end
end

for i=Flat.p.spec_num+1:length(fcH.hl_spcgrm_wline)
    set(fcH.hl_spcgrm_wline{i},'visible','off');
    %     if ishandle(fcH.hl_spcgrm_wline{i})
    %         delete(fcH.hl_spcgrm_wline{i})
    %     end
end
fcH.hl_spcgrm_wline=fcH.hl_spcgrm_wline(1:Flat.p.spec_num);

for i=Flat.p.spec_num+1:length(fcH.hf_spcgrm_text)
    set(fcH.hf_spcgrm_text{i},'visible','off');
    %     if ishandle(fcH.hf_spcgrm_text{i})
    %         delete(fcH.hf_spcgrm_text{i})
    %     end
end
fcH.hf_spcgrm_text=fcH.hf_spcgrm_text(1:Flat.p.spec_num);

for i=Flat.p.spec_num+1:length(fcH.hf_spcgrm_textL)
    set(fcH.hf_spcgrm_textL{i},'visible','off');
    %     if ishandle(fcH.hf_spcgrm_textL{i})
    %         delete(fcH.hf_spcgrm_textL{i})
    %     end
end
fcH.hf_spcgrm_textL=fcH.hf_spcgrm_textL(1:Flat.p.spec_num);

for i=Flat.p.spec_num+1:length(fcH.hf_spcgrm_textR)
    set(fcH.hf_spcgrm_textR{i},'visible','off');
    %     if ishandle(fcH.hf_spcgrm_textR{i})
    %         delete(fcH.hf_spcgrm_textR{i})
    %     end
end
fcH.hf_spcgrm_textR=fcH.hf_spcgrm_textR(1:Flat.p.spec_num);

for i=Flat.p.spec_num+1:length(fcH.hf_spcgrm_marktext)
    set(fcH.hf_spcgrm_marktext{i},'visible','off');
    %     if ishandle(fcH.hf_spcgrm_marktext{i})
    %         delete(fcH.hf_spcgrm_marktext{i})
    %     end
end

fcH.hf_spcgrm_marktext=fcH.hf_spcgrm_marktext(1:Flat.p.spec_num);

% return the handle to the base workspace
assignin('base','Flat', Flat);
assignin('base','fcH', fcH);
end


