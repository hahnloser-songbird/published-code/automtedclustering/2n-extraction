function [Flat,fcH]=func_spec_keyrelease2(Flat,fcH)
Flat.v.key_mod_spec='';
scanrate=Flat.scanrate;

if Flat.v.spec_plot_IDs==0
    pps=Flat.v.leftmost_element:min(Flat.v.leftmost_element+Flat.p.spec_num,length(Flat.v.order_sequence));
    Flat.v.spec_plot_IDs=Flat.v.order_sequence(pps);
end
[Flat.V.pre Flat.V.post]=func_sequence_context(Flat,abs(Flat.v.spec_plot_IDs));
str_pre=func_sequence_context_names(Flat.V.pre(abs(Flat.v.spec_plot_IDs)));
str_post=func_sequence_context_names(Flat.V.post(abs(Flat.v.spec_plot_IDs)));

winl=ceil(Flat.p.spec_winls*scanrate);    
winr=ceil(Flat.p.spec_winrs*scanrate);

fsize=ceil(12-Flat.p.spec_num/10);

l=min(length(Flat.v.spec_plot_IDs),length(fcH.hl_spcgrm_wline));
% l = Flat.p.spec_num;

for i=1:l
    i0=abs(Flat.v.spec_plot_IDs(i));
    i0str=num2str(i0);
    set(fcH.hf_spcgrm_text{i},'visible','on','string',i0str);
  %  set(fcH.hf_spec_text{i},'visible','on','string',i0str);
    
    set(fcH.hf_spcgrm_textL{i},'visible','on','string',str_pre{i},'fontsize',fsize+2*(Flat.v.plot_ordered==4));
    hpos=get(fcH.hf_spcgrm_textR{i},'position');
    set(fcH.hf_spcgrm_textR{i},'visible','on','string',str_post{i},'fontsize',fsize+2*(Flat.v.plot_ordered==5),'Position',[0.95*(Flat.p.spec_winls+Flat.p.spec_winrs) hpos(2:end)]);
    
    set(fcH.hf_spcgrm_text{i},'position',[0.01 (i-1)*8000+6000 0 ]);
 %     set(fcH.hf_spcgrm_text{i},'position',[0.01,(i-1)*8000+6000,0],'String',num2str(Flat.p.spec_num+1-i),'color','w','fontsize',fsize, 'BackgroundColor', 'k', 'FontWeight', 'bold','linewidth',1);
 
    if (Flat.v.spec_plot_IDs(i)>0)
        if isfield(Flat.v,'spec_redtext_i') && ismember(i,Flat.v.spec_redtext_i)
            set(fcH.hf_spcgrm_text{i},'color','r');
        else
        set(fcH.hf_spcgrm_text{i},'color',[1 1 1]);
   %     set(fcH.hf_spec_text{i},'color',[1 1 1]);
        end
    else
        set(fcH.hf_spcgrm_text{i},'color',[0.4 0.4 1]);
%         if (Flat.v.spec_plot_IDs(i)<0)
%             set(fcH.hf_spec_text{i},'color',[0.4 .4 1]);
%         else
%             set(fcH.hf_spec_text{i},'visible','off');
%         end
    end
end

  
for i=length(Flat.v.spec_plot_IDs)+1:Flat.p.spec_num
    set(fcH.hf_spcgrm_text{i},'visible','off');
    set(fcH.hf_spcgrm_textL{i},'visible','off');
    set(fcH.hf_spcgrm_textR{i},'visible','off');
end

%     firstbuff=Flat.spec.firstbuff{Flat.spec_curr_ID}(Flat.v.curr_clust);
%     numbuffs=Flat.spec.numbuffs{Flat.spec_curr_ID}(Flat.v.curr_clust);
%     title_str=['W' num2str(Flat.spec_curr_ID) ' || ' Flat.spec_names{Flat.spec_curr_ID} ' (' num2str(firstbuff) '+' num2str(numbuffs) ')'];
%
%     set(fcH.hf_spcgrm,'name',title_str);
%
if get(fcH.hf_spcgrm, 'UserData') == 1% Flat.v.plot_clust==1
    set(fcH.hf_spcgrm, 'UserData', 0);
    
    Flat.v.plot_clust=1;
    [Flat,fcH]=plot_figs(Flat,fcH);
    
end