function [Flat,fcH]=func_set_spec_consistency(Flat,fcH)


if ~Flat.p.spec_full_mode
    %% stay within cutout
    Flat.spec.samp_left{Flat.spec_curr_ID}(Flat.v.curr_clust)= min(Flat.spec.samp_left{Flat.spec_curr_ID}(Flat.v.curr_clust),Flat.spec.samp_left{1}(Flat.v.curr_clust));
    Flat.spec.samp_left{Flat.spec_curr_ID}(Flat.v.curr_clust)= min(Flat.spec.samp_left{Flat.spec_curr_ID}(Flat.v.curr_clust),Flat.spec.samp_left{1}(Flat.v.curr_clust));
    Flat.spec.samp_right{Flat.spec_curr_ID}(Flat.v.curr_clust)= min(Flat.spec.samp_right{Flat.spec_curr_ID}(Flat.v.curr_clust),Flat.spec.samp_right{1}(Flat.v.curr_clust));
    Flat.spec.samp_right{Flat.spec_curr_ID}(Flat.v.curr_clust)= min(Flat.spec.samp_right{Flat.spec_curr_ID}(Flat.v.curr_clust),Flat.spec.samp_right{1}(Flat.v.curr_clust));
end

%% end after beginning
Flat.spec.samp_left{Flat.spec_curr_ID}(Flat.v.curr_clust)= max(Flat.spec.samp_left{Flat.spec_curr_ID}(Flat.v.curr_clust),1-Flat.spec.samp_right{Flat.spec_curr_ID}(Flat.v.curr_clust));
Flat.spec.samp_right{Flat.spec_curr_ID}(Flat.v.curr_clust)= max(Flat.spec.samp_right{Flat.spec_curr_ID}(Flat.v.curr_clust),1-Flat.spec.samp_left{Flat.spec_curr_ID}(Flat.v.curr_clust));

if ~Flat.p.spec_full_mode && Flat.spec_curr_ID==1 % changing the cutout win
    
    %% check spec 1 consistency (cutout win)
    if Flat.spec.samp_left{1}(Flat.v.curr_clust)<Flat.spec.samp_left{5}(Flat.v.curr_clust)
        fprintf('You are cutting into %s\n',Flat.spec_names{5});
        Flat.spec.samp_left{1}(Flat.v.curr_clust)=max(Flat.spec.samp_left{1}(Flat.v.curr_clust),Flat.spec.samp_left{5}(Flat.v.curr_clust));
    end
    
    if Flat.spec.samp_right{1}(Flat.v.curr_clust)<Flat.spec.samp_right{5}(Flat.v.curr_clust)
        fprintf('You are cutting into %s\n',Flat.spec_names{5});
        Flat.spec.samp_right{1}(Flat.v.curr_clust)=max(Flat.spec.samp_right{1}(Flat.v.curr_clust),Flat.spec.samp_right{5}(Flat.v.curr_clust));
    end
    
    %% check spec 2-4 consistency (syllable, nnet, pca)
    for i=2:4
        if Flat.spec.samp_left{i}(Flat.v.curr_clust)>Flat.spec.samp_left{1}(Flat.v.curr_clust)
            fprintf('You are cutting into %s\n',Flat.spec_names{i});
            Flat.spec.samp_left{i}(Flat.v.curr_clust)=min(Flat.spec.samp_left{i}(Flat.v.curr_clust),Flat.spec.samp_left{1}(Flat.v.curr_clust));
        end
        if Flat.spec.samp_right{i}(Flat.v.curr_clust)>Flat.spec.samp_right{1}(Flat.v.curr_clust)
            fprintf('You are cutting into %s\n',Flat.spec_names{i});
            Flat.spec.samp_right{i}(Flat.v.curr_clust)=min(Flat.spec.samp_right{i}(Flat.v.curr_clust),Flat.spec.samp_right{1}(Flat.v.curr_clust));
        end
    end
end

%% clust win
if  Flat.spec_curr_ID==5
    %% make all equal!
    Flat.spec.samp_left{5}=Flat.spec.samp_left{5}(Flat.v.curr_clust)*ones(Flat.p.num_clust,1);
    Flat.spec.samp_right{5}=Flat.spec.samp_right{5}(Flat.v.curr_clust)*ones(Flat.p.num_clust,1);
    
    % increase other cutout wins if too small
    if Flat.p.spec_full_mode
        Flat.spec.samp_left{1}=repmat(max(cellfun(@max,Flat.spec.samp_left)),Flat.p.num_clust,1);
        Flat.spec.samp_right{1}=repmat(max(cellfun(@max,Flat.spec.samp_right)),Flat.p.num_clust,1);
    else
        for i=1:Flat.p.num_clust
            Flat.spec.samp_left{1}(i)=max(Flat.spec.samp_left{1}(i),Flat.spec.samp_left{5}(1));
            Flat.spec.samp_right{1}(i)=max(Flat.spec.samp_right{1}(i),Flat.spec.samp_right{5}(1));
        end        
    end
end


%% set expected spec.numbuffs and spec.firstbuff
for i=1:length(Flat.spec_names)
    Flat.spec.numbuffs{i}=max(0,func_numbuffs(Flat.spec.samp_left{i}+Flat.spec.samp_right{i},Flat.p.nfft,Flat.p.nonoverlap));
    Flat.spec.firstbuff{i}=-Flat.spec.samp_left{i}/Flat.p.nonoverlap;
end
Flat.spec.numbuffs{5}=Flat.spec.numbuffs{5}(Flat.v.curr_clust)*ones(Flat.p.num_clust,1);
Flat.spec.firstbuff{5}=Flat.spec.firstbuff{5}(Flat.v.curr_clust)*ones(Flat.p.num_clust,1);

Flat.v.numbuffs=func_numbuffs(Flat.spec.samp_left{5}(1)+Flat.spec.samp_right{5}(1),Flat.p.nfft,Flat.p.nonoverlap);

if length(Flat.p.channel_mic_display)==1
    Flat.p.syllable.X(Flat.p.channel_mic_display).numbuffs=Flat.v.numbuffs;
    Flat.p.syllable.X(Flat.p.channel_mic_display).samp_left=Flat.spec.samp_left{5}(1);
end


