function [pre,post]=func_sequence_context(Flat,hi)

% if isempty(hi)
%     y=input('Compute syllable graph on all Tags (enter 1), Flat.v.select (enter 2) ');
%     if y==1
%         hi=1:Flat.num_Tags;
%     elseif y==2
%         hi=Flat.v.select;
%     end
% end

% hi should contain syllables only
pre=Flat.V.pre;
pre(hi)=Flat.X.clust_ID(max(hi-1,1));

for i=1:length(hi)
    pre_i=max(1,hi(i)-1);
    hid=Flat.X.DATindex(hi(i));
    pre_id=Flat.X.DATindex(pre_i);
%     if hi(i)==1383
%         keyboard
%     end
    if hi(i)==1 || ~strcmp(Flat.DAT.filename{hid},Flat.DAT.filename{pre_id}) || ~strcmp(Flat.DAT.subdir{hid},Flat.DAT.subdir{pre_id}) % first element or new file?
        if Flat.X.indices_all(hi(i))>Flat.p.graph_max_gap_ms/1000*Flat.scanrate % bos
            pre(hi(i))=-3;
        else % bof
            
            pre(hi(i))=-1;
        end
    elseif Flat.X.indices_all(hi(i))-Flat.X.data_off(pre_i)-Flat.X.indices_all(pre_i)>Flat.p.graph_max_gap_ms/1000*Flat.scanrate
        pre(hi(i))=-3;
        
    end
end

post=Flat.V.post;
post(hi)=Flat.X.clust_ID(min(hi+1,Flat.num_Tags));

for i=1:length(hi)
    post_i=min(Flat.num_Tags,hi(i)+1);
    post_id=Flat.X.DATindex(post_i);
    hid=Flat.X.DATindex(hi(i));
    if hi(i)==Flat.num_Tags ||  ~strcmp(Flat.DAT.filename{hid},Flat.DAT.filename{post_id}) ||  ~strcmp(Flat.DAT.subdir{hid},Flat.DAT.subdir{post_id})
        if Flat.X.indices_all(hi(i))<Flat.DAT.eof(Flat.X.DATindex(hi(i)))-Flat.p.graph_max_gap_ms/1000*Flat.scanrate % eos
            post(hi(i))=-4;
        else
            post(hi(i))=-2;
        end
       elseif Flat.X.indices_all(post_i)-Flat.X.data_off(hi(i))-Flat.X.indices_all(hi(i))>Flat.p.graph_max_gap_ms/1000*Flat.scanrate
        post(hi(i))=-4;
        end
end
