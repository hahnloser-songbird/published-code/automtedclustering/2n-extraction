function Flat=func_evalkey_downarrow(Flat,hi,src)

if Flat.num_Tags==0
    return
end
Flat.v.curr_clust=min(max(Flat.X.clust_ID),Flat.v.curr_clust+1);
%Flat.v.select_clust=func_select_elements_clust(Flat,Flat.v.curr_clust,Flat.v.select);
Flat.v.run_select_clust=1;
[Flat Flat.v.select Flat.v.select_clust]=func_select_mode(Flat);

if src==20
    if isempty(Flat.v.leftmost_element)
        Flat.v.leftmost_element=1;
    end
    if (Flat.v.leftmost_element~=1 && Flat.v.leftmost_element==1+length(hi)-Flat.v.max_elements_on_screen)
         Flat.v.leftmost_element=max(1,1+length(Flat.v.select_clust)-Flat.v.max_elements_on_screen);
    else
        Flat.v.leftmost_element=1;
    end
    
elseif src==24
    if (Flat.v.leftmost_element~=1 && Flat.v.leftmost_element==1+length(hi)-Flat.p.spec_num)
         Flat.v.leftmost_element=max(1,1+length(Flat.v.select_clust)-Flat.p.spec_num);
    else
        Flat.v.leftmost_element=1;
    end
    
end
