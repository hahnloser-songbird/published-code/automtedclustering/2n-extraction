% FLATCLUST: A song and neural data browser.
%
% if you work with a new recording, first run 'getparse', then 'getflats',
% then run 'flatclust'. 
% flatclust operates on 2 variables in the workspace: Flat and Flats. Flat
% is the current working archive (what you see on the screen), and Flats is
% a collection of 60 archives among which the first is always the training
% archive named 'Train'. To make an archive the working archive, type 'shift+a' 
% and select the right archive. This executes a command such as
% 'Flat=Flats{3}'. Note: '1' is always the training archive. When you change working archive, you'll lose
% the changes you made to the old archive unless you update Flats (type 'alt+s'
% and then select 'XXX to Flats'). 
%
% VIEWS: In Fig. 20, the clusters can be viewed by using the up- and down
% arrows. By default, no empty clusters are
% allowed in the training archive, but they are allowed in the other
% archives (to change, edit 'Flat.p.allow_empty_clusters'). 
% To change the viewing mode: type 'o' to open the sequence-order
% menu. You can also use the shortcuts 'n' for nearest-neighbor sorting,
% and 'c' for clustered sorting in which elements are sorted by their
% distance to  a clicked element in the cluster (that element is put to the
% far right on the display). The right and left context views sort the
% elements by the cluster ID of the preceding or following element. Type '1',
% '2', or '3' to decrease the x-resolution, go to the highest
% resolution by typing '0'. Clustering is done on principal component
% analysis (PCA)-compredded data ('shift+r'), to switch between the normal view and the
% PCA-compressed view, type 'v'. To view the full spectrogram of the
% clicked component, type 'q' (this will also put the clicked component to
% the left of the screen) and it will show the spectrograms of
% Flat.p.spec_num other components in Figure 24 and their Flat-indices in
% white. The arrow keys allow you to scroll in the spectrogram display. Use
% 'ctrl + 1 or 9'  in figure 24 to change the left boundary, and or 'alt + 1 or 9' to change the right boundary.
%
% SPECTROGRAMS: Flat.p.nfft is the buffersize of the log power spectrograms and
% Flat.p.nonoverlap the nonoverlap. Flat.p.spec_log_min is the offset before taking the logarithm (to
% avoid taking the logarithm of zero)
%
% TEMPLATE: Select a particular element and click 'shfit+t', this will make this
% element the template for that cluster and display it in Figure 23. Type
% 'q' on Fig. 23 to view the template context in figure 24, highlighted in
% red. Type 'space' on figure 23 to switch to the clicked cluster.
%
% MANUAL SORTING: To delete an element in a cluster and move it to the last
% cluster, click on it and type 'd'. To complete remove an element type
% 'shift+d' (use with extreme caution). To grow a cluster and view more elements like it, type 'g'. To
% insert an element from the grow examples into a cluster, click on it and
% type 'i'. The white number indicates the cluster of origin. Repeated
% insertions and deletions are possible by clicking 'd' and 'i' many times.
% To move an element into another cluster, type 'shfit+m' and select the target
% cluster, to repeat the move for the next element, simply type 'm'. To
% merge (unite) two clusters, type 'u'. To delete a selection (from a
% cluster to the entire archive), click 'alt+d' 
%
% CONTEXT VIEW: to sort the cluster elements by the cluster IDs of adjacent
% elements, type 'o' and chose the right or left context option. Note, the
% adjacent cluster IDs are always shown in green on Figure 24, irrespective
% of the view mode. To see the adjacent spectrograms of a particular
% element, click on it and type '[' or ']'. To move all elements preceeded
% or followd by a particular cluster ID, go to the first one and type 'alt
% + m'. Note: to move quickly between first elements of various
% subclusters, use 'shift + [' and 'shift + ]'.
%
% DURATION VIEW: To sort the elements of a cluster by their duration, type
% 'o' and select 'Duration'. Then select the syllable mode in Figure 24 (by
% typing 'shift+z' until 'Syllable' appears in the title bar). You should
% then see vertical white lines that mark the end of the syllable. If you see
% that a duration is way too short, then you can either delete that
% element, or grow it into the next following or preceding element using
% 'alt + [' and 'alt + ]'. If a duration is too long, you can divide the
% syllable into sub-syllables by re-tagging (type 'shift+r' in Figure 24 after
% having delimited the expected division point by the red dashed lines). To
% move the red dashed lines, switch their moving mode to unconstrained
% (type 'a' in Figure 24 until you see 'U' appear on Title bar; then move
% the lines using the keys '1', '2', '9', and '0'). When you re-tag you can
% either only retag the currently selected element (single), or you can
% retag all elements of duration at least as long as the selected one (All
% below).
%
% CLASSIFYING AND REARRANGING CLUSTERS: If you think you have no false
% negatives and positives, type 'shift+k', this will keep (or classify) the
% cluster and make it the first cluster (or any subsequent cluster if
% classified clusters already exist). Tu unclassify, type 'shift+u'). Classifying
% clusters allows you to rearrange their order and to make the cluster
% elements invisible when growing other clusters (using 'g') and to
% recompute the cluster memberships of the remaining non-classified
% elements (using 'shift+r').
%
%
% LOAD, SAVE, RECLUSTER: If you're done clustering, save the archive
% into RAM or disk by pressing 'alt+s'. Load an archive using 'alt+l' (old:
% 'shift+s' and 'shift+l'). If you want to cluster an archive based on the
% training archive,  type 'shift+r' (recluster based on train). Clustering of
% imported elements is done using Flats{1} and the nearest neighbor method
% (Flat.p.import_method=0) or the mahalanobis distance (Flat.p.import_method=1). 
%
% IMPORT data: To import multiunit or stack data (pitch, sound amplitude, etc) according to the specifics of your experiment, type 'shift+i'. To
% import the latest elements from your running experiment, type 'ctr+i' (in essence, this runs
% 'Parse=parser_meta' and 'Flats=parse2flats(Parse)').
%
% IMPORT RMSfilt elements: edit ConfigPluginTemplate and make sure fb_plugin_RMS_500_7K() is selected.
% run the filebrowser with the fb_plugin_RMS_500_7K plugin and select a
% threshold value just above noise level (not any higher!). Then insert the
% selected threshold value into ConfigPluginTemplate (TAG_THRESHOLD). Run
% InitVariables again. Then, in your current archive, type 'shift+i', select Flat, and
% select 'new elements'. 
%
% Note: when you work with data containing both song and playback files,
% then you must select the TAG_THRESHOLD in a playback file, not in a song file, because the noise
% level is always higher during playback than during song! I recommend using one threshold
% for both song and playback files rather than two thresholds. (If you absolutely
% want two thresholds, then first import using the
% threshold for song (as above), then delete the 'RMSfilt' elements associated with 'Playback' ('alt+d' in the tagboard), 
% and rename 'RMSfilt' to ' RMSfilt0' ('r' on the tagboard). Then change the tag_threshold in ConfigPluginTemplate to the playback value,
% rerun InitVariable, reimport 'RMSfilt', delete the 'RMSfilt' elements
% associated with song, and rename 'RMSfilt0' to 'RMSfilt').
%
% Note for mirroring experiments: When you play back song files recorded on the
% same day, you need to adapt the fb_plugin_RMS_500_7K threshold to song
% files in ConfigPluginTemplate, irrespective of playback files. Then, you only need to manually cluster Song RMSfilt elements (Song slave), 
% but not Playback elements. You can inherit the Playback RMSfilt elements (their onset, offset, and clust_IDs) 
% from corresponding Song elements by simply executing func_Playback_RMSfilt_inherit_clustID ('alt+e' on Fig. 20).
% Ideally, to avoid the miss of vocalizations during playback, run the func_BA_mirror analysis first (identifies Playback corrupt). 
%
%
% NEW TRAIN: To extract a new training archive from the current working archive, click
% 'shift+e'.
%
% POST PROCESSING (PCA AND NEURAL NET): To see the principal components of
% the current cluster plus the variance explained, type 'shift+p'. The principal
% components are also stored in Flat.pcs_per_clust (the first one is the
% mean). To create a perceptron for the current cluster, type 'alt+n'.
%
% WINDOWS FOR CUTOUT, SYLLABLES, NEURAL NET, PCA, AND CLUSTERING: you can
% change these winbuffersizedows on figure 24 using 'alt'+'1' or '2' for the
% pre-samples and 'alt'+'9'+'0' for the post-samples. If you change the
% cutout window, you will have to reimport the data unless you are in Flat.p.spec_full_mode=1 (fullspec). 
%
% SUMMARY PLOTS: To view example spectrograms of each cluster in which all
% adjacent cluster are also labelled, click 'v' on Figure 24 (you must
% first define cluster names). To view the graph of syllable sequences,
% type 'shift+v'.
%
%
% NOTE ON THE TAGGING FUNCTION (PLUGIN): You may also want to think about the
% tagging function, a function that eliminates noise gives you less work
% during clustering, but may give you a less robust perceptron.
%
% Flat.Tags: To switch between ALL_CLUST and single clust modes, type 'a'.
%
% MOVE UNWANTED FILES TO JUNKFOLDER: You can group clusters into 9
% different groups by typing 'shift 1' to 'shift 9'. The group names are
% given in Flat.p.groupnames and appear on the title bar, with default
% values '1=Syllable', '2=Call', '3=Noise', etc. Move whichever files you
% want into a junk folder (or the complementary of that group) using the
% function f_move_to_junk(Flat). For example, to move only those files in
% which the bird sings 'not all song syllables' you must combine the
% clusters in the song-syllable group using the logical operation AND. To
% move the files in which he sings 'none  of the song syllables' you must
% use OR. The junk folders are in the Data folder and have a leading 'J'.
% When you feel comfortable, delete the junk folders. To make sure all
% archives (in Flats) have the same groupings, update the group names (with
% 'shift+b') and then 'update Flats'. This will transfer the grouping of the
% current archive to all other archives.
%
% MISCELLANEOUS: To sort the elements in a cluster using a custom values,
% write the values to Flat.custom and select custom sort ('o').
% To obtain more colorful spectrograms in Fig 24 (and see weak signals),
% scale your data by a multiplier larger that one in Flat.p.spec_raw_scale.
%
% To find syllable pairs separated by less than t_offs (in milliseconds),
% use func_get_pairs_index

%todo: 

% 'x'and 'r'in tagboard fig.
% unite func_extract_data_fullspec and func_extract_data 
% clearn up spec_full_mode in plot_spec
%remove do_raw;
% remove dat fields when keep_raw trimmed
% fix func_get_specgrams
%reduce use of cellfun (as in check_consistency),
% check correspondence of Flats with
%Parse, put file fields (e.g. birdnames) into .DAT, are duration of pre and postsamples
% saved somewhere ?  remove Flat.p.Datthresh, show single spikes in DAT fig.
% put diretoryDelimiter to Flat.v.dd...
% set Flat.X.tag_threshold when 'R' on fig 24
%
% note: when adding a new field to Flat, update func_make_compatible, func_default_Flat_values,
% func_remove_elements, func_Flat_import, and maybe also
% FlatClusConfigTemplate, channels='electrode' instead of '2', load
% arbitrary archives (compute Flat.v.ind automatically)

% Missing DAT field: create in func_make_compatible, insert in
% func_trimm_file_list2 and func_get_data_parallel_cl

% For flatclust set the parameters in FlatClustConfigTemplate.m (copy these files to your
% data folder). If you change the latter parameters, activate them by
% typing Flat.p=FlatClustConfigTemplate; You can also change parameters by
% typing 'B' (parameter board) and selecting the parameter group you want
% to edit.
%


% to run flatclust on the Linux Opteron workstation with two screens there may be Java errors while figure update.
% problem occurs with Linux, NVidia drivers"
% Here's the solution (*):
% edit matlabrc.m
% and append:
% set(0,'DefaultFigureRenderer','OpenGL')
% set(0,'DefaultFigureRendererMode', 'manual') 

if ~exist('paths', 'var')
    disp('please run "InitVariables" - then try again.');
    return;
end

if exist('Parse','var') && exist('cfg','var') && exist('Flats','var') && ~strcmp(cfg.birdname,Flats{2}.birdname)
    disp('wrong birdname, run "InitVariables" or "clear Flats" or "clear Parse" - then try again');
    return
end
fprintf('\nTo restart, type ''clear Flats;flatclust'' \n');
%fprintf('Change parameters by typing Flat.p=FlatClustConfigTemplate; and then run flatclust again.\n');

%sizes = cellfun(@(x) size(x, 1), Flat.indices);
%bla = arrayfun(@(x,y) repmat(x,1, y), 1:lO, sizes, 'UniformOutput', 0 );
%bla = [bla{:}];

%% copy FlatClustConfigtemplate if not already done
d=dir([paths.bird_path directoryDelimiter 'FlatClustConfigTemplate.m']);
if isempty(d)
    fc_templ=[paths.homedir directoryDelimiter 'Templates' directoryDelimiter 'FlatClustConfigTemplate.m'];
    if exist(fc_templ,'file')==2
       copyfile(fc_templ,paths.bird_path);
        fprintf('Copied %s to %s\n',fc_templ,paths.bird_path);
     end
end

%% copy PluginConfigtemplate if not already done
d=dir([paths.bird_path directoryDelimiter 'PluginConfigTemplate.m']);
if isempty(d)
    pc_templ=[paths.homedir directoryDelimiter 'Templates' directoryDelimiter 'PluginConfigTemplate.m'];
    if exist(pc_templ,'file')==2
        copyfile(pc_templ,paths.bird_path);
        fprintf('Copied %s to %s\n',pc_templ,paths.bird_path);
    end
end


close_flat_figs;

%% create struct Flat
if ~exist('Flats', 'var')
    clear Flat;
    
    %%  import fixed values
    [dummy,Flats]=func_default_Flat_values(0);
end

Flats=func_make_compatible(Flats);
Flat=func_set_flat_default(Flats);

Flat.v.curr_clust=1;
Flat.v.select_tagboard_on=0;
%Flat.v.tagview=0;

%% add your userID here if you want flatclust to start with a minumum 
% number of figures (only 20, 24 & 25).
% Do not close figure 24! it will break updating the elements in figure 20 
% (I haven't figured out yet why).
flatclust_min_number_of_windows = 0;
if ismember(cfg.userID, [2 16 11])
    flatclust_min_number_of_windows = 1;
end

%% clear meta handle
clear fcH;

%% make figure invisible during initialization

if cfg.userID == 56
    f_vis = 'off';
else
    f_vis = 'on';
end


%% main fig
fcH.hf_clust=figure(20);clf; 
set(fcH.hf_clust, 'BusyAction', 'cancel', ...
    'Interruptible', 'off', ...
    'KeyPressFcn', {@evalkey_clust, }, ...
    'Tag', 'FlatClustMainWindow', ...
    'CloseRequestFcn', 'close_flat_figs;', ...
    'MenuBar', 'none', ...
    'WindowButtonDownFcn',{@func_mousepress, },...
    'Visible',f_vis);
set(fcH.hf_clust,'position',[30 Flat.v.Screensize(2)-250 Flat.v.Screensize(1)-35 200],'ToolBar','none');
fcH.ha_clust=axes; 
%set(fcH.ha_clust,'Visible','off');
text_color = [1 0 0];
marktext_color= [1 0.7 1];

Ldisp=length(Flat.p.channel_mic_display);
fcH.hf_clust_I=imagesc(zeros(Ldisp*length(Flat.p.nfft_i),Flat.v.numbuffs));axis xy; hold on
for i=1:12
fcH.hf_clust_chan_name{i}=text(1,.9*length(Flat.p.nfft_i),'','color','w');
end
fcH.hf_clust_chan_consistent=text(1,0.05*length(Flat.p.nfft_i),'');
%axis off;

% scrollbars
Flat.v.color_scrollbar_interior = [0 0 0];%[1, 0, 0]*0.8;
Flat.v.color_scrollbar_border = [1 1 1];%[1, 1, 1]*0.5;
if Flat.p.draw_scrollbars
    fcH.hf_clust_scrollbar_border = rectangle('Position', [0, 0, 1, 1], 'FaceColor', Flat.v.color_scrollbar_border, 'EdgeColor', Flat.v.color_scrollbar_border);
    fcH.hf_clust_scrollbar_interior = rectangle('Position', [0, 0, 1, 1], 'FaceColor', Flat.v.color_scrollbar_interior, 'EdgeColor', Flat.v.color_scrollbar_interior);
end

% text marks
xvec=1:Flat.v.numbuffs;
for i=1:ceil(Flat.v.Screensize(1)/20)
    fcH.hf_clust_wl{i}=plot(.5+Flat.v.numbuffs*i*[1 1],[1 Flat.p.nfft_i(ceil(length(Flat.p.nfft_i)/4))],'w');
    fcH.hf_clust_curve{i}=plot(xvec+(i-1)*Flat.v.numbuffs,0*xvec,'w');
    fcH.hf_clust_text{i}=text(Flat.v.numbuffs*(i-.5),ceil(length(Flat.p.nfft_i)/32),'0','color', text_color, 'BackgroundColor', 'k', 'FontWeight', 'bold','fontsize',11,'visible','off');
    fcH.hf_clust_marktext{i}=text(Flat.v.numbuffs*(i-.5),Flat.v.marktext_ypos,'','color',marktext_color, 'BackgroundColor', 'k', 'FontWeight', 'bold');
end

set(fcH.ha_clust,'position',[0 0 1 1],'Visible','off','box','off');
%set(fcH.ha_clust,'position',[0 0 1 1],'Visible','off','DrawMode','fast','box','off');


%% grow fig
if flatclust_min_number_of_windows == 0
    fcH.hf_grow=figure(21);clf;
    set(fcH.hf_grow,'BusyAction','cancel', ...
        'KeyPressFcn', {@evalkey_clust, }, 'MenuBar', 'none', ...
        'WindowButtonDownFcn',{@func_mousepress, },...
        'Visible',f_vis);
    set(gca,'box','off');
    fcH.ha_grow=axes; 
    set(fcH.hf_grow,'position',[30 Flat.v.Screensize(2)-490 Flat.v.Screensize(1)-470 200],'ToolBar','none');
    set(fcH.hf_grow,'name','Grow Suggestions');
    fcH.hf_grow_I=imagesc(zeros(length(Flat.p.nfft_i),Flat.v.numbuffs*Flat.p.num_grow));axis xy; hold on
    
    for i=1:Flat.p.num_grow
        fcH.hf_grow_wl{i}=plot(.5+Flat.v.numbuffs*i*[1 1],[Flat.p.nfft_i(1) Flat.p.nfft_i(ceil(length(Flat.p.nfft_i)/4))],'w');
        fcH.hf_grow_text{i}=text(Flat.v.numbuffs*(i-.5),ceil(length(Flat.p.nfft_i)/32),'','color',[1 1 1], 'BackgroundColor', 'k', 'FontWeight', 'bold');
        fcH.hf_grow_marktext{i}=text(Flat.v.numbuffs*(i-.5),Flat.v.marktext_ypos,'','color',marktext_color, 'BackgroundColor', 'k', 'FontWeight', 'bold');
    end
set(fcH.ha_grow,'position',[0 0 1 1],'Visible','off');    
end

%% PCA fig
if flatclust_min_number_of_windows == 0
    fcH.hf_PCA=figure(22);clf;fcH.ha_PCA=axes; 
    set(fcH.ha_PCA,'position',[0 0 1 1]); axis xy;
    set(fcH.hf_PCA,'position',[435 Flat.v.Screensize(2)-505 400 200],...
        'ToolBar','none', 'MenuBar', 'none', 'Visible',f_vis);
    set(fcH.hf_PCA,'name','Principal Components');
    set(fcH.hf_PCA,'BusyAction','cancel', 'KeyPressFcn', {@evalkey_pcs, });
    fcH.hf_PCA_image=image(0); hold on;
    for i=1:5%Flat.v.pcs_num_display
        fcH.hf_PCA_text{i}=text((5-1)*(i+.33),-ceil(length(Flat.p.nfft_i)/32),'');
        fcH.hf_PCA_bline{i}=plot(.5+242*i*[1 1],[Flat.p.nfft_i(1) Flat.p.nfft_i(end)],'k');
    end
end

%% Template fig
if flatclust_min_number_of_windows == 0
    fcH.hf_Templ=figure(23);clf;
    set(fcH.hf_Templ, 'KeyPressFcn', {@evalkey_Templ, },...
        'WindowButtonDownFcn',{@func_mousepress, },...
        'Visible',f_vis);
    set(fcH.hf_Templ,'name','Templates');
    set(gca,'box','off');
    fcH.ha_templ=axes; set(fcH.ha_templ,'position',[0 0 1 1]);
    hold on;
    fcH.hf_templ_I=imagesc(zeros(length(Flat.p.nfft_i),Flat.v.numbuffs));axis xy; hold on
    
    for i=1:Flat.p.num_clust
        fcH.hf_templ_wl{i}=plot(.5+Flat.v.numbuffs*i*[1 1],[Flat.p.nfft_i(1) Flat.p.nfft_i(ceil(length(Flat.p.nfft_i)/4))],'w');
        fcH.hf_templ_text{i}=text(Flat.v.numbuffs*(i-.5),ceil(length(Flat.p.nfft_i)/32),num2str(i), 'color', 'w', 'BackgroundColor', 'k', 'FontWeight', 'bold', 'fontsize', 8);
        fcH.hf_templ_text_top1{i}=text(Flat.v.numbuffs*(i-.9),ceil(length(34*Flat.p.nfft_i)/32),num2str(i), 'color', 'w', 'BackgroundColor', 'k', 'FontWeight', 'bold', 'fontsize', 8);
        fcH.hf_templ_text_top2{i}=text(Flat.v.numbuffs*(i-.9),ceil(length(30*Flat.p.nfft_i)/32),num2str(i), 'color', 'w', 'BackgroundColor', 'k', 'FontWeight', 'bold', 'fontsize', 8);
    end
    axis xy;
end
%% Spectrogram fig
fcH.hf_spcgrm=figure(24);clf; hold on
set(fcH.hf_spcgrm,'ResizeFcn','Flat.v.spec_resize=1;', 'MenuBar', 'none', ...
    'WindowScrollWheelFcn',{@func_mousewheel,}, ...
    'WindowButtonDownFcn',{@func_mousepress, }, ...
    'keyReleaseFcn','func_spec_keyrelease;',...
    'Visible',f_vis);
fcH.ha_spcgrm=axes;
set(fcH.ha_spcgrm,'position',[0 0 1 1]);
%set(fcH.ha_spcgrm,'position',[0 0 1 1], 'DrawMode', 'fast');
set(fcH.hf_spcgrm, 'BusyAction', 'cancel', ...
    'Interruptible', 'off', 'position', [Flat.v.Screensize(1)-440 40 400 Flat.v.Screensize(2)-100],'ToolBar','none');  % 'MenuBar','none'
set(fcH.hf_spcgrm,'name','Selected sample, press q to update');
set(fcH.hf_spcgrm, 'KeyPressFcn', {@evalkey_spec, }, 'UserData', 0);

fcH.hi_spcgrm=imagesc('XData', 1:1000, 'YData', zeros(250, 1), 'CData', zeros(250, 1000));
fcH.hl_spcgrm{1}=line([0 0],[0 1e4],'Color','r','LineStyle','-.','Visible','off');
fcH.hl_spcgrm{2}=line([0 0],[0 1e4],'Color','r','LineStyle',':','Visible','off');
fcH.hl_spcgrm{3}=line([0 0],[0 1e4],'Color','r','LineStyle',':','Visible','off');
%ht_spcgrm=text('String','');
axis xy
hold on;
fcH.hi_spcgrm_mask=image(0);
% for i=1:Flat.p.spec_num
%     fcH.hf_spec_text{i}=text(0,0,num2str(i),'color','w','visible','off','linewidth',1,'fontsize',11, 'BackgroundColor', 'k', 'FontWeight', 'bold');
% end

%% correct Parse file?     
if ~exist('Parse','var') 
    disp('Warning: No Parse variable loaded - please run getparse');
elseif isfield(Flat,'REC') && ~isempty(Parse) && ~strcmp(Flat.REC.birdname{1},Parse.setting(1).birdname)
    disp('Wrong Parse file - now attempting to load correct one');
    try
        load([paths.bird_path directoryDelimiter 'MetaData' directoryDelimiter 'Parse.mat']); % load Rsetting, RFile, RAct
    catch
        disp('No Parse file found');
    end
end


%% this is a fix because we initialize the graphic before we have the data
if Flat.num_Tags>0
    scanrate=Flat.scanrate;
elseif exist('output_data', 'var')
    scanrate=output_data(1).scanrate;
elseif exist('Parse','var') && ~isempty(Parse)
    scanrate=Parse.setting(1).scanrate;
elseif exist('cfg', 'var') && isfield(cfg,'scanrate')
    scanrate=cfg.scanrate;
else
    scanrate=32000;
end

if isempty(scanrate)
    scanrate=32000;
end

hold on;
fcH.hbig_spcgrm=text('position',[Flat.p.spec_winls*.666 scanrate/8*Flat.p.spec_num],'string','','color','w','fontsize',36);
for i=1:Flat.p.spec_num
    fcH.hf_spcgrm_text{i}=text(0.01,(i-1)*scanrate/4+6000,num2str(Flat.p.spec_num+1-i),'color','w','fontsize',11, 'BackgroundColor', 'k', 'FontWeight', 'bold','linewidth',1);
    fcH.hf_spcgrm_textL{i}=text(0.01,(i-1)*scanrate/4+2000,num2str(Flat.p.spec_num+1-i),'color','g','fontsize',9,'BackgroundColor', 'k', 'linewidth',1,'fontweight','bold');
    fcH.hf_spcgrm_textR{i}=text(0.95,(i-1)*scanrate/4+2000,num2str(Flat.p.spec_num+1-i),'color','g','fontsize',9,'BackgroundColor', 'k','linewidth',1,'fontweight','bold');
    fcH.hf_spcgrm_marktext{i}=text(0.93*(Flat.p.spec_winls+Flat.p.spec_winrs),(i-1)*scanrate/4+6000,num2str(Flat.p.spec_num+1-i),'color',marktext_color,'fontsize',11,'BackgroundColor', 'k','fontweight','bold','linewidth',1,'visible','off');
    fcH.hl_spcgrm_wline{i}=plot(Flat.p.spec_winls*[1 1],[(i-1)*scanrate/4 (i-1)*scanrate/4+8000],'color','w','visible','on','linewidth',1);
    % for j=1:10
    %  hl_spcgrm_taglines{i,j}=plot(Flat.p.spec_winls*[1 1],[(i-1)*scanrate/4 (i-1)*scanrate/4+8000],'color','y','visible','off','linewidth',1);
    % end
end

if Flat.p.draw_scrollbars
    figure(24); 
    fcH.hf_spcgrm_scrollbar_border = rectangle('Position', [0, 0, 1, 1], 'FaceColor', Flat.v.color_scrollbar_border, 'EdgeColor', Flat.v.color_scrollbar_border);
    fcH.hf_spcgrm_scrollbar_interior = rectangle('Position', [0, 0, 1, 1], 'FaceColor', Flat.v.color_scrollbar_interior, 'EdgeColor', Flat.v.color_scrollbar_interior);
end

%% DAT figure
if flatclust_min_number_of_windows == 0
%[fcH, Flat] = fc_init_DAT_plot(fcH, Flat);
fcH.hf_dat=figure(25);  
set(fcH.hf_dat,'color',[0 0 0], 'Visible',f_vis); clf;
set(fcH.hf_dat, 'menubar', 'none', 'colormap', hot, 'name','Flatclust: DAT');

set(fcH.hf_dat, 'Interruptible', 'off', 'BusyAction', 'cancel', ...
    'KeyPressFcn', {@evalkey_dat, }, ...
    'KeyReleaseFcn', {@evalkey_datR, }, ...
    'MenuBar', 'none', ...
    'WindowButtonDownFcn', {@func_mousepress, });

set(fcH.hf_dat,'position',[Flat.v.Screensize(1)-855 40 400 Flat.v.Screensize(2)-330],'ToolBar','none');
Flat.v.DATshow_single_unit_spk_esc = 1;

end

%% EOT Figure
if flatclust_min_number_of_windows == 0
    %if ~isfield(fcH, 'skEOT_main') || createNew
    fcH.EOT_main = figure(26); clf;
    set(fcH.EOT_main, 'menubar', 'none', 'ToolBar', 'none', ...
        'Color', [1, 1, 1], 'Position', [26   179   723   464],...
        'Visible',f_vis);
    fcH.EOT_axis{1} = axes(); axis xy; hold on;
    grid on;
    fcH.EOT_axis{2} = axes('Visible', 'off'); axis xy; hold on;
    grid on;
    
    for a = 1:skEOT.nAxes
        for j = 1:skEOT.maxLineSeries  % max num colors   length(obj.coloringClasses)
            fcH.EOT_2Dplot{a}{j} = plot(fcH.EOT_axis{a},1,1, '.', 'MarkerSize', 1);
        end
    end
    for a = 1:skEOT.nAxes
        for j = 1:skEOT.maxLineSeries  % max num colors   length(obj.coloringClasses)
            fcH.EOT_2Dplot_secondary{a}{j} = plot(fcH.EOT_axis{a},1,1, '.', 'MarkerSize', 1);
        end
    end
    
    for a = 1:skEOT.nAxes
        fcH.EOT_2Dplot_mean{a} = plot(fcH.EOT_axis{a}, 1, 1, '.');
        %fcH.EOT_title{a} = title(fcH.EOT_axis{a}, 'ABC');
        fcH.EOT_ylabel{a} = ylabel(fcH.EOT_axis{a}, 'ABC', 'fontsize', 11, 'fontweight', 'bold', 'Interpreter', 'none');
        fcH.EOT_xlabel{a} = xlabel(fcH.EOT_axis{a}, 'ABC', 'fontsize', 11, 'fontweight', 'bold');
    end
    
    % annotations exist just once (in axis 1)
    fcH.EOT_marks = plot(fcH.EOT_axis{1},1,1, 'o');
    fcH.EOT_segmentID = text(1,1, '12345', 'Parent', fcH.EOT_axis{1});
    for j = 1:skEOT.maxAnnotations
        fcH.EOT_annotationText{j} = text(1,1,'HDGBL', 'Visible', 'off', 'Parent', fcH.EOT_axis{1}, 'Rotation', 75);
        fcH.EOT_annotationLine{j} = line('Visible', 'off', 'Parent', fcH.EOT_axis{1});
    end
    
    axes(fcH.EOT_axis{1});
    hold all;
    for j = 1:skEOT.maxPeriods
        fcH.EOT_annotationPatch{j} = patch([1, 1], [1, 1], [1, 1], 'Visible', 'off');
    end
    
    % fcH.skEOT_axis = gca(); %get(fcH.skEOT_2Dplot, 'Parent');
    set(fcH.EOT_main, 'KeypressFcn', {@evalkey_EOT, }, 'Interruptible', 'off',...
        'BusyAction', 'cancel', 'Visible', f_vis);
    
    %fcH.EOT_statusFrame = uiflowcontainer('v0','Parent', fcH.EOT_main, 'Position', [0, 0.92, 1, 0.08]);
    
    fcH.EOT_scatter = figure(27); clf;
    set(fcH.EOT_scatter, 'menubar', 'none', 'Color', [1, 1, 1], ...
        'toolbar', 'none', 'Visible',f_vis);
    fcH.EOT_scatter_axis=axes('fontsize', 11, 'fontweight', 'bold');
    grid on;
    box off;
    axis xy;
    hold on; %necessary
    %set(fcH.EOT_scatter_axis,'position',[0.1 0.1 .85 .85]);
    
    fcH.EOT_scatter_2Dplot_background = plot(fcH.EOT_scatter_axis,1,1, '.', 'MarkerSize', 1);
    for j = 1:skEOT.maxLineSeries  % max num colors   length(obj.coloringClasses)
        fcH.EOT_scatter_2Dplot{j} = plot(fcH.EOT_scatter_axis,1,1, '.', 'MarkerSize', 1); %, 'EraseMode', 'xor');
        
    end
    
    fcH.EOT_scatter_selection_rectangle = rectangle('Visible', 'off');
    
    fcH.EOT_scatter_2Dplot_lines = patch([2,1,NaN 2, 1], [2,1,NaN 1, 2], 0, 'EdgeColor', [0, 1, 0], 'EdgeAlpha', 'flat',...
        'FaceVertexAlphaData', [0, 1,0, 0, 1]', 'Parent', fcH.EOT_scatter_axis, 'AlphaDataMapping', 'none');
    fcH.EOT_scatter_ylabel = ylabel('ABC', 'fontsize', 11, 'fontweight', 'bold', 'Interpreter', 'none');
    fcH.EOT_scatter_xlabel = xlabel('ABC', 'fontsize', 11, 'fontweight', 'bold', 'Interpreter', 'none');
    fcH.EOT_scatter_title=title('');
    set(fcH.EOT_scatter, 'KeypressFcn', {@evalkey_EOT, }, 'Interruptible', 'off',...
        'BusyAction', 'cancel');
    
    for a = 1:skEOT.nAxes
        fcH.EOT_statusText{a} = text(0.05,0.1, 'StatusText', 'Visible', 'off', 'Parent', fcH.EOT_axis{a}, 'Units', 'pixels', 'FontSize', 10,...
            'Color', [1, 1, 1], 'BackgroundColor', [0, 0, 0], 'Units', 'normalized', 'Interpreter', 'none');
    end
    
    %% define EOT Object
    fcH.EOTObj = skEOT();
    if isfield(Flat.p, 'eotState')
        fcH.EOTObj.loadState(Flat.p.eotState);
        fcH.EOTObj.forceNextUpdate = true;
    end
    Flat.v.PLOT.eotFigure = false;
    fcH.EOTObj.configureAxes(fcH, 1);
    fcH = fcH.EOTObj.setupFigure27Plugins(fcH);
    %fcH = fcH.EOTObj.createEOTStatusArea(fcH);
    
    %  obj.absoluteTime = false;
    %end
end

if flatclust_min_number_of_windows == 0
    [Flat, fcH] = fci_setupFigure28(Flat, fcH);
    Flat.v.PLOT.figure28 = false;
end


%%
fcH=func_fig_init(fcH,Flat,scanrate,1);

%% Table
if flatclust_min_number_of_windows == 0
    if 0%~isfield(fcH,'hf_table') || ~ishandle(fcH.hf_table)
        fcH.hf_table = figure(100);clf;% hold on;
        set(fcH.hf_table,'PaperType','A4','PaperPositionMode','auto','CloseRequestFcn','if isfield(fcH, ''hf_table_ha''); delete(fcH.hf_table_ha);end;delete(fcH.hf_table)','BusyAction','cancel','KeyPressFcn', {@evalkey_table, },'Name','empty','renderer','painters','PaperPositionMode','auto','menubar','none','position',[1 Flat.v.Screensize(2)-90 200 65]);
    end
end

%% plot clustered data
%[Flat Flat.v.select Flat.v.select_clust]=func_select_elements(Flat,Flat.v.curr_clust,Flat.v.tagview,Flat.select);
Flat.v.select_tagboard_on=0;
Flat.v.all_clust_mode=1;
%Flat.v.tagview=0;
[Flat, Flat.v.select, Flat.v.select_clust]=func_select_mode(Flat);
if isfield(cfg,'skip_import_prompt') 
    Flat.v.plot_clust_fast = 1;
end
Flat=func_set_consistencies(Flat);

%Flat.p.do_sound_RMS_plug=0;

[Flat,fcH]=func_set_spec_consistency(Flat,fcH);
Flat.v.plot_clust=1;
Flat.v.plot_clust_white_lines=1; 
[Flat,fcH]=plot_figs(Flat,fcH);
%Flat=func_update_templates(Flat);
[Flat,fcH]=func_plot_templates(Flat,fcH);

figure(fcH.hf_clust);

%% clear variables
clear text_color;
clear marktext_color;
clear i;
DataDirs;

%% show figures

if strcmp(f_vis , 'off')
    set(findobj('Type', 'Figure'), 'Visible', 'on');
    figure(fcH.hf_spcgrm);
    figure(fcH.hf_clust);
end