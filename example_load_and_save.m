%% 
close all; clear;
addpath(genpath(cd))

%%  User defined input relevant to load your own data

Fs = 32000; % sampling frequency
archiveName = 'myArchive'; % the data will be saved in this subfolder 


%% load data

answer = questdlg('Load from archive or from raw (.wav) data?', ...
	'options', ...
	'archive','raw', 'cancel');

switch answer
    
    case 'raw'
        %%%% run this section to load raw .wav files and to compute spectrograms
        
        [Flats, cfg] = FlatsFromWAV('g17y2', {}, Fs);
        Flat = Flats{2};
        Flat.v.RMS='RMSfilt';
        userID = 0;
        cfg.userID = 0;
        directoryDelimiter = filesep;
        
        % compute spectrograms
        import = Flat.v.import;
        import.fullspec = 1;
        Flat = func_get_data_parallel_cl(Flat, 1:length(Flat.X.data), import);
        
        %%%% save data to archive
        Flat.p.savename = archiveName;
        Flat=func_evalkey_S2(Flat, 2);
        
        disp('%===== loaded and saved raw data successfully ====%');
        
    case 'archive'
        %%%% load data as Flat from archive
        
        [paths, home_dir] = getPaths();
        
        % load cfg and initiate relevant meta variables
        CurrentBirdInit;
        
        % load Flat with flatclust functions
        [~,Flats]=func_default_Flat_values(10);
        [Flats, first_loaded, FlatsInfo] = func_load_Flat2(Flats, paths, 0);
        Flats=func_help_tags_write(Flats);
        Flat=Flats{first_loaded};
        
        % some additional assignments
        Flat.p.channel_mic_display = 1;
        Flat.p.channel_mic = 1;
        sz = get(0,'screensize');
        Flat.v.Screensize = sz(3:4);
        
        disp('%===== loaded archived data successfully ====%');
    
    otherwise
        disp('%===== could not load data ====%');
end


%% threshold segmentation and oversegmentation

Flat.p.channel_mic = 1;
[Flat, fcH] = func_import_elements_from_DAT(Flat, []);

% select only elements from RMSfilt
Flat.v.select = find(strcmp(Flat.Tags(5,:), 'RMSfilt'));
Flat.v.select_clust = Flat.v.select;
Flat.umap.X0.select = Flat.v.select;


%% initatie spectrogram context figure

Flat.v.order_sequence = 1:length(Flat.X.indices_all);
func_init_fig_spec(Flat, []);

% relevant commands to initiate the proper settings
Flat.p.spec_num = 16; Flat.v.zoom_level = 2;
evnt.Key = 'downarrow'; evnt.Modifier = 'shift'; fcH = evalkey_spec(24, evnt);
Flat.v.plot_clust_fast = 0; func_spec_keyrelease;


%% compute umap embedding and 2D visualization

% compute PCA
Flat.num_PCs = 300; % number of principle components
Flat=func_Flat_PCA(Flat,0,0,1,Flat.v.select_clust, Flat.num_PCs);

% compute umap embedding
[fcH,Flat]=func_umap_init(fcH,Flat,1);


%% save results

% Flat.p.savename = 'XXX';  % change your archive folder name here
Flat=func_evalkey_S2(Flat, 2);


%% Export annotations as .csv file

TT_export_archive_annotations(Flat);