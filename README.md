GUI for two neighborhood (2N) extraction of vocalizations in the planar embedding and code to load, preprocess, and save data. 
See example_extraction_g17y2.m for an example application and instructions of how to use the GUI and example_load_and_save.m for instructions on how to load, preprocess and save data.

Prerequisites:
The GUI and auxiliary code were written in Matlab R2019b and require the Image Processing Toolbox and 
Curve Fitting Toolbox. The UMAP embedding requires the algorithm’s implementation in Matlab from the 
file exchange server (https://ch.mathworks.com/matlabcentral/fileexchange/71902-uniform-manifold-approximation-and-projection-umap).


This code is licensed via the MIT License (see included file LICENSE).
Copyright: (c) 2022 ETH Zurich, Corinna Lorenz, Richard H.R. Hahnloser
Reference (please cite):
Interactive extraction of diverse vocal units from a planar embedding without the need for prior sound segmentation
Corinna Lorenz, Xinyu Hao, Tomas Tomka, Linus Rüttimann and Richard H.R. Hahnloser

DOI: 10.3389/fbinf.2022.966066

Email: rich@ini.ethz.ch

Work address: Institute of Neuroinformatics, Winterthurerstrasse 8057,
ETH Zurich and University of Zurich

Website: https://gitlab.switch.ch/hahnloser-songbird/published-code/automtedclustering/2n-extraction
