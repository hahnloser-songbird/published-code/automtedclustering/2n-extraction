%% please document all variables that are used. this file should be a
%% reference file.

cfg.gpudevice=0;

% needed for all age dependent things
cfg.birthday = datenum([2000 01 01]);

% by default the song channel is the first channel. Josh records the song
% in the last channel. set this variable to a value different to 1 to
% specifiy your song channel.
cfg.songchannel = 1;

% used inside of 'multiplot.m' to display filtered data 
% (choose 0 for non-filter display)
cfg.do_filter= 1;

% needed for analyze
cfg.do_labview = 1;
cfg.do_labviewRT = 1; 

% needed for spikesort
cfg.file_format = 4;

cfg.flatclust.data=1;
cfgf.flatclust.multiunit=1;
cfg.flatclust.STACK=1;

userID = 0; cfg.userID = userID;

% 
% cfg.scanrate=33000; % sampling rate to be used for recording
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%% song recognition stuff%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % cfg.fs=33333; % sampling rate; - not used anymore
% %fs=32000;
% cfg.t_rt=1/cfg.scanrate;  % for real-time model - must agree with fs !!!!!!!
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%buffer parameters for Matlab And Simulink%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% cfg.buffersize = 256;
% cfg.overlap=(1-1/2)*cfg.buffersize;
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%% Compression parameters%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% cfg.offsets = 0.01*2.^(0:8);
% %cfg.kmin=1;   %vector number energy correspoding to the frequencies
% %cfg.kmax=64;  
% %cfg.kdelta=2;  % 2
% 
% cfg.LPC = [];
% cfg.do_pitch=0;
% cfg.do_pitch_error=0;
% 
% 
% 
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%% Recorder.m %%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% cfg.caller='Feedback';
% cfg.birdnameR=birdName; % name must be lower case only
% 
% 
% %%%%%%%%%%%%%CHANNELS
% 
% %For MCC card, all the channel numbers you'll use for the experiment should
% %be in continuous order, for example: 1, 2 and 3, not 1, 2 and 4.
% cfg.micro=0;         % -1 = off
% cfg.speaker_input=1;  % -1 = off
% cfg.trig_channel=2; % -1 = off
% cfg.channels=[];  % [] = empty, electrode channels to be recorded
% cfg.channels_o=[0]; % 0 for Nidaq, 1 for winsound
% %scanrate=33113;
% cfg.scanrate=33333;
% %scanrate=32000;
% %For MCC card, all the input range should be the same!
% cfg.MicRange=[-10 10]; % Physical range
% cfg.ElRange=[-10 10]; %For MCC card 
% %MicRange=[-5 5]; % Physical range
% %ElRange=[-5 5]; %For MCC card 
% 
% cfg.pre_trigger_int=1;  %pre-trigger in seconds
% 
% cfg.mid_trigger_int=2;  % if no trigger signal during this period, recording stops
% % trigger during recording
% cfg.post_trigger_int=1; %post-trigger in seconds
% cfg.on_amp_scale=500; % sets the ON-sensitivity of the microphone
% cfg.off_amp_scale=200; % sets the OFF-sensitivity of the microphone
% cfg.loudspeaker_scale=.3;  % sets the scaling of the loudspeaker channel
% cfg.amp_drop=1;  % the factor below which the signal on the microphone must drop to stop triggering
% 
% %input_device='nidaq'; input_hwaddress='1';
% cfg.input_device='nidaqmx'; input_hwaddress='Dev1';
% %input_device='mcc'; input_hwaddress='0'; % Claude 03.08.2006 Measurement Computing's PCIDAS1602/16 has 
% % no 'channel gain queue', so we can't record from channels of non-continuous numbers such as [0 7 15]. 
% % We have to use channels [0 1 2] for this card.
% cfg.output_device='winsound'; output_hwaddress='0';
% 
% cfg.do_smart=0;
% cfg.do_filter=0; % 1=use oldfashioned filter
% cfg.do_recognition=0; % 1= use song recognition
% cfg.do_external=1; % 1=external triggering
% 
% % we do this in analyze.m
% % if do_labviewRT
% %     DetectSyl=input('Which Syllable would you like to detect? (0 for nothing)');
% %     if DetectSyl
% %         fprintf('Now loading %s\n',[bird_path 'RecognizerData\' birdName '-Syl' num2str(DetectSyl)]);
% %         load([bird_path 'RecognizerData\' birdName '-Syl' num2str(DetectSyl)]);
% %         save('E:\LabVIEW\perceptron','b1','b2','w1','w2','-V6');
% %     end
% % end
% 
% 
% %
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
% %%%%%%%%%%%%%%% Playback.m %%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% cfg.pre_bos_int=2; % time before BOS playbacks in seconds
% cfg.post_bos_int=2; % time after BOS playbacks in seconds
% cfg.scanrate_o=44100;
% %Channels
% cfg.channels_o=[1 2]; % 1 and 2 for winsound (1=speaker, 2=control signal), from SOUNDCARD!!!!!!
% cfg.speaker_input_playback=5;  % -1 = off, on BNC-2090
% cfg.control_input_playback=-1;  % -1=off, control signal giving the beginning of BOS playback, on BNC-2090.
% 
% 
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
% %%%%%%%%%%%%%%% analyze.m %%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% cfg.birdnameA_default=birdName;
% cfg.analyzeWAV=0;
% 
% 
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
% %%%%%%%%%%%%%%% ExtractSong.m %%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% cfg.half_silence=320; % maximum halfinterval between syllables, in ms
% cfg.min_song_dur=600+2*cfg.half_silence;% minimum song duration, in ms
% cfg.min_syl_dur=40; 
% cfg.max_syl_dur=400; % ms
% cfg.min_number_syllables=2;  % minimum number of syllables per song
% [cfg.aa cfg.bb]=ellip(4,.1,40,[.01],'high');
% cfg.scanrateWAV=44100;
% cfg.FileStartES='';  % ='' for all files; ='silver10U-f0041.daq' for special file 
% cfg.FileStopES='';  % ='' for all files; ='silver10U-f0041.daq' for special file 
% cfg.PitchThresh=1000;
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%% TrainIt.m %%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% cfg.TotDistractBuffs=2000;
% 
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
% %%%%%%%%%%% add_to_archive.m %%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% % what's the threshold to distinguish between noise and a syllable?
% cfg.Ethresh=0.4;
% %Ethresh=20;
% % define fs, buffersize overlap, Ethresh
% cfg.BufferMargin=1;
% 
% cfg.DelayedBuffers=0; % recognition with two delayed buffers, in Buffer units
% cfg.DelayedBuffersms=cfg.DelayedBuffers*(cfg.buffersize-cfg.overlap)/cfg.scanrate*1000;
% 
% cfg.winl_arch=500; % for 'm' button, in ms (size of syllable context)
% cfg.winr_arch=500; % for 'm' button, in ms
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%% seelct_and_align.m %%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% cfg.num_recs_to_analyze=24;
% cfg.winl=.300; 
% cfg.winr=.500; %in s
% cfg.estimated_noisedur=100; % in ms, no two triggers in this intervals
% 
% 
% %%%%%%%%%%%%%%%%%YYYY-MM-DD
% %%%%%%%%%%%%%%%%%%%%%%%%%%%
% cfg.first_file='BIRDNAME-FILENAME'; % file index at which to start!
% cfg.last_file='red6U-f0465';
% 
% cfg.buffersizeA=cfg.buffersize; 
% cfg.overlapA=cfg.overlap;
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%% auto_align.m%%%%%%%%%%%%%%%%%%%%%]
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%% YYYY-MM-DD
% cfg.detect_start=1; 
% cfg.detect_end=160;
% cfg.detect_thresh=0.9; % with noise
% %detect_thresh=0.8; % without noise
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%% raster.m %%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% cfg.channels_raster=[-1 1 2 3]; % -1 = microphone, 1=channel 1, 2=channel 2, 3= channel 3;
% cfg.max_examples=-1;    % -1 = off
% 
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%% sound_align.m %%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% %%%%%%%%%%%%%%%%YYYY-MM-DD (Filenumbers)
% cfg.thresh_fine=-2.5; %threshold for syllable onset
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%% average_traces.m %%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% cfg.gwinl=500; %smoothing kernel window
% 
% %%%%%%%%%%%%%% YYYY-MM-DD (Filenumbers)
% %thresholds for channel_raster first no is microphone, other are electrodes
% cfg.threshes=0.5*[0 0.06 0.06 0.3]; 
% 
% 
% 
