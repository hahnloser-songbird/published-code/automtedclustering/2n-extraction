function Flat = FlatClustConfigTemplate()

Flat.STACKscaled=1;
%Flat.Ver=1;
Flat.num_train=400; % maximum number of Tags used for training
Flat.var_explain=0.85; % amount of variance to be captured by principal comonents
Flat.max_elementsPC=10000; % maximum number of elements for principal component analysis
%(increasing this number may give more acurate
%results, but will be slower

%% clustering parameters
Flat.num_clust=20; % number of clusters
Flat.cluster_algorithm=0; % 0=clusterdata, 1=kmeans
Flat.clusterdata_linkage='ward';  % if 'single' not OK, choose 'ward' if first cluster is too large
Flat.clusterdata_distance='mahalanobis'; %'cityblock' or 'mahalanobis'
Flat.import_cluster=0; % 0= no clustering on import, 1= cluster on train, 2=cluster on subsong_small.mat
Flat.import_is_song=0;

%% group names
Flat.groupnames={'Syllable','Call','Noise','Playback','Other','','','',''};

%% Flats names  ('Train' is reserved!)
%Flat.names={'Train','Archive 1','Archive 2','Archive 3','Archive 4','Archive 5','Archive 6','Archive 7','Archive 8','Archive 9'};
Flat.names=cellfun(@(x) ['Archive ' num2str(x)],num2cell(1:120),'UniformOutput',false);
Flat.names{1}='Train';


%% import from output_data
Flat.nfft=512;
Flat.nfft_i=1:round(Flat.nfft/4);
Flat.nonoverlap=128;
Flat.samp_left=512; % Can be smaller than output_data.samp_left!
Flat.samp_right=5*512; % Can be smaller than output_data.samp_right!
Flat.channel_no=1;

%% growing, viewing, importing, extracting
Flat.num_grow=24; % number of elements to search (grow cluster)
Flat.max_elements_grow_search=40; % maximum number of elements in cluster to be used for growing the cluster
Flat.num_nearest_neighbors_for_order_sequence=8; % for ordered plotting of cluster elements
Flat.num_nearest_neighbors_for_clust=1;
Flat.import_method=0; % 0: nearest neighbor, 1: mahalanobis
Flat.grow_method=1; % 0: nearest neighbor, 1: mahalanobis
Flat.max_num_mahalanobis=8; % number of PCs to be used for mahalanobis method, only clusters larger than this number can be
% processed!
Flat.extract_method=2; % 0: random over all clusters (num_train over all)
% 1: random for each cluster (Flat.extract_per_clust per clust)
Flat.extract_per_clust=40;

%% Perceptron
Flat.perceptron_per_clust=10; % minimum number of elements for training a perceptron
Flat.perceptron_num_hidden=3;
Flat.perceptron_ignore=6;
Flat.perceptron_num_pcs=20;

%% Spectrogram
Flat.spec_winls=1; % in seconds, data record shown on left in figure 24
Flat.spec_winrs=1; % in seconds, data record shown on right in figure 24
Flat.spec_num=16;  % number of spectrograms shown
Flat.spec_raw_scale=1;
Flat.spec_raw_offset=0;

Flat.spec_full_mode=0;

%% transition matrix
Flat.graph_max_gap_ms=150;
Flat.graph_thr_percent=1;
Flat.graph_clusts=1:4;
Flat.graph_compressed=0;

%% Tags
Flat.tagnames={'is_classified','is_hidden','Site','Mark'};

Flat.savename='Archives';

%% func_get_data_new
Flat.exec_get_fcn='';   %OLD: func_exec_part_raw(Flat,spec_ID,data,elements(elements_i))
% Flat.XXX(i)=func_YYY(Flat,data,p,i);
% Flat.XXX{i}=func_YYY(Flat,data,p,i);
% Flat.DAT.XXX{i}=func_YYY(Flat,data,p,i);

%% DAT
Flat.channels_load={''};
Flat.channel_mic='';
%Flat.DATplugin={'','','','','','','','','',''};
Flat.DATplugin={'fb_plugin_pitch_HPS','fb_plugin_RMS_500_7K','','','','','','','',''};

Flat.DATwinl_s=.3;  % in seconds
Flat.DATwinr_s=.7;
%Flat.DATcurr_chan=[];
Flat.DATnum_lines=700;
Flat.DATkeep_raw=0;
% DAT filter
Flat.DATplugin_nonoverlap=128;
Flat.DATfilt_highpass = 300;
Flat.DATfilt_order = 4;
Flat.DATfilt_channels={''};

Flat.DATthresh_low_lim=75; % in muV
Flat.DATthresh=150;

Flat.tagpointer_sort=1;

%importI
Flat.importI_function='';

%Default cluster names
for j=1:Flat.num_clust
    Flat.clust_names{j}=['C' num2str(j)];
end

Flat.display_marks=0;

Flat.allow_empty_clusters=0;

Flat.do_raw=0;


end
