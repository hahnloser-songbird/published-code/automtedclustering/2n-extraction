function Plugin = PluginConfig() %#ok<FNDEF>
% FileBrowser-Plugin configuration file
% You can test this config file by running:
% $ InitVariables();
% $ [a, all_return_vars, status] = configure_plugin(cfg);

%% what plugin do you want to use?
Plugin(1).Name = 'fb_plugin_RMS_500_7K()';

%% DEPENDENT VARIABLES 
% Are there any dependent variables that we should initialize, that are 
% needed to set any of the parameters found below?
% e.g.:
% $ a = fb_plugin_RMS_500_7K();
% $ nfft = a.buffersize;
% 'nfft' needs to be defined, because 'TAG_PRESAMPLES' needs 'nfft'.
% 'nfft' is a local variable that will be created and used for the
% configuration of the plugin. The plugin itself does not necessarily need
% to have a 'nfft' property.
%
%
% translates to this:
Plugin(1).Dependencies{1} = 'nfft';
Plugin(1).DependencieValues{1} = 'a.buffersize';

% you can also use this notation
% Plugin(1).Dependencies{1} = 'nfft';
% Plugin(1).DependencieValues{1} = 'buffersize';

%% for each parameter you want to set, use a "ParamName" - "Value" pair.
% see fb_plugin_abstract_tag.m for the definition of each parameter.

i = 1; % parameter index.
Plugin(1).ParamName{i} = 'TAG_THRESHOLD';
Plugin(1).Value{i} = 0.005;

i = i + 1;
Plugin(1).ParamName{i} = 'TAG_PRESAMPLES';
Plugin(1).Value{i} = '1 * nfft';

i = i + 1;
Plugin(1).ParamName{i} = 'TAG_POSTSAMPLES';
Plugin(1).Value{i} = '5 * nfft';

i = i + 1;
Plugin(1).ParamName{i} = 'TAG_DATA_TYPE';
Plugin(1).Value{i} = 1;

i = i + 1;
Plugin(1).ParamName{i} = 'TAG_RETURN_DATA_AS_INT16';
Plugin(1).Value{i} = 1;

i = i + 1;
Plugin(1).ParamName{i} = 'TAG_TRANSFORM_DATA_DYNAMIC_RANGE';
Plugin(1).Value{i} = 5;

i = i + 1;
Plugin(1).ParamName{i} = 'TAG_MULTIDIM_DATA_MODE_ENABLED';
Plugin(1).Value{i} = 0;

i = i + 1;
Plugin(1).ParamName{i} = 'TAG_MULTIDIM_USE_CHANNEL';
Plugin(1).Value{i} = 1;

i = i + 1;
Plugin(1).ParamName{i} = 'TAG_SAMPLES_BETWEEN_INDICES';
Plugin(1).Value{i} = 0;

i = i + 1;
Plugin(1).ParamName{i} = 'nonoverlap';
Plugin(1).Value{i} = 2;

end
%% EOF
