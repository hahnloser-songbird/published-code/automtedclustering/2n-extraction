%% User defined input
close all; clear; clc;
% the path to the repository
addpath(genpath(cd))

% the data directory
paths.bird_path = fullfile(cd, 'g17y2'); 
paths.bird_path = [paths.bird_path filesep];
addpath(paths.bird_path);


%% load data as Flat from archive - do not change this section

% initiate relevant variables
CurrentBirdInit;

% load Flat variable 
[~,Flats]=func_default_Flat_values(10);
[Flats, first_loaded, FlatsInfo] = ...
    func_load_Flat2(Flats, paths, 1, 'Archives', [], {'g17y2.mat'});
Flats=func_help_tags_write(Flats);
Flat=Flats{first_loaded};

% some additional assignments
Flat.p.channel_mic_display = 1;
Flat.p.channel_mic = 1;
sz = get(0,'screensize');
Flat.v.Screensize = sz(3:4);


%% initialze context figure 24 - do not change this section

% select only RMSfilt elements 
Flat.v.select = find(strcmp(Flat.Tags(5,:), 'RMSfilt'));
Flat.v.select_clust = Flat.v.select;

func_init_fig_spec(Flat, []);
evnt.Key = 'downarrow'; evnt.Modifier = 'shift'; 
fcH = evalkey_spec(24, evnt);
Flat.v.plot_clust_fast = 0; func_spec_keyrelease;

%% start umap gui reusing the previously computed 2D embedding 
[fcH,Flat]=func_umap_init(fcH,Flat,0,0);

