function Flat=func_remove_harmonics(Flat)
if  ~isfield(Flat.v.tags,'activator') || ~isfield(Flat.v,'tagboard0') || Flat.v.tags.activator>length(Flat.v.tagboard0)
    return
end
u=Flat.v.tagboard0{Flat.v.tags.activator};
for i=1:length(u)-1
    hstr=str2num(u{i});
    if isfield(Flat.p,'Activators') && isnumeric(hstr) && ~isempty(hstr) && le(hstr,length(Flat.p.Activators))
        act_str=Flat.p.Activators{hstr};
        if isempty(strfind(Flat.p.Activators{hstr},'activator_threshold_history'))
            continue
        end
        if isfield(Flat.p,'DetectorsOfActivators') && ~isempty(Flat.p.DetectorsOfActivators{hstr}{1})
            if isempty(strfind(Flat.p.DetectorsOfActivators{hstr}{1},'detector_harmonics'))
                continue
            end
        end
        [Flat,select_harmonics]=func_select_tagboard(Flat,[],'activator',{u{i}});
        harm_f=Flat.X.DATindex(select_harmonics);
        [harm_fu,I,J]=unique(harm_f);
        all_files=find(ismember(Flat.X.DATindex,harm_fu));
        not_harm_f=Flat.X.DATindex(setdiff(all_files,select_harmonics));
        to_del=select_harmonics(ismember(harm_f,unique(not_harm_f)));
        if isempty(to_del)
            return
        end
        fprintf('Now removing %d/%d superfluous harmonics detected elements\n',length(to_del),length(select_harmonics));
        Flat=func_remove_elements(Flat,to_del);
    end
end
end