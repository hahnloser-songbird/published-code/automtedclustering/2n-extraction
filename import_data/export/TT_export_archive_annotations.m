function TT_export_archive_annotations(Flat, s, exclude_clusts, is_meta)
if nargin<1
    Flat = evalin('base', 'Flat');
end
if nargin<2
    s = Flat.v.select;
end
if nargin<3
    exclude_clusts = [];
end
if nargin<4
    is_meta = false;
end

paths = evalin('base', 'paths');
annotpath = [paths.bird_path, 'AnnotationData\'];

if ~isfolder(annotpath)
    mkdir(annotpath)
end

if ~isempty(exclude_clusts)
    s(ismember(Flat.X.clust_ID(s),exclude_clusts))=[];
end

if is_meta
    % export vocal annotations
    itag = find(strcmp(Flat.p.tagnames,'Archive'));
    days = unique(Flat.Tags(itag,:));
    for day=days
        out = [annotpath, day{1}, '_vocal_annotations.csv'];
        ss=s(strcmp(Flat.Tags(itag,s),day));
        f = Flat.DAT.filename(Flat.X.DATindex(ss));
        on = Flat.X.indices_all(ss);
        dur = Flat.X.data_off(ss);
        clust = double(Flat.X.clust_ID(ss));
        cHeader = {'file' 'onset' 'duration' 'cluster_id'};
        T = table(f', on', dur', clust', 'VariableNames', cHeader);
        writetable(T, out);
        
        % export file info
        dats= strcmp(Flat.DAT.subdir,day);
        out = [annotpath, day{1}, '_file_info.csv'];
        f = Flat.DAT.filename(dats);
        t = Flat.DAT.timestamp(dats);
        eof = Flat.DAT.eof(dats);
        cHeader = {'file' 'timestamp' 'eof'};
        T = table(f', t', eof', 'VariableNames', cHeader);
        writetable(T, out);
    end
else
    % export vocal annotations
    day = Flat.DAT.subdir{1};
    out = [annotpath, day, '_vocal_annotations.csv'];
    f = Flat.DAT.filename(Flat.X.DATindex(s));
    on = Flat.X.indices_all(s);
    dur = Flat.X.data_off(s);
    clust = double(Flat.X.clust_ID(s));
    cHeader = {'file' 'onset' 'duration' 'cluster_id'};
    T = table(f', on', dur', clust', 'VariableNames', cHeader);
%     cHeader = {'file' 'onset' 'duration'};
%     T = table(f', on', dur', 'VariableNames', cHeader);
    writetable(T, out);
    
    % export file info
    out = [annotpath, day, '_file_info.csv'];
    f = Flat.DAT.filename;
    t = Flat.DAT.timestamp;
    eof = Flat.DAT.eof;
    cHeader = {'file' 'timestamp' 'eof'};
    T = table(f', t', eof', 'VariableNames', cHeader);
    writetable(T, out);
end

end