function Flat=func_evalkey_S2(Flat,which,verbose)

if nargin<3
    verbose=1;
end

if nargin<2
    %cfg=evalin('base','cfg');
    [which,status] = listdlg('PromptString','Save ?', 'SelectionMode','single','ListString',{[Flat.name ' to Flats'],[Flat.name ' to Disk (and Flats)'],[Flat.p.savename ' to Disk (and Flats)']},'ListSize',[300,80]);
    drawnow;    % Bugfix to prevent blocking on Windows; Important!
else
    status=(which>0 && which<4);
end

save_opt='-v6'; % fast, but no files larger than 2GB

% if iscell(Flat.DAT.data{1}) % for several channels we get big files
%     save_opt='';%'-v7.3';
% end

%% all cases
if status
    Flats=evalin('base','Flats');
    
    try
        FlatsInfo = evalin('base', 'FlatsInfo');
    catch
        FlatsInfo = fci_defaultFlatsInfo(Flats);
    end
    
    ind=Flat.v.ind;
    Flats{ind}=Flat;
    %     assignin('base','Flats',Flats);
    %     fprintf('Updated Flats (saved %s)\n',Flat.name);
else
    return
end


%ask_for_light_save=0;
%save_light=0;
switch which
    case 1
        assignin('base','Flats',Flats);
        return
    case 2
        to_save=Flat.v.ind;
    case 3
        to_save=1:length(Flats);
        %        ask_for_light_save=1;
        
        % check that all birdnames are the same (prevents saving archives
        % to the wrong folder when working with Project mode)
        if numel(unique(cellfun(@(x) x.birdname,Flats,'UniformOutput',false)))>2
            disp('multiple birds loaded - save archives seperately');
            return
        end
end

paths=evalin('base','paths');
dd=filesep;
%save_str='Flats-';
hspec=cell(1,length(Flats));
fname_path=[paths.bird_path 'ArchiveCollection' dd Flat.p.savename dd];

% if ask_for_light_save
%     y=input('Save just a duplicate, light version of archives (yes=1) ? ');
%     if y==1
%         save_light=1;
%         fname_path=[paths.bird_path 'ArchiveCollection' dd 'Light-' Flat.p.savename dd];
%         rmfields={};
%         for i=1:length(Flats)
%             fns_all={};call=0;
%             if isfield(Flats{i},'user')
%                 fns=fieldnames(Flats{i}.user);
%                 for j=1:length(fns)
%                     if length(Flats{i}.user.(fns{j}))==Flats{i}.num_Tags
%                         call=call+1; fns_all{call}=fns{j};
%                     end
%                 end
%             end
%         end
%         fns_all=unique(fns_all); call=0;
%         for j=1:length(fns_all)
%             fprintf('Save %s ',fns_all{j});
%             y=input(' ? (yes=1) ');
%             if ~(y==1)
%                 call=call+1; rmfields{call}=fns_all{j};
%             else
%                 fprintf('Saving %s \n',fns_all{j});
%             end
%         end
%
%     end
% end

%% multiunit firing rate
%r=cellfun(@length,Flat.DAT.multiunit_peaks2)/2/Flat.scanrate;

if ~(exist(fname_path)==7) % does directory exist ?
    mkdir(fname_path)
end

if ~exist([fname_path 'Decoupled'],'dir')
    mkdir(fname_path,'Decoupled');
end

% save FlatsInfo
save([paths.bird_path 'ArchiveCollection' dd Flat.p.savename dd 'FlatsInfo.mat'], 'FlatsInfo');

%% don't bother users with boardlist stuff - it's dangerous!
try
    cfg = evalin('base','cfg');
catch me %#ok<NASGU>
    cfg = [];
end

BOARDLIST_check = 1;
% add your user id to the following array
if ~isempty(cfg) && ismember(cfg.userID, [2 ] )
    BOARDLIST_check = 0;
end


% try % make sure "ParforProgressStarter2" didn't get moved to a different directory
%     ppm = ParforProgressStarter2('Saving archives...', sum(cellfun(@(x) x.num_Tags > 0, Flats(to_save))), 0.05);
% catch me
%     if strcmp(me.message, 'Undefined function or method ''ParforProgressStarter2'' for input arguments of type ''char''.')
%         error('Could not find "ParforProgressStarter2" - please re-run InitVariables()');
%     else
%         % this should NEVER EVER happen.
%         msg{1} = 'Unknown error while initializing "ParforProgressStarter2" (please report to kotowicz@ini.ch):';
%         msg{2} = me.message;
%         print_error_red(msg);
%         % backup solution so that we can still continue.
%         ppm.increment = nan(1, nbr_files);
%     end
% end

%% save archives
for i=to_save
    
    if Flats{i}.num_Tags>0
        %      ppm.increment(); %#ok<PFBNS>
        
        %% ask if files not in Boardlist should be removed from archive
        %         in_boards=[];
        %         for k=1:length(Flats{i}.BOARDLIST)
        %             if ~isempty(Flats{i}.BOARDLIST(k).tagboard)
        %                 [Flats{i},h]=func_select_tagboard(Flats{i},Flats{i}.BOARDLIST(k).tagboard);
        %                 in_boards=union(in_boards,h);
        %             end
        %         end
        %         if ~isempty(in_boards)
        %             files=unique(Flats{i}.X.DATindex(in_boards));
        %             not_in_boards=setdiff(1:length(Flats{i}.DAT.filename),files);
        %
        %             if ~isempty(not_in_boards) && verbose && BOARDLIST_check % Boardlist is used and some tags are not in boardlist
        %                 fprintf('\n%s: %d/%d files are not part of Flat.BOARDLIST!\ndo you want me to remove them ?\n',Flats{i}.name,length(not_in_boards),length(Flats{i}.DAT.filename));
        %                 y=input('(yes=1, no = enter) ');
        %                 if y==1
        %                     to_remove=find(ismember(Flats{i}.X.DATindex,not_in_boards));
        %                     Flats{i}=func_remove_elements(Flats{i},to_remove,1);
        %                 end
        %                 figure(20);
        %             end
        %         end
        
        
        %% remove unnecessary harmonics detector elements
        Flats{i}=func_remove_harmonics(Flats{i});
        
        %% check whether needs trimming
        %Flats{i}=func_trimm_file_list3(Flats{i});
        %          [Flats{i},indices]=func_reorder_by_time_and_trimm2(Flats{i});
        Flats{i}=func_reorder_by_time(Flats{i});
        Flats{i}=func_help_tags_per_file_to_DAT(Flats{i});
        Flath = Flats{i};
        if Flath.p.save_spectrogram == 0
            Flath.X.specdata=cell(1,Flath.num_Tags); % remove unnecessary info
        end
        
        %% TAKE out fields - must be filled in in func_make_compatible
        Flath.v.sequence=cell(25,Flath.p.num_clust);
        Flath.Bz=[]; Flath.Bz_mask=[];
        Flath.v.select=[];
        Flath.v.select_clust=[];
        Flath.v.DATspk_done=zeros(1,Flath.num_Tags);
        Flath.v.DATspk=cell(1,Flath.num_Tags);
        Flath.v.DATI=[]; Flath.v.DATIs=[]; Flath.v.DATIc=[];
        if Flath.p.spec_full_mode && isfield(Flat.v,'templates')
            Flath.v.templates=[];
        end
        Flath.v.DATspec=cell(Flath.p.num_clust,1);
        
        for ii=1:length(Flath.p.CHANNEL_MIC)
            if  isfield(Flath.V,['PCcoeffs' num2str(ii)])
                Flath.V=rmfield(Flath.V,['PCcoeffs' num2str(ii)]);
            end
        end
        
        disp(['saving ' Flath.name ' to ' fname_path ' ...']);
        
        %% save data
        if Flath.p.spec_full_mode==0
            data=Flath.X.data;
            
            save([fname_path 'Decoupled' dd Flath.name '=data.mat'], 'data',save_opt);
            fprintf('Flat.X.data saved\n');
            
        elseif isfield(Flath.X,'data')
            ts = [fname_path 'Decoupled' dd Flath.name '=data.mat'];
            fprintf('Flat.X.data not saved to %s\n',ts);
            Flath.X=rmfield(Flath.X,'data');
        end
        %Flath.X.data=cell(1,Flat.num_Tags);
        
        if ismember(Flath.name(1),{'@','#'})
            Flath.namebase=Flath.name(2:end);
          %  spre='@';
        else
        %    spre='';
            Flath.namebase=Flath.name;
        end
        
        %% save decoupled DAT fields
        if ~(length(Flath.DAT.filename)==Flath.v.DAT_size_at_load_time) || (isfield(Flath.DAT,'data') && ~(length(Flath.DAT.data)==Flath.v.DAT_size_at_load_time)) || (isfield(Flath.DAT,'STACK') && ~(size(Flath.DAT.STACK,2)==Flath.v.DAT_size_at_load_time))
            if ~Flath.v.save.DAT_data || ~Flath.v.save.DAT_multiunit || ~Flath.v.save.DAT_STACK
                %                 disp('DAT changed - I will save all DAT fields - press a key to continue.');
                %                 pause
                Flath.v.DAT_size_at_load_time=length(Flath.DAT.filename);
                Flath.v.save.DAT_data=1; Flath.v.save.DAT_multiunit=1; Flath.v.save.DAT_STACK=1;
            end
        end
        % datas=fieldnames(Flat.DAT);
        if isfield(Flath.p,'CHANNEL_MIC') && length(Flath.p.CHANNEL_MIC)>1
            Flath.DAT.data=cell(1,length(Flath.DAT.filename));
            Flath.v.save.DAT_datas=Flath.v.save.DAT_data; Flath.v.save.DAT_data=0;
            %                     if ~isfield(Flath.DAT,'datas')
            %                           Flath.DAT.datas=cell(1,length(Flath.DAT.filename));
            %                     end
            [Flath,Flats{i}]=func_save_decoupled_DAT(Flath,Flats{i},dd,fname_path,'datas',save_opt);
        else
            [Flath,Flats{i}]=func_save_decoupled_DAT(Flath,Flats{i},dd,fname_path,'data',save_opt);
        end
        
        %    [Flath,Flats{i}]=func_save_decoupled_DAT(Flath,Flats{i},dd,fname_path,'data',save_opt);
        [Flath,Flats{i}]=func_save_decoupled_DAT(Flath,Flats{i},dd,fname_path,'STACK',save_opt);
        [Flath,Flats{i}]=func_save_decoupled_DAT(Flath,Flats{i},dd,fname_path,'multiunit',save_opt,1);
        
        
        %% save useful fields separately in Appendix
        if ~exist([fname_path 'Appendix'],'dir')
            mkdir(fname_path,'Appendix');
        end
        if isfield(Flath.p,'save_appendix')
            for k=1:length(Flath.p.save_appendix)
                field=Flath.p.save_appendix{k};
                if isfield(Flath,field)
                    X=Flath.(field);
                    Flath=rmfield(Flath,field);
                    %                     if strcmp(field,'BOARDLIST') && Flath.v.save_BOARDLIST==0
                    %                         continue
                    %                     else
                    %                         Flath.v.save_BOARDLIST=0;
                    %                     end
                    
                    % Check size of each file. If it's smaller than 1.5GB,
                    % use the default saving (fast but not support > 2GB).
                    % If file is big (e.g. many smooth windows used for
                    % analysis), use '-v7.3', otherwise 'X' won't get saved
                    % at all.
                    s = whos('X');
                    fprintf(['Saving Flat.' field '\n']);
                    if (s.bytes / 1024 / 1024 / 1024) < 1.5
                        save([fname_path 'Appendix' dd Flath.namebase '=' field '.mat'], 'X', save_opt);
                    else
                        save([fname_path 'Appendix' dd Flath.namebase '=' field '.mat'], 'X', '-v7.3','-nocompression');
                    end
                end
            end
        end
        
        
        %% remove redundant fields
        if isfield(Flath,'BOARD')
            Flath=rmfield(Flath,'BOARD');
        end
        [Flath.Tags,Flath.v.tagboard0,Flath.v.tagboard0i]=func_help_tagboard0(Flath.Tags,Flath.p.tagnames,Flath.DAT.timestamp(Flath.X.DATindex(end)),1e15);
        Flath=rmfield(Flath,'Tags');
        if isfield(Flath.v,'grow_examples_all')
            Flath.v=rmfield(Flath.v,'grow_examples_all');
        end
        if isfield(Flath.v,'tmp_spec_data')
            Flath.v=rmfield(Flath.v,'tmp_spec_data');
        end
        % Flath.v=rmfield(Flath.v,'select');
        % Flath.v=rmfield(Flath.v,'select_clust');
        % Flath.v=rmfield(Flath.v,'DATspk');
        % Flath.v=rmfield(Flath.v,'DATspk_done');
        % if Flath.p.spec_full_mode && isfield(Flat.v,'templates')
        % Flath.v=rmfield(Flath.v,'templates');
        % end
        
        
        %% save user field
        if isfield(Flath, 'user') % && length(Flath.user.data)==Flath.num_Tags
            user = Flath.user; %#ok<NASGU>
            Flath = rmfield(Flath, 'user');
            
            disp(['saving #USER#' Flath.name ' ...']);
            % save .user into separate file
            save([fname_path '#USER#' Flath.name '.mat'], 'user',save_opt);
            % save memory
            clear user;
        else
            ts = [fname_path '#USER#' Flath.name '.mat'];
            % fprintf('Did not save %s\n',ts);
        end
        
        % save the archive without .user
        s = whos('Flath');
        if (s.bytes / 1024 / 1024 / 1024) < 1.5
           save([fname_path Flath.name '.mat'], 'Flath',save_opt); %, '-v7.3');
        else
            save([fname_path Flath.name '.mat'], 'Flath','-v7.3','-nocompression'); %, '-v7.3');
        end
        % save([fname_path Flath.name '.mat'], 'Flath',save_opt, '-v7.3');
        % save memory
        clear Flath;
    end
end

% remove progress monitor (if we actually have one)
% if ~isa(ppm, 'struct')
%     delete(ppm);
% end


%% Save Parse along with Flats
if evalin('base','exist(''Parse'',''var'')')
    Parse=evalin('base','Parse');
    [found_error Flat_index] =func_check_parse_flats_consistency(Parse,Flats);
    if found_error
        fprintf('There is an inconsistency between your Parse and Flats variables\n');
        fprintf('Flats index: %d\n', Flat_index);
        fprintf('I recommend to load the correct Parse file!\n');
        keyboard
    end
    
    if isfield(Parse,'saved') && Parse.saved==0
        %y=input('Saving Parse.mat  (yes==1) ?');
        y=1;
        if y==1
            Parse.saved=1;
            save([paths.bird_path dd 'MetaData' dd 'Parse'],'Parse','-v6');
            fprintf('saved Parse.mat\n');
            assignin('base','Parse',Parse);
        end
    end
end
Flat=Flats{ind};
Flat.v.DAT_size_at_load_time=length(Flat.DAT.filename);
fprintf('done saving.\n\n');
assignin('base','Flats',Flats);
fprintf('Updated Flats (saved %s)\n',Flat.name);


% %% rewrite spectrograms
% for i=1:length(Flats)
%     if Flats{i}.num_Tags>0
%         Flats{i}.X.specdata=hspec{i};
%         hspec{i}=[];
%     end
% end
end
%% EOF


function [Flath,Flath2]=func_save_decoupled_DAT(Flath,Flath2,dd,fname_path,save_field,save_opt,multiple_match)

if nargin<7
    multiple_match=0;
end

save_var_name=['DAT_' save_field];

switch multiple_match
    case 1
        fn=fieldnames(Flath.DAT);
        which_multi=find(~cellfun(@isempty,strfind(fn,save_field)));
        if ~isempty(which_multi)
            % for k=1:length(fn)
            for k=which_multi'
                DAT.(fn{k})=Flath.DAT.(fn{k});
                Flath.DAT=rmfield(Flath.DAT,fn{k});
            end
            if Flath.v.save.(save_var_name)
                b=whos('DAT');
                if b.bytes/1024^3<2
                    save([fname_path 'Decoupled' dd Flath.namebase '=DAT_' save_field '.mat'],'DAT',save_opt);
                else
                    disp('large file - saving in version 7.3 - very slow ...');
                    save([fname_path 'Decoupled' dd Flath.namebase '=DAT_' save_field '.mat'],'DAT','-v7.3','-nocompression');
                end
                fprintf('DAT.%s saved\n',save_field);
            end
        end
        
    case 0
        if isfield(Flath.DAT,save_field) && ~isempty(Flath.DAT.(save_field))
            if  any(any(~cellfun(@isempty,Flath.DAT.(save_field)))) && Flath.v.save.(save_var_name)
                DAT.(save_field)=Flath.DAT.(save_field);
                b=whos('DAT');
                if b.bytes/1024^3<2
                    save([fname_path 'Decoupled' dd Flath.namebase '=DAT_' save_field '.mat'],'DAT',save_opt);
                else
                    disp('large file - saving in version 7.3 - very slow ...');
                    %  save([fname_path 'Decoupled' dd Flath.name '=DAT_' save_field '.mat'],'DAT','-v7.3','-nocompression');
                    savefast([fname_path 'Decoupled' dd Flath.namebase '=DAT_' save_field '.mat'],'DAT');
                end
                fprintf('DAT.%s saved\n',save_field);
            end
            Flath.DAT=rmfield(Flath.DAT,save_field);
        end
end
Flath.v.save.(save_var_name)=0;
Flath2.v.save.(save_var_name)=0;

end