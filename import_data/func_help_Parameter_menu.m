function Flat=func_help_Parameter_menu(Flat)

Flat.p.enter_p{1}=sort({'do_smart_move','num_train','num_grow','spec_full_mode','var_explain','channel_no','import_method','grow_method','max_elements_grow_search','max_num_mahalanobis','clusterdata_linkage','clusterdata_distance','display_marks','allow_empty_clusters','spec_num','exec_get_fcn','spec_winls','spec_winrs','save_spectrogram','verbose','DATstepsize_marklines_ms'});
Flat.p.enter_p{10}=sort({'perceptron_ignore','perceptron_num_hidden','perceptron_num_pcs','perceptron_per_clust','perceptron_spec_thresh','perceptron_within_ms'});
Flat.p.enter_p{3}={'graph_max_gap_ms','graph_thr_percent','graph_compressed'};
Flat.p.enter_p{4}={'names'};
Flat.p.enter_p{5}={'clust_names'};
Flat.p.enter_p{6}={'groupnames','tagnames','savename','save_appendix'};
Flat.p.enter_p{7}={'Detectors','Activators','Effectors'};
channel_help=sort({'DATwinl_s','DATwinr_s','DATnum_lines','DATthresh_low_lim'});
Flat.p.enter_p{8}={channel_help{:}};
Flat.p.enter_p{9}={'Tables','Table_plot','Table_frate_limit','Table_min_n'};
Flat.p.enter_p{2}={'Channel_names','CHANNEL_MIC','channel_mic_display','CHANNELS_ELECTRODES','CHANNELS_FILTER','DATfilt_highpass','DATfilt_order','CHANNELS_PLUGIN_DATA','DATplugin','DATplugin_nonoverlap','CHANNELS_KEEP_RAW','importI_function','import_cluster','import_is_song'};
Flat.p.enter_p{11}={'SPARSE_compute','SPARSE_apply','SPARSE_nFeatures','SPARSE_nIterations','SPARSE_filt_width','SPARSE_dynamic_range','SPARSE_buffersize','SPARSE_startsample','SPARSE_nonoverlap','SPARSE_name'};
Flat.p.enter_p{12}={'tag_master', 'tag_master_str','tags_quantize_threshold','Tagboard_execute'};%,'Tagboard_plot'};
Flat.p.enter_p{13}={'spk_burst_lim_ms'};
Flat.p.enter_p{14}={'umap'};%MinNumBuffSyl','NumPixels','ParentMaxSize','GPUdevice'};
Flat.p.enter_p{15}={'brute_force'};%MinNumBuffSyl','NumPixels','ParentMaxSize','GPUdevice'};


Flat.p.enter_p_dat=sort({'stereotypy_win_ms','DAT_dspecgram','DAT_colormap','DAT_frate_smooth_ms','DAT_frate_signrank','DAT_curve_smooth','DAT_curve_std','DAT_frate_tagpoint_pair_sig','DAT_tagpoint_pair_sig','spk_burst_lim_ms','DATstepsize_marklines_ms'});
Flat.p.enter_names={'General','Import','Graph','Archive names','Cluster names','Group/Tag/Save names','Detect Act Effect','Channels','Tables','Perceptron','Sparse','Tagboard','Spikes','UMAP','UMAP.brute_force'};

end