function Flat = func_get_data_parallel_cl(Flat, elements,import, dd, cfg, paths)

%% get mandatory params - if not given.
if nargin < 4
    dd = evalin('base','directoryDelimiter');
    cfg = evalin('base', 'cfg');
    paths = evalin('base', 'paths');
end

PBdir = GUI_filetype_filter_setup();

%% Determine what to import
if nargin < 3 || ~any(struct2array(import))
    [Flat.v.import,ok]=func_help_what_to_import(Flat);
    if ~ok
        return
    end
else
    Flat.v.import=import;
end

%% Return if nothing to do
if (Flat.num_Tags==0 && ~any(Flat.v.import.plugin_import)) || isempty(Flat.DAT.filename)
    return
end
needs_microphone=(Flat.v.import.sam || Flat.v.import.fullspec || Flat.v.import.clustspec || Flat.v.import.plugin_import);

ALL_CHANNELS=unique([needs_microphone*Flat.p.CHANNEL_MIC,...
    Flat.p.CHANNELS_KEEP_RAW, ...
    Flat.v.import.electrode*Flat.p.CHANNELS_ELECTRODES,...
    Flat.v.import.plugin_data.*Flat.p.CHANNELS_PLUGIN_DATA]);

ALL_CHANNELS=setdiff(ALL_CHANNELS,0);

%[reloaddemory,reloadSpecFromMemory,slicedDAT,slicedDATSPEC]=func_help_reloadFromMemory(Flat,elements,ALL_CHANNELS);
[reloadFromMemory,slicedDAT]=func_help_reloadFromMemory(Flat,elements,ALL_CHANNELS);

%% prepare some stuff before importing
if  Flat.v.import.electrode ||  any(Flat.v.import.plugin_data) || Flat.v.import.sam || any(Flat.p.CHANNELS_FILTER)
    Flat=func_prepare_get_channel(Flat);
end

if Flat.v.import.fullspec && Flat.v.import.clustspec
    disp('choose either fullspec or clustspec');
    return
end

if Flat.v.import.fullspec
    Flat.p.spec_full_mode=1;
    %Flat.X.data(:)={[]};
    if isfield(Flat.X,'data')
        Flat.X=rmfield(Flat.X,'data');
    end
    if isfield(Flat.X,'specdata')    %YR is this right?%
        Flat.X=rmfield(Flat.X,'specdata');
    end
    %Flat.v.pca_recompute=1;
    
elseif Flat.v.import.clustspec
    Flat.p.spec_full_mode=0;
    Flat.DAT.data=cell(1,get_number_of_recordings(Flat));
end

if  Flat.v.import.plugin_import==1
    if ~isfield(Flat.DAT,'import_plugin_force_same') || Flat.DAT.import_plugin_force_same~=1
        Flat.DAT.import_plugin=configure_plugin(cfg);
    else
        Flat.DAT = rmfield(Flat.DAT,'import_plugin_force_same');
    end
    if isprop(Flat.DAT.import_plugin,'nonoverlap')
        fprintf('Importing new elements with %s, nonoverlap: %d\n',Flat.DAT.import_plugin.Name,Flat.DAT.import_plugin.nonoverlap);
    else
        fprintf('Importing new elements with %s \n',Flat.DAT.import_plugin.Name);
    end
    % Flat.DAT;
end
[exec_flat,exec_dat,do_cell,exec_fname,fh]=func_prepare_exec(Flat);

%% get list  and prepare subdirs, files, elements
DATi=unique(Flat.X.DATindex(elements));
if cfg.file_format == 12
    files = num2cell(Flat.DAT.recording(DATi));
else
    files=Flat.DAT.filename(DATi);
end
subdirs=Flat.DAT.subdir(DATi);
elements_i=cell(1,length(DATi));
% global Flat2;files=Flat2.DAT.filename;subdirs=Flat2.DAT.subdir;elements_i=cell(1,length(files));DATi=1:length(files);
% Flat.DAT.CHANNELS_SAVED(:,85:length(files))=Flat.DAT.CHANNELS_SAVED(:,1:length(files)-85+1);Flat.DAT.GAIN(:,85:length(files))=Flat.DAT.GAIN(:,1:length(files)-85+1);
% keyboard
for p=1:length(DATi)
    elements_i{p}=find(Flat.X.DATindex(elements)==DATi(p));
    %elements_i{p}=find(Flat.X.DATindex(elements)==p);
end
DATout=cell(1,length(files));
nbr_files = length(files);

%% initialize variables needed inside parfor loop
file_format = cfg.file_format;
if file_format == 11 ||  file_format == 112
    load_path_default = paths.neural_path;
elseif file_format == 12  
    load_path_default = fullfile(paths.neural_path, 'kwik');
else
    load_path_default = paths.load_path_default;
end
    
%% Should be simplified
% CHANNEL_MIC = [];
% if isfield(Flat.v,'CHANNEL_MIC') && ~isempty(Flat.p.CHANNEL_MIC)
%   A  CHANNEL_MIC = Flat.p.CHANNEL_MIC;
% end

%% Create Sparse Flat to minimize memory usage due to parallel processing
% In the parfor loop only sparseFlat but not Flat should be used.
sparseFlat = getSparseFlat(Flat);
%slicedDAT=func_help_get_sliced(Flat,DATi,reloadFromMemory,nbr_files);
% XXX should we temporarily delete the data fields in Flat.DAT ?


%% PARFOR LOOP
disp('Starting to load files in "func_get_data_parallel_cl" ...');
start_time = tic;
%keyboard
num_Tags_old=Flat.num_Tags;

% (Potentially) create parallel pool here, so that the progress monitor wil work
% correctly.
% if ~verLessThan('matlab', '8.3')
%     if isfield(cfg,'use_parfor') && cfg.use_parfor==0
%        num_workers=1;
%     else
%        num_workers=Inf;
%     end
%     
% %     if ~isempty(gcp('nocreate'))
% %         gcp_temp=gcp('nocreate');
% %         if (num_workers==1 && gcp_temp.NumWorkers~=1) || (num_workers>1 && gcp_temp.NumWorkers==1)
% %             delete(gcp('nocreate'))
% %         end
% %     end
% %     if isempty(gcp('nocreate')) && num_workers==1
% %         parpool(1);
% %     end
% %     gcp;
% end


%% Progress Monitor
% if 1
%     try % make sure "ParforProgressStarter2" didn't get moved to a different directory
%         ppm = ParforProgressStarter2([Flat.name ': Loading files in "func_get_data_parallel_cl" ...'], nbr_files, 0.05);
%     catch me
%         if strcmp(me.message, 'Undefined function or method ''ParforProgressStarter2'' for input arguments of type ''char''.')
%             error('Could not find "ParforProgressStarter2" - please re-run InitVariables()');
%         else
%             % this should NEVER EVER happen.
%             msg{1} = 'Unknown error while initializing "ParforProgressStarter2" (please report to kotowicz@ini.ch):';
%             msg{2} = me.message;
%             print_error_red(msg);
%             % backup solution so that we can still continue.
%             ppm.increment = nan(1, nbr_files);
%         end
%     end
% end


h=zeros(1,length(sparseFlat.p.CHANNEL_MIC));
minspec=cell(nbr_files,1); minspec(:)={h};
parfor p = 1 : nbr_files
    
    
    sd=subdirs{p};
    fn=files{p};

%     ppm.increment(p); %#ok<PFBNS>
    
    %% get data from disk or from memory
    data = [];   data1 = []; length_data=[]; channel_mic = [];
    if reloadFromMemory
        %         %    pm.log('reloading from memory');
        %%XXX currently Flat.v.import.plugin_import only runs on
        %the Microphone channel
        %%% checks?
        if sparseFlat.v.import.clustspec || sparseFlat.v.import.fullspec || sparseFlat.v.import.plugin_import || sparseFlat.v.import.sam
            data1=double(slicedDAT{sparseFlat.p.CHANNEL_MIC,DATi(p)})/2^15*sparseFlat.dynamic_range;
        end
        data=cellfun(@(x) double(x)/2^15*sparseFlat.dynamic_range,slicedDAT(:,DATi(p)),'uniformoutput',false);
        length_data=sparseFlat.DAT.eof(DATi(p));
    else
        % make sure to check whether file loading succeeded.
        % I prefix all not needed variables with NU (Not Used) in order to
        % prevent mix ups.
        if cfg.file_format==3000
            aux = sparseFlat.p.birdradio;
        else
            aux = [];
        end
        
        
        [data, NUscanrate, NUall_return_vars, NUfileinfo, status] =...
            load_file_generic(load_path_default, file_format, ...
            sd, dd, fn, [], 1, PBdir, -1, 0, 0, aux); %#ok<ASGLU>
        
        if status == 1 || isempty(data)
            fprintf('file didn''t load correctly: %s : %s \n', sd, fn);
            DATout{p}.data=[];
            DATout{p}.eof=0;
            DATout{p}.to_be_removed=elements(elements_i{p});
            continue
        else
            if cfg.file_format==13 && isfield(sparseFlat.p,'SdrDAQ') && any(sparseFlat.p.SdrDAQ)
                hh=min(size(NUall_return_vars(10).value.DAQmxChannels,1),size(data,2));
                data=[data(:,1:hh);NUall_return_vars(10).value.DAQmxChannels(1:hh,sparseFlat.p.SdrDAQ)'];
                % elseif cfg.file_format==10
                NUall_return_vars(10).value=rmfield(NUall_return_vars(10).value,'DAQmxChannels');
            end
            
            % filter data if applicable
            if isfield(sparseFlat.p, 'CHANNELS_FILTER') && ~isempty(sparseFlat.p.CHANNELS_FILTER) %&& ismember(CHANNEL,sparseFlat.p.CHANNELS_FILTER)
                %              if sparseFlat.p.channels_filter_type==1
                for i=1:length(sparseFlat.p.CHANNELS_FILTER)
                    j=sparseFlat.p.CHANNELS_FILTER(i);
                    data(j,:)=filter(sparseFlat.v.DATfilt.aa, sparseFlat.v.DATfilt.bb, data(j,:));
                    %                         %% OLD:   dat=ftfil(-dat,sparseFlat.scanrate,sparseFlat.p.DATfilt_highpass,8000);
                    %                         elseif sparseFlat.p.channels_filter_type==2
                    %                             dat=ftfil(-dat,sparseFlat.scanrate,300,10000);
                end
                %               end
            end
            
            channel_mic=sparseFlat.DAT.CHANNELS_SAVED(sparseFlat.p.CHANNEL_MIC, DATi(p));
            channel_mic=channel_mic(find(channel_mic));
            %  disp(['Mic channel: ' num2str(channel_mic)]);        %added by Alexei

            %else
                data1 = data(channel_mic,:); % check this ----------------------------------------------
            %   data1=gpuArray(data1);
            %end
        end
        length_data = size(data,2);
    end

    %  length_data = length(data1);
    if cfg.file_format == 12 && size(data, 1)==1% in case of kwd files only the mic is loaded
        gains=sparseFlat.DAT.GAIN(channel_mic,DATi(p));
    else
        gains=sparseFlat.DAT.GAIN(:,DATi(p));
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% DAT
    DATout{p}=func_get_data_new_channels2_cl(sparseFlat,data,gains,DATi(p),reloadFromMemory,ALL_CHANNELS);
    DATout{p}.eof=length_data;
    DATout{p}.to_be_removed=[];
    
    %% import plugin for indices_all (import_plugin_import)
    if  sparseFlat.v.import.plugin_import==1
        DATout{p}=func_do_plugin_import(sparseFlat,DATout{p},data1,length_data);
    end
    
    %% fullspec
    % compute spectrogram
    if sparseFlat.v.import.fullspec
        %    DATdata = func_data_to_specgram(data1, 512, sparseFlat.scanrate, twin, sparseFlat.v.spec_plot.ovlp);
        if length(channel_mic)>1
            for i=1:length(channel_mic)
                DATout{p}.datas{i}=func_raw_to_data_cl(data1(i,:),sparseFlat,2,sparseFlat.p.spec_log_min)';
                minspec{p}(i)=min(min(DATout{p}.datas{i}));
           end
            DATout{p}.data=DATout{p}.datas{i}';%func_raw_to_data_cl(data1(1,:),sparseFlat,2,sparseFlat.p.spec_log_min);
        else
            DATout{p}.data=func_raw_to_data_cl(data1,sparseFlat,2,sparseFlat.p.spec_log_min);
            minspec{p}=min(min(DATout{p}.data));            
        end
    end
    
    %% INTAN and NIDQ (Neuropixels) data
    if file_format==11 && isfield(sparseFlat.p.import_INTAN,'chan')
%         d=data(sparseFlat.p.import_INTAN.chan,:)>1e3;
        d=data(sparseFlat.p.import_INTAN.chan,:);
        onsets=find(diff(d)>0);
        DATout{p}.AI={onsets}; % perceptron onsets

    elseif file_format==12

        d=data(sparseFlat.p.import_INTAN.chan,:);
        onsets=find(diff(d)>0);
        DATout{p}.AI={onsets}; % perceptron onsets
        
    elseif file_format==13 || file_format==10 || file_format==3000% linus' new data format
        DATout{p}.Sdr=NUall_return_vars(10).value;
        
    elseif file_format == 112 && size(data, 1)>=3 && Flat.p.import_INTAN.do==1
        perc_onsets{p} = find(diff(data(3,:))>0); % perceptron onsets (p is the file index in this case)
    end
    
    
    
    %% SAM
    if sparseFlat.v.import.sam
        if size(data1, 2) < 1
            fprintf('Error: File %i is empty.\n', DATi(p));
        else
            DATout{p}.samFeatures = sam_analysis(data1, sparseFlat.scanrate, 1,sparseFlat.p.SAMparam); % XXXjosh
            %             DATout{p}.samFeatures = sam_analysis(data1, sparseFlat.scanrate{elements(elements_i{p}(1))}, 1,sparseFlat.p.SAMparam);
            %             try
            %                 DATout{p}.samFeatures = sam_analysis(data1, sparseFlat.scanrate{elements(elements_i{p}(1))}, 1,sparseFlat.p.SAMparam);
            %             catch
            %                 disp(p)
            %                 keyboard
            %             end
        end
    end
    
    %% cluster the indices
    els=elements(elements_i{p});
    DATout{p}.F.which=zeros(1,length(els));
    % DATout{p}.F.firstbuff=zeros(1,length(els));
    % DATout{p}.F.DAT.eof(Flat.X.DATindex)=zeros(1,length(els));
    if sparseFlat.v.import.clustspec
        DATout{p}.F.data=cell(1,length(els));
    end
    
    %% per element (syllable)
    for i=1:length(els)
        
        cid=sparseFlat.X.clust_ID(els(i));
        samp_left=sparseFlat.spec.samp_left{1}(cid); % shorthand
        samp_right=sparseFlat.spec.samp_right{1}(cid);
        i0=sparseFlat.X.indices_all(els(i));
        
        DATout{p}.F.which(i)=els(i);
        if sparseFlat.v.import.clustspec
            if i0 > samp_left && i0 + samp_right < length_data
                DATout{p}.F.data{i}=func_raw_to_data_cl(data1(i0-samp_left-Flat.p.nfft+1:i0+samp_right-Flat.p.nfft),sparseFlat,1,sparseFlat.p.spec_log_min);
                
            else
                DATout{p}.to_be_removed=[DATout{p}.to_be_removed els(i)];
                DATout{p}.F.data{i}=[];
            end
        end
        
        
        %% exec XXX redo
        % Flat.XXX(i)=func_YYY(Flat,data,p,i);
        % Flat.XXX{i}=func_YYY(Flat,data,p,i);
        if exec_flat
            if do_cell
                %DATout{p}.F.(exec_fname){i}=feval(fh,Flat,data,p,els(i));
            else
                %DATout{p}.F.(exec_fname)(i)=feval(fh,Flat,data,p,els(i));
            end
        end
        
    end
    
    % Flat.DAT.XXX{i}=func_YYY(Flat,data,p,[]);
    if exec_dat
        %DATout{p}.(exec_fname)=feval(fh,Flat,data,p,[]);
    end
    
end

% remove progress monitor (if we actually have one)
% if ~isa(ppm, 'struct')
%     delete(ppm);
% end
if  sparseFlat.v.import.fullspec
    Flat.p.spec_min=1e9*ones(1,length(sparseFlat.p.CHANNEL_MIC));
    for i=1:length(minspec)
        for j=1:length(sparseFlat.p.CHANNEL_MIC)
            Flat.p.spec_min(j)=min(Flat.p.spec_min(j),minspec{i}(j));
        end
    end
end
%% write back to Flat
Flat=func_write_back_to_Flat(Flat,DATout,DATi,{'to_be_removed','dummy_plug','dummy','F','eof','AI'});
% if isfield(Flat.v,'tags_quantized')
%     Flat.v=rmfield(Flat.v,'tags_quantized');
% end

%% write back plugin
if any(Flat.v.import.plugin_data)
    for i=find(Flat.v.import.plugin_data)% find(Flat.p.CHANNELS_PLUGIN_DATA)
        %        Flat.DAT.STACK(Flat.v.stack.plugin(i),:)=Flat.DAT.(['plugin' num2str(i)]);
        if isfield(Flat.DAT,'STACK') && isempty(Flat.DAT.STACK)
            Flat.DAT=rmfield(Flat.DAT,'STACK');
        end
        Flat.DAT.STACK(Flat.v.stack.plugin(i),DATi)=Flat.DAT.(['plugin' num2str(i)])(DATi);
        Flat.DAT=rmfield(Flat.DAT,['plugin' num2str(i)]);
        ii=Flat.v.stack.plugin(i);
        Flat.DAT.stack(ii)=func_help_stack_min_max(Flat.DAT.STACK(ii,:),Flat.DAT.stack(ii),Flat.p.STACKscaled);
        %        Flat.DAT.stack(ii).max=double(max(cellfun(@max,Flat.DAT.STACK(ii,:),'UniformOutput',false)))/2^15*max(abs(Flat.DAT.stack(ii).dynamic_range)); % inverse compression formula
    end
end

%% write SAM to STACK
if Flat.v.import.sam
    for i=find(~cellfun(@isempty,Flat.DAT.samFeatures))
        for j=1:length(Flat.p.SAMfeatureNames)
            Flat.DAT.STACK{Flat.v.stack.sam(j),i}=int16(Flat.DAT.samFeatures{i}(:,j+1)'/max(abs(Flat.p.SAMdynamic_range{j}))*2^15);
        end
    end
    for j=1:length(Flat.p.SAMfeatureNames)
        jj=Flat.v.stack.sam(j);
        Flat.DAT.stack(jj)=func_help_stack_min_max(Flat.DAT.STACK(jj,:),Flat.DAT.stack(jj),Flat.p.STACKscaled);
        %       Flat.DAT.stack(jj).min=double(min(cellfun(@(x) min(x),Flat.DAT.STACK(jj,:),'UniformOutput',false)))/2^15*max(abs(Flat.DAT.stack(jj).dynamic_range)); % inverse compression formula
        %       Flat.DAT.stack(jj).max=double(max(cellfun(@(x) max(x),Flat.DAT.STACK(jj,:),'UniformOutput',false)))/2^15*max(abs(Flat.DAT.stack(jj).dynamic_range)); % inverse compression formula
    end
    Flat.DAT=rmfield(Flat.DAT,'samFeatures');
    
end

%% remove unused entries
[Flat,was_removed]=func_remove_unused_entries(Flat,DATout);
fprintf('Removed %d elements\n',length(was_removed));

%% Write newly imported elements to Flat
if Flat.v.import.plugin_import
    tag_str=Flat.DAT.import_plugin.Name;
    ons=cell(1,get_number_of_recordings(Flat)); offs=ons;
    ons(DATi)=Flat.DAT.import_plugin_on(DATi);    
    offs(DATi)=Flat.DAT.import_plugin_off(DATi);
    %   Flat=func_write_imported_elements_to_Flat(Flat,Flat.DAT.import_plugin_on,Flat.DAT.import_plugin_off,[],[],0,[],[],[],tag_str);
    Flat=func_write_imported_elements_to_Flat(Flat,ons,offs,[],[],0,[],[],[],tag_str);
    Flat.v.plot_tagboard=1;
    Flat.v.tagboard0_replot=1;
end

%% Write newly imported elements to Flat
if (file_format==11 || file_format == 12) && Flat.p.import_INTAN.do==1
    
    tag_str=Flat.p.import_INTAN.name;
    ons=cell(1,get_number_of_recordings(Flat));offs=ons;
    ons(DATi)=Flat.DAT.INTAN(DATi); offs(DATi)=ons;
    [Flat,percs]=func_select_tagboard(Flat,[],Flat.p.tagnames{Flat.v.tags.activator},tag_str);
    Flat=func_remove_elements(Flat,percs,0,0);
    Flat=func_write_imported_elements_to_Flat(Flat,ons,offs,[],[],0,[],[],[],tag_str);
    Flat.v.plot_tagboard=1;
    Flat.v.tagboard0_replot=1;
   
elseif file_format == 112 && Flat.p.import_INTAN.do==1 
    
    fprintf('load perceptron from din 01 \n');
    tag_str= Flat.p.import_INTAN.name;
    
    ons= perc_onsets; offs(DATi)=perc_onsets;
   
    [Flat,percs]=func_select_tagboard(Flat,[],Flat.p.tagnames{Flat.v.tags.activator},tag_str);
    Flat=func_remove_elements(Flat,percs,0,0);
    Flat=func_write_imported_elements_to_Flat(Flat,ons,offs,[],[],0,[],[],[],tag_str);
    Flat.v.plot_tagboard=1;
    Flat.v.tagboard0_replot=1;
   
elseif file_format==13 % linus' new data format
    %%
    if isfield(Flat.DAT.Sdr{1},'DAQmxChannelsFs') && Flat.DAT.Sdr{1}.DAQmxChannelsFs~=Flat.scanrate
        disp('Scanrate problem');
    end
    Flat=sdr_to_stack(Flat,'SdrSignalStrength',[-100 0]);
    AA=Flat.DAT.Sdr{1}.ChannelList;
    fmin=.95*1e6*min(min(AA{:,3:4})); fmax=1.05*1e6*max(max(AA{:,3:4}));
    Flat=sdr_to_stack(Flat,'SdrReceiverFreq',[fmin fmax]);
    Flat=sdr_to_stack(Flat,'SdrCarrierFreq',[fmin fmax]);
    stacks=[Flat.v.stack.SdrCarrierFreq Flat.v.stack.SdrReceiverFreq Flat.v.stack.SdrSignalStrength];
    for jj=1:length(stacks)
        Flat.DAT.stack(jj)=func_help_stack_min_max(Flat.DAT.STACK(jj,:),Flat.DAT.stack(jj),Flat.p.STACKscaled); %% RH: note: max/min scaling is different here!!!
%            hmin=cellfun(@(x) min(x),Flat.DAT.STACK(jj,:),'UniformOutput',false);Flat.DAT.stack(jj).min=double(min([hmin{:}]));
%            hmax=cellfun(@(x) max(x),Flat.DAT.STACK(jj,:),'UniformOutput',false);Flat.DAT.stack(jj).max=double(max([hmax{:}]));
        if isempty(Flat.DAT.stack(jj).comment)
            Flat.DAT.stack(jj).comment=1; %define multiplier
        end
    end
    
    for i=1:length(Flat.DAT.Sdr)
        Flat.DAT.Sdr{i}=rmfield(Flat.DAT.Sdr{i},'CarrierFreq');
        Flat.DAT.Sdr{i}=rmfield(Flat.DAT.Sdr{i},'ReceiverFreq');
        Flat.DAT.Sdr{i}=rmfield(Flat.DAT.Sdr{i},'SignalStrength');
    end
    
elseif file_format==3000 % birdradio v1.0
    buffS = Flat.p.birdradio.FsDivider;
    for i=1:Flat.p.birdradio.NradioChannels
        dyn_range = [-100 0];
        [Flat,Flat.DAT.stack,Flat.v.stack.SdrSignalPower(i)]=func_stack_add(Flat,['SdrSignalPower' num2str(i)],'void',buffS,buffS,dyn_range,1);
        stacki_sigPow=Flat.v.stack.SdrSignalPower(i);
        
        dyn_range = [-100 0];
        [Flat,Flat.DAT.stack,Flat.v.stack.SdrNoisePower(i)]=func_stack_add(Flat,['SdrNoisePower' num2str(i)],'void',buffS,buffS,dyn_range,1);
        stacki_noisePow=Flat.v.stack.SdrNoisePower(i);
        
        try
            dyn_range = [-1 1]*Flat.p.birdradio.radioChannelRange;
        catch
            Flat.p.birdradio.radioChannelRange = Flat.p.birdradio.randioChannelRange;
            dyn_range = [-1 1]*Flat.p.birdradio.radioChannelRange;
        end
        [Flat,Flat.DAT.stack,Flat.v.stack.SdrCarrierFreq(i)]=func_stack_add(Flat,['SdrCarrierFreq' num2str(i)],'void',buffS,buffS,dyn_range,1);
        stacki_carFrq=Flat.v.stack.SdrCarrierFreq(i);
        
        for ii=1:length(Flat.DAT.Sdr)
            Flat.DAT.STACK{stacki_sigPow,ii}=single(((Flat.DAT.Sdr{ii}.SignalPower(:,i)')-Flat.DAT.stack(stacki_sigPow).dynamic_range(1))/diff(Flat.DAT.stack(stacki_sigPow).dynamic_range)*2^15);
            Flat.DAT.STACK{stacki_noisePow,ii}=single(((Flat.DAT.Sdr{ii}.NoisePower(:,i)')-Flat.DAT.stack(stacki_noisePow).dynamic_range(1))/diff(Flat.DAT.stack(stacki_noisePow).dynamic_range)*2^15);
            Flat.DAT.STACK{stacki_carFrq,ii}=single(((Flat.DAT.Sdr{ii}.CarrierFreq(:,i)')-Flat.DAT.stack(stacki_carFrq).dynamic_range(1))/diff(Flat.DAT.stack(stacki_carFrq).dynamic_range)*2^15);
        end
    end
    
    if Flat.p.birdradio.NradioChannels == 0
        Flat.v.stack.SdrSignalPower = [];
        Flat.v.stack.SdrNoisePower = [];
        Flat.v.stack.SdrCarrierFreq = [];
    end
    
    dyn_range = [0 1];
    [Flat,Flat.DAT.stack,Flat.v.stack.Door]=func_stack_add(Flat,'Door','void',buffS,buffS,dyn_range,1);
    stacki_door=Flat.v.stack.Door;
    for ii=1:length(Flat.DAT.Sdr)
        Flat.DAT.STACK{stacki_door,ii}=int16(((Flat.DAT.Sdr{ii}.Door(:)')-Flat.DAT.stack(stacki_door).dynamic_range(1))/diff(Flat.DAT.stack(stacki_door).dynamic_range)*2^15);
    end
    
    stacks=[Flat.v.stack.SdrCarrierFreq Flat.v.stack.SdrSignalPower Flat.v.stack.SdrNoisePower Flat.v.stack.Door];
    
    if Flat.p.birdradio.fileVersion ~= 0 
        dyn_range = [0 1];
        [Flat,Flat.DAT.stack,Flat.v.stack.Light]=func_stack_add(Flat,'Light','void',buffS,buffS,dyn_range,1);
        stacki_light=Flat.v.stack.Light;
        for ii=1:length(Flat.DAT.Sdr)
            Flat.DAT.STACK{stacki_light,ii}=int16(((Flat.DAT.Sdr{ii}.Light(:)')-Flat.DAT.stack(stacki_light).dynamic_range(1))/diff(Flat.DAT.stack(stacki_light).dynamic_range)*2^15);
        end
        
        stacks=[stacks Flat.v.stack.Light];
    end
    
    
    for jj=1:length(Flat.DAT.stack)
        Flat.DAT.stack(jj)=func_help_stack_min_max(Flat.DAT.STACK(jj,:),Flat.DAT.stack(jj),Flat.p.STACKscaled); %% RH: note: max/min scaling is different here!!!
%            hmin=cellfun(@(x) min(x),Flat.DAT.STACK(jj,:),'UniformOutput',false);Flat.DAT.stack(jj).min=double(min([hmin{:}]));
%            hmax=cellfun(@(x) max(x),Flat.DAT.STACK(jj,:),'UniformOutput',false);Flat.DAT.stack(jj).max=double(max([hmax{:}]));
        if isempty(Flat.DAT.stack(jj).comment)
            Flat.DAT.stack(jj).comment=1; %define multiplier
        end
    end
    
%     for i=1:length(Flat.DAT.Sdr)
%         Flat.DAT.Sdr{i}=rmfield(Flat.DAT.Sdr{i},'CarrierFreq');
%         Flat.DAT.Sdr{i}=rmfield(Flat.DAT.Sdr{i},'SignalPower');
%         Flat.DAT.Sdr{i}=rmfield(Flat.DAT.Sdr{i},'NoisePower');
%         Flat.DAT.Sdr{i}=rmfield(Flat.DAT.Sdr{i},'Door');
%         if Flat.p.birdradio.fileVersion ~= 0 
%             Flat.DAT.Sdr{i}=rmfield(Flat.DAT.Sdr{i},'Light');
%         end
%     end
    
elseif file_format==10 
        if Flat.DAT.Sdr{1}.DAQmxChannelsFs~=Flat.scanrate
        disp('Scanrate problem');
        end
        fmin=0.95*min(min(Flat.DAT.Sdr{1}.CarrierFreq)); fmax=1.05*max(max(Flat.DAT.Sdr{1}.CarrierFreq));
        Flat=sdr_to_stack(Flat,'SdrCarrierFreq',[fmin fmax]);
        Flat=sdr_to_stack(Flat,'SdrSignalStrength',[-100 0]);
        stacks=[Flat.v.stack.SdrCarrierFreq Flat.v.stack.SdrSignalStrength];
        for jj=1:length(stacks)
            %            Flat.DAT.stack(jj)=func_help_stack_min_max(Flat.DAT.STACK(jj,:),Flat.DAT.stack(jj));
            hmin=cellfun(@(x) min(x),STACK,'UniformOutput',false);Flat.DAT.stack(jj).min=double(min([hmin{:}]));
            hmax=cellfun(@(x) max(x),STACK,'UniformOutput',false);Flat.DAT.stack(jj).max=double(max([hmax{:}]));
            
            if isempty(Flat.DAT.stack(jj).comment)
                Flat.DAT.stack(jj).comment=1; %define multiplier
            end
        end
end

% %% mean and var
% if Flat.v.import.sam
%     Flat=func_stack_mean_var(Flat);
% end

%% master
if isfield(Flat.p,'tag_master_str') && ~isempty(Flat.p.tag_master_str)
    Flat=func_tag_master_and_slave(Flat,Flat.p.tag_master_str);
end

%% trimm
%Flat=func_trimm_file_list3(Flat);
% [Flat,indices]=func_reorder_by_time_and_trimm2(Flat);
Flat=func_reorder_by_time(Flat);

toc(start_time) % Elapsed time is

if Flat.num_Tags > 0 && Flat.num_PCs == 0
    % skip calculating PCA if requested - useful for mass importing, I can
    % calculate the PCA later.
    if ~isfield(Flat.p, 'calc_pca') || (isfield(Flat.p, 'calc_pca') && Flat.p.calc_pca == 1)
        % don't ask about the PCAs - just do it.
        Flat = func_Flat_PCA(Flat, 0, 0, 1);
    end
end

if Flat.v.import.sam
    %% mean and var
    if isfield(Flat.v,'import_control_i') && Flat.v.import_control_i==1
        Flat=func_stack_mean_var(Flat,num_Tags_old+1:Flat.num_Tags); %% looks like this are too many elements (RMSfilt AND activator-triggered)
        Flat.v=rmfield(Flat.v,'import_control_i');
    else
        Flat=func_stack_mean_var(Flat);
    end
end

% remove skype cleanup when reimporting spectrograms
if Flat.v.import.fullspec
    if isfield(Flat.p,'skype')
        if isfield(Flat.p.skype,'mode')
            Flat.p.skype.mode=2*ones(1,length(Flat.p.CHANNEL_MIC));
        end
        if isfield(Flat.p.skype,'echo_DATclean')
            Flat.p.skype.echo_DATclean=0;
        end
    end
end
%[Flat.Tags,Flat.v.tagboard0,Flat.v.tagboard0i]=func_help_tagboard0(Flat.Tags,Flat.p.tagnames,Flat.timestamp(end),200);


Flat.v.import.plugin_import=0; % =1 slows everything down a lot when importing next time
Flat.v.save.DAT_STACK=any(Flat.v.import.plugin_data) || Flat.v.import.sam;
Flat.v.import.plugin_data=zeros(size(Flat.p.CHANNELS_PLUGIN_DATA)); % =1 slows everything down a lot when importing next time
Flat.v.import.clustspec=0;
Flat.v.save.DAT_data=Flat.v.import.fullspec;
Flat.v.import.fullspec=0;
Flat.v.save.DAT_multiunit=Flat.v.import.electrode;
Flat.v.import.electrode=0;
Flat.v.import.sam=0;
Flat.v.import.dummy=0;

end
%% EOF
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [sparseFlat] = getSparseFlat(Flat)

% These are the fields from Flat that are needed inside the parfor loop.
% We create a sparse Flat variable by copying each of these into sparseFlat
% (if the field exists in Flat)
fieldsToCopy = { ...
    'Flat.p.Channel_names', 'Flat.DAT.CHANNELS_SAVED', 'Flat.p.CHANNELS_FILTER',...
    'Flat.p.CHANNEL_MIC','Flat.DAT.eof','Flat.p.DATfilt_channels','Flat.p.DATfilt_highpass',...
    'Flat.DAT.GAIN','Flat.v.DATfilt', 'Flat.p.CHANNELS_KEEP_RAW',...
    'Flat.dynamic_range', 'Flat.p.DATplugin','Flat.p.SAMparam', 'Flat.DAT.curr_plug',...
    'Flat.v.plugin_str', 'Flat.p.CHANNELS_ELECTRODES', 'Flat.v.peak_str',...
    'Flat.v.loc_str', 'Flat.p.DATthresh_low_lim','Flat.p.spec_log_min',...
    'Flat.v.cross_str', 'Flat.DAT.thresh', 'Flat.DAT.CHANNELS_SAVED',...
    'Flat.p.nfft', 'Flat.p.DATplugin_nonoverlap',...
    'Flat.p.nfft_i', 'Flat.v.import','Flat.p.SdrDAQ', 'Flat.p.birdradio'...
    'Flat.DAT.import_plugin', 'Flat.X.clust_ID','Flat.p.num_clust','Flat.spec.samp_left',...
    'Flat.spec.samp_right', 'Flat.X.indices_all', 'Flat.p.nonoverlap', ...
    'Flat.p.CHANNELS_PLUGIN_DATA','Flat.dynamic_range_spec','Flat.p.spec_full_mode',...
    'Flat.v.stack','Flat.DAT.stack','Flat.scanrate','Flat.v.SpecMat','Flat.p.spec_raw_scale','Flat.p.channels_filter_type','Flat.p.import_INTAN'};


nUndef = [];
for k = 1:length(fieldsToCopy)
    try
        eval(['sparse' fieldsToCopy{k} ' = ' fieldsToCopy{k} ';']);
    catch
        nUndef = [nUndef, k];
    end
end


%% verbose
% if ~isempty(nUndef) > 0
%     fprintf('%i/%i fields found and copied into sparseFlat. Fields not found:\n', length(fieldsToCopy) - length(nUndef), length(fieldsToCopy));
%     for j = 1:length(nUndef)
%         fprintf('%s,\n', fieldsToCopy{nUndef(j)});
%     end
% end

end
%% EOF

function [reloadFromMemory,slicedDAT]=func_help_reloadFromMemory(Flat,elements,ALL_CHANNELS)

slicedDAT=cell(length(Flat.p.Channel_names),get_number_of_recordings(Flat));
reloadFromMemory = true;

for i=1:length(ALL_CHANNELS)
    chan=ALL_CHANNELS(i);
    str=['data' num2str(chan)];
    if ~isfield(Flat.DAT,str)
        reloadFromMemory = false;
        break
    end
    reloadFromMemory=reloadFromMemory & ~any(cellfun(@isempty,Flat.DAT.(['data' num2str(chan)])(Flat.X.DATindex(elements))));
    if reloadFromMemory
        slicedDAT(chan,:)=Flat.DAT.(str);
    else
        break;
    end
end

if reloadFromMemory
    fn=fieldnames(Flat.DAT);
    num_data=sum(~cellfun(@isempty,(strfind(fn,'data'))));
    if num_data<2
        reloadFromMemory=false;
    end
end
if reloadFromMemory
    fprintf('%s: Loading from memory\n',Flat.name);
else
    fprintf('%s: Loading from disk\n',Flat.name);
end
end
%% EOF

function DATout=func_do_plugin_import(sparseFlat,DATout,data1,length_data)
DATout.dummy_plug           = sparseFlat.DAT.import_plugin;
DATout.dummy_plug.Scanrate  = sparseFlat.scanrate;              % set scanrate (otherwise scanrat default = 32kHz is used) <ah>
DATout.dummy_plug.tag_data(data1);                              % which data to tag ?
DATout.import_plugin_on     = DATout.dummy_plug.TAG_INDICES;
DATout.import_plugin_off    = DATout.dummy_plug.TAG_INDICES_OFF;
% if  237409==size(data1,2)
%     keyboard
% end
DATout.spec                 = cell(1,length(DATout.import_plugin_on));
sampleft                    = sparseFlat.spec.samp_left{1}(sparseFlat.p.num_clust);
sampright                   = sparseFlat.spec.samp_right{1}(sparseFlat.p.num_clust);
to_be_removed               = zeros(1,length(DATout.import_plugin_on));
if ~sparseFlat.p.spec_full_mode
    for j=1:length(DATout.import_plugin_on)
        i0=DATout.import_plugin_on(j);
        %% into last cluster
        if i0 > sampleft && i0 + sampright < length_data
            DATout.spec{j}=func_raw_to_data_cl(data1(i0-sampleft-sparseFlat.p.nfft+1:i0+sampright-sparseFlat.p.nfft),sparseFlat,1,sparseFlat.p.spec_log_min);
        else
            to_be_removed(j)=1;
        end
    end
    if any(to_be_removed)
        DATout.spec(find(to_be_removed))=[];
        DATout.import_plugin_on(find(to_be_removed))=[];
        DATout.import_plugin_off(find(to_be_removed))=[];
    end
end
end
%% EOF

function Flat=func_write_back_to_Flat(Flat,DATout,DATi,exclude_fields)
%% Flat.DAT
fnA=cellfun(@fieldnames,DATout,'UniformOutput',false);
fn=fnA{1};
for i=2:length(DATout)
    fn=union(fn,fnA{i});
end
%fn=union(fn{:});
%fn=fieldnames(DATout{1});
fn=setdiff(fn,exclude_fields);
for i=1:length(fn)
    if ~isfield(Flat.DAT,fn{i})
        Flat.DAT.(fn{i})=cell(1,get_number_of_recordings(Flat));
    end
    for p=1:length(DATout)
        if isfield(DATout{p},fn{i})
            Flat.DAT.(fn{i}){DATi(p)}=DATout{p}.(fn{i});
        end
    end
end
for p=1:length(DATout)
    if isfield(DATout{p},'eof')
        Flat.DAT.eof(DATi(p))=DATout{p}.eof;
    end
    
    if isfield(DATout{p},'AI')
        Flat.DAT.INTAN(DATi(p))=DATout{p}.AI;
    end
end




%% Flat
for p=1:length(DATout)
    if isfield(DATout{p},'F')
        fn=fieldnames(DATout{p}.F);
        fn=setdiff(fn,'which');
        for i=1:length(DATout{p}.F.which)
            iflat=DATout{p}.F.which(i);
            for j=1:length(fn)
                Flat.X.(fn{j})(iflat)=DATout{p}.F.(fn{j})(i);
            end
        end
    end
end
end
%% EOF

function [Flat,to_be_removed]=func_remove_unused_entries(Flat,DATout)

to_be_removed=[];
for p=1:length(DATout)
    if ~isempty(DATout{p}.to_be_removed)
        to_be_removed=[to_be_removed DATout{p}.to_be_removed];
    end
end
if ~isempty(to_be_removed)
    Flat=func_remove_elements(Flat,to_be_removed,0,1);
    Flat.v.sequence=cell(25,Flat.p.num_clust);
end
end
%% EOF


% function slicedDAT=func_help_get_sliced(Flat,DATi,reloadFromMemory,nbr_files)
%
% if reloadFromMemory
%     fprintf('Reloading from memory.\n');
%     for p = 1 : nbr_files
%         for j=1:length(Flat.v.CHANNELS_LOAD)
%             CHANNEL=Flat.v.CHANNELS_LOAD(j);
%             %% XXX shouldn't we clear Flat.DAT.data....??? (to avoid memory
%             %% problems)
%             slicedDAT(p).(['data' num2str(CHANNEL)]) = Flat.DAT.(['data' num2str(CHANNEL)]){DATi(p)}; %#ok<AGROW>
%         end
%     end
% else
%     % all referenced array indices have to be there for a sliced array in a parfor
%     % loop
%     slicedDAT(1 : nbr_files) = NaN;
% end
% end
%% EOF

% reloadFromMemory = true; %true
% for  p = 1 : nbr_files
%     for j=1:length(Flat.DAT.CHANNELS_SAVED(:, DATi(p)))
%             channel=Flat.DAT.CHANNELS_SAVED(j, DATi(p));
%             if channel == 0
%                 % not saved
%             else
%                 if ~isfield(Flat.DAT, ['data' num2str(j)]) || ...
%                         isempty(Flat.DAT.(['data' num2str(j)]))
%                     reloadFromMemory = false;
%                 end
%             end
%     end
% end