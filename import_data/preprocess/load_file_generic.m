
function [data, scanrate, all_return_vars, fileinfo, status] = ...
    load_file_generic(data_path, file_format, selectedSubDir, dd, filename, ...
    meta_file, file_load_mode, PBdir, channel, ...
    header_only, return_pointer, aux)

% generic file loader

    
    if nargin < 12
        aux = [];
    end
    
    % you can request a pointer to the data instead of the data.
    % by default we return the data itself.
    if nargin < 11
        return_pointer = 0;
    end

    if nargin < 10
        header_only = 0;
    end

    if nargin < 9
        channel = -1;
    end    
    
    if nargin < 8
        disp('Usage: ');
        data = []; scanrate = [];  all_return_vars = [];  fileinfo = [];  status = 1; 
        return;
    end

    
    %% PREPARE FOR FILE LOADING
    
    % note that for hdf5 files, the filename is not indicating the
    % recording therefore, we give the function the recording name instead
    % and the filename is changed to its predefined name for now (the name
    % should not be hardcoded :-/)
    if file_format == 12
        rec_id = filename; % this is actually a number
        filename = 'stream.kwd'; % TODO: make this generic
    end
    
    % filename needs to be the full path to the selected file.
    full_filename = fullfile(data_path, selectedSubDir, filename);
    
    
    
    full_filename_meta = [];
    if ~isempty(meta_file)
        full_filename_meta = [data_path selectedSubDir dd meta_file];
    end
    
    % remap PBdir.ind to analyze's "pb_flag"
    pb_flag = 0; %remap_pbdir_ind_to_pb_flag(PBdir);
    
    %%  setup default return variables
    [data, scanrate, all_return_vars] = load_file_analyze_setup_default_return_params();
    fileinfo.timestamp = 0;
    fileinfo.size = 0;
    fileinfo.full_filename = [];

    %%
    d = dir(full_filename);
    
    % easy check whether file exists or not.
    if size(d, 1) == 0
        status = 1;
        disp(['File "' full_filename '" does not exist']);
        return;
    end
    
    fileinfo.timestamp = d.date;
    fileinfo.size = d.bytes;
    fileinfo.full_filename = full_filename;
    fileinfo.full_subdir = fullfile(data_path, selectedSubDir);
    
    %% TRY TO INITIATE A FILE LOADING 
    
    %try
    if 1

        % load a wav file

        if PBdir.ind == 8 || file_format == 0
            [data, scanrate, all_return_vars, status] = load_wav(full_filename);
            return;
        end


        % see variables_and_parameter_description.m for valid "file_format"
        % values
        switch file_format
            case 0
                disp('Error loading file with "load_file_generic.m":');
                disp('file_format = 0 is not supported, only works with WAV files.');
                
            case 1
                [data, scanrate, all_return_vars, status, timestamp] = load_daq(full_filename, full_filename_meta, pb_flag, '', '', channel, header_only, return_pointer);
                if ~isempty(timestamp)
                    fileinfo.timestamp = timestamp;
                end

            case {2, 7}
                [data, scanrate, status] = load_labview_acute(full_filename, 0, channel, header_only, return_pointer);
                % make sure to take correct scanrate
                scanrate = fix_labview_scanrate_acute_setup([], scanrate);

            case 3
                [data, scanrate, status] = load_labview_acute(full_filename, 1, channel, header_only, return_pointer);

            case 4
                [data, scanrate, all_return_vars, status, timestamp] = load_labview_chronic(full_filename, 1, channel, header_only, return_pointer);
                if ~isempty(timestamp)
                    fileinfo.timestamp = timestamp;
                end

            case 5
                [data, scanrate, status] = load_labview_TPLSM(full_filename);

            case 6
                [data, scanrate, status] = load_labview_acute(full_filename, 2, channel, header_only, return_pointer);
                
            case 8
                [data, scanrate, all_return_vars, status] = load_labview_chronic(full_filename, 1, channel, header_only, return_pointer);
                
            case 9
                [data, scanrate, all_return_vars, status] = load_obs3d(full_filename, 0, 0, header_only);
           
            case 10 % Backpack from Linus R�timann and Jan Naef 
                % Flat.p.nfft=384; Flat.p.nonoverlap=96;
                
                [data, scanrate, all_return_vars, status] = load_backpack(full_filename);
              
                % some of Linus data files have the stuff below
                %Sdr.DAQmxChannels=data; 
                Sdr.DAQmxChannelsFs=scanrate;
                Sdr.CarrierFreq=all_return_vars(5).value; Sdr.CarrierFreqFs=5;
                Sdr.ReceiverFreq=0*all_return_vars(5).value; Sdr.ReceiverFreqFs=5;
                Sdr.SignalStrength=all_return_vars(6).value; Sdr.SignalStrengthFs=5;
                %Sdr.ChannelList=readtable([stem 'SdrChannelList.csv']);

                all_return_vars(10).name='Sdr';
                all_return_vars(10).value=Sdr;

                
            case 11 % Intan data
                
                [data, scanrate,all_return_vars,status,timestamp] = ...
                    func_read_intan_RHD2000(full_filename);
                
                if ~isempty(timestamp)
                    fileinfo.timestamp = timestamp;
                end
    
                
                
            case 12 % Open Ephys, hdf5
                
                if channel == -1
                    [data, scanrate, status, timestamp] = ...
                        load_hdf5_data(full_filename, rec_id);
                else
                    [data, scanrate, status, timestamp] = ...
                        load_hdf5_data(full_filename, rec_id, channel);
                end
                if ~isempty(timestamp)
                    fileinfo.timestamp = timestamp;
                end
                
                
            case 13 % Linus new data
                [data,scanrate]=audioread(full_filename);
                if ~isempty(data)
                    status=0;
                end
                
                % rrr hack to remove offset on microphone
%                  [aa,bb] = butter(8, 400/(32000/2), 'high');
%                  data(1,:)=filter(aa,bb,data(1,:));
%                 
                tokens =regexp(filename,'(\S+_)SdrChannels.(\w+)$', 'tokens', 'once');
                stem = tokens{1};
                audioFormat = tokens{2};
                %stem=full_filename(1:end-15);
                [Dq,fsq]=audioread(fullfile(data_path, selectedSubDir,[stem 'DAQmxChannels.' audioFormat]));
                if fsq~=scanrate
                    Dq = resample(Dq, scanrate, fsq);
                    %fsq = scanrate;
                end
                [Dc,fsc]=audioread(fullfile(data_path, selectedSubDir,[stem 'SdrCarrierFreq.' audioFormat]));
                [Dr,fsr]=audioread(fullfile(data_path, selectedSubDir,[stem 'SdrReceiveFreq.' audioFormat]));
                [Ds,fss]=audioread(fullfile(data_path, selectedSubDir,[stem 'SdrSignalStrength.' audioFormat]));

                % correct receiver freq jumps in SdrChannels 
                nSdrChannels = size(Dr,2);
                nSamples = size(data,1);
                tmp = upsample([zeros(1,nSdrChannels); diff(Dr)],scanrate/fsr);
                recf=cumsum(tmp)/120e3;
                recf = recf(1:nSamples,:);
                data = data+recf;
                
                % this part has bad performance (could be improved)
                for i=1:nSdrChannels
                    jumplist = find(tmp(:,i)~=0);
                    for j=1:length(jumplist)
                        data(jumplist(j)-10:jumplist(j)+10,i) = linspace(data(jumplist(j)-10,i),data(jumplist(j)+10,i),21);
                    end
                end
                % end correct
                
                data=data';
                
                Sdr.DAQmxChannels=Dq; Sdr.DAQmxChannelsFs=fsq;
                Sdr.CarrierFreq=single(Dc); Sdr.CarrierFreqFs=fsc;
                Sdr.ReceiverFreq=single(Dr); Sdr.ReceiverFreqFs=fsr;
                Sdr.SignalStrength=single(Ds); Sdr.SignalStrengthFs=fss;
                Sdr.ChannelList=readtable(fullfile(data_path, selectedSubDir,[stem 'SdrChannelList.csv']));
                
                all_return_vars(10).name='Sdr';
                all_return_vars(10).value=Sdr;
            case 3000 % Birdradio 
                if ~isempty(aux)
                    birdradio = aux; 
                else
                    if isempty(getCurrentTask()) %check whether not in parfor
                        birdradio = evalin('base', 'Flat.p.birdradio');
                    else
                        error('load_file_generic executed in parfor with empty aux argument. birdradio variable can not be loaded from base workspace');
                    end
                end
                
                [tdms, ~] = TDMS_getStruct(full_filename,2,{'UTC_DIFF', 0});
                
                groupNames = {tdms.groups.id};
                
                if birdradio.fileVersion == 0
                    daqId = find(strcmpi(groupNames,'Microphone'));
                    radioId = find(strcmpi(groupNames,'Accelerometer'));
                    auxId = find(strcmpi(groupNames,'Digital'));
                    
                    propNames.center_frq = 'center frq';
                    propNames.sender_id = 'sender_id';
                    propNames.sender_color = 'color';
                else
                    daqId = find(strcmpi(groupNames,'DAQChannels'));
                    radioId = find(strcmpi(groupNames,'RadioChannels'));
                    auxId = find(strcmpi(groupNames,'Auxillary'));
                    
                    propNames.center_frq = 'CenterFrequency';
                    propNames.sender_id = 'SenderID';
                    propNames.sender_color = 'SenderColor';
                end
                
                nRadio = birdradio.NradioChannels;
                nDaq = birdradio.NdaqChannels;
                

                
                if nRadio ~= 0
                    nSamplesRadio = length(tdms.groups(radioId).chans(1).data);
                end
                
                if nDaq ~= 0
                    nSamplesDaq = length(tdms.groups(daqId).chans(1).data);
                end
                
                if nRadio ~= 0 && nDaq ~= 0 
                    if nSamplesRadio~=nSamplesDaq
                        warning('%s: Length of radio (%i) and daq (%i) channels don''t match', filename, nSamplesRadio, nSamplesDaq);
                    end
                    nSamples = min(nSamplesRadio,nSamplesDaq);
                elseif nRadio ~= 0
                    nSamples = nSamplesRadio;
                elseif nDaq ~= 0
                    nSamples = nSamplesDaq;
                else
                    nSamples = 0;
                end
                
                if mod(nSamples,birdradio.FsDivider) ~= 0
                    warning('%s: nSamples is not a multiple of %i. It will be rounded.', filename, birdradio.FsDivider);
                    nSamples = round(nSamples/birdradio.FsDivider)*birdradio.FsDivider;
                end
                
                data = zeros(nRadio+nDaq,nSamples);
                 
                Sdr.center_frq = zeros(nRadio,1);
                Sdr.sender_id = cell(nRadio,1);
                Sdr.sender_color = cell(nRadio,1);
                for i=1:nRadio
                    data(i,:)= tdms.groups(radioId).chans(birdradio.whichRadioChannels(i)).data(1:nSamples)*birdradio.gainRadioChannels;
                end
                try

                    for i=1:nRadio
                        iCh = birdradio.whichRadioChannels(i);
                        centerFrqId = find(strcmpi(tdms.groups(radioId).chans(iCh).propNames, propNames.center_frq));
                        Sdr.center_frq(i) = tdms.groups(radioId).chans(iCh).propValues{centerFrqId};
                        
                        sender_idId = find(strcmpi(tdms.groups(radioId).chans(iCh).propNames, propNames.sender_id));
                        Sdr.sender_id{i} = tdms.groups(radioId).chans(iCh).propValues{sender_idId};
                        
                        sender_colorId = find(strcmpi(tdms.groups(radioId).chans(iCh).propNames, propNames.sender_color));
                        clr_int = tdms.groups(radioId).chans(iCh).propValues{sender_colorId};
                        Sdr.sender_color{i} = ['#' dec2hex(clr_int,6)];
                    end

                catch
                    warning('%s: Problem reading metadata. Assigning value [].', filename);
                end
                for i=1:nDaq
                    data(nRadio+i,:)= tdms.groups(daqId).chans(birdradio.whichDaqChannels(i)).data(1:nSamples);
                end
                
                nSamplesFrameCh = nSamples/birdradio.FsDivider;
                Sdr.CarrierFreq = zeros(nSamplesFrameCh,nRadio);
                Sdr.SignalStrength = zeros(nSamplesFrameCh,nRadio);
                
                
                for i=1:nRadio
                    frameSignalsId = find(strcmpi(groupNames,['FrameSignals ' birdradio.radioChanNames{i}]));
                    if isempty(frameSignalsId)
                        error('%s: Could not find FrameSignals for bird %s. Valid group names are: %s', filename, birdradio.radioChanNames{i}, sprintf('%s, ', groupNames{:}));
                    end
                    chanNames = {tdms.groups(frameSignalsId).chans.id};
                    FRQchanId = find(strcmpi(chanNames,'FRQ'));
                    Sdr.CarrierFreq(:,i) = tdms.groups(frameSignalsId).chans(FRQchanId).data(1:nSamplesFrameCh);
                    sigPowChanId = find(strcmpi(chanNames,'PowRM'));
                    Sdr.SignalPower(:,i) = tdms.groups(frameSignalsId).chans(sigPowChanId).data(1:nSamplesFrameCh);
                    noisePowChanId = find(strcmpi(chanNames,'PowNM'));
                    Sdr.NoisePower(:,i) = tdms.groups(frameSignalsId).chans(noisePowChanId).data(1:nSamplesFrameCh);
                       
                    %correct for missalignement of FrameSignals with main
                    %signal
                    Sdr.CarrierFreq(:,i) = [Sdr.CarrierFreq(2:end,i);Sdr.CarrierFreq(end,i)];
                    Sdr.SignalPower(:,i) = [Sdr.SignalPower(2:end,i);Sdr.SignalPower(end,i)];
                    Sdr.NoisePower(:,i) = [Sdr.NoisePower(2:end,i);Sdr.NoisePower(end,i)];
                end
                
                
               
                auxChanNames = {tdms.groups(auxId).chans.id};
                doorChanId = find(strcmpi(auxChanNames,'Door'));
                Sdr.Door = logical(tdms.groups(auxId).chans(doorChanId).data);
                
                if birdradio.fileVersion ~= 0
                    lightChanId = find(strcmpi(auxChanNames,'Light'));
                    Sdr.Light = single(tdms.groups(auxId).chans(lightChanId).data);
                end
                
                rangeId = find(strcmpi(tdms.propNames,'Range__Hz'));
                Sdr.range = tdms.propValues{rangeId};
                    
                all_return_vars(10).name='Sdr';
                all_return_vars(10).value=Sdr;
                
                status = 0;
            case 112 % spike glx data, for microphone and TTL data (Neuropixel)
                
                if channel == -1
                    [meta, data, din] = read_nidqbin(filename, ...
                        fullfile(data_path, selectedSubDir), 1);
                else
                    [meta, data, din] = read_nidqbin(filename, ...
                        fullfile(data_path, selectedSubDir), channel);
                end
                
                fprintf('########### \n LOADED %s \n ################# \n', filename);

                fprintf(' adding digital channels \n');
                data = [data; double(din)]; 
                
                scanrate = str2double(meta.niSampRate);
                fileinfo.timestamp = datenum(strrep(meta.fileCreateTime, 'T', ' '));
                status = 0;
                
                
                %% old labview feelab acute
            case 69
                status=0;
                fi=fopen(full_filename,'r','ieee-be');
                hdrlen=fread(fi,1,'long');
                
                datleng1=fread(fi,1,'long');
                chlist=fread(fi,datleng1,'ubit8');
                
                datleng=fread(fi,1,'long');
                
                nchannels=fread(fi,1,'long');
                fprintf('number of channels: %d\n',nchannels);
                
                scale=zeros(nchannels,1);
                for i=1:nchannels
                    strleng=fread(fi,1,'long');
                    str=fread(fi,strleng,'ubit8');
                    
                    a=fread(fi,22,'ubit8');
                    scale(i)=fread(fi,1,'float');
                    a=fread(fi,4,'ubit8');
                end
                scanrate=fread(fi,1,'float');
                
                ch_clock=fread(fi,1,'float');
                bytes_so_far=4+datleng1+8+nchannels*(4+strleng+30)+8;
                user_string_len=hdrlen-bytes_so_far;
                user_string='';
                if (user_string_len~=0)
                    fread(fi,user_string_len,'uchar');
                end
                fprintf('%s\n',user_string);
                data=double(fread(fi,[nchannels inf],'short'));
                for i=1:nchannels
                    data(i,:)=-data(i,:)*scale(i);
                end
                
            otherwise
                disp('Error loading file with "load_file_generic.m":');
                disp(['This file format ' num2str(file_format) ' is not supported - maybe you have choosen the wrong file format and file filter option.']);
                status = 1;
        end

        %% needs to be fixed! file_load_mode 0 = int16, 1 = double
        % file_load_mode    1 = native format
        %                   0 = int16
        
        % make sure to use [] brackets around this, otherwise this step is
        % not safe for the empty case
        dyn_range_value = [all_return_vars(strcmpi({all_return_vars.name}, 'dynamic_range')).value];
        if file_load_mode == 0
            if isempty(dyn_range_value)
                disp('Can not convert data to INT16, because no dynamic range is given.');
            else
                if return_pointer == 0
                    data = int16((data ./ dyn_range_value) .* double(intmax('int16')));
                else
                    data.Value = int16((data.Value ./ dyn_range_value) .* double(intmax('int16')));
                end
            end
        end


        % check for strange scanrates
        if file_format ~= 12 && file_format ~= 112 && file_format ~= 3000
            
            rounded_scanrate = round(scanrate);
            
            if (rounded_scanrate - scanrate)  ~= 0
                scanrate = rounded_scanrate;
                disp(['Your scanrate was funny, I rounded it to: ' num2str(scanrate)]);
            end
        end
        
    %%  IN CASE SOMETHING WENT WRONG - this should never happen, because
    % each file loading function has it's own try / catch.
    else
  %      catch me
        disp(['An error occured inside : ' mfilename ]);
        disp(me.message)
                
        disp(' ');
        disp('failed loading: ');
        disp(full_filename);
        disp(['File format: ' num2str(file_format)]);
        status = 1;
    end
    
end


%% EOF