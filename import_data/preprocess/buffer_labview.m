function buffered_data = buffer_labview(vector, buffersize, overlap)

% buffer_labview buffers a signal vector into a matrix of data frames.
%    Y = buffer_labview(X,N) partitions signal vector X into nonoverlapping data
%    segments (frames) of length N.  Each data frame occupies one
%    column in the output matrix, resulting in a matrix with N rows.
%
%    Y = buffer_labview(X,N,P) specifies an integer value P which controls the amount
%    of overlap or underlap in the buffered data frames.
%    - If P>0, there will be P samples of data from the end of one frame
%      (column) that will be repeated at the start of the next data frame.
%    - If P<0, the buffering operation will skip P samples of data after each
%      frame, effectively skipping over data in X, and thus reducing the
%      buffer "frame rate".
%    - If empty or omitted, P is assumed to be zero (no overlap or underlap).

buffered_data = [];

if nargin < 2
    disp('specify vector and buffersize')
    return
end
% if overlap is not specified, set to 0
if nargin < 3
    overlap = 0;
end
non_overlap = buffersize - overlap;

vectorsize = size(vector);

% make sure that vector is a row vector
if vectorsize(1) < vectorsize(2)
    vector = vector';
    vectorsize = size(vector);
end

if buffersize > vectorsize(1)
    disp('buffersize should be smaller than the size of the input vector.')
    return
end

% calculate the number of buffers
number_of_buffers = floor((vectorsize(1)-buffersize)/non_overlap)+1;

%initialize bufferd_data array
%buffered_data = gpuArray(zeros(buffersize, number_of_buffers));
buffered_data = zeros(buffersize, number_of_buffers);

for count = 0:number_of_buffers-1
    buffered_data(:,count+1) = ...
        vector(count*non_overlap+1:count*non_overlap+buffersize);
end
end
