function parameters = pack_variables(variable_list)
% packs a whole bunch of variables and puts them into one structure

        % get rid of unassigned variables
        variable_list(find(cellfun(@(x) ~isempty(x), (strfind({variable_list.class}, 'unassigned'))))) = []; %#ok<FNDSB>

        % how many variables are left?
        how_many_variables = size(variable_list, 1);

        % preallocate the structure
        parameters = struct('name', cell(1, how_many_variables) , 'value', cell(1, how_many_variables));
        
        for i = 1 : how_many_variables
            parameters(i).name = variable_list(i).name;
            parameters(i).value = evalin('caller', variable_list(i).name);         
        end
        
end

%% EOF
