function unpack_variables(parameters)
% Unpacks a whole bunch of variables from a parameters structure
% You can only use this function if the 'caller' function contains no
% nested functions!

        how_many_variables = size(parameters, 2);
   
        for i = 1 : how_many_variables
            assignin('caller', parameters(i).name, parameters(i).value);
        end
        
end

%% EOF
