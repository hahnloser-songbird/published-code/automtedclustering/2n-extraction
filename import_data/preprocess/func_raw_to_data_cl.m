function y = func_raw_to_data_cl(hh, sparseFlat, is_int,spec_log_min)
%%
% hh is the raw microphone signal

if nargin<5
    spec_lin=0;
end
ham = hamming(sparseFlat.p.nfft);
%     buff = buffer(hh, sparseFlat.p.nfft, sparseFlat.p.nfft - sparseFlat.p.nonoverlap, 'nodelay');
buff = buffer_labview(sparseFlat.p.spec_raw_scale*hh, sparseFlat.p.nfft, sparseFlat.p.nfft - sparseFlat.p.nonoverlap);
%buff=gpuArray(buff); ham=gpuArray(ham);
if isnan(spec_log_min)
    hh2 = abs(fft(buff.*(ham*ones(1, size(buff, 2)))));
else
    hh2 = log(spec_log_min + abs(fft(buff.*(ham*ones(1, size(buff, 2))))));
end
%     hh3=abs(fft2(hh2)); %hh2=4*hh3/max(max(hh3))*max(max(hh2));
%     hh2=log(.1+spec_log_min);
%hh2=gather(hh2);
numbuffs = func_numbuffs(length(hh), sparseFlat.p.nfft, sparseFlat.p.nonoverlap);

switch is_int
    case 1
        y = int8(reshape(hh2(sparseFlat.p.nfft_i,:), length(sparseFlat.p.nfft_i)*numbuffs,1)*128/ sparseFlat.dynamic_range_spec);
        
    case 2
        y = int8(hh2(sparseFlat.p.nfft_i,:)*128/ sparseFlat.dynamic_range_spec);
        
    otherwise
        %size(hh2(sparseFlat.p.nfft_i,:))
        %numbuffs
        
        % dirty fix for stupid bug where func_numbuffs above returns the
        % wrong number of buffers (I think it should use ceil instead of
        % floor). AK.
        if numel(hh2(sparseFlat.p.nfft_i,:)) ~= length(sparseFlat.p.nfft_i)*numbuffs
            y = reshape(hh2(sparseFlat.p.nfft_i,:), numel(hh2(sparseFlat.p.nfft_i,:)), 1);
        else
            y = reshape(hh2(sparseFlat.p.nfft_i,:), length(sparseFlat.p.nfft_i)*numbuffs, 1);
        end
        
        % y = reshape(hh2(sparseFlat.p.nfft_i,:),[],1);
end

end
%% EOF


