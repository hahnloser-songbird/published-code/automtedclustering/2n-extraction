%function DATout=func_get_data_new_channels2_cl(sparseFlat, sd, fn, data, fni, elements_i)
function DATout=func_get_data_new_channels2_cl(sparseFlat, data,gains, fni,reloadFromMemory,ALL_CHANNELS)
% This function gets sparseFlat where
%
%
% parallel version
DATout.dummy=[];

%for j=1:length(sparseFlat.v.CHANNELS_LOAD)
for j=1:length(ALL_CHANNELS)
    % CHANNEL=sparseFlat.v.CHANNELS_LOAD(j);
    CHANNEL=ALL_CHANNELS(j);
    channel=sparseFlat.DAT.CHANNELS_SAVED(CHANNEL,fni);
    if channel>0
        if reloadFromMemory
            dat=data{CHANNEL};
        else
            dat=data(channel,:);
        end
    else
        dat=zeros(1,size(data,2));
    end
    gain=double(gains(CHANNEL));
    
    if channel==0
        fprintf('Channel %s not saved\n',sparseFlat.p.Channel_names{j});
        continue
    end
    
    % filter data if applicable
    if sparseFlat.v.import.electrode && ~reloadFromMemory
        if isfield(sparseFlat.p, 'CHANNELS_FILTER') && ~isempty(sparseFlat.p.CHANNELS_FILTER) && ismember(CHANNEL,sparseFlat.p.CHANNELS_FILTER)
         if sparseFlat.p.channels_filter_type==1   
             dat=filter(sparseFlat.v.DATfilt.aa, sparseFlat.v.DATfilt.bb, -dat);
         %% OLD:   dat=ftfil(-dat,sparseFlat.scanrate,sparseFlat.p.DATfilt_highpass,8000);
         elseif sparseFlat.p.channels_filter_type==2  
             dat=ftfil(-dat,sparseFlat.scanrate,300,10000);
         end
        end
    end
    
    %% store in DAT -- memory intense !
    if ~reloadFromMemory &&  any(sparseFlat.p.CHANNELS_KEEP_RAW==CHANNEL)
        dat_str=['data' num2str(CHANNEL)];
        DATout.(dat_str)=int16(2^15/sparseFlat.dynamic_range*dat);
    end
    curr_dat=int16(2^15/sparseFlat.dynamic_range*dat);
    
    %% run plugin on DAT
    if any(sparseFlat.v.import.plugin_data) && ismember(CHANNEL,sparseFlat.p.CHANNELS_PLUGIN_DATA)%% any(sparseFlat.p.CHANNELS_PLUGIN_DATA==CHANNEL)
        for k=find(sparseFlat.p.CHANNELS_PLUGIN_DATA==CHANNEL & sparseFlat.v.import.plugin_data)
       %for k=find(sparseFlat.v.import.plugin_data) 
       plugin_trace=sparseFlat.DAT.curr_plug{k}.transform_function(dat);%,sparseFlat.p.nfft,sparseFlat.p.DATplugin_nonoverlap,sparseFlat.p.nfft);
            % ind=sparseFlat.v.stack.plugin(CHANNEL);
            ind=sparseFlat.v.stack.plugin(k);
            ma=max(plugin_trace);
            if ma>sparseFlat.DAT.stack(ind).dynamic_range(2)
                sparseFlat.DAT.stack(ind).dynamic_range(2)=ma;
            end
        mi=min(plugin_trace);
            if mi<sparseFlat.DAT.stack(ind).dynamic_range(1)
                sparseFlat.DAT.stack(ind).dynamic_range(1)=mi;
            end
          
            %   DATout.(['plugin' num2str(CHANNEL)])=int16(plugin_trace/max(abs(sparseFlat.DAT.stack(ind).dynamic_range))*2^15);
            DATout.(['plugin' num2str(k)])=int16(plugin_trace/max(abs(sparseFlat.DAT.stack(ind).dynamic_range))*2^15);
        end
    end
    %   figure;plot(curr_dat);pause
    %% run compression and extract peaks
    %   if sparseFlat.v.import.electrode && isfield(sparseFlat.v, 'ELECTRODE_CHANNELS') && ~isempty(sparseFlat.p.CHANNELS_ELECTRODES)...
    if sparseFlat.v.import.electrode &&  ~isempty(sparseFlat.p.CHANNELS_ELECTRODES) && ismember(CHANNEL,sparseFlat.p.CHANNELS_ELECTRODES);
        [DATout.(sparseFlat.v.peak_str{CHANNEL}) DATout.(sparseFlat.v.loc_str{CHANNEL})]=func_multiunit_compress(...
            curr_dat,sparseFlat.dynamic_range,gain,sparseFlat.p.DATthresh_low_lim(CHANNEL));
        DATout.(sparseFlat.v.cross_str{CHANNEL})=func_multiunit_crossings(DATout.(sparseFlat.v.peak_str{CHANNEL}),...
            sparseFlat.dynamic_range,gain,DATout.(sparseFlat.v.loc_str{CHANNEL}),sparseFlat.DAT.thresh(CHANNEL,fni));
    end
    
end
end
%% EOF