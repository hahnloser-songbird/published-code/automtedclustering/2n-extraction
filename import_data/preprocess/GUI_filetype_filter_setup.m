function PBdir = GUI_filetype_filter_setup()
% extended version of setup_file_list_filter.m

    PBdir.names = {'Directed', 'Undirected', 'Tutor', ...
        'Forward', 'Reverse', 'Conspecific', 'Show All', 'WAV', ...
        'TIFF', 'All Chronic', 'TagBoard', 'FileBrowser', 'SSS'};
    
    PBdir.label = {'f', 'f', 'T', ...
        'F', 'R', 'C', 'A', 'W', ...
        'T', 'Ch', 'TagBoard', 'FileBrowser', 'SSS'};
    
    PBdir.songtype = {'D', 'U', 'B', ...
        'B', 'B', 'B', '*', '*.wav', ...
        '*.tiff', '*', '*', '*', '*'};
  
    % which entry is a "master" entry, ie, bring different types
    % together? ('All Chronic' qualifies as master)
    PBdir.is_master = [0 0 0, ...
        0 0 0 0 0, ...
        1 0 0 0 0];
    
    %% categories
    PBdir.belongs_to_category = [1 1 1, ...
        1 1 1 2 3, ...
        3 1 2 2 2];

    % what are the rough categories? 
    % we distinguish between chronic & acute data, as well as files that
    % are sorted by file extension (tiff, wav)
    % acute can be anything.
    PBdir.categories = {'chronic', 'acute', 'extension'};    
    
    %%
    % what is the currently selected entry?
    PBdir.ind = 7;
    PBdir.prev = 7;    
    
end
%% EOF
