function [data, scanrate, all_return_vars] = load_file_analyze_setup_default_return_params(yA_str, yA_id)

    if nargin == 0
        yA_str = [];
        yA_id = [];
    end

    data = [];
    scanrate = [];
    aiA = [];
    Nplot_id = [];
    micro_iA = [];
    speaker_input_iA = [];
    filename_userdataA = [];
    go_on = -1;

    %% put everything together
    all_return_vars = load_files_pack_wrapper(aiA, go_on, Nplot_id, micro_iA, speaker_input_iA, '', filename_userdataA, yA_str, yA_id);
    
end
%% EOF