function [data, scanrate, all_return_vars, status] = load_wav(filename)
% [data, scanrate, status, go_on, micro_iA, speaker_input_iA, yA_str, yA_id, Nplot_id] = load_wav(filename)
% loads wav file and returns all parameters as might be needed for analyze

    %% setup default parameters needed for analyze

    [data, scanrate, all_return_vars] = load_file_analyze_setup_default_return_params();
    unpack_variables(all_return_vars);

    %% try to load data file
    try
        [data, scanrate] = audioread(filename);
        data = data';
        status = 0;
        
        % parameters needed for analyze
        go_on = 1;
        Nplot_id = 3 + size(data,1);
        micro_iA = 1;

    catch me
        status = load_file_failed(mfilename, me.stack, me, fi);
    end
    
    %% pack all remaining variables
    [all_return_vars] = load_files_pack_wrapper(aiA, go_on, Nplot_id, micro_iA, speaker_input_iA, filename, filename_userdataA, yA_str, yA_id);
    
end
%% EOF