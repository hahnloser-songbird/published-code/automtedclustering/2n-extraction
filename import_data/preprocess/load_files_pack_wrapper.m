function all_return_vars = load_files_pack_wrapper(aiA, go_on, Nplot_id, micro_iA, speaker_input_iA, filenameA, filename_userdataA, yA_str, yA_id, dynamic_range, numchannels) %#ok<INUSD>
% wrapper function to isolate the file loading workspace in order to pack
% variables more efficiently.
% this function is needed whenever we want to load files and variables for
% analyze
%
% !! DO NOT CHANGE THE NAMES OF THE INPUT VARIABLES!!
%
%
% packs all additional variables into a structure.
% use 'unpack_variables' to get them back.
    
    % count how many variables we have and put them all into one
    % 'parameters' structure
    all_variables = whos;
    all_return_vars = pack_variables(all_variables);

end
%% EOF