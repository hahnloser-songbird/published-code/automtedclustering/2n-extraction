function status = load_file_failed(crash_file, entire_stack, me, fi)

    %% close file handle if one is open - something bad might have
    % happend before
    if isfi(fi)
        fclose(fi);
    end

    %% inform user about the error:
    disp(' ');
    disp(' ');    
    disp('############# FILE LOADING ERROR ################### ');
    disp(' ');
    custom_message = ['File loading failed at the following step: ' crash_file ];
    custom_db_stack(me.message, entire_stack, crash_file, custom_message, me.identifier);
    disp('############# FILE LOADING ERROR  - END ############ ');    
    disp(' ');
    disp(' ');
    %% indicate the crash
    status = 1;
    
end
%% EOF