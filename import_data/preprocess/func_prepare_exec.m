function [exec_flat,exec_dat,do_cell,exec_fname,fh]=func_prepare_exec(Flat)


exec_flat=0; exec_dat=0;
do_cell=0; exec_fname=''; fh=[];


if ~isempty(Flat.p.exec_get_fcn)
    if regexp(Flat.p.exec_get_fcn,'{i}')
        do_cell=1;
    else
        do_cell=0;
    end
    
    if findstr(Flat.p.exec_get_fcn,'.DAT')
        exec_dat=1;
        exec_fname=regexp(Flat.p.exec_get_fcn,'Flat.DAT.([\w-+\_]+)[\(\{]','tokens');
        exec_fname=exec_fname{1}{1};
        if ~isfield(Flat.DAT,exec_fname)
            Flat.DAT.(exec_fname)=cell(size(Flat.DAT.filename));
        end
    else
        exec_flat=1;
        exec_fname=regexp(Flat.p.exec_get_fcn,'Flat.([\w-+\_]+)[\(\{]','tokens');
        exec_fname=exec_fname{1}{1};
        if ~isfield(Flat,exec_fname)
            if do_cell
                Flat.(exec_fname)=cell(1,Flat.num_Tags);
            else
                Flat.(exec_fname)=zeros(1,Flat.num_Tags);
            end
        end
        
    end
    fcn=regexp(Flat.p.exec_get_fcn,'=(.*)\(','tokens');
    %  fh=evalc(['@' fcn{1}{1}]);
   % eval(['fh=@' fcn{1}{1} ';']);
    fh=str2func(fcn{1}{1});
end

