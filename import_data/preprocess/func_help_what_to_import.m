
function [import,ok]=func_help_what_to_import(Flat,what_to_import_string)

if nargin<2
    do_regular = true;
else
    do_regular = false;
end
    
import=func_help_import_define;
ok=true;
if Flat.num_Tags==0
    return
end
prompt_string = {['clustspec (' Flat.p.Channel_names{Flat.p.CHANNEL_MIC} ')'],['fullspec (' Flat.p.Channel_names{Flat.p.CHANNEL_MIC} ')'],'new elements (PluginConfigTemplate)',['Sound Analysis Matlab (' Flat.p.Channel_names{Flat.p.CHANNEL_MIC} ')']};

%% SAM
key_plugin_sam=4;


%% electrode
key_electrode=-1;
if ~isempty(Flat.p.CHANNELS_ELECTRODES)
prompt_string{end+1}='multiunit (';
    for i=1:length(Flat.p.CHANNELS_ELECTRODES)
        prompt_string{end}=[prompt_string{end} ' ' Flat.p.Channel_names{Flat.p.CHANNELS_ELECTRODES(i)}];
    end
    prompt_string{end}=[prompt_string{end} ')'];
    key_electrode=length(prompt_string);
end


%% plugin data
key_plugin_data=-1;
if isfield(Flat.p, 'DATplugin') && ~isempty(Flat.p.DATplugin) && ~isempty(Flat.p.CHANNELS_PLUGIN_DATA) && any(Flat.p.CHANNELS_PLUGIN_DATA)
   which=find(Flat.p.CHANNELS_PLUGIN_DATA);
   for j=which
    prompt_string{end+1}=['plugin_data (' Flat.p.DATplugin{j} ') on' ];
   % for i=1:length(Flat.p.CHANNELS_PLUGIN_DATA)
        prompt_string{end}=[prompt_string{end} ' ' Flat.p.Channel_names{Flat.p.CHANNELS_PLUGIN_DATA(j)}];
   % end
    key_plugin_data(j)=length(prompt_string);
   end
    prompt_string{end}=[prompt_string{end} ')'];
end


prompt_string={prompt_string{:},'dummy'};
if do_regular
    [answer, v] = listdlg('PromptString','What do you want to import:','SelectionMode','multiple','ListSize',[460,200],'ListString', prompt_string);
else
    [dummy answer] = ismember({what_to_import_string},prompt_string);
end
drawnow;    % Bugfix to prevent hanging on Windows; Important;

if isempty(answer)
    ok=false;
    return
end
import.clustspec=any(answer==1);
import.fullspec=any(answer==2);
import.plugin_import=any(answer==3);
import.electrode=any(answer==key_electrode);
%import.plugin_data=any(answer==key_plugin_data);
h=ismember(key_plugin_data,answer);
import.plugin_data(h)=1;
import.sam=any(answer==key_plugin_sam);
import.dummy=(answer==length(prompt_string));

if import.plugin_import==1 && any(Flat.X.clust_ID==Flat.p.num_clust)
    fprintf('Last cluster non empty - not importing !\n');
    ok=false;
    import.plugin_import=0;
end

end
%% EOF