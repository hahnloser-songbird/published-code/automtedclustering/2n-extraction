function Flat=func_finalize_make_compatibe(Flat)

%% sort by TIME !!!
% Flat=func_reorder_by_time_and_trimm2(Flat);
Flat=func_reorder_by_time(Flat);

%% make table in BOARDS compatible

Flat.BOARD=func_make_table_compatible(Flat.BOARD,Flat.name);
for j=1:length(Flat.BOARDLIST)
    Flat.BOARDLIST(j)=func_make_table_compatible(Flat.BOARDLIST(j),Flat.name);
end

%    Flat=func_tag_master_and_slave(Flat);
Flat=func_check_num_clust_consistency(Flat);
end

function BOARD=func_make_table_compatible(BOARD,archivename)
tables=func_help_get_tables_from_BOARD(BOARD);
for j=1:length(tables)
    BOARD.(tables{j})=func_define_table(archivename,BOARD.(tables{j}));
end
end
