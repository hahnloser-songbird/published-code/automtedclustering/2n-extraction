function Flat=func_help_tags_per_file_to_DAT(Flat)
if Flat.num_Tags==0
    return
end
disp('now writing Tags per file to DAT');
%keyboard
l = get_number_of_recordings(Flat);
tags_i=find(Flat.p.tagnames_per_file);
ln=length(Flat.p.tagnames);
Flat.DAT.Tags=cell(ln,l);
Flat.DAT.Tags(:)={''};
for i=1:l
    hi=find(Flat.X.DATindex==i);
%     if isempty(hi)
%         disp('problem with file tag ');
%  %       keyboard
%     end
    for j=1:length(tags_i)
        u=unique(Flat.Tags(tags_i(j),hi));
        if length(u)==1
         %   hi2=find(Flat.X.DATindex(Flat.num_Tags+1:end)==i);
            Flat.DAT.Tags(tags_i(j),i)=u;
%             if tags_i(j)==14 && ~strcmp(u{1}(1:2),'20')
%                 keyboard
%             end
        elseif length(u)>1
            fprintf('Warning: File tag has multiple strings in a single file\n');
%            keyboard
        end
    end
end
end