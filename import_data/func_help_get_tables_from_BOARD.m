function     tables=func_help_get_tables_from_BOARD(BOARD)

fn=fieldnames(BOARD);
tables=regexp(fn,'(table\_\w*)','tokens');
tables=[tables{:}];
tables=[tables{:}];
% if isempty(tables) && ~isempty(BOARD.tagboard) && any(~cellfun(@isempty,BOARD.tagboard))% any([BOARD.tagboard{:}])
%     disp('No table defined')
%     return
% end