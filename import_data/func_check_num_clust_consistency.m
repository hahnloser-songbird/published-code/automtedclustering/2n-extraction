function Flat=func_check_num_clust_consistency(Flat)

%% check spec
if Flat.p.num_clust>length(Flat.spec.samp_left{1})
    ns=length(Flat.spec.samp_left{1});
    for j=ns+1:Flat.p.num_clust
        for k=1:5
            Flat.spec.samp_left{k}(j)=Flat.spec.samp_left{k}(ns);
            Flat.spec.samp_right{k}(j)=Flat.spec.samp_right{k}(ns);
            Flat.spec.numbuffs{k}(j)=Flat.spec.numbuffs{k}(ns);
            Flat.spec.firstbuff{k}(j)=Flat.spec.firstbuff{k}(ns);
        end
    end
end


%% check template
if Flat.p.num_clust>length(Flat.templates)
    for j=length(Flat.templates):Flat.p.num_clust
        Flat.templates(j)=0;
    end
end

%% check cluster and neighbor sequence
if Flat.p.num_clust>size(Flat.v.sequence,2)
    for j=size(Flat.v.sequence,2):Flat.p.num_clust
        Flat.v.sequence(:,j)=cell(size(Flat.v.sequence,1),1);
    end
end

%% check clust_names
if Flat.p.num_clust>length(Flat.p.clust_names)    
    for j=length(Flat.p.clust_names)+1:Flat.p.num_clust
        Flat.p.clust_names{j}=['Cluster ' num2str(j)];
    end
end

l=length(Flat.v.DATspec);
if l<Flat.p.num_clust
    Flat.v.DATspec(end+1:Flat.p.num_clust)=cell(1,Flat.p.num_clust-l);
end

%% check clust ID
Flat.X.clust_ID=min(Flat.X.clust_ID,Flat.p.num_clust);



