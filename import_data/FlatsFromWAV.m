function [Flats, cfg]=FlatsFromWAV(birdname, subdirs_todo, scanrate, dynamic_range)
% files should be in subfolders starting with '20' inside 'Data' folder,
% if this is not the case, then this format is imposed


if nargin == 2
    scanrate = 20000;
    dynamic_range = 5;
elseif nargin ==3
    dynamic_range = 5;
end

do_human=true;
sort_numerically=0;
run_init_variables = 1;

%%
if do_human %only for visualizing spectograms, find ideal for human
    nfft_human = 512; % corresponds to 4 ms with a sample rate of 32kHz
    nonoverlap_human = nfft_human/4;% 1/4-th of nfft
    N=128;
    nfft_i_human = 1:N;
    spec_log_min_human= 0.1;
end

%% user fields
if run_init_variables == 1
    [paths, home_dir] = getPaths;
    
    birdname = home_dir{end-2};
    
    assignin('base', 'paths', paths);
    assignin('base', 'birdname', birdname);
    
    v = sscanf(version, '%d.%d.%d');
    cfg.matlab_version =  [1 0.01 0.001] * v;
    [~, hn] = system('hostname');
    cfg.hostname = hn(1:end-1);
    cfg.birdname = birdname;
    cfg.birthday = 730486;
    cfg.file_format = 0;    % important for load_file_generic
    cfg.userID = 0;
    assignin('base','cfg', cfg);
else
    cfg=evalin('base','cfg');
end


[Flat,Flats]=func_default_Flat_values(0);
Flats=func_make_compatible(Flats);

dd=filesep;


if isempty(subdirs_todo)
    disp('load all subdirectories')
    d=dir(paths.load_path_default);
    d = d(3:end);
    subdirs_todo={d([d.isdir]).name};
end

if sort_numerically
    snum=str2num(str2mat(subdirs_todo));
    [snumS,snumi]=sort(snum,'ascend');
    subdirs_todo=subdirs_todo(snumi);
end



%% note: subdirs must be in Data folder
for curr_arch=2:length(subdirs_todo)+1
    
    subdir=subdirs_todo{curr_arch-1};
    
    d=dir([paths.load_path_default dd subdir dd '*.wav']);
    if isempty(d)
        warning('data folder does not contain wav files - return')
        return
    end
    
    files={d(1:end).name};
    l=length(files);
    timestamps=[d(1:end).datenum];
    
    %% generic
    subdirs=cell(1,l); subdirs(:)={subdir};
    
    for i=1:length(Flats) %%% why loop over all Flats inside a loop over all Flats?
        [Flats{i}.dynamic_range, Flats{i}.dynamic_range_spec] = ...
            func_get_dynamic_range(Flat,[],5,Flats{i}.p.spec_log_min);
    end
    
    %curr_arch=2;
    Flats{curr_arch}.name=subdir;
    Flats{curr_arch}.DAT.subdir=subdirs;
    Flats{curr_arch}.DAT.filename=files;
    Flats{curr_arch}.DAT.timestamp=timestamps; % this is a hack!
    Flats{curr_arch}.DAT.eof=zeros(size(files));
    Flats{curr_arch}=func_append_zeros_meta(Flats{curr_arch},0,l);
    
    Flats{curr_arch}.X.indices_all(:)=8*Flats{curr_arch}.p.nfft;
    Flats{curr_arch}.scanrate=scanrate;
    Flats{curr_arch}.birdname=birdname;
    Flats{curr_arch}.X.clust_ID(:)=1;
    Flats{curr_arch}.X.DATindex=1:length(files);
    Flats{curr_arch}.num_Tags=l;
    
    Flats{curr_arch}.p.CHANNEL_MIC(1)=1;
    
    if do_human
        Flats{curr_arch}.p.nfft_i=nfft_i_human;
        Flats{curr_arch}.p.nfft=nfft_human;
        Flats{curr_arch}.p.nonoverlap=nonoverlap_human;
        Flats{curr_arch}.p.spec_log_min=spec_log_min_human;
    end
    
    Flats{curr_arch}.p.CHANNELS_PLUGIN_DATA(1:2)=1;
    
    Flats{curr_arch}.DAT.CHANNELS_SAVED(1,1:length(files))=1;
    Flats{curr_arch}.DAT.GAIN(1,1:length(files))=50;
    
    Flats{curr_arch}.REC.birdname{curr_arch}=birdname;
    
    Flats{curr_arch}=func_reorder_by_time(Flats{curr_arch});
    
    % flag all files with the activator Tag set to 98
    Flats{curr_arch}.p.tagnames{end+1}='activator';
    Flats{curr_arch}.p.tagnames_per_file(end+1)=0;
    Flats{curr_arch}.Tags(end+1,:)={'98'};
    Flats{curr_arch}.v.tags.activator=length(Flats{curr_arch}.p.tagnames);
    
    
end
% Flats=func_make_compatible(Flats);
end

% The DIR or LS functions in MATLAB sort the strings in ASCII dictionary order. This can cause a problem if you want to sort numbered files which do not have leading zeros.
% http://www.mathworks.com/matlabcentral/answers/102864-why-do-the-dir-and-ls-functions-in-matlab-not-sort-numerical-filenames-in-numerical-order
% @Anja: function to bw writen for general windows explorer sorting
function [sortedfiles] = sortfiles(files)
fprintf('check sorting\n');

str = cell(length(files),1);
for jj = 1 :length(files)
    str{jj} = strrep(files(jj).name,'_','#');
end
[strsorted, pos] = sort(str);
if any(diff(pos)<0)
    fprintf('resorted!\n');
end

sortedfiles = files(pos);
end
