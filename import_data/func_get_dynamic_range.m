function [dynamic_range dynamic_range_spec]=func_get_dynamic_range(Flat,output_data,dynamic_range,spec_log_min)
% this is a bit of a hack
if nargin<2
    dynamic_range=5;
elseif nargin<3
    dynamic_range=output_data(1).all_return_vars(3).value;
    if isempty(dynamic_range)
        dynamic_range=5; % fix for very old matlab files
    end
end
if nargin<4
    spec_log_min = 0.1;
end
dynamic_range=max(dynamic_range);
ham=hamming(Flat.p.nfft);

hh=dynamic_range*sin((1:(Flat.spec.samp_left{5}(1)+Flat.spec.samp_right{5}(1)))/Flat.p.nfft*4*pi);
buff = buffer(hh,Flat.p.nfft,Flat.p.nfft-Flat.p.nonoverlap,'nodelay');
if isnan(spec_log_min)
hh2 = abs(fft(buff.*(ham*ones(1,size(buff,2)))));
else
hh2 = log(spec_log_min+abs(fft(buff.*(ham*ones(1,size(buff,2))))));
end
dynamic_range_spec=max(abs(min(min(hh2))),max(max(hh2)));
