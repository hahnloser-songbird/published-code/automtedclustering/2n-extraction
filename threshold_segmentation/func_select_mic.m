function  mic_select=func_select_mic(Flat,channel_mic_display,also_Short)
% select microphone elements 
if nargin < 3
    also_Short = 0;
end
if nargin<2
    s=Flat.v.RMS;
    if ~strcmp(Flat.v.RMS,Flat.p.Channel_syl_tagstring{Flat.p.channel_mic_display})
        disp('Something is wrong with the microphone selection');
        keyboard
    end
elseif isfield(Flat.p, 'Channel_syl_tagstring')
    s=Flat.p.Channel_syl_tagstring{channel_mic_display};
else
    s = 'RMSfilt';
end
    %if length(Flat.p.CHANNEL_MIC)>1
%    [Flat,mic_select]=func_select_tagboard(Flat,[],Flat.p.tagnames{Flat.v.tags.activator},{s,'RMSfilt',['D' s], [s 'S'],['D' s 'S']});
if also_Short ==1  
    [Flat,mic_select]=func_select_tagboard(Flat,[],Flat.p.tagnames{Flat.v.tags.activator},{s,[s,'S']});
else
    [Flat,mic_select]=func_select_tagboard(Flat,[],Flat.p.tagnames{Flat.v.tags.activator},{s});
end
%    [Flat,mic_select]=func_select_tagboard(Flat,[],Flat.p.tagnames{Flat.v.tags.activatorM},{s,[s 'S']});
%else
%     [Flat,mic_select]=func_select_tagboard(Flat,[],Flat.p.tagnames{Flat.v.tags.activator},{Flat.v.RMS});
%end
end