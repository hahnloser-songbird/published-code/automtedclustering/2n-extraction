function evalkey_import_el_from_DAT(src, evnt)
modifier = evnt.Modifier;

%disp('hi')
Flat = evalin('base', 'Flat');
fcH = evalin('base', 'fcH');

S=Flat.v.import_DAT;
P=Flat.p.syllable;
% keypress skype

switch evnt.Key
    
    
    %$  Compute threshold based on baackground noise.$%
    %*  *%
    case 'g'
        Flat=func_DAT_threshold(Flat,P);
        [Flat,fcH]=func_import_elements_from_DAT(Flat,fcH);
        
        %$  Increase smoothing window (fewer syllables).$%
        %*  *%
    case 'equal'
        if isempty(modifier)
            Flat.p.syllable.(P.method)(P.X(P.imicsN).imics).smoothingWidth=Flat.p.syllable.(P.method)(P.X(P.imicsN).imics).smoothingWidth+1;
            
            %$  Zoom in (Sdr STACK).$%
            %* Works only with Linus' latest data format*%
        elseif strcmp(modifier,'shift')
            % if isfield(Flat.p,'SdrNchannels')
            if isfield(Flat.v,'DATstack') & Flat.v.DATstack>0
                if isempty(Flat.DAT.stack(Flat.v.DATstack).comment)
                    Flat.DAT.stack(Flat.v.DATstack).comment=1;
                else
                    Flat.DAT.stack(Flat.v.DATstack).comment=Flat.DAT.stack(Flat.v.DATstack).comment*1.2;
                end
            end
            % Flat.p.SdrStackMultiplier=Flat.p.SdrStackMultiplier*1.1;
        end
        [Flat,fcH]=func_import_elements_from_DAT(Flat,fcH);
        
        %$  Decrease smoothing window (more syllables).$%
        %*  *%
    case 'hyphen'
        if isempty(modifier)
            Flat.p.syllable.(P.method)(P.X(P.imicsN).imics).smoothingWidth=max(1,Flat.p.syllable.(P.method)(P.X(P.imicsN).imics).smoothingWidth-1);
            
            %$  Zoom out (Sdr STACK).$%
            %* Works only with Linus' latest data format*%
        elseif strcmp(modifier,'shift')
            if isfield(Flat.v,'DATstack') & Flat.v.DATstack>0
                if isempty(Flat.DAT.stack(Flat.v.DATstack).comment)
                    Flat.DAT.stack(Flat.v.DATstack).comment=1;
                else
                    
                    Flat.DAT.stack(Flat.v.DATstack).comment=max(Flat.DAT.stack(Flat.v.DATstack).comment/1.2,1);
                    %Flat.p.SdrStackMultiplier=Flat.p.SdrStackMultiplier/1.1;
                end
            end
        end
        [Flat,fcH]=func_import_elements_from_DAT(Flat,fcH);
        
        %$  Move right in current file.$%
        %* Moves the window to the right. *%
    case 'rightarrow'
        
        if isempty(modifier)
            if isfield(Flat.DAT,'datas')
                L=size(Flat.DAT.datas{S.fileN}{P.X(P.imicsN).imics},1);
            else
                L=size(Flat.DAT.data{S.fileN},2);
            end
            Flat.v.import_DAT.i0=max(min(L-S.ncols+1,S.i0+round(S.ncols/2)),1);
            [Flat,fcH]=func_import_elements_from_DAT(Flat,fcH);
            
            %$  Switch to next file.$%
            %*  *%
        elseif strcmp(modifier,'shift')
            Flat.v.import_DAT.fileN=min(get_number_of_recordings(Flat),S.fileN+1);
            Flat.v.import_DAT.i0=1;
            [Flat,fcH]=func_import_elements_from_DAT(Flat,fcH);
            %$  Browse files, skipping 500 files at once.$%
            %*  *%
        elseif strcmp(modifier,'control')
            Flat.v.import_DAT.fileN=min(get_number_of_recordings(Flat),S.fileN+500);
            Flat.v.import_DAT.i0=1;
            [Flat,fcH]=func_import_elements_from_DAT(Flat,fcH);
            %$  Switch to next channel.$%
            %*  *%
        elseif strcmp(modifier,'alt')
            if Flat.p.channel_mic_display < length(Flat.p.CHANNEL_MIC)
                Flat.p.channel_mic_display = Flat.p.channel_mic_display+1;
                %Flat.v.DATstack = Flat.v.DATstack+1;
                [Flat,fcH]=func_import_elements_from_DAT(Flat,fcH);
            end
        end
        
        %$  Move left in current file.$%
        %* Moves the window to the left. *%
    case 'leftarrow'
        if isempty(modifier)
            Flat.v.import_DAT.i0=max(1,S.i0-round(S.ncols/2));
            [Flat,fcH]=func_import_elements_from_DAT(Flat,fcH);
            
            %$  Switch to previous file.$%
            %*  *%
        elseif strcmp(modifier,'shift')
            Flat.v.import_DAT.fileN=max(1,S.fileN-1);
            Flat.v.import_DAT.i0=1;
            [Flat,fcH]=func_import_elements_from_DAT(Flat,fcH);
            %$  Browse files backwards, skipping 500 files at once.$%
            %*  *%
        elseif strcmp(modifier,'control')
            Flat.v.import_DAT.fileN=max(1,S.fileN-500);
            Flat.v.import_DAT.i0=1;
            [Flat,fcH]=func_import_elements_from_DAT(Flat,fcH);
            %$  Switch to previous channel.$%
            %*  *%
        elseif strcmp(modifier,'alt')
            if Flat.p.channel_mic_display > 1
                Flat.p.channel_mic_display = Flat.p.channel_mic_display-1;
                %Flat.v.DATstack = Flat.v.DATstack-1;
                [Flat,fcH]=func_import_elements_from_DAT(Flat,fcH);
            end
        end
        
        %$  Increase lower syllable threshold.$%
        %*   *%
    case 'uparrow'
        if isempty(modifier)
            if Flat.p.STACKscaled==1 && isfield(Flat.p,'ImportCustom') && Flat.p.ImportCustom==1 && ~isempty(Flat.v.DATstack) && Flat.v.DATstack>0 % new stack plus custom curve (e.g., sdr_condition, wingflap)
                %                stack=Flat.DAT.stack(Flat.v.DATstack);
                %                 thet=P.(P.method)(P.imicsN).thets+(stack.max-stack.min)/100;
                %                 thet=min(max(thet,stack.min),stack.max);
                thet=P.(P.method)(P.imicsN).thets+0.01;
                thet=min(max(thet,1),2); % see func_pseudo_RMS_from_spec
            else
                thet=P.(P.method)(P.imicsN).thets*1.1;
            end
            log_s=get(fcH.hf_import_DAT.plots,'ydata');
            Flat.p.syllable.(P.method)(P.imicsN).thets=thet;
            set(fcH.hf_import_DAT.plot_thet,'ydata',log(thet*[1 1]));
            set(fcH.hf_import_DAT.axes1,'ylim',[min(min(log_s),min(log(thet))) max(max(log_s),max(log(thet)))]);
            
            %$  Increase upper syllable threshold.$%
            %*  *%
        elseif strcmp(modifier,'shift')
            thetU=P.(P.method)(P.imicsN).thetsU*1.1;
            log_s=get(fcH.hf_import_DAT.plots,'ydata');
            Flat.p.syllable.(P.method)(P.imicsN).thetsU=thetU;
            set(fcH.hf_import_DAT.plot_thetU,'ydata',log(thetU*[1 1]),'visible','on');
            set(fcH.hf_import_DAT.axes1,'ylim',[min(min(log_s),min(log(thetU))) max(max(log_s),max(log(thetU)))]);
            
            %$  Inccrease skype playback-suppression threshold.$%
            %*  *%
        elseif strcmp(modifier,'alt')
            S=Flat.v.skype;
            h1=find(Flat.p.skype.mode==1);
            S.thresh1=Flat.p.skypeGLM.thresh1; S.thresh1(h1)=Flat.p.skypeCUT.thresh1(h1);
            S.thresh1(S.suppress.filt_no)=S.thresh1(S.suppress.filt_no)*1.1;
            Flat.v.skype=S;
            h1=find(Flat.p.skype.mode==1); h2=find(Flat.p.skype.mode==2);
            Flat.p.skypeCUT.thresh1(h1)=S.thresh1(h1);
            Flat.p.skypeGLM.thresh1(h2)=S.thresh1(h2);
            [Flat,fcH]=func_import_elements_from_DAT(Flat,fcH);
        end
        
        %$  Decrease lower syllable threshold.$%
        %*  *%
    case 'downarrow'
        if isempty(modifier)
            if Flat.p.STACKscaled==1 && isfield(Flat.p,'ImportCustom') && Flat.p.ImportCustom==1 && ~isempty(Flat.v.DATstack) && Flat.v.DATstack>0 % new stack plus custom curve (e.g., sdr_condition, wingflap)
                %                 stack=Flat.DAT.stack(Flat.v.DATstack);
                %                 thet=P.(P.method)(P.imicsN).thets-(stack.max-stack.min)/100;
                %                 thet=min(max(thet,stack.min),stack.max);
                thet=P.(P.method)(P.imicsN).thets-0.01;
                thet=min(max(thet,1),2); % see func_pseudo_RMS_from_spec
            else
                thet=P.(P.method)(P.imicsN).thets/1.1;
            end
            log_s=get(fcH.hf_import_DAT.plots,'ydata');
            Flat.p.syllable.(P.method)(P.imicsN).thets=thet;
            set(fcH.hf_import_DAT.plot_thet,'ydata',log(thet*[1 1]));
            if ~isempty(log_s)
                set(fcH.hf_import_DAT.axes1,'ylim',[min(min(log_s),min(log(thet))) max(max(log_s),max(log(thet)))]);
            end
            
            %$  Decrease upper syllable threshold.$%
            %*  *%
        elseif strcmp(modifier,'shift')
            thetU=max(P.(P.method)(P.imicsN).thets, P.(P.method)(P.imicsN).thetsU/1.1);
            log_s=get(fcH.hf_import_DAT.plots,'ydata');
            Flat.p.syllable.(P.method)(P.imicsN).thetsU=thetU;
            set(fcH.hf_import_DAT.plot_thetU,'ydata',log(thetU*[1 1]),'visible','on');
            if ~isempty(log_s)
                set(fcH.hf_import_DAT.axes1,'ylim',[min(min(log_s),min(log(thetU))) max(max(log_s),max(log(thetU)))]);
            end
            
            %$  Decrease skype playback-suppression threshold.$%
            %*  *%
        elseif strcmp(modifier,'alt')
            S=Flat.v.skype;
            h1=find(Flat.p.skype.mode==1);
            S.thresh1=Flat.p.skypeGLM.thresh1; S.thresh1(h1)=Flat.p.skypeCUT.thresh1(h1);
            S.thresh1(S.suppress.filt_no)=S.thresh1(S.suppress.filt_no)/1.1;
            Flat.v.skype=S;
            h1=find(Flat.p.skype.mode==1); h2=find(Flat.p.skype.mode==2);
            Flat.p.skypeCUT.thresh1(h1)=S.thresh1(h1);
            Flat.p.skypeGLM.thresh1(h2)=S.thresh1(h2);
            [Flat,fcH]=func_import_elements_from_DAT(Flat,fcH);
        end
        
        %$  move upper green line up.$%
        %* region where power spectrogram is summed *%
    case '0'
        Flat.p.syllable.(P.method)(P.imicsN).high=...
            min(max(P.(P.method)(P.imicsN).high+100,P.(P.method)(P.imicsN).low+100),size(Flat.DAT.data{1},1)*Flat.scanrate/Flat.p.nfft);
        [Flat,fcH]=func_import_elements_from_DAT(Flat,fcH);
        
        %$  move upper green line down.$%
        %* region where power spectrogram is summed *%
    case '9'
        Flat.p.syllable.(P.method)(P.imicsN).high=...
            min(max(P.(P.method)(P.imicsN).high-100,P.(P.method)(P.imicsN).low+100),size(Flat.DAT.data{1},1)*Flat.scanrate/Flat.p.nfft);
        [Flat,fcH]=func_import_elements_from_DAT(Flat,fcH);
        
        %$  move lower green line up.$%
        %* region where power spectrogram is summed *%
    case '2'
        Flat.p.syllable.(P.method)(P.imicsN).low=...
            max(min(P.(P.method)(P.imicsN).low+100,P.(P.method)(P.imicsN).high-100),1*Flat.scanrate/Flat.p.nfft);
        [Flat,fcH]=func_import_elements_from_DAT(Flat,fcH);
        
        %$  move lower green line down.$%
        %* region where power spectrogram is summed *%
    case '1'
        Flat.p.syllable.(P.method)(P.imicsN).low=...
            max(min(P.(P.method)(P.imicsN).low-100,P.(P.method)(P.imicsN).high-100),1*Flat.scanrate/Flat.p.nfft);
        [Flat,fcH]=func_import_elements_from_DAT(Flat,fcH);
        
        %$  Change STACK for this channel (only for Linus' Sdr files). $%
        %*  Plots the curve on top of the spectrogram. *%
    case {'rightbracket','leftbracket'}
        cfg=evalin('base','cfg');
        
        if cfg.file_format==13 || cfg.file_format==3000 && ~isempty(Flat.v.DATstack) && Flat.v.DATstack>0
            %             fnames={'SdrSignalStrength','SdrReceiverFreq','SdrCarrierFreq'};
            %             %fn=fieldnames(Flat.v.stack);
            %             fn={Flat.DAT.stack.name};
            %             currname=Flat.DAT.stack(Flat.v.DATstack).name;
            %             currno=str2num(currname(end));
            %             whichno=find(ismember(fnames,currname(1:end-1)));
            %             if strcmp(evnt.Key,'rightbracket')
            %                 whichno=rem(whichno,3)+1;
            %             else
            %                 whichno=rem(whichno+1,3)+1;
            %             end
            %             newname=[fnames{whichno} currname(end)];
            %             h=find(ismember(fn,newname));
            %             if isempty(h)
            %                 disp('not possible');
            %             else
            %               Flat.v.DATstack=h;
            %             end
            if strcmp(evnt.Key,'rightbracket')
                Flat.v.DATstack = rem(Flat.v.DATstack,length(Flat.DAT.stack))+1;
            else
                Flat.v.DATstack = rem(Flat.v.DATstack-2+length(Flat.DAT.stack),length(Flat.DAT.stack))+1;
            end
            
            [Flat,fcH]=func_import_elements_from_DAT(Flat,fcH);
        end
        
        %$  Plot RMS histogram across all days for selected channel.$%
        %*  opens new window. *%
    case 'a'
        width=1.5;
        c=0;
        did_once=0;
        %l=0:10:4000;
        y=zeros(60,500);
        Flats=evalin('base','Flats');
        for i=2:length(Flats)
            if Flats{i}.num_Tags==0
                continue
            end
            fprintf('day %d\n',i);
            c=c+1;
            L=length(Flats{i}.DAT.filename);
            s=cell(L,1);
            for j=1:L
                S.fileN=j;
                s{j}=func_pseudo_RMS_from_spec(Flats{i},S,P,1)';
            end
            if did_once
                y(c,:)=hist([s{:}],x);
            else
                [y(c,:),x]=hist([s{:}],500);
                did_once=1;
            end
        end
        figure(1);clf; plot(x,log(1+y(1:c,:))');
        legend(Flats{2}.p.names(2:c+1));
        xlabel('Sound Intensity'); ylabel('Histogram');
        
        thrs=zeros(c,1); % threshold based on noise peak
        for i=1:c
            ion=find(y(i,:),1,'first');
            [dummy,ipeak]=max(y(i,:));
            thrs(i)=x(floor(ipeak+width*(ipeak-ion)));
        end
        ymax=max(max(log(1+y)));
        hold on; plot([thrs thrs]',[0 ymax]);
        
        SM=P.(P.method)(P.imicsN); % threshold used for syllable extraction
        plot([SM.thets SM.thets],[0 ymax/2],'k','linewidth',2);
        disp('Black line= threshold in Flat.v.import_DAT');
        
        %$  Display help.$%
        %* You're looking at it. *%
    case 'h'
        if isempty(modifier)% strcmp(modifier,'shift')
            % func_show_help_menu();
            Flat.v.run_select_clust=0;
            Flat.v.plot_clust=0;
            Flat.v.run_select=0;
            Flat.v.run_set_consistency=0;
            
            currentPath = [mfilename('fullpath') '.m'];
            func_show_help(currentPath);
        end
        
        %$  Copy parameters from other channel.$%
        %*  *%
    case 'c'
        if isempty(modifier)% strcmp(modifier,'shift')
            % func_show_help_menu();
            
            y=input('From which channel do you want to copy parameters? (enter=none)');
            
            if ~ismember(y,[P.X.imics])
                if y==P.X(P.imicsN).imics
                    disp('The chosen channel is currently open, choose another one.')
                    return
                end
                return
            end
            Flat.p.syllable.(P.method)(P.imicsN)=P.(P.method)(y);
            [Flat,fcH]=func_import_elements_from_DAT(Flat,fcH);
        end
        
        %$  Toggle DATmasknoise on or off.$%
        %*  Set Flat.p.DATmasknoise=~Flat.p.DATmasknoise *%
    case 'n'
        if isempty(modifier)
            if isfield(Flat.p,'DATmasknoise')
                Flat.p.DATmasknoise(Flat.p.channel_mic_display)=~Flat.p.DATmasknoise(Flat.p.channel_mic_display);
                if Flat.p.DATmasknoise(Flat.p.channel_mic_display)
                    s='on';  else s='off';
                end
                fprintf('Datmasknoise %s\n',s);
            else
                disp('No DATmasknoise, run func_noise_kmeans first');
            end
            
            %$  Filter out noise.$%
            %*  Run func_noise_kmeans. Set SongOnOff=1 on tagboard and put noise to last cluster and song to cluster 1.*%
        elseif strcmp(modifier,'shift')
            if ~isfield(Flat.DAT,'mask')
                disp('extract syllables first by pressing w');
                return
            end
            Flat=func_noise_kmeans(Flat,0,Flat.p.channel_mic_display);
        end
        
        
        %$  Define 'up' tag.$%
        %*  For automated syllable segmentation of backpack accelerometer data. *%
    case {'u','d'}
        pp = func_mouseclick_element(Flat, 'ha_import2');
        if ~isfield(Flat.v,'import_DAT_backpack_segm') || isempty(Flat.v.import_DAT_backpack_segm)
            Flat.v.import_DAT_backpack_segm.file=Flat.v.import_DAT.fileN;
            Flat.v.import_DAT_backpack_segm.buff=Flat.v.import_DAT.i0+pp;
            Flat.v.import_DAT_backpack_segm.type=evnt.Key;
            fprintf('1 label in Flat.v.import_DAT_backpack_segm\n');
        else
            Flat.v.import_DAT_backpack_segm.file(end+1)=Flat.v.import_DAT.fileN;
            Flat.v.import_DAT_backpack_segm.buff(end+1)=Flat.v.import_DAT.i0+pp;
            Flat.v.import_DAT_backpack_segm.type(end+1)=evnt.Key;
            fprintf('%d+ %d- labels in Flat.v.import_DAT_backpack_segm\n',sum(ismember(Flat.v.import_DAT_backpack_segm.type,'u')),sum(ismember(Flat.v.import_DAT_backpack_segm.type,'d')));
        end
        %$  write all elements to Flat (only from selected channel).$%
        %*  Parameters are written into Flat.p.syllable. *%
    case 'w'
        
        if isfield(Flat.p,'skype') && any(Flat.p.skype.mode(Flat.p.skype.which.filtered)==2)
            disp('For skype birds first run GLM on all channels - aborting');
            return
        end
        
        % check clust window
        if Flat.v.tsne.clust_dense_mode==1 &&  Flat.spec.samp_left{5}(1)
            disp('Left Samples of clust win must be zero, please correct or set Flat.v.tsne.clust_dense_mode=0 !');
            return
        end
        
        do_skype_cleanup=0;
        do_skype_GLM=0;
        do_tsne=0;
        do_umap=0;
        
        if isempty(modifier)
            if isfield(Flat.p,'skype') && (~isfield(Flat.p.skype,'echo_DATclean') || Flat.p.skype.echo_DATclean==0)
                disp('For skype birds, the first time import all channels simultaneously (shift+w) - aborting');
                return
            end
            import_channels=P.imicsN;
            disp('segmenting single channel');
            
            %$  write all elements to Flat (all channels) and use same parameter for following days.$%
            %* Parameters are written into Flat.p.syllable *%
        elseif strcmp(modifier,'shift')
            % import_channels= Flat.p.skype.which.filtered;
            
            if isfield(Flat.p,'skype')
                do_skype_GLM=1;
                do_skype_cleanup=1;
                import_channels= Flat.p.skype.which.filtered;
            else
                disp('segmenting all channels');
                import_channels=P.X.imics; %1:length(P.imics);
            end
            
            %$  Write all elements on all channels to Flat and train TSNEs.$%
            %* Parameters are written into Flat.p.syllable and TSNEs are trained on all channels*%
        elseif strcmp(modifier,'alt')
            disp('segmenting all channels and running TSNE');
            if isfield(Flat.p,'skype')
                do_skype_GLM=1;
                do_skype_cleanup=1;
                import_channels= Flat.p.skype.which.filtered;
            else
                disp('segmenting all channels');
                import_channels=P.X.imics; %1:length(P.imics);
            end
            do_tsne=1;
            if ~Flat.spec.samp_left{5}(1)==0
                Flat.spec.samp_left{5}(:)=0;
                Flat.spec.samp_right{5}(:)=4032;
                Flat.spec.numbuffs{5}(:)=60;
            end
            disp('Set Clust Win to 60');
        else
            return
        end
        
        if isfield(Flat.p,'skype')
            for i=1:length(Flat.DAT.filename)
                Flat.DAT.mask{i}=zeros(length(Flat.p.skype.which.filtered),size(Flat.DAT.datas{i}{1},1));
            end
        end
        
%         fprintf('1: single crossing (old school) \n2: multi crossings (segment+cluster in tsne)\n3: everything (umap)!\n0: abort\n');
%         ychoice=input('Make a choice: ');
        ychoice = 3;
        %%%%%%%%%%%% UMAP
        if ychoice==3
            P=Flat.p.syllable; P.imicsN=Flat.p.channel_mic_display;
            SM=P.(P.method)(P.imicsN);
            %  .(Flat.p.syllable.method)(Flat.p.syllable.X(Flat.p.syllable.imicsN).imics);
            [Flat,thresh]=func_DAT_threshold(Flat,P,0);
            if ~isequal(SM.thets,thresh)
                disp('Threshold manipulated, abort or press 1 to continue');
                y=input('');
                if ~(y==1)
                    return
                end
            end
            % smw=Flat.p.syllable.(Flat.p.syllable.method)(Flat.p.syllable.X(Flat.p.syllable.imicsN).imics).smoothingWidth;
            smw=SM.smoothingWidth;
            if ~(1==smw)
                fprintf('Smoothing width should be 1, hit enter to continue with your value of %d\n',smw);
                pause
            end
            do_umap=1;
            Flat.v.tsne.clust_dense_mode=2;
            
            if ~isfield(Flat.p,'umap') || ~isfield(Flat.p.umap,'MinNumBuffSyl')
                Flat.p.umap.MinNumBuffSyl=10;
            end
            fprintf('Min num buffs per syl: %d (must be even, for some radiobirds, set to 8)\n',Flat.p.umap.MinNumBuffSyl)
            y=input('   enter new value (enter=keep)');
            if ~isempty(y)
                Flat.p.umap.MinNumBuffSyl=y;
            end
            
            
            if ~isfield(Flat.p,'umap') || ~isfield(Flat.p.umap,'ClustWinSize')
                Flat.p.umap.ClustWinSize=16;
            end
            fprintf('Clust win size: %d \n',Flat.p.umap.ClustWinSize)
            y=input('   enter new value (enter=keep)');
            if ~isempty(y)
                Flat.p.umap.ClustWinSize=y;
            end
            
            if ~isfield(Flat.p,'umap') || ~isfield(Flat.p.umap,'ClustWinOffset')
                Flat.p.umap.ClustWinOffset=-4;
            end
            fprintf('Clust win offset (in buffers): %d \n',Flat.p.umap.ClustWinOffset)
            y=input('   enter new value (enter=keep)');
            if ~isempty(y)
                Flat.p.umap.ClustWinOffset=y;
            end
            
            if  ~isfield(Flat.p.umap,'buffs_beyond_offset')
                Flat.p.umap.buffs_beyond_offset=6;
            end
            fprintf('Number of buffers beyond syllable offset: %d \n',Flat.p.umap.buffs_beyond_offset)
            y=input('   enter new value (enter=keep)');
            if ~isempty(y)
                Flat.p.umap.buffs_beyond_offset=y;
            end
            
            Flat.p.umap.MinNumTimeSegments=Flat.p.umap.buffs_beyond_offset+Flat.p.umap.MinNumBuffSyl-Flat.p.umap.ClustWinSize-Flat.p.umap.ClustWinOffset;
            fprintf('Syllable coverage=%d Buffers\n',Flat.p.umap.MinNumTimeSegments);
            if Flat.p.umap.MinNumTimeSegments<2
                disp('You have less than 2 time segments per syllable, this might not be enough - aborting');
                return
            end
            Flat.umap.appendixB=Flat.p.umap.MinNumBuffSyl-Flat.p.umap.MinNumTimeSegments;
            
            Flat.spec.numbuffs{5}(:)=Flat.p.umap.ClustWinSize;
            Flat.spec.firstbuff{5}(:)= Flat.p.umap.ClustWinOffset; % assuming 4ms per buffer
            Flat.spec_curr_ID=2; %5 @Corinna 10/02/2022
            
            Flat.spec.samp_left{5}(:)=-Flat.p.nonoverlap*Flat.spec.firstbuff{5}(:);
            Flat.spec.samp_right{5}(:)=Flat.p.nonoverlap*(Flat.spec.numbuffs{5}(:)-1)-Flat.spec.samp_left{5}(:)+Flat.p.nfft;
            Flat.v.spec_skip_plot=1;
            
            
            if isfield(Flat,'umap')  && isfield(Flat.umap,'RMSold')
                [Flat,sels,board] = func_select_tagboard(Flat,[],'activator',{Flat.umap.RMSold});
                Flat=func_remove_elements(Flat,sels);
                Flat.v.RMS=Flat.umap.RMSold; Flat.p.Channel_syl_tagstring{Flat.p.channel_mic_display}=Flat.v.RMS;
            end
            Flat.p.syllable.multi=0;
            Flat.v.pca_segmented=0;
            Flat.v.umap.ranPC=0;
            Flat=func_DAT_Song_on_off(Flat,-1);
            
        elseif ychoice==2
            Flat.v.tsne.clust_dense_mode=1;
            Flat.p.syllable.multi=1;
            Flat.v.pca_segmented=1;
            
        elseif ychoice==1
            Flat.v.tsne.clust_dense_mode=0;
            Flat.p.syllable.multi=0;
            if isfield(Flat.v.tags,'TSNE')
                [Flat]=func_help_remove_tagfield(Flat,'TSNE');
            end
           
        else
            return
        end
        
        
        if ~do_umap
            fprintf('Num PCA coefficients: %d\n ',Flat.num_PCs);
            npc=input('Enter new number (return=keep ) ');
            if ~isempty(npc)
                Flat.num_PCs=npc;
            end
        end
        
        if do_tsne==1
            yy=0;
        else
            yy=0;
            %  yy=input('Segment syllables an all archives (Flats, yes=1, no=0)?: ');
        end
        
        orig_mic=Flat.p.channel_mic_display;
        % Compute syllables in just single archive
        P.syl=Flat.p.syllable;
        P.askjoin=-1;
        %   if do_skype_GLM
        keep_pca=1;
        if do_umap
            keep_pca=input('try reusing principal components (yes=1) ? ');
            if keep_pca~=1
                keep_pca=0;
            end
            if keep_pca==1
                Flat.v.umap.ranPC=1;
            end
        end

        %   Flat=func_import_elements_from_DAT_write_to_Flat(Flat,import_channels,P,~do_umap);
        Flat=func_import_elements_from_DAT_write_to_Flat(Flat,import_channels,P,keep_pca);
        if ychoice==1 && isfield(Flat,'Z')
            Flat=rmfield(Flat,'Z');
        end
        %  else
        %    Flat=func_import_elements_from_DAT_write_to_Flat(Flat,import_channels,P);
        % end
        
        %       Flat.DAT.mask{import_channels,:}=mask;
        if ~do_umap
            for i=1:length(import_channels)
                Flat.p.tsne{import_channels(i)}=Flat.v.tsne;
                Flat=func_mic_toggle_and_pca(Flat,Flat.p.channel_mic_display,import_channels(i),1);
            end
        end
        
        % final echo cleanup of DAT.datas
        if do_skype_cleanup
            Flat=func_skype_DAT_cleanup(Flat,import_channels,Flat.DAT.mask);
        end
        
        
        Flats = evalin('base', 'Flats');
        if yy==0
            %   [Flat,mask]=func_import_elements_from_DAT_write_to_Flat(Flat,import_channels,P);
            
            % write syllable parameters to Flats if empty
            for a=1:length(Flats)
                if Flats{a}.num_Tags==0
                    continue
                end
                if ~isfield(Flats{a}.p,'syllable') %|| ~isequal(Flats{a}.p.syllable,Flat.p.syllable)
                    Flats{a}.p.syllable= Flat.p.syllable;
                    Flats{a}.spec.samp_left{5}(:)=Flat.spec.samp_left{5}(1);
                    Flats{a}.spec.samp_right{5}(:)=Flat.spec.samp_right{5}(1);
                    Flats{a}.spec.numbuffs{5}(:)=Flat.spec.numbuffs{5}(1);
                    Flats{a}.v.pca_segmented=Flat.v.pca_segmented;
                    Flats{a}.v.numbuffs=Flat.v.numbuffs;
                    fprintf('Archive %a: wrote Parameters Flat.p.syllable for syllable extraction\n',a);
                end
            end
            
            % Compute syllables in all archives
        elseif yy==1
            Flats{Flat.v.ind}=Flat;
            P.syl=Flat.p.syllable;
            P.askjoin=Flat.p.syllable.opt;
            for a=1:length(Flats)
                if Flats{a}.num_Tags==0 || a==Flat.v.ind
                    continue
                end
                
                if do_skype_GLM % train GLM and regress-suppress filtered channel
                    for i=1:length(import_channels)
                        Flats{a}=func_skype_initialize(Flats{a});
                        %Flats{a}.p.skype.which=Flat.p.skype.which;
                        Flats{a}.p.skypeGLM.thresh0=Flat.p.skypeGLM.thresh0;
                        Flats{a}.p.skypeGLM.thresh1=Flat.p.skypeGLM.thresh1;
                        Flats{a}.v.skype.suppress=func_skype_suppress_chans(Flats{a},import_channels(i));
                        if isfield(Flat.v,'tsne') && isfield(Flat.v.tsne,'clust_dense_mode')
                            Flats{a}.v.tsne.clust_dense_mode=Flat.v.tsne.clust_dense_mode;
                        end
                        Flats{a}.dynamic_range_spec=Flat.dynamic_range_spec;
                        Flats{a}.dynamic_range=Flat.dynamic_range;
                        Flats{a}.spec.samp_left{5}(:)=Flat.spec.samp_left{5}(1);
                        Flats{a}.spec.samp_right{5}(:)=Flat.spec.samp_right{5}(1);
                        Flats{a}.spec.numbuffs{5}(:)=Flat.spec.numbuffs{5}(1);
                        Flats{a}.v.numbuffs=Flat.v.numbuffs;
                        Flats{a}.v.pca_segmented=Flat.v.pca_segmented;
                        Flats{a}=func_skype_GLM_learn(Flats{a},Flats{a}.v.skype.suppress);
                        Flats{a}=func_skype_evalkeyW(Flats{a},Flats{a}.v.skype.suppress,import_channels(i));
                        Flats{a}.p.skypeCUT.thresh1=Flat.p.skypeCUT.thresh1;
                    end
                end
                
                if do_skype_GLM
                    Flats{a}=func_import_elements_from_DAT_write_to_Flat(Flats{a},import_channels,P);
                    Flats{a}=func_skype_DAT_cleanup(Flats{a},import_channels,Flats{a}.DAT.mask);
                else
                    Flats{a}=func_import_elements_from_DAT_write_to_Flat(Flats{a},import_channels,P);
                end
                %                 for i=1:length(import_channels)
                %                     Flats{a}=func_mic_toggle_and_pca(Flats{a},Flats{a}.p.channel_mic_display(1),import_channels(i));
                %                 end
            end
        end
        assignin('base', 'Flats', Flats);
        
        if ~do_umap
            Flat=func_mic_toggle_and_pca(Flat,Flat.p.channel_mic_display,orig_mic);
        end
        
        if do_tsne
            [fcH,Flat]=func_tsne_all(fcH,Flat);
        end
        
        
        %%%
        Flat.v.tagboard0_recompute=1;
        Flat.v.tagboard0_replot=1;
        Flat.v.run_select=1;
        Flat.v.run_select_clust=1;
        % redefine Flat.v.tagboard0 and Flat.v.select
        [Flat Flat.v.select Flat.v.select_clust]=func_select_mode(Flat);
        Flat.v.plot_tagboard=1;
%         [Flat,fcH]=plot_figs(Flat,fcH);
        close_import_DAT_figs
        disp('Done');
        
        %$  Reinitialize import parameters for this channel.$%
        %*  *%
    case 'x'
        if isempty(modifier)
            P=Flat.p.syllable;
            Flat.p.syllable.(P.method)(P.imics(P.imicsN)).smoothingWidth=1;
            Flat.p.syllable.(P.method)(P.imics(P.imicsN)).minsS=NaN;
            disp('Reinitializing import parameters for this channel');
            [Flat,fcH]=func_import_elements_from_DAT(Flat,fcH);
            
            %$  Reinitialize import parameters for all channels.$%
            %*  *%
        elseif strcmp(modifier,'shift')
            Flat.p.syllable=[];
            [Flat,fcH]=func_import_elements_from_DAT(Flat,fcH);
            disp('Reinitializing import parameters for all channels');
        end
        
        %$ Go to max of stack plot/RMS in file $%
        %* *%
    case 'm'
        if isfield(Flat.p, 'ImportCustom') && Flat.p.ImportCustom
            x = func_stack_extract2(Flat,Flat.v.DATstack,Flat.v.import_DAT.fileN);
        else
            [x,D,L]=func_pseudo_RMS_from_spec(Flat,S,P,1,Flat.p.channel_mic_display);
        end
        if isempty(modifier)
            [~,i] = max(x);
            %$ Go to next potential element (stack plot/RMS > thres) $%
            %* *%
        elseif strcmp(modifier,'shift')
            if isfield(Flat.p, 'ImportCustom') && Flat.p.ImportCustom
                thres = Flat.DAT.stack(Flat.v.DATstack).min+(Flat.p.syllable.sum(Flat.p.channel_mic_display).thets-1)*(Flat.DAT.stack(Flat.v.DATstack).max-Flat.DAT.stack(Flat.v.DATstack).min);
            else
                thres = Flat.p.syllable.sum(Flat.p.channel_mic_display).thets;
            end
            
            startind = ceil(Flat.v.import_DAT.i0 * Flat.p.nonoverlap / Flat.DAT.stack(Flat.v.DATstack).nonoverlap);
            i = find(diff(x(startind:end)>thres)==1,1);
            if isempty(i)
                disp('end of file');
                i = 0;
            end
            i = i+startind;
        end
        Flat.v.import_DAT.i0 = round(i*Flat.DAT.stack(Flat.v.DATstack).nonoverlap/Flat.p.nonoverlap);
        [Flat,fcH]=func_import_elements_from_DAT(Flat,fcH);
        %$ Show movie excerpt (only for Sdr data).$%
        %* You need to load the movie file first and name it Birdmovie.*%
    case 'y'
        if isempty(modifier)
            onoff=floor([S.i0 S.i0+round(S.ncols)]*Flat.p.nonoverlap);
            %onoff=floor([S.i0 S.i0+round(S.ncols)]*Flat.p.nonoverlap);
            sdr_movie_show(Flat,Flat.v.import_DAT.fileN,onoff);
            
            %$  Play audio excerpt.$%
            %*  *%
        elseif strcmp(modifier,'control')
            onoff=floor([S.i0 S.i0+round(S.ncols)]*Flat.p.nonoverlap);
            
            file_format = evalin('base', 'cfg.file_format;');
            
            if file_format==3000
                aux = Flat.p.birdradio;
            else
                aux = [];
            end
        
            [data, NUscanrate, NUall_return_vars, NUfileinfo, status] = load_file_generic(evalin('base','paths.load_path_default;'), file_format, ...
            Flat.DAT.subdir{Flat.v.import_DAT.fileN}, filesep, Flat.DAT.filename{Flat.v.import_DAT.fileN}, [], 1, GUI_filetype_filter_setup(), -1, 0, 0, aux); %#ok<ASGLU>
        
            x = data(Flat.p.channel_mic_display,onoff(1):min(onoff(2),size(data,2)));
            [bb,aa] = butter(4,300/(Flat.scanrate/2),'high');
            x = filter(bb,aa,x);
            x = x(round(Flat.scanrate*0.05):end);
        
%             figure(55);
%             plot(x);
            soundsc(x,Flat.scanrate);
            
%             figure(56);
%             iCh = Flat.p.channel_mic_display;
%             x1 = (0:1:length(data(iCh,:))-1)/Flat.scanrate;
%             
%             stackName = {Flat.DAT.stack.name};
%             k1 = find(ismember(stackName,['SdrCarrierFreq' num2str(iCh)]));
%             
%             sig1S = func_stack_extract2(Flat,k1,Flat.v.import_DAT.fileN);
%             x2 = ((0:1:length(sig1S)-1)-1/2)*512/Flat.scanrate;
%             x3 = ((0:1:length(sig1S)-1)+0)*512/Flat.scanrate;
%             
%             plot(x1, data(iCh,:)/Flat.p.birdradio.gainRadioChannels, x2, sig1S, x3, decimate(data(iCh,:)/Flat.p.birdradio.gainRadioChannels,512,'fir'));
            
%             %             [y,Fs] = audioread(fullfile(evalin('base','paths.load_path_default;'),Flat.DAT.subdir{Flat.v.import_DAT.fileN},Flat.DAT.filename{Flat.v.import_DAT.fileN}),onoff*Fs/Flat.scanrate);
%             %             soundsc(y(:,Flat.p.channel_mic_display),Fs);
%             [y,Fs] = audioread(fullfile(evalin('base','paths.load_path_default;'),Flat.DAT.subdir{Flat.v.import_DAT.fileN},strrep(Flat.DAT.filename{Flat.v.import_DAT.fileN},'SdrChannels', 'DAQmxChannels')),onoff*32000/Flat.scanrate);
%             soundsc(y(:,1),Fs);
            
            %$  Plot all channels and stacks. (Linus - Sdr Data)$%
            %*  *%
        elseif strcmp(modifier,'alt')
            
%             thresWingflap = 1.93e+03;
%             thresCarrierJump = 9.83e+04;
            
            stackName = {Flat.DAT.stack.name};
            onoff=floor([S.i0 S.i0+round(S.ncols-1)]*Flat.p.nonoverlap+Flat.p.nfft/2-Flat.p.nonoverlap);
            %ts = Flat.X.indices_all(Flat.v.last_clicked)/Flat.scanrate;
            %twin = [-5.3 29]*0.004+ts;
            %twin = [-10 3]+ts;
            figure(44);
            nsubplot = 8;
            specClim = [-127 100];
            fmax = size(Flat.DAT.datas{1}{1},2)*Flat.scanrate/Flat.p.nfft/1000;
            
            t=tiledlayout(nsubplot,1, 'TileSpacing', 'tight', 'Padding', 'compact');
            
            title(t,sprintf('file: %s, onset: %.2f s',Flat.DAT.filename{Flat.v.import_DAT.fileN},onoff(1)/Flat.scanrate), 'Interpreter', 'none');
            
            iCh1 = 1; %Flat.p.channel_mic_display;
            iCh2 = 2;
            
            iChMic = Flat.p.birdradio.NradioChannels+1;
            
            ax = nexttile;
            imagesc(onoff/Flat.scanrate, [0 1]*fmax, Flat.DAT.datas{Flat.v.import_DAT.fileN}{iChMic}(S.i0:S.i0+round(S.ncols-1),:)');
            ax.YDir = 'normal';
            title(Flat.p.Channel_names{iChMic}, 'Color','k')
            caxis([-127 100])
            
            ax = nexttile;
            imagesc(onoff/Flat.scanrate, [0 1]*fmax, Flat.DAT.datas{Flat.v.import_DAT.fileN}{iCh1}(S.i0:S.i0+round(S.ncols-1),:)');
            ax.YDir = 'normal';
            title(Flat.p.Channel_names{iCh1}, 'Color','#0072BD')
            caxis(specClim)
            
            ax = nexttile;
            imagesc(onoff/Flat.scanrate, [0 1]*fmax, Flat.DAT.datas{Flat.v.import_DAT.fileN}{iCh2}(S.i0:S.i0+round(S.ncols-1),:)');
            ax.YDir = 'normal';
            title(Flat.p.Channel_names{iCh2}, 'Color','#D95319')
            caxis(specClim)
            
%             nexttile
%             e1 = sum(Flat.DAT.datas{Flat.v.import_DAT.fileN}{iCh1}(S.i0:S.i0+round(S.ncols),1:2),2);
%             e2 = sum(Flat.DAT.datas{Flat.v.import_DAT.fileN}{iCh2}(S.i0:S.i0+round(S.ncols),1:2),2);
%             x = linspace(onoff(1)/Flat.scanrate,onoff(2)/Flat.scanrate, length(e1));
%             plot(x,e1,x,e2);
%             title('sum(specbin(1:2))')
            
            nexttile
            k1 = find(ismember(stackName,['SdrCarrierFreq' num2str(iCh1)]));
            sig1 = func_stack_extract2(Flat,k1,Flat.v.import_DAT.fileN);
            
            k2 = find(ismember(stackName,['SdrCarrierFreq' num2str(iCh2)]));
            sig2 = func_stack_extract2(Flat,k2,Flat.v.import_DAT.fileN);
                                              
            samples = round((onoff(1)-1)/Flat.DAT.stack(k1).nonoverlap)+1:round((onoff(2)-1)/Flat.DAT.stack(k1).nonoverlap)+1;
            %x = linspace((samples(1)-1)*Flat.DAT.stack(k1).nonoverlap+Flat.DAT.stack(k1).nonoverlap/2,(samples(end)-1)*Flat.DAT.stack(k1).nonoverlap+Flat.DAT.stack(k1).nonoverlap/2, length(samples))/Flat.scanrate;
            x = linspace((samples(1)-1)*Flat.DAT.stack(k1).nonoverlap-Flat.DAT.stack(k1).nonoverlap/2,(samples(end)-1)*Flat.DAT.stack(k1).nonoverlap-Flat.DAT.stack(k1).nonoverlap/2, length(samples))/Flat.scanrate;
            
            plot(x,sig1(samples)/1e6,x,sig2(samples)/1e6);
            title('SdrCarrierFreq');
            
            nexttile
            k1 = find(ismember(stackName,['SdrSignalPower' num2str(iCh1)]));
            sig1S = func_stack_extract2(Flat,k1,Flat.v.import_DAT.fileN);
            
            k2 = find(ismember(stackName,['SdrSignalPower' num2str(iCh2)]));
            sig2S = func_stack_extract2(Flat,k2,Flat.v.import_DAT.fileN);
                                  
            %samples = ceil(onoff(1)/Flat.DAT.stack(k1).nonoverlap):round(onoff(2)/Flat.DAT.stack(k1).nonoverlap);
            %x = linspace(0,diff(onoff)/Flat.scanrate, length(samples));
            
            plot(x,sig1S(samples),x,sig2S(samples));
            title('SdrSignalPower');
            
            nexttile
            k1 = find(ismember(stackName,['SdrNoisePower' num2str(iCh1)]));
            sig1N = func_stack_extract2(Flat,k1,Flat.v.import_DAT.fileN);
            
            k2 = find(ismember(stackName,['SdrNoisePower' num2str(iCh2)]));
            sig2N = func_stack_extract2(Flat,k2,Flat.v.import_DAT.fileN);
                                  
            %samples = ceil(onoff(1)/Flat.DAT.stack(k1).nonoverlap):round(onoff(2)/Flat.DAT.stack(k1).nonoverlap);
            %x = linspace(0,diff(onoff)/Flat.scanrate, length(samples));
            
            plot(x,sig1N(samples),x,sig2N(samples));
            title('SdrNoisePower');
            
            nexttile
            plot(x,sig1S(samples)-sig1N(samples),x,sig2S(samples)-sig2N(samples));
            title('SdrRadioSNR');
            
            nexttile
            k1 = find(ismember(stackName,['SdrSignalLoss' num2str(iCh1)]));
            sig1N = func_stack_extract2(Flat,k1,Flat.v.import_DAT.fileN);
            
            k2 = find(ismember(stackName,['SdrSignalLoss' num2str(iCh2)]));
            sig2N = func_stack_extract2(Flat,k2,Flat.v.import_DAT.fileN);
                                              
            plot(x,sig1N(samples),x,sig2N(samples));
            title('SdrSignalLoss');
            
            linkaxes([nexttile(1), nexttile(2), nexttile(3), nexttile(4), nexttile(5), nexttile(6), nexttile(7), nexttile(8)],'x');
            xlim(onoff/Flat.scanrate)
            
            
            
%             subplot(nsubplot,1,4);
%             wingflapInd = find(ismember(stackName,'sdrWingflap'));
%             wingflap = func_stack_extract2(Flat,wingflapInd,Flat.v.import_DAT.fileN);
%             wingflap = wingflap(S.i0:S.i0+round(S.ncols));
%             plot(linspace(0,diff(onoff)/Flat.scanrate, length(wingflap)),wingflap);
%             hold on; plot([0,diff(onoff)/Flat.scanrate], [1 1]*thresWingflap,'r'); hold off;
%             xlim([0,diff(onoff)/Flat.scanrate]);
%             title('sdrWingflap')            
            
%             subplot(nsubplot,1,6);
%             carrierJumpsInd = find(ismember(stackName,'sdrCarrierJumps'));
%             carrierJumps = func_stack_extract2(Flat,carrierJumpsInd,Flat.v.import_DAT.fileN);
%             carrierJumps = carrierJumps(samples);
%             plot(linspace(0,diff(onoff)/Flat.scanrate, length(carrierJumps)),carrierJumps);
%             hold on; plot([0,diff(onoff)/Flat.scanrate], [1 1]*thresCarrierJump,'r'); hold off;
%             xlim([0,diff(onoff)/Flat.scanrate]);
%             title('sdrCarrierJumps')
            
            
            %             k2 = find(ismember(stackName,'SdrCarrierFreq2'));
            %             k3 = find(ismember(stackName,'SdrCarrierFreq3'));
            %             %sig2 = Flat.DAT.STACK{k2,Flat.v.import_DAT.fileN}(round(onoff(1)/Flat.DAT.stack(k2).nonoverlap):round(onoff(2)/Flat.DAT.stack(k2).nonoverlap));
            %             %sig3 = Flat.DAT.STACK{k3,Flat.v.import_DAT.fileN}(round(onoff(1)/Flat.DAT.stack(k3).nonoverlap):round(onoff(2)/Flat.DAT.stack(k3).nonoverlap));
            %
            % %             sig2 = func_stack_extract(Flat,k2),Flat.v.last_clicked, onoff);
            % %             sig3 = func_stack_extract(Flat,k3),Flat.v.last_clicked, onoff);
            %             sig2 = func_stack_extract2(Flat,k2,Flat.v.import_DAT.fileN); %sig2 = sig2(round(onoff(1)/Flat.DAT.stack(k2).nonoverlap):round(onoff(2)/Flat.DAT.stack(k2).nonoverlap));
            %             sig3 = func_stack_extract2(Flat,k3,Flat.v.import_DAT.fileN); %sig3 = sig3(round(onoff(1)/Flat.DAT.stack(k3).nonoverlap):round(onoff(2)/Flat.DAT.stack(k3).nonoverlap));
            %
            %             samples = round(onoff(1)/Flat.DAT.stack(k2).nonoverlap):round(onoff(2)/Flat.DAT.stack(k2).nonoverlap);
            %
            %             d = designfilt('lowpassfir', 'FilterOrder', 20, 'CutoffFrequency', 0.1);
            %             d2 = designfilt('lowpassfir', 'FilterOrder', 1000, 'CutoffFrequency', 0.0001);
            %
            %             sig2_d = filtfilt(d,sig2);
            %             sig3_d = filtfilt(d,sig3);
            %
            %             sig2_d2 = filtfilt(d2,sig2);
            %             sig3_d2 = filtfilt(d2,sig3);
            %
            %             sig2_d=sig2_d(samples);
            %             sig3_d=sig3_d(samples);
            %             sig2_d2=sig2_d2(samples);
            %             sig3_d2=sig3_d2(samples);
            %
            %             sig2=sig2(samples);
            %             sig3=sig3(samples);
            %
            %
            %             x = linspace(0,diff(onoff)/Flat.scanrate, length(sig2));
            %             subplot(nsubplot,1,1);
            %             plot(x,sig2-sig2(1),x,sig3-sig3(1)); legend('female','male');
            %             xlim([0,diff(onoff)/Flat.scanrate]);
            %
            %
            % %             subplot(nsubplot,1,2);
            % %             plot(x,filtfilt(d,abs([diff(sig2) 0])),x,filtfilt(d,abs([diff(sig3) 0]))); legend('female','male');
            % %             xlim([0,diff(onoff)/Flat.scanrate]);
            % %             subplot(nsubplot,1,3);
            % %             plot(x,abs(filtfilt(d,[diff(sig2) 0])),x,abs(filtfilt(d,[diff(sig3) 0]))); legend('female','male');
            % %             xlim([0,diff(onoff)/Flat.scanrate]);
            %
            %
            %
            % %             subplot(nsubplot,1,2);
            % %             plot(x,sig2_d2,x,sig2_d);
            % %             xlim([0,diff(onoff)/Flat.scanrate]);
            % %             subplot(nsubplot,1,3);
            % %             plot(x,sig3_d2,x,sig3_d);
            % %             xlim([0,diff(onoff)/Flat.scanrate]);
            %
            % % subplot(nsubplot,1,4);
            % % plot(x,sig2_d2-sig2_d,x,sig3_d2-sig3_d); legend('female','male');
            % % xlim([0,diff(onoff)/Flat.scanrate]);
            %
            %             subplot(nsubplot,1,5);
            %             imagesc([0,diff(onoff)/Flat.scanrate], [1 0], Flat.DAT.datas{Flat.v.import_DAT.fileN}{2}(S.i0:S.i0+round(S.ncols),:)');
            %             subplot(nsubplot,1,6);
            %             imagesc([0,diff(onoff)/Flat.scanrate], [1 0], Flat.DAT.datas{Flat.v.import_DAT.fileN}{3}(S.i0:S.i0+round(S.ncols),:)');
            %
            %             subplot(nsubplot,1,7);
            %             e = sum(Flat.DAT.datas{Flat.v.import_DAT.fileN}{3}(S.i0:S.i0+round(S.ncols),:),2);
            %             plot(linspace(0,diff(onoff)/Flat.scanrate, length(e)),e);
            %             xlim([0,diff(onoff)/Flat.scanrate]);
            %
            %             subplot(nsubplot,1,2);
            %             fs = Flat.scanrate/Flat.p.nonoverlap;
            %             nfft = 128;
            %             nhop = 16;
            %             [s,f,t, ps] = spectrogram(e,round(1*nfft),nfft-nhop,nfft, fs, 'onesided', 'yaxis');
            %             logs = 20*log10(abs(s)+0.05);
            %             image(t, f(3:end), abs(s(3:end,:))*0.001, 'CDataMapping', 'direct');
            %             colormap('jet');
            %             set(gca,'YDir', 'normal');
            %             hold on; plot([0,diff(onoff)/Flat.scanrate], [1 1]*50,'r'); plot([0,diff(onoff)/Flat.scanrate], [1 1]*70,'r'); hold off;
            %             xlim([0,diff(onoff)/Flat.scanrate]);
            %
            %             subplot(nsubplot,1,3);
            %             wingflap = abs([0;diff(e)]);
            %
            %             d_wing = designfilt('lowpassfir', 'FilterOrder', 100, 'CutoffFrequency', 0.02);
            %             wingflap = filtfilt(d_wing, wingflap);
            %             plot(linspace(0,diff(onoff)/Flat.scanrate, length(e)),wingflap);
            %             hold on; plot([0,diff(onoff)/Flat.scanrate], [1 1]*1000,'r'); hold off;
            %
            %             subplot(nsubplot,1,4);
            %             freqs = [50,70];
            %             bins = round(freqs(1)/fs*nfft)+1:round(freqs(2)/fs*nfft);
            %             plot(t,sum(abs(s(bins,:)).^2,1));
            %             hold on; plot([0,diff(onoff)/Flat.scanrate], [1 1]*2e9,'r'); hold off;
            %
            %
            %
            %             linkaxes([subplot(nsubplot,1,1),subplot(nsubplot,1,2),subplot(nsubplot,1,3),subplot(nsubplot,1,4),subplot(nsubplot,1,5),subplot(nsubplot,1,6),subplot(nsubplot,1,7)],'x');
            %colorbar;
            
            
            %             subplot(nsubplot,1,7);
            %
            %             k2 = find(ismember(stackName,'SdrSignalStrength2'));
            %             k3 = find(ismember(stackName,'SdrSignalStrength3'));
            %             sig2 = func_stack_extract2(Flat,k2,Flat.v.import_DAT.fileN); sig2 = sig2(round(onoff(1)/Flat.DAT.stack(k2).nonoverlap):round(onoff(2)/Flat.DAT.stack(k2).nonoverlap));
            %             sig3 = func_stack_extract2(Flat,k3,Flat.v.import_DAT.fileN); sig3 = sig3(round(onoff(1)/Flat.DAT.stack(k3).nonoverlap):round(onoff(2)/Flat.DAT.stack(k3).nonoverlap));
            %
            %             x = linspace(0,diff(onoff)/Flat.scanrate, length(sig2));
            %             plot(x,sig2,x,sig3); legend('female','male');
            %             xlim([0,diff(onoff)/Flat.scanrate]);
            
            %$  Plot all channels (Linus - Sdr Data)$%
            %*  *%
        elseif strcmp(modifier,'shift')
            %max(max(Flat.DAT.datas{Flat.v.import_DAT.fileN}{3}(S.i0:S.i0+round(S.ncols),:)))
            %min(min(Flat.DAT.datas{Flat.v.import_DAT.fileN}{3}(S.i0:S.i0+round(S.ncols),:)))
            
            toffset = [0 0];
            %lim = [-60, -20]+128;
            freqbins = 1:128;
            onoff=floor([S.i0+toffset(1) S.i0+round(S.ncols)+toffset(2)]*Flat.p.nonoverlap);
            figure(44);
            ax = subplot(3,1,1);
            dat1 =Flat.DAT.datas{Flat.v.import_DAT.fileN}{1}(S.i0+toffset(1):S.i0+round(S.ncols)+toffset(2),freqbins);%+128;
            %dat1(:,1:5) = lim(1);
            imagesc([0,diff(onoff)/Flat.scanrate],[0 8], dat1');%,lim);
            ax.YDir = 'normal';
            ylabel('freq. [kHz]')
            title('stationary microphone')
            ax = subplot(3,1,2);
            
            dat2 = Flat.DAT.datas{Flat.v.import_DAT.fileN}{2}(S.i0+toffset(1):S.i0+round(S.ncols)+toffset(2),freqbins);%+128;
            %dat2(:,1:5) = lim(1);
            imagesc([0,diff(onoff)/Flat.scanrate], [0 8], dat2');%,lim);
            ax.YDir = 'normal';
            ylabel('freq. [kHz]')
            title('backpack female')
            
            %lim = [-58, -30];
            ax = subplot(3,1,3);
            
            dat3 = Flat.DAT.datas{Flat.v.import_DAT.fileN}{3}(S.i0+toffset(1):S.i0+round(S.ncols)+toffset(2),freqbins);%+128;
            %dat3(:,1:4) = lim(1);
            imagesc([0,diff(onoff)/Flat.scanrate], [0 8], dat3');%,lim);
            ax.YDir = 'normal';
            ylabel('freq. [kHz]')
            xlabel('time [s]');
            title('backpack male')
        end
    
    %$ Save spectrogram to disk.$%
    %* *%
    case 's'
        vars.file = Flat.DAT.filename{Flat.v.import_DAT.fileN};
        vars.channel_mic_display = Flat.p.channel_mic_display;
        vars.scanrate = Flat.scanrate;
        vars.nonoverlap = Flat.p.nonoverlap;
        vars.onoff=floor([S.i0 S.i0+round(S.ncols-1)]*Flat.p.nonoverlap+Flat.p.nfft/2-Flat.p.nonoverlap);
        vars.onoffSec = vars.onoff / Flat.scanrate;
        vars.nfft = Flat.p.nfft;
        vars.Channel_names = Flat.p.Channel_names;
        vars.datas = Flat.DAT.datas{Flat.v.import_DAT.fileN};
        for i=1:length(vars.datas)
            vars.datas{i} = vars.datas{i}(S.i0:S.i0+round(S.ncols-1),:);
        end
        
        save('fig16dat.mat','-struct','vars');
    %$ Switch spec color limit mode (CLimMode = 'auto'/'manual').$%
        %* *%
    case 'l'
        if strcmpi(fcH.hf_import_DAT.spec.Parent.CLimMode, 'auto')
            fcH.hf_import_DAT.spec.Parent.CLimMode = 'manual';
        else
            fcH.hf_import_DAT.spec.Parent.CLimMode = 'auto';
        end
        fprintf('fcH.hf_import_DAT.spec.Parent.CLimMode = ''%s'', .CLim = [%i,%i]\n', fcH.hf_import_DAT.spec.Parent.CLimMode, fcH.hf_import_DAT.spec.Parent.CLim(1), fcH.hf_import_DAT.spec.Parent.CLim(2));
    %$  Move left in current file by one buffer.$%
    %* Moves the window to the left by one buffer. *%
    case 'comma'
        if isempty(modifier)
            Flat.v.import_DAT.i0=max(1,S.i0-1);
            [Flat,fcH]=func_import_elements_from_DAT(Flat,fcH);
            
            %$  Move left in current file by 20 buffer.$%
            %* Moves the window to the left by 20 buffer. *%
        elseif strcmp(modifier,'shift')
            Flat.v.import_DAT.i0=max(1,S.i0-20);
            [Flat,fcH]=func_import_elements_from_DAT(Flat,fcH);
        end
    %$  Move right in current file by one buffer.$%
    %* Moves the window to the right by one buffer. *%
    case 'period'
        if isempty(modifier)
            if isfield(Flat.DAT,'datas')
                L=size(Flat.DAT.datas{S.fileN}{P.X(P.imicsN).imics},1);
            else
                L=size(Flat.DAT.data{S.fileN},2);
            end
            Flat.v.import_DAT.i0=max(min(L-S.ncols+1,S.i0+1),1);
            [Flat,fcH]=func_import_elements_from_DAT(Flat,fcH);
            %$  Move right in current file by 20 buffer.$%
            %* Moves the window to the right by 20 buffer. *%
        elseif strcmp(modifier,'shift')
            if isfield(Flat.DAT,'datas')
                L=size(Flat.DAT.datas{S.fileN}{P.X(P.imicsN).imics},1);
            else
                L=size(Flat.DAT.data{S.fileN},2);
            end
            Flat.v.import_DAT.i0=max(min(L-S.ncols+1,S.i0+20),1);
            [Flat,fcH]=func_import_elements_from_DAT(Flat,fcH);
        end  
        
    case 'doesnotexist'
        
    otherwise
        return
end


assignin('base', 'Flat', Flat);
assignin('base', 'fcH', fcH);

end