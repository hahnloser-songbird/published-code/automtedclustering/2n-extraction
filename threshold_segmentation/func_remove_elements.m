function Flat = func_remove_elements(Flat, i, do_trimm, force)
% removes all indices "i" from all fields found in Flat
% indices will only be removed, if the individual fields in Flat.X have the size Flat.num_Tags
% do_trimm=1: if some files have no elements associated with them, they are
% removed from the archive
% force: RMSfilt elements can only be deleted if entire file is removed
% unless force = true

if nargin<4
    force=0;
end
if nargin<3 || isempty(do_trimm) 
    do_trimm=1;
end

Flat.v.tagboard0_recompute=1;
Flat.v.tagboard0_replot=1;
 
% if Flat.v.select_tagboard_on
%     fprintf('close tagboard for speedier operation\n');
%    [Flat.Tags,Flat.v.tagboard0,Flat.v.tagboard0i]=func_help_tagboard0(Flat.Tags,Flat.p.tagnames,Flat.timestamp(end),200); 
% end
num_Tags = Flat.num_Tags;
% 
% if Flat.p.do_raw && ~(length(i)==Flat.num_Tags)
%     indices=sort(i,'descend');
%     [Flat.V.pre Flat.V.post]=func_sequence_context(Flat,indices);
%     for j=indices
%         if Flat.V.pre(max(1,j-1))>0
%             Flat.raw{max(1,j-1)}=[Flat.raw{max(1,j-1)} Flat.raw{j}];
%         elseif Flat.V.post(j)>0
%             Flat.raw_pre{j+1}=[Flat.raw_pre{j} Flat.raw{j}];         
%         end
%     end
% end

%%%%%%%%%%%%%%%%%

if isfield(Flat,'Tags') && isfield(Flat.v,'RMS') && ismember(Flat.v.RMS,unique(Flat.Tags(Flat.v.tags.activator,i))) && ~force
    disp('RMSfilt elements should only be deleted if entire file is removed - nothing deleted!');
    y=input('Do you know what you are doing (y==1) ? ');
    if ~(y==1)
        yes=input(' Are you sure you want to do this (yes==1) ');
        if ~(yes==1)
            return
        end
    end
end
% 
% if isfield(Flat,'Z') & isfield(Flat.Z,'stencil')
%     for j=1:length(i)
%         Flat.Z.stencil(Flat.X.custom(1,i(j)):Flat.X.custom(2,i(j)))=0;
%     end
% end

%% 3 functions: func_remove_elements, func_append_zeros_meta, func_reorder_by_indices_meta
%Flat = func_remove_routine_old(Flat, i, num_Tags);
Flat.X = func_remove_routine(Flat.X, i, num_Tags);
Flat.V = func_remove_routine(Flat.V, i, num_Tags);
%Flat.v = func_remove_routine_old(Flat.v, i, num_Tags);
try 
    Flat.Tags(:,i)=[];
catch
    (' ### warning: Reference to non-existent field .Tags %n continued..');
end

% if isfield(Flat.v,'PCcoeffs')
%     for j=1:length(Flat.v.PCcoeffs)
%         Flat.v.PCcoeffs{i}(:,j)=[];
%     end
% end

if isfield(Flat,'umap') && isfield(Flat.v,'RMS') && ~isempty(Flat.v.RMS) && strcmp(Flat.v.RMS(1),'#')
    s=find(ismember(Flat.v.select,i));  
    Flat.umap.X = func_remove_routine(Flat.umap.X, s, length(Flat.v.select)); % RRR
end

%Flat.v.DATspk_done(i)=[];
%Flat.v.DATspk(i)=[];
% if isfield(Flat.p,'CHANNEL_MIC')
%     if isfield(Flat.v,'PCcoeffs')
%         for j=1:length(Flat.v.PCcoeffs)
%             if size(Flat.v.PCcoeffs{j},2)==num_Tags
%                 Flat.v.PCcoeffs{j}(:,i)=[];
%             else
%                 Flat.v.PCcoeffs{j}=[];
%             end
%         end
%     else
%         Flat.v.PCcoeffs=cell(1,length(Flat.p.CHANNEL_MIC));
%     end
% else
%     Flat.v.PCcoeffs={};
% end

%%%%%%%%%%%%%%%%

%% Boards
if isfield(Flat,'BOARDLIST')
    for j=1:length(Flat.BOARDLIST)       
        if isfield(Flat.BOARDLIST(j),'spec_i') && ~isempty(Flat.BOARDLIST(j).spec_i)
           Flat.BOARDLIST(j).spec_i=Flat.BOARDLIST(j).spec_i-sum(le(i,Flat.BOARDLIST(j).spec_i));
           if  Flat.BOARDLIST(j).spec_i==0
               Flat.BOARDLIST(j).spec_i=[];
           end
        end
    end
end

if isfield(Flat,'lists') && isfield(Flat.lists,'i')
    Flat.lists.i = func_remove_routine_old(Flat.lists.i, i, num_Tags);
end

if isfield(Flat,'user')
    Flat.user = func_remove_routine_old(Flat.user, i, num_Tags);
end

h = Flat.X.clust_ID(find(ismember(Flat.Tags(1,:),'1')));
Flat.num_classified = length(unique(h));
Flat.num_classified=0;
Flat.num_Tags = length(Flat.X.indices_all);
Flat.templates(Flat.templates > Flat.num_Tags) = 0;
Flat.v.DATspk_done=zeros(1,Flat.num_Tags);
Flat.v.DATspk=cell(1,Flat.num_Tags);


if do_trimm
%Flat=func_trimm_file_list3(Flat,do_trimm);
Flat=func_help_correct_DAT_timestamp_ordering(Flat,do_trimm);
end

Flat.v.run_select=1:length(Flat.X.indices_all);
Flat.v.run_select_clust=1:length(Flat.X.indices_all);

% [Flat Flat.v.select Flat.v.select_clust]=func_select_mode(Flat);


end

%% HELPERFUNCTION
function Flat = func_remove_routine_old(Flat, p, num_Tags)

    fn = fieldnames(Flat);
    for i = 1 : length(fn)
        if size(Flat.(fn{i}), 2) == num_Tags
            Flat.(fn{i})(:,p) = [];
        end
    end
end
%% EOF