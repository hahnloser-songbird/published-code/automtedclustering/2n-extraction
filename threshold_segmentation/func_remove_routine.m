function X = func_remove_routine(X, p, num_Tags)

    fn = fieldnames(X);
    for i = 1 : length(fn)
        if size(X.(fn{i}), 2) == num_Tags
            X.(fn{i})(:,p) = [];
        else
            error(['func_remove_elements: Problem with field ' fn{i} ' !!!']);
        end
    end
end