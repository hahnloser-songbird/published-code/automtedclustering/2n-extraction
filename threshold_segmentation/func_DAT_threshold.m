function [Flat,thresh]=func_DAT_threshold(Flat,P,save_thresh, Nstd)
% the argument P is optional
% keep_v=1 is to prevent changes of Flat.v

if nargin<4
    Nstd=5;
end
if nargin<3
    save_thresh=1;
end
if nargin<2 || isempty(P)
    P=Flat.p.syllable;  P.imicsN=Flat.p.channel_mic_display;
    SM=P.(P.method)(P.imicsN);
    if ~isfield(P,'els') || P.els(1)~=floor(SM.low/Flat.scanrate*Flat.p.nfft) || P.els(end)~=floor(SM.high/Flat.scanrate*Flat.p.nfft)
        P.els=max(floor(SM.low/Flat.scanrate*Flat.p.nfft),1):floor(SM.high/Flat.scanrate*Flat.p.nfft);
    end
end

if length(Flat.p.channel_mic_display)>1
    disp('More than one channel displayed! Please only display a single channel before running this Program...')
    return
end


Twins=0.5;
alpha=0.1;
Nbuffs=floor(Twins*Flat.scanrate/Flat.p.nonoverlap); % per second, this is the search interval for silence periods
%Nint=3;
%fprintf('Search for %d intervals of silence (black)\n',Nint);
fprintf('threshold = %d stds above mean intensity (average across %d percent %d ms-windows with lowest std)\n',Nstd,100*alpha,Twins*1000);
l=length(Flat.DAT.filename);

%Nall=sum(Flat.DAT.eof)/Flat.p.nonoverlap;


silAll=cell(1,l); sAll=silAll;

for i=1:l
    S.fileN=i; S.i0=1; S.ncols=func_numbuffs(Flat.DAT.eof(i),Flat.p.nfft,Flat.p.nonoverlap);
    
    [s,D,L]=func_pseudo_RMS_from_spec(Flat,S,P,1,Flat.p.channel_mic_display);
    sAll{i}=s';
    %     if 1
    %         li=length(s);
    %         figure(13); clf; plot(1:li,s); hold on; plot([li-Nbuffs li-Nbuffs], [min(s) max(s)],'k');
    %         plot(li-Nbuffs:li, s(li-Nbuffs:li),'r');
    %         fprintf('%d - ',cs);
    %         y=input('ok (yes=1)?');
    %         if y==1
    %             silAll{i}=s(li-Nbuffs:li)';
    %             cs=cs+1;
    %         end
    %         if cs==Nint 
    %             break
    %         end
    %     end
end
sALL=[sAll{:}];
L=floor(length(sALL)/Nbuffs);
ci=0;
M=zeros(1,L); S=M;
for i=1:L
    M(i)=mean(sALL(ci+1:ci+Nbuffs));
    S(i)=std(sALL(ci+1:ci+Nbuffs));
    ci=ci+Nbuffs;
end
s=[silAll{:}];
%
% figure(11);clf; [y,x]=hist(s,100);
% plot(x,y); m=mean(s); st=std(s);
% hold on; plot([m m],[min(y) max(y)],'k');
% plot([m-st m-st],[min(y) max(y)],'k--');
% plot([m+st m+st],[min(y) max(y)],'k--');
%
% thresh=m+5*st

% dy=diff(y);
% figure(12); clf; plot(x(1:100),dy(1:100)); [dummy,thresh]=min(dy(1:100));
% figure(12);clf; %subplot(211);hist(M,100); xlabel('Mean'); subplot(212);hist(S,100); xlabel('Std');
% plot(M,S,'x');
[Smin,Smini]=sort(S,'ascend');
L05=floor(L*alpha);
% hold on;
% plot(M(Smini(1:L05)),Smin(1:L05),'rx');
% xlabel('Mean'); ylabel('Std');

m=mean(M(Smini(1:L05))); st=sqrt(mean(Smin(1:L05).^2));
thresh=m+Nstd*st;
ms=sort(M,'ascend');
fi=find(m>ms,1,'last');
if fi/length(M)>3*alpha
    disp('Problem - presumably some large and stable background noise in recording');
    pause
end

% i=1;
% li=length(sAll{i});
% figure(13); clf; plot(1:li,sAll{i}); hold on; plot([1 li],thresh*[1 1],'r');
if save_thresh
    Flat.v.DATthresh.Nstd=Nstd;
    Flat.v.DATthresh.Twins=Twins;
    Flat.v.DATthresh.alpha=alpha;
    Flat.p.syllable.(P.method)(P.imicsN).thets=thresh;
end
end