function  [Flat,select]=func_write_imported_elements_to_Flat(Flat,ons,offs,clust_IDs,custom_sorts,new_flat,Tags, filename, do_reset,tag_str,do_pca)

%% Input
%  new_flat  1=new Flat from scratch, 0= no new Flat
%  do_reset  1=   func_Flat_reset(Flat) called at the end to initial Flat
if nargin<11
    do_pca=1;
end
if nargin<10
    tag_str='99';
end

if ismember({tag_str},Flat.v.RMS)
    Flat.v.DATSongDur=[];
end

%% Tags: idea is to inherit everything except activator and except content
%% of 'Tags', which is written explicitly to the newly added elements

% tag_str   activator string
% it is possible to create a new index and specify what filename should be
% used.


%if nargin<11
%    start_pos=Flat.num_Tags;
%end

if nargin <9
    do_reset=1;
end

if nargin < 8
    filename = [];
end

if nargin < 7
    Tags = [];
end

% inherit: e.g. when a new tag falls within an old one (onset time after, offset time before old tag), 
% then that tag would be inherited, if it is from an inheritable tagfield.
if isfield(Flat,'p')
    inherit = 1:length(Flat.p.tagnames); % by default, inherit everything
end

if ~isempty(Tags)
    for i=1:length(Tags)
        if ~isempty(Tags{i})
            inherit=setdiff(inherit, find(sum(~cellfun(@isempty, Tags{i}), 2)));
      %      inherit=setdiff(inherit, find(~cellfun(@isempty, Tags{i})));
        end
    end
end

% if isempty(Tags)
%     dont_inherit = [];
% else
%     for i=1:length(Tags)
%         if ~isempty(Tags{i})
%             dont_inherit=union(dont_inherit, find(sum(~cellfun(@isempty, Tags{i}), 2)));
%         end
%     end
% end


if isfield(Flat.v.tags,'activator')
    if find(Flat.v.tags.activator==inherit)
        %         fprintf('Problem!\n');
        %         return
        %     else
        % never inherit the activator
        inherit=setdiff(inherit,Flat.v.tags.activator);
    end
end


%% problem if archive empty!
scanrate=Flat.scanrate;
birdname=Flat.birdname;
%file_extension=Flat.filenames{1}(end-2:end);

if new_flat
    % need to extract tags/file here, since all elements in flat will be
    % deleted
    %% inherit Tags from same file
    if ~isfield(Flat.p,'tagnames_per_file')
        Flat.p.tagnames_per_file=zeros(length(Flat.p.tagnames),1);
    end
    if length(Flat.p.tagnames_per_file)<length(Flat.p.tagnames)
        Flat.p.tagnames_per_file(end+1:length(Flat.p.tagnames))=0;
    end
    str = Flat.p.tagnames(find(Flat.p.tagnames_per_file));
    if ~isempty(str)
        fprintf('File tags are:\n');
        for i=1:length(str)
            fprintf('%s ',str{i});
        end
        fprintf('\n');
    end
    tags_i=find(Flat.p.tagnames_per_file);
    for i=1:get_number_of_recordings(Flat)
        hi=find(Flat.X.DATindex==i);
        Flat.Tags(tags_i,hi)=Flat.DAT.Tags(tags_i,i);
    end
    %         tags_i=find(Flat.p.tagnames_per_file);
    %         Flat.DAT.Tags = cell(max(tags_i), get_number_of_recordings(Flat));
    %         for i=1:get_number_of_recordings(Flat)
    %             hi=find(Flat.X.DATindex==i);
    %             if isempty(hi)
    %                 continue
    %             end
    %             for j=1:length(tags_i)
    %                 u=unique(Flat.Tags(tags_i(j),hi));
    %                 if length(u)==1
    %                     hi2=find(Flat.X.DATindex(Flat.num_Tags+1:end)==i);
    %                     Flat.DAT.Tags(tags_i(j), i) = u;
    %                 elseif length(u) == 0
    %                     fprintf('No tags in file');
    %                 else
    %                     fprintf('Problem: File tag has multiple strings in a single file\n');
    %                 end
    %             end
    %         end
    
    Flat=func_remove_elements(Flat,1:Flat.num_Tags,0);
end

DATi=find(~cellfun(@isempty,ons));
nO=Flat.num_Tags; % old number of elements?
%nO=start_pos;  % new number of elements?
nN=sum(cellfun(@length,ons(DATi)));
fprintf('Now writing %d imported elements to Flat (currently %i elements)\n',nN, nO);

Flat=func_append_zeros_meta(Flat,nO,nN);
%Flat.v=func_append_zeros(Flat.v,nO,nN);
if isfield(Flat,'user')
    Flat.user=func_append_zeros_old(Flat.user,nO,nN);
end


for i0=1:length(DATi)
%     if i0==40
%         keyboard
%     end
%     i0
% if length(Flat.X.indices_all)>nO
%     keyboard
% end
    i=DATi(i0);
    l=length(ons{i});
    Flat.X.indices_all(nO+1:nO+l)=ons{i};
    Flat.X.data_off(nO+1:nO+l)=offs{i}-ons{i};
%     if any(offs{i}-ons{i}<0)
%         continue
%     end
    
    %Flat.filenames(nO+1:nO+l)={[Flat.DAT.filename{i}(1:end-3) file_extension]};
    
    
    %% make sure to use the correct filenames / subdir / timestamp / spec
    if isempty(filename)
        index_to_use = i;
    else
        index_to_use = find(strcmp(Flat.DAT.filename, filename));
        % in the worst case, we have to fall back to 'i'
        if isempty(index_to_use)
            disp(['could not find DAT.filename entry for this file. Will assign to file #' num2str(i)]);
            index_to_use = i;
        elseif numel(index_to_use) > 1
            disp('File is more than once in Flat.DAT, will use first index');
            index_to_use = index_to_use(1);
        end
    end
    
    %% the following fields need to use the correct index!
    % Flat.filenames(nO+1:nO+l) = {Flat.DAT.filename{index_to_use}}; %YR comment it? %
    % Flat.subdir(nO+1:nO+l) = {Flat.DAT.subdir{index_to_use}}; %YR comment it? %
    % Flat.timestamp(nO+1:nO+l) = Flat.DAT.timestamp(index_to_use);
    if ~Flat.p.spec_full_mode && isfield(Flat.DAT,'spec') && ~isempty(Flat.DAT.spec{index_to_use})
        Flat.X.data(nO+1:nO+l) = Flat.DAT.spec{index_to_use};
        %     else
        %         keyboard
    end
    Flat.X.DATindex(nO+1:nO+l) = index_to_use;
%     if any(index_to_use==0)%length(Flat.X.DATindex)<length(Flat.X.indices_all)
%         keyboard
%     end
    %   Flat.DAT.eof(Flat.X.DATindex(nO+1:nO+l)) = Flat.DAT.eof(Flat.X.DATindex(index_to_use));
    
    
    %Flat.scanrate(nO+1:nO+l) = {scanrate}; % ugly
    Flat.scanrate=scanrate;
    Flat.birdname=birdname; % XXX, ugly, should do check
    
    %% remaining stuff
    if isempty(clust_IDs)
        Flat.X.clust_ID(nO+1:nO+l)=Flat.p.num_clust; % put into last cluster
    else
        % Flat.X.clust_ID is only int8, so tell user that something might go
        % wrong
        if sum(clust_IDs{i} > 255)
            disp(['clustIDs: ' num2str(clust_IDs{i}) ' will be clipped!']);
        end
        Flat.X.clust_ID(nO+1:nO+l)=clust_IDs{i};
    end
    
    if ~isempty(custom_sorts)
        if isfield(Flat.X,'custom') & ~isequal(size(Flat.X.custom,1),size(custom_sorts{i},1))
            Flat.X.custom(size(Flat.X.custom,1)+1:size(custom_sorts{i},1),:)=0;
        end
        Flat.X.custom(:,nO+1:nO+l)=custom_sorts{i};
    end
    
    %% inherit Tags
    %    Flat.Tags(:,nO+1:nO+l) = cell(size(Flat.Tags(:,nO+1:nO+l)));
    write_tagstr=true;
    if ~isempty(Tags) && ge(length(Tags),i) && ~isempty(Tags{i})
        %  Flat.Tags(:,nO+1:nO+l) = Tags(i); % RH needed to fix
        Flat.Tags(:,nO+1:nO+l) = Tags{i}; % RH needed to fix
        %  func_import_elements_from_DAT_write_to_Flat 10.7.2017
        if ~isfield(Flat.v.tags,'activator') || any(~cellfun(@isempty,Tags{i}(Flat.v.tags.activator,:)))
            write_tagstr=false;
        end
    end
    
    %     if ~isempty(inherit) % do you want ot inherit?
    %         % check if Tags is empty, if so, then generate a cell array of the
    %         % correct size
    %         if isempty(Tags)
    %             Flat.Tags(:,nO+1:nO+l) = cell(size(Flat.Tags(:,nO+1:nO+l)));
    %         else
    %             Flat.Tags(:,nO+1:nO+l) = Tags{i};
    %         end
    %     end
    
    if isfield(Flat.v,'tags') && isfield(Flat.v.tags,'activator') && write_tagstr
        Flat.Tags(Flat.v.tags.activator,nO+1:nO+l)={tag_str};
    end
    if ~isempty(Flat.PCs) && size(Flat.PCs,1)>1 && ~isempty(Flat.DAT.data{1}) % RH hack, to consider the case in which spectrograms are not loaded in Lfilt
        %                Flat=func_Flat_PCA(Flat,0);
        if do_pca
            spec_data = func_extract_data(Flat, 5, nO+1:nO+l,1);
            try
                Flat.X.PCcoeffs(:,nO+1:nO+l) = Flat.PCs * (spec_data-repmat(Flat.PCmean,1,size(spec_data,2))); % coefficients (compressed data)
            catch bla
                %fprintf('PCA error - run PCA first!\n');
                Flat.X.PCcoeffs(:,nO+1:nO+l)=0;
            end
        else
            Flat.X.PCcoeffs(:,nO+1:nO+l)=0;
        end
    end
    if length(Flat.p.CHANNEL_MIC)>1
        for ii=1:length(Flat.p.CHANNEL_MIC)
            if isfield(Flat.V,['PCcoeffs' num2str(ii)])
                Flat.V.(['PCcoeffs' num2str(ii)])(:,nO+1:nO+l)=0;
            end
        end
    end
    nO=nO+l;
end


%% dont' overwrite the "Unit" and the "activator" tags
% keyboard
% to_remove = [];
%tags_i=1:length(Flat.p.tagnames);
%
% unit_id = find(strcmp(Flat.p.tagnames, 'Unit'));
% if ~isempty(unit_id)
%     to_remove = [to_remove unit_id];
% end

% if isfield(Flat.v.tags,'activator')
% %    to_remove = [to_remove Flat.v.tags.activator];
% tags_i(Flat.v.tags.activator)=[];
% end

% tags_i(to_remove)=[];

%% inherit Tags from same file
if ~isfield(Flat.p,'tagnames_per_file')
    Flat.p.tagnames_per_file=zeros(length(Flat.p.tagnames),1);
end

if length(Flat.p.tagnames_per_file)<length(Flat.p.tagnames)
    Flat.p.tagnames_per_file(end+1:length(Flat.p.tagnames))=0;
end

str = Flat.p.tagnames(find(Flat.p.tagnames_per_file));
if ~isempty(str)
    fprintf('File tags are:\n');
    for i=1:length(str)
        fprintf('%s ',str{i});
    end
    fprintf('\n');
end

tags_i=find(Flat.p.tagnames_per_file);

%% XXX hack
% emT=find(cellfun(@isempty,Flat.Tags));
% Flat.Tags(emT)={''};

%if new_flat
 %   use the previously extracted tags/file
    for j=1:length(tags_i)
        for k = 1:get_number_of_recordings(Flat) 
            Flat.Tags(tags_i(j), Flat.X.DATindex == k) = Flat.DAT.Tags(tags_i(j), k);
        end
    end
% else %%% RRR removed Cannes 2017
%     for i0=1:length(DATi)
%         i=DATi(i0);
%         hi=find(Flat.X.DATindex(1:Flat.num_Tags)==i);
%         if isempty(hi)
%             continue
%         end
%         for j=1:length(tags_i)
%             u=unique(Flat.Tags(tags_i(j),hi));
%             if length(u)==1
%                 hi2=find(Flat.X.DATindex(Flat.num_Tags+1:end)==i);
%                 Flat.Tags(tags_i(j),Flat.num_Tags+hi2)=u;
%             else
%                 fprintf('Warning: File tag has multiple strings in a single file\n');
%             end
%         end
%     end
% end

%% inherit Tags from earlier Tags (except activator)
% (if one exists, or else inherit from a later Tag)
inherit=setdiff(inherit,tags_i);

file_timestamp_old = to_row_vector(Flat.DAT.timestamp(Flat.X.DATindex(1:Flat.num_Tags))*24*3600);
element_onset_old = to_row_vector(Flat.X.indices_all(1:Flat.num_Tags)/scanrate);

file_timestamp_new = to_row_vector(Flat.DAT.timestamp(Flat.X.DATindex(Flat.num_Tags+1:nO))*24*3600);
element_onset_new = to_row_vector(Flat.X.indices_all(Flat.num_Tags+1:nO)/scanrate);

times_old= file_timestamp_old+element_onset_old;
times_new=file_timestamp_new+element_onset_new;


[times, times_i]=sort([times_old times_new]);
curr_old=1;

%to_inherit=setdiff(tags_i,dont_inherit);
for i=1:nO
    if times_i(i)<Flat.num_Tags+1
        curr_old=times_i(i);
    else
        Flat.Tags(inherit,times_i(i))=Flat.Tags(inherit,curr_old);
    end
end

%%
Flat.num_Tags=nO;
Flat.v.DATspk_done=zeros(1,Flat.num_Tags);
Flat.v.DATspk=cell(1,Flat.num_Tags);

Flat=func_reorder_by_time(Flat);

Flat.v.tagboard0_recompute=1;
Flat.v.tagboard0_replot=1;


%% Short_RMS
S = {'RMS(500Hz-7kHz)','RMS(400Hz-5kHz)'}; %added by Alexei 05.04.2013, corrected 09.04.2013
[tf,loc]=ismember(tag_str,S);                        %added by Alexei 05.04.2013
if loc>0                           %strcmp(tag_str,'RMS(500Hz-7kHz)')
    min_syl_dur_ms=40;
    [Flat,hi99]=func_select_tagboard(Flat,[],'activator',S(loc));%RMS(500Hz-7kHz)
    Flat.Tags(Flat.v.tags.activator,hi99)={Flat.v.RMS}; %{'RMSfilt'};
    short_syls=Flat.X.data_off(hi99)<min_syl_dur_ms/1000*Flat.scanrate;
    Flat.Tags(Flat.v.tags.activator,hi99(short_syls))={[Flat.v.RMS 'S']};
    Flat.v.tagboard0_recompute=1;
    Flat.v.tagboard0_replot=1;
    
    %% graph_context
    %  [Flat,hiRMS]=func_select_tagboard(Flat,[],'activator',{'RMSfilt'});
    %  [Flat,Flat.v.graph_context,Flat.Tags] = func_help_graph_context(Flat,hiRMS);
    
end


Flat.v.run_select=1;
Flat.v.run_select_clust=1;

%% redefine Flat.v.tagboard0 and Flat.v.select
[Flat Flat.v.select Flat.v.select_clust]=func_select_mode(Flat);

Flat.v.plot_tagboard=1;

if ~Flat.p.spec_full_mode && isfield(Flat.DAT,'spec')
    Flat.DAT=rmfield(Flat.DAT,'spec');
end

if isfield(Flat.v,'Song_on_off_do') &&  isfield(Flat.p,'DATSong_on_off_min_dur_ms') && Flat.v.Song_on_off_do==1
    [Flat,Flat.DAT.Song_on_off,Flat.Tags]=func_DAT_Song_on_off(Flat,Flat.p.DATSong_on_off_min_dur_ms,Flat.p.DATSong_on_off_inter_song_ms,Flat.p.DATSong_on_off_margin_ms,0);
%    Flat=func_DAT_Song_on_off_to_board(Flat);
    Flat.v.Song_on_off_do=0;
end

if do_reset
    Flat=func_Flat_reset(Flat);
end

end
%% EOF


function a = to_row_vector(a)

sz = size(a);
if sz(1) > 1 && sz(2)==1
    a = a';
end
end
