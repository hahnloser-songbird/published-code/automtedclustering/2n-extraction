function [Flat,select,select_clust]=func_select_mode(Flat,BOARD)

%
% %%% @CL: circumvent that we don't have a tagboard
if size(Flat.Tags, 1)>4
    if any(strcmp(unique(Flat.Tags(5,:)), '@RMSfilt'))
        
        select= find(strcmp(Flat.Tags(5,:), '@RMSfilt'));
        select_clust=find(strcmp(Flat.Tags(5,:), '@RMSfilt'));
        
    else
        Flat.v.all_clust_mode = 1;
        select= find(strcmp(Flat.Tags(5,:), 'RMSfilt'));
        select_clust=find(strcmp(Flat.Tags(5,:), 'RMSfilt'));
    end
    


    if Flat.v.run_select_clust
        if Flat.v.all_clust_mode
            select_clust=select;
        else
            select_clust=func_select_elements_clust(Flat,Flat.v.curr_clust,select);
        end
    else
        select_clust=Flat.v.select_clust;
    end
    
elseif size(Flat.Tags, 1)<=4 && nargin == 1
    
    [Flat,select]=func_select_tagboard(Flat,Flat.BOARD.tagboard);
     if Flat.v.all_clust_mode
        select_clust=select;
    else
        select_clust=func_select_elements_clust(Flat,Flat.v.curr_clust,select);
    end
end

return
% %%%


if Flat.v.run_select
    %     switch tagview
    %         case 0
    %             select=1:Flat.num_Tags;
    %         case 1
    %             [Flat,select]=func_select_tagboard(Flat,[],Flat.p.tagnames{Flat.v.tags_curr_i},Flat.v.tags_curr_ij{Flat.v.tags_curr_i});
    %
    %         case 2
    if nargin==2
        [Flat,select]=func_select_tagboard(Flat,BOARD.tagboard);
    else
        [Flat,select]=func_select_tagboard(Flat,Flat.BOARD.tagboard);
    end
    %    end
else
    select=Flat.v.select;
end
