function Flat=func_import_elements_from_DAT_write_to_Flat(Flat,import_channels,P,do_pca)
%P.askjoin: -1=ask, 1=join
% (spectrogram) mask: =1 if syl, =0 otherwise

if nargin<4
    do_pca=1;
end

if isfield(Flat.v,'import_DAT')
    S=Flat.v.import_DAT;
else
    S.fileN = 1;
    S.i0=1;
    S.ncols=Flat.v.Screensize(1);
end
%P=Flat.p.syllable;
% if Flat.v.tsne.clust_dense_mode
%     str='D';
% else
%     str='';
% end

if isempty(P.syl)
    P.syl=Flat.p.syllable;
else
    Flat.p.syllable=P.syl;
end

%% add activatorM is not done yet
%Flat=func_help_tagname_add(Flat,'activatorM',0,'',0);

for k=import_channels
    %    [Flat,selection]=func_select_tagboard(Flat,[],Flat.p.tagnames(Flat.v.tags.activator),{Flat.p.Channel_names{P.imics(k)},[str,Flat.p.Channel_names{P.imics(k)}]});
    selection=func_select_mic(Flat,P.X(k).imics,1);
    Flat=func_remove_elements(Flat,selection,0,1);
end


if Flat.v.tsne.clust_dense_mode==1
    if P.askjoin==-1
        y=input('Do you want to consider joining short syllables into longer ones (yes=1) ? ');
    else
        y=P.askjoin;
    end
    
    if y==1
        options.join=1;
        Flat.p.syllable.opt=1;
    else
        options.join=0;
        Flat.p.syllable.opt=0;
    end
end

% Compute syllable on & offsets
if isfield(Flat.p, 'umap')
    numbuffs_remove=Flat.p.umap.MinNumBuffSyl-Flat.p.umap.MinNumTimeSegments; %number of buffers to remove
end
l=length(import_channels);
L=get_number_of_recordings(Flat);
clust_ID_new=length(Flat.p.clust_names)-1;
onsA=cell(l,L);
offsA= onsA; custsA=onsA; clust_IDs=onsA; %TagsA=cell(l,L);%cell(
%if nargout>1
mask=cell(l,L);
%end
t0=Flat.DAT.timestamp(1);
Z.buffer=int32(zeros(1,1e8));
Z.DATindex=int32(zeros(1,2*L));
Z.indices_all=int64(zeros(1,2*L));

Z.Yindex=Z.buffer;
Y.Zindex=Z.buffer;

cZ=0; cY=0;
for i=1:L
    %   if nargout>1 %&& length(Flat.p.CHANNEL_MIC)>1
    %        mask{i}=logical(zeros(l,size(Flat.DAT.datas{i}{1},1)));
    mask{i}=logical(zeros(l,size(Flat.DAT.data{i},2)));
    %   end
    %    TagsA1=cell(length(Flat.p.tagnames),L);
    ki=0;
    for k=import_channels
        try
            winS=Flat.p.syllable.X(k).numbuffs;
        catch
            winS = 16;
        end
        thet=P.(P.method)(k).thets;
        thetU=P.(P.method)(k).thetsU;
        P.imicsN=k;
        P.els=max(floor(P.(P.method)(k).low/Flat.scanrate*Flat.p.nfft),1):floor(P.(P.method)(k).high/Flat.scanrate*Flat.p.nfft);
        S.fileN=i;
        sd=func_pseudo_RMS_from_spec(Flat,S,P,1,k);
        %   masknoise=Flat.DAT.maskDiffNoise{S.fileN}(k,:);
        
        if Flat.v.tsne.clust_dense_mode==2 % everything suprathreshold is a syllable!
            sup_thresh=sd>thet;
            % widen by 1
            %sup_thresh2=conv(sup_thresh,[1 1 1]);
            %sup_thresh=sup_thresh2(2:end-1)>0;
            %figure(11); clf; figure;plot(sup_thresh); hold on; plot(sup_thresh2>0,'r--');
            %length(sup_thresh2)-length(sup_thresh)
            %keyboard
            sup_threshB=[0 sup_thresh(:)' 0];
            onsh=find(diff(sup_threshB)==1); offsh=find(diff(sup_threshB)==-1)-1;
            %
            %             onsh=1+find(diff(sup_thresh)==1); offsh=find(diff(sup_thresh)==-1);
            %             if isempty(onsh) || isempty(offsh)
            %                 continue
            %             end
            %             if onsh(1)<Flat.p.umap.MinNumBuffSyl
            %                 onsh=onsh(2:end);
            %                 offsh=offsh(2:end);
            %             end
            %             if length(onsh)>length(offsh)
            %                 onsh=onsh(1:end-1);
            %             end
            %             if length(offsh)>length(onsh)
            %                 offsh=offsh(2:end);
            %             end
            Bfile=Flat.scanrate/Flat.p.nonoverlap*(Flat.DAT.timestamp(i)-t0)*86400;
            ons=[];custs=[];custs2=[];
            for j=1:length(onsh)
                if offsh(j)-onsh(j)>=Flat.p.umap.MinNumBuffSyl-1 % syllable must contain at least Flat.p.umap.MinNumBuffSyl+1 buffers
                    newi=onsh(j):(offsh(j)-numbuffs_remove); % ignore last few buffers of syllable (all syllables look alike there)
                    ons=[ons newi];
                    custs=[custs 1:length(newi)];
                    custs2=[custs2 -length(newi):-1];
                    
                    dZ=offsh(j)-onsh(j);
                    Z.buffer(cZ+1:cZ+1+dZ)=round(Bfile+(onsh(j):offsh(j)));
                    Z.DATindex(cZ+1:cZ+1+dZ)=i;
                    Z.indices_all(cZ+1:cZ+1+dZ)=Flat.p.nfft+round(((onsh(j)-1):(onsh(j)-1+dZ))*Flat.p.nonoverlap);

                    dY=dZ-numbuffs_remove;
                    Z.Yindex(cZ+1:cZ+1+dY)=(cY+1):(cY+1+dY);
                    Y.Zindex(cY+1:cY+dY+1)=(cZ+1):(cZ+dY+1);
                    cY=cY+dY+1;
                    cZ=cZ+dZ+1;
                end
            end
            c=length(custs);
            
            offs=ons+1;
            
            custsA{k,i}=[custs;custs2;0*custs;0*custs];
            if isfield(Flat.X,'custom') && size(Flat.X.custom,1)==1
                Flat.X=rmfield(Flat.X,'custom');
            end
            
        elseif Flat.v.tsne.clust_dense_mode==1 % dense mode (two threshold)
            Flat.p.min_syl_dur_buff=40/1000*Flat.scanrate/Flat.p.nonoverlap;
            % figure(5);clf; imagesc(Flat.DAT.data{i}); axis xy
            [ons,offs,thrLow,onofflow]=func_help_ons_offs3(sd,thet,thetU,winS,Flat.p.min_syl_dur_buff,options);
            Flat.v.tsne.on_off_options=options;
            Flat.v.pca_min=min(min(Flat.DAT.data{1}));
            custsA{k,i}=onofflow;
        else % old fashioned
            [ons,offs,maxs]=func_help_ons_offs(sd,thet);
            custsA{k,i}=maxs;
        end
        %end
        
        %% write to Flat
        onsA{k,i}=Flat.p.nfft+round((ons-1)*Flat.p.nonoverlap);
        offsA{k,i}=Flat.p.nfft+round((offs-1)*Flat.p.nonoverlap); %[offs{file_with_playback} candidate_offsets];
        offsA{k,i}=max(offsA{k,i},onsA{k,i}+1); % make sure onsA~=offsA due to rounding!
        clust_IDs{k,i}=clust_ID_new;%[clust_IDs{file_with_playback} Flat.X.clust_ID(candidate_elements)];
        % TagsA{k,i}=cell(length(Flat.p.tagnames),length(ons));
        % TagsA{k,i}(:)={''};
        %TagsA1=cell(length(Flat.p.tagnames),length(ons));
        %        TagsA{k,i}(Flat.v.tags.activatorM)={tagstringM};
        %    TagsA{k,i}(Flat.v.tags.activatorM,:)={tagstringM};
        %     TagsA{k,i}(Flat.v.tags.activator,:)={'RMSfilt'};
        %TagsA{k,i}{Flat.v.tags.activatorM}=tagstringM;
        % if nargout>1
        ki=ki+1;
        for ii=1:length(ons)
            mask{i}(ki,ceil(ons(ii)):ceil(offs(ii)))=1;
        end
        if isfield(Flat.p,'DATmasknoise') &&  Flat.p.DATmasknoise(ki)
            try
                Flat.DAT.mask{i}(ki,:)=Flat.DAT.mask{i}(ki,:) | mask{i}(ki,:); %% add mask not to loose the old mask
            end
        else
            if isfield(Flat.p,'skype')
                Flat.DAT.mask{i}(ki,:)=mask{i}(ki,:);
            else
                Flat.DAT.mask{i}(ki,:)=0;%mask{i}(ki,:);
            end
        end
        
        %end
        %TagsA{k}=TagsA1;
    end
    %     if i==61
    %         keyboard
    %     end
end
Flat.Z.buffer=Z.buffer(1:cZ);
Flat.Z.DATindex=Z.DATindex(1:cZ);
Flat.Z.indices_all=Z.indices_all(1:cZ);
Flat.Z.Yindex=Z.Yindex(1:cZ);
Flat.Y.Zindex=Y.Zindex(1:cY);

% if Flat.p.DATmask(import_channels
% for i=1:get_number_of_recordings(Flat)
%     Flat.DAT.mask{i}(import_channels,:)=mask{i};
% end
% %end

%% TSNE stuff
%Tagstr='RMSfilt';%MMVer2
for k=import_channels
    % if length(Flat.p.CHANNEL_MIC)==1
    %     Tagstr='RMSfilt';
    % else
    if length(import_channels)==1
        Tagstr=Flat.v.RMS;%[Flat.p.Channel_names{Flat.p.CHANNEL_MIC(P.X(k).imics)}];
    else
        Tagstr=Flat.p.Channel_names{Flat.p.CHANNEL_MIC(P.X(k).imics)};
        %        Tagstr=Flat.p.Channel_names{P.X(k).imics};
    end
    
    %   else
    %    end
    %  Flat=func_write_imported_elements_to_Flat(Flat,onsA(k,:),offsA(k,:),clust_IDs(k,:),custsA(k,:),0,TagsA(k,:),[],[],Tagstr);%MMVer2
    
    if length(import_channels)==1
        Tagstr=Flat.v.RMS;%[Flat.p.Channel_names{Flat.p.CHANNEL_MIC(P.X(k).imics)}];
    else
        Tagstr=[Flat.p.Channel_names{Flat.p.CHANNEL_MIC(P.X(k).imics)}];
    end
    Flat.v.Song_on_off_do=1;
    if Flat.v.tsne.clust_dense_mode==1
        disp('Two Thresholds Mode');
        %         Tagstr=['D' Flat.p.Channel_names{Flat.p.CHANNEL_MIC(P.imics(k))}];
        
        %    Tagstr=[Flat.p.Channel_syl_tagstring{k}];
        Flat=func_write_imported_elements_to_Flat(Flat,onsA(k,:),offsA(k,:),clust_IDs(k,:),custsA(k,:),0,[],[],[],Tagstr);
        
        if ~isfield(Flat.v.tags,'TSNE')
            fcH=evalin('base','fcH');
            Flat=func_help_tagname_add(Flat,'TSNE',0,'-');
            Flat.v.tagboard0_recompute=1;
            [Flat,fcH]=func_tagboard_define(Flat,fcH);
            Flat.v.plot_numbutton=1;
            Flat.v.plot_tagboard=1;
            [Flat,fcH]=plot_figs(Flat,fcH);
            assignin('base','fcH',fcH);
        end
        
        [Flat,sel]=func_select_tagboard(Flat,[],Flat.p.tagnames{Flat.v.tags.activator}, Tagstr);
        
        Flat.Tags(Flat.v.tags.TSNE,sel)={'-'};
        if ~isfield(Flat.X,'onofflow')
            Flat.X.onofflow=zeros(1,Flat.num_Tags);
        end
        Flat.X.onofflow(sel)=Flat.X.custom(sel);
        %                 if ~isfield(Flat.X,'thr_high')
        %                     Flat.X.thr_high=zeros(1,Flat.num_Tags);
        %                 end
        %                 which_high=sel(Flat.X.custom(sel)==0);
        %                 Flat.X.thr_high(which_high)=1;
        %                 Flat.Tags(Flat.v.tags.TSNE,which_high)={'InH'};
        
        % Flat.Tags(Flat.v.tags.TSNE,s(Flat.X.thr_high(s)==1))={'InH'}; % set all elements to be clustered to 'IN' by default
    elseif Flat.v.tsne.clust_dense_mode==2 % umap
        Flat=func_write_imported_elements_to_Flat(Flat,onsA(k,:),offsA(k,:),clust_IDs(k,:),custsA(k,:),0,[],[],[],Tagstr,do_pca);
        Flat.X.umap_time=Flat.X.custom;
        Flat.X=rmfield(Flat.X,'custom');
        
    else
        Flat=func_write_imported_elements_to_Flat(Flat,onsA(k,:),offsA(k,:),clust_IDs(k,:),[],0,[],[],[],Tagstr);
        % remove short elements
        min_syl_dur_ms=40;
        
        %   [Flat,hi99]=func_select_tagboard(Flat,[],'activator',Flat.p.Channel_names{Flat.p.CHANNEL_MIC(P.imics(k))});%RMS(500Hz-7kHz)
        [Flat,hi99]=func_select_tagboard(Flat,[],'activator','RMSfilt');%MMVer2
        short_syls=Flat.X.data_off(hi99)<min_syl_dur_ms/1000*Flat.scanrate;
        %   Flat.Tags(Flat.v.tags.activator,hi99(short_syls))={[Flat.p.Channel_names{Flat.p.CHANNEL_MIC(P.imics(k))},'S']};
        Flat.Tags(Flat.v.tags.activator,hi99(short_syls))={'RMSfiltS'}; %MMVer2
    end
end

% if recomp_PCA
%     Flat=func_Flat_PCA(Flat,0,0,1);
% else
%     Flat.PCmean=P.PCmean{Flat.p.channel_mic_display};
%     Flat.PCs=P.PCs{Flat.p.channel_mic_display};
%     Flat=func_Flat_PCA(Flat,Flat.PCs,Flat.PCmean);
% end

%if nargout>1



if isfield(Flat.v.tsne,'select0')
    Flat.v.tsne=rmfield(Flat.v.tsne,'select0');
end
end