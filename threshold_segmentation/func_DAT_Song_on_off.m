function [Flat,on_off,Tags]=func_DAT_Song_on_off(Flat,min_dur_ms,inter_song_ms,margin_ms,do_ask,ignore_clust)
if nargin==2 & min_dur_ms==-1
    if isfield(Flat.p,'DATSong_on_off_min_dur_ms')
     %   [Flat,tagstring]=func_help_remove_tagfield(Flat,{'SongOnOff','PreSylDur','SongDur','PostSylDur'});
            [Flat,tagstring]=func_help_remove_tagfield(Flat,'SongOnOff');
             [Flat,tagstring]=func_help_remove_tagfield(Flat,'PreSylDur');
            [Flat,tagstring]=func_help_remove_tagfield(Flat,'SongDur');
            [Flat,tagstring]=func_help_remove_tagfield(Flat,'PostSylDur');
        Flat.p=rmfield(Flat.p,'DATSong_on_off_min_dur_ms');
        Flat.p=rmfield(Flat.p,'DATSong_on_off_inter_song_ms');
        Flat.p=rmfield(Flat.p,'DATSong_on_off_margin_ms');
        Flat.v.tagboard0_recompute=1;
        Flat.v.tagboard0_replot=1;
    end
    return
end

if nargin< 6
    ignore_clust=[];
end

Tags=Flat.Tags;
%if ~isfield(Flat.v,'song')
Flat=func_tagboard_songdur_presyldur(Flat,ignore_clust);
Flat.v.tagboard0_recompute=1;
Flat.v.tagboard0_replot=1;
%end

if ~isfield(Flat.p,'DATSong_on_off_min_dur_ms')
    Flat.p.DATSong_on_off_min_dur_ms=500;
end

if ~isfield(Flat.p,'DATSong_on_off_inter_song_ms')
    Flat.p.DATSong_on_off_inter_song_ms=200;
end

if ~isfield(Flat.p,'DATSong_on_off_margin_ms')
    Flat.p.DATSong_on_off_margin_ms=32;
end

if ~isfield(Flat.v,'DATSongDur')
    Flat.v.DATSongDur=zeros(1,Flat.num_Tags);
end

BOARD.tagboard=cell(1,length(Flat.p.tagnames));
BOARD.tagboard{Flat.v.tags.activator}={Flat.v.RMS};

fieldname='Song_on_off';

%[Flat,selection]=func_select_tagboard(Flat,BOARD.tagboard);
selection=func_select_mic(Flat,Flat.p.channel_mic_display);
board_selection_DAT=unique(Flat.X.DATindex(selection));

if nargin<5
    do_ask=1;
end

Flat=func_help_tagname_add(Flat,'SongOnOff',0,'0');


if nargin<2
    fprintf('Set min_dur_ms (enter=%d, -1=all): ',Flat.p.DATSong_on_off_min_dur_ms)
    y=input('');
    if ~isempty(y)
        if y==-1
            if ~isfield(Flat.DAT,'Song_on_off')
                Flat.DAT.Song_on_off=cell(1,get_number_of_recordings(Flat));
            end
            on_off=Flat.DAT.Song_on_off;
            for i=board_selection_DAT
                on_off{i}=[1;Flat.DAT.eof(i)];
            end
            return
        end
        Flat.p.DATSong_on_off_min_dur_ms=y;
    end
elseif ~isempty(min_dur_ms)
    Flat.p.DATSong_on_off_min_dur_ms=min_dur_ms;
end

if nargin<3
    fprintf('Set Intersong_halfwidth_ms (enter=%d): ',Flat.p.DATSong_on_off_inter_song_ms)
    y=input('');
    if ~isempty(y)
        Flat.p.DATSong_on_off_inter_song_ms=y;
    end
elseif ~isempty(inter_song_ms)
    Flat.p.DATSong_on_off_inter_song_ms=inter_song_ms;
end

if nargin<4
    fprintf('Set margin_ms (enter=%d): ',Flat.p.DATSong_on_off_margin_ms)
    y=input('');
    if ~isempty(y)
        Flat.p.DATSong_on_off_margin_ms=y;
    end
elseif ~isempty(margin_ms)
    Flat.p.DATSong_on_off_margin_ms=margin_ms;
end

[on_off,on_off_el,dur_s]=func_DAT_on_off2(Flat,BOARD,board_selection_DAT,Flat.p.DATSong_on_off_min_dur_ms,Flat.p.DATSong_on_off_inter_song_ms,fieldname,Flat.p.DATSong_on_off_margin_ms);

%%
Flat=func_DAT_Song_on_off_correct_Tags(Flat,on_off);

%%
% Flat.Tags(Flat.v.tags.SongOnOff,:)={'0'};
% Flat.Tags(Flat.v.tags.SongOnOff,find(on_off_el==1))={'1'};
% Flat.Tags(Flat.v.tags.SongOnOff,find(on_off_el==-1))={'-1'};
%Flat.v.DATSongDur=dur_s;

Tags=Flat.Tags;
Flat.v.tagboard0_recompute=1;

end

