function Flat=func_reorder_by_indices_all(Flat,sti)
if nargin<2
    sti=[];
end
% RRR check
% Then Flat.X and Flat.Tags
l=length(Flat.DAT.filename);
new_list=zeros(1,Flat.num_Tags);
Xold=Flat.X;
Xnew=Xold;
Tags=Flat.Tags;
Tagsnew=Tags;
V=Flat.V;
Vnew=Flat.V;

cxi=0;
fn=fieldnames(Flat.X);
fnV=fieldnames(Flat.V);
%did_resort=0;
for fi=1:l
    if isempty(sti)
        el=find(Flat.X.DATindex==fi);
    else
        el=find(Flat.X.DATindex==sti(fi));
    end
    %     if issorted(Flat.X.indices_all(el))
    %         continue
    %     end
    %     did_resort=1;
    %     fprintf('File %d: indices_all sorted\n',fi);
    [dummy sort_ind]=sort(Flat.X.indices_all(el),'ascend');
    
    el_sort=el(sort_ind);
    new_list(cxi+1:cxi+length(el))=fi;
    for i=1:length(fn)
        if size(Xold.(fn{i}),2)==Flat.num_Tags
            Xnew.(fn{i})(:,cxi+1:cxi+length(el))=Xold.(fn{i})(:,el_sort);
        end
    end
    for i=1:length(fnV)
        if size(V.(fnV{i}),2)==Flat.num_Tags
            Vnew.(fnV{i})(:,cxi+1:cxi+length(el))=V.(fnV{i})(:,el_sort);
        end
    end
    
    Tagsnew(:,cxi+1:cxi+length(el))=Tags(:,el_sort);
    cxi=cxi+length(el);
end
%if did_resort
Xnew.DATindex=new_list;
Flat.X=Xnew;
Flat.Tags=Tagsnew;
Flat.V=Vnew;

Flat.v.tagboard0_recompute=1;
Flat.v.run_select=1;
Flat.v.run_select_clust=1;
[Flat Flat.v.select Flat.v.select_clust]=func_select_mode(Flat);
%end
end