function [Flat,sels,board] = func_select_tagboard(varargin)
%function [select,board] = func_select_tagboard(varargin)
%   Flat=varargin{1};
%   board=varargin{2};
    

    Flat=varargin{1};
    board=varargin{2};
    sels=[];
%     if strcmp('rms',varargin{end})
%         do_rms=1;
%         varargin=varargin(1:end-1);
%     else
%         do_rms=0;
%    end
    
    if Flat.num_Tags==0
        return
    end

    if Flat.v.tagboard0_recompute
        [Flat.Tags,Flat.v.tagboard0,Flat.v.tagboard0i]=...
            func_help_tagboard0(Flat.Tags,Flat.p.tagnames,...
            Flat.DAT.timestamp(Flat.X.DATindex(end)),...
            Flat.p.tags_quantize_threshold);
        Flat.v.tagboard0_recompute=0;
        
        %%% XXX this was a bug - there should be no mention of Flat.v.select_clust
        %%% in this fucntion!!!
        %         try
        if Flat.v.plot_ordered==9 || Flat.v.plot_ordered==13
            [Flat.v.tagpointer_str,Flat.v.tagpointer_indices]=func_help_tagpointer_indices(Flat,Flat.v.select_clust);
        end
        %         catch
        %             disp('bug!!');
        %         end
        %
    end
    
    %multimic
%     if do_rms && length(Flat.p.CHANNEL_MIC)>1 && length(Flat.p.channel_mic_display)==1%MMVer1
%         wh=find(ismember(Flat.v.tagboard0{Flat.v.tags.activator},Flat.p.Channel_syl_tagstring{Flat.p.channel_mic_display}));
%         Flat.v.tagboard0{Flat.v.tags.activator}{wh}=Flat.v.RMS;
%     end
    
    if isempty(board)
        board=cell(length(Flat.p.tagnames),1);% board{Flat.v.tags_curr_i}=Flat.v.tags_curr_ij{Flat.v.tags_curr_i};
    end
    %hi=func_select_tagboard(Flat,board);


    if nargin>2
        for i=3:2:nargin
            j=find(ismember(Flat.p.tagnames,varargin{i}));
            if ~isempty(j)
                board{j}=varargin{i+1};
            end
        end
    end

    sels = 1 : Flat.num_Tags;
    
 
    %%
 % tic
  hh=zeros(1,Flat.num_Tags); 
  ci=0;
  for i = 1 : length(board)
      if ~isempty(board{i}) && length(Flat.v.tagboard0i)>=i
          % ismember is super slow.
          % [dummy new_select] = ismember(Flat.Tags(i,:), board{i});
          
          %                  % this is up to 40 times faster:
          %             nbr_select = size(board{i}, 2); % nbr selected options
          %             new_select = zeros(nbr_select, Flat.num_Tags); % preallocate
          %             for j = 1 : nbr_select % find the matching tags for each selection
          %                 new_select(j, :) = strcmp(Flat.Tags(i, :), board{i}(j));
          %             end
          %             new_select = sum(new_select, 1); % combine all selections
          %
          %             %
          %             select = intersect(select, find(new_select));
          
          %% even faster
          ci=ci+1;
        %  fields=find(ismember(Flat.v.tagboard0{i},board{i}));
        try
            new_select=[Flat.v.tagboard0i{i}{ismember(Flat.v.tagboard0{i},board{i})}];
        catch
          new_select=cat(1,Flat.v.tagboard0i{i}{ismember(Flat.v.tagboard0{i},board{i})});
        end
   %       new_select=combine(Flat.v.tagboard0i{i}{find(ismember(Flat.v.tagboard0{i},board{i}))});
          %select = intersect(select, find(new_select));
          hh(new_select)=hh(new_select)+1;
      end
  end
  sels=find(hh==ci);
 % toc
    
 %% remove noise 
if Flat.p.ignore_noise_rms && ~isempty(board{Flat.v.tags.activator}) && ismember({Flat.v.RMS},board{Flat.v.tags.activator})
     noise_clust_ID=find(ismember(Flat.p.clust_names,'Noise'));
     noiseGroupID=find(ismember(Flat.p.groupnames,'Noise'));
     if ~isempty(noiseGroupID) && ~isempty(Flat.group{noiseGroupID})
     noise_clust_ID=union(noise_clust_ID,Flat.group{noiseGroupID});
     end
     if isempty(noise_clust_ID)
         disp('No noise clust found');
     else
         sels(ismember(Flat.X.clust_ID(sels),noise_clust_ID))=[];
     end
   %  Flat.p.ignore_noise_rms=0;
end

% if do_rms && length(Flat.p.CHANNEL_MIC)>1 && length(Flat.p.channel_mic_display)==1%MMVer1
%     Flat.v.tagboard0{Flat.v.tags.activator}{wh}=Flat.p.Channel_syl_tagstring{Flat.p.channel_mic_display};
% end
% Multimic case, enfore single mic choice if one spectrogram shown%MMVer2
% if ~isempty(sels) && length(Flat.p.CHANNEL_MIC)>1 && length(Flat.p.channel_mic_display)==1 && strcmp(Flat.Tags{Flat.v.tags.activator,sels(1)},Flat.v.RMS)
%     if ~(length(board{Flat.v.tags.activatorM})==1)
%         sels=intersect(sels,Flat.v.tagboard0i{Flat.v.tags.activatorM}{Flat.p.channel_mic_display});
%     end  
% end

 
end

% old
% select=1:Flat.num_Tags;
% for i=1:size(Flat.v.tagboard,1)
%     hi2=[];
%     for j=1:length(Flat.v.tagboard{i})
%         [new_select dummy]=func_select_elements(Flat,[],1,Flat.select,Flat.p.tagnames{i},Flat.v.tagboard{i}{j});
%         hi2=union(hi2,new_select);
%     end
%     if ~isempty(hi2)
%         select=intersect(select,hi2);
%     end
% end

%% very old
%
% %% select from tagboard
% for i=1:size(h_tagbutton,1)
%     hi2=[];
%     for j=1:size(h_tagbutton,2)
%         if ~isempty(h_tagbutton{i,j}) && ishandle(h_tagbutton{i,j}) && get(h_tagbutton{i,j},'Value')
%             Flat.v.tagboard{i}{end+1}= get(h_tagbutton{i,j},'String');
%             %hi2=union(hi2,func_select_elements(Flat,Flat.v.curr_clust,1,Flat.select,Flat.p.tagnames{i},get(h_tagbutton{i,j},'String')));
%             [new_select dummy]=func_select_elements(Flat,[],1,Flat.select,Flat.p.tagnames{i},get(h_tagbutton{i,j},'String'));
%             hi2=union(hi2,new_select);
%         end
%     end
%     if ~isempty(hi2)
%         select=intersect(select,hi2);
%     end
% end