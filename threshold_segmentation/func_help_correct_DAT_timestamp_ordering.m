% this is the new func_reorder_by_time_and_trimm2 function
function Flat=func_help_correct_DAT_timestamp_ordering(Flat,do_trimm)
    if nargin<2
        do_trimm=0;
    end
    
    % To fix some problem I had with Dina's data
    if (isfield(Flat.v, 'is_old_time_ordering') && Flat.v.is_old_time_ordering==1)
        disp('Old Archive with timestamps set to end of files, no overlap test performed!');
        
    elseif (isfield(Flat.v, 'do_not_correct_DAT_timestamps') && Flat.v.do_not_correct_DAT_timestamps==1)
        disp('No overlap test performed! -> DAT timestamps not corrected');
        
    elseif get_number_of_recordings(Flat)>1
        nOverlap=find(diff(Flat.DAT.timestamp)<Flat.DAT.eof(1:end-1)/Flat.scanrate/24/3600);
        if length(nOverlap)>0
            fprintf('%d Files are overlapping!\n',length(nOverlap));
            h1=diff(Flat.DAT.timestamp); h2=(Flat.DAT.eof(1:end-1)/Flat.scanrate)/24/3600;
            fprintf('max overlap %f s\n',24*3600*max(h2(nOverlap)-h1(nOverlap)));
            if ~isfield(Flat.p, 'autoCorrectFileOverlaps') || ~Flat.p.autoCorrectFileOverlaps
%                 disp('press a key to correct');
%                 pause
%             else
                disp('correcting...');
            end
            while length(nOverlap)>0 % this code moves the file endings around to
                h1=diff(Flat.DAT.timestamp); h2=(Flat.DAT.eof(1:end-1)/Flat.scanrate+.1)/24/3600;
                Flat.DAT.timestamp(nOverlap+1)=Flat.DAT.timestamp(nOverlap+1)+h2(nOverlap)-h1(nOverlap);
                nOverlap=find(diff(Flat.DAT.timestamp)<Flat.DAT.eof(1:end-1)/Flat.scanrate/24/3600);
            end
        end
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% compute correct index list (if broken)
    % if any(diff(Flat.DAT.timestamp)<0) || any(diff(Flat.X.DATindex)<0) || max(Flat.X.DATindex)>get_number_of_recordings(Flat)
    %     disp('Timestamp ordering problem in Flat.DAT or Flat.X.DATindex')
    % elseif ~do_trimm
    %      return
    % end
    
    if do_trimm % do trimm
        uDATindex=unique(Flat.X.DATindex);
        not_in_Flat=setdiff(1:get_number_of_recordings(Flat),uDATindex);
        curr_numfiles=get_number_of_recordings(Flat);
        fnames=fieldnames(Flat.DAT);
        for i=1:length(fnames)
            if size(Flat.DAT.(fnames{i}),2)==curr_numfiles
                Flat.DAT.(fnames{i})(:,not_in_Flat) = [];
            end
        end
        Flat.v.save.DAT_data=1;
        Flat.v.save.DAT_multiunit=1;
        Flat.v.save.DAT_STACK=1;
        
        not_in_Flat=sort(not_in_Flat,'descend');
        for i=1:length(not_in_Flat)
            which=Flat.X.DATindex>not_in_Flat(i);
            Flat.X.DATindex(which)=Flat.X.DATindex(which)-1;
        end
    end
    
    if any(diff(Flat.DAT.timestamp)<0) || any(diff(Flat.X.DATindex)<0) || any(Flat.X.DATindex>get_number_of_recordings(Flat))
        disp('Timestamp ordering problem in Flat.DAT or Flat.X.DATindex');
        
        if isfield(Flat.v, 'is_old_time_ordering') && Flat.v.is_old_time_ordering==1
            disp('The code only works for new time format - what to do? ');
            keyboard
        end
        
        % First Flat.DAT
        t=Flat.DAT.timestamp; l=length(t);
        [st,sti]=sort(Flat.DAT.timestamp,'ascend');
        fn=fieldnames(Flat.DAT);
        DATnew=Flat.DAT;
        for fi=1:length(t)
            for i=1:length(fn)
                if size(DATnew.(fn{i}),2)==l
                    DATnew.(fn{i})(:,fi)=Flat.DAT.(fn{i})(:,sti(fi));
                end
            end
        end
        Flat.DAT=DATnew;
        
        % RRR check
        % Then Flat.X and Flat.Tags
        new_list=zeros(1,Flat.num_Tags);
        Xold=Flat.X;
        Xnew=Xold;
        Tags=Flat.Tags;
        Tagsnew=Tags;
        V=Flat.V;
        Vnew=Flat.V;
        
        cxi=0;
        fn=fieldnames(Flat.X);
        fnV=fieldnames(Flat.V);
        for fi=1:l
            el=find(Flat.X.DATindex==sti(fi));
            if issorted(Flat.X.indices_all(el))
                continue
            end
            fprintf('File %d: indices_all sorted\n',fi);
            [dummy sort_ind]=sort(Flat.X.indices_all(el),'ascend');
            el_sort=el(sort_ind);
            new_list(cxi+1:cxi+length(el))=fi;
            for i=1:length(fn)
                if size(Xold.(fn{i}),2)==Flat.num_Tags
                    Xnew.(fn{i})(:,cxi+1:cxi+length(el))=Xold.(fn{i})(:,el_sort);
                end
            end
            for i=1:length(fnV)
                if size(V.(fnV{i}),2)==Flat.num_Tags
                    Vnew.(fnV{i})(:,cxi+1:cxi+length(el))=V.(fnV{i})(:,el_sort);
                end
            end
            
            Tagsnew(:,cxi+1:cxi+length(el))=Tags(:,el_sort);
            cxi=cxi+length(el);
        end
        Xnew.DATindex=new_list;
        Flat.X=Xnew;
        Flat.Tags=Tagsnew;
        Flat.V=Vnew;        
    end
    
    %% trimm: remove non-appearing files (from DAT)
    uDATindex=unique(Flat.X.DATindex);
    not_in_Flat=setdiff(1:get_number_of_recordings(Flat),uDATindex);
    if ~isempty(not_in_Flat)
        % if auto_trimm == 0
        fprintf('\n!! Archive %s: I found %d files\nin Flat.DAT that are not in Flat! \n',Flat.name,length(not_in_Flat));
        disp('files not in Flat:');
    %    disp(not_in_Flat);
        y = 0;
        %y=input('do you want me to remove those (to save memory, yes = 1) ?\n');
        %  else
        %      y = 1;
        % end
 
    end
    
end

