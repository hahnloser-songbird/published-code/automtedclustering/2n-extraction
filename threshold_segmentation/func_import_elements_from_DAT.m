function [Flat,fcH]=func_import_elements_from_DAT(Flat,fcH)
% eval key: evalkey_import_el_from_DAT

if length(Flat.p.channel_mic_display)>1
    disp('must select single channel - aborting');
    return
end
%% define S from Flat.v.import_DAT
if isfield(Flat.v,'import_DAT')
    S=Flat.v.import_DAT;
    S.fileN=min(S.fileN,get_number_of_recordings(Flat));
else
    S.fileN = 1;
    S.i0=1;
    %    S.method = 'sum';
end
S.ncols=floor(Flat.v.Screensize(1)); % RRR: must go fcH, not to Flat


%% define P from Flat.p.syllable
if isfield(Flat.p,'syllable') && ~isempty(Flat.p.syllable)
    P=Flat.p.syllable;
else
    P=[];
end
% if ~isfield(P,'did_run') || P.did_run==0
%     for i=1:length(P.imics)
%         P.(P.method)(i).thets=80;
%         P.(P.method)(i).thetsU=120;
%     end
% end

%%% @CL: uncomment being. % P is not initiated in example_script
if  ~isfield(P,'imics')
% initialize
    P.imics=1:length(Flat.p.channel_mic);%Flat.p.CHANNEL_MIC; % Microphone channels to be used for syllable detection (index into Flat.p.Channel_names)
    P.imicsN = Flat.p.channel_mic_display;
    P.method='sum';
    P.multi=0;
    P.opt=0;

    switch P.method
        case 'sum'
            for i=1:length(P.imics)
                P.(P.method)(i).smoothingWidth=6;
                P.(P.method)(i).thets=80;
                P.(P.method)(i).thetsU=120;
                P.(P.method)(i).low=100;
                P.(P.method)(i).high=size(Flat.DAT.data{1},1)*Flat.scanrate/Flat.p.nfft-100;
            end
        otherwise
            disp('invalid mode selected please check...')
            return
    end
end
%%% @CL: uncomment end

%% Check Only one channel selected
if P.imicsN~=Flat.p.channel_mic_display
    if length(Flat.p.channel_mic_display)>1
        disp('More than one channel displayed! Please only display a single channel before running this Program...')
        return
    end
    P.imicsN=Flat.p.channel_mic_display;
    disp(['Channel has changed - new channel is: ',num2str(P.X(P.imicsN).imics)])
end

if ~isfield(P.(P.method)(P.imicsN),'thetsU') || isempty(P.(P.method)(P.imicsN).thetsU)
    P.(P.method)(P.imicsN).thetsU=120;
    %  P.(P.method)(P.imicsN).thetsU=P.(P.method)(P.imicsN).thets+.2*abs(P.(P.method)(P.imicsN).thets);
end
%%
% XXX possible also compute range of y axis here on never change it
% subsequently

SM=P.(P.method)(P.imicsN);

if ~isfield(P,'els') || P.els(1)~=floor(SM.low/Flat.scanrate*Flat.p.nfft) || P.els(end)~=floor(SM.high/Flat.scanrate*Flat.p.nfft)
    P.els=max(floor(SM.low/Flat.scanrate*Flat.p.nfft),1):floor(SM.high/Flat.scanrate*Flat.p.nfft);
end

do_rec= ~isfield(SM,'minsS') || isnan(SM.minsS);
% if ~do_rec
%     y=input('Recompute spec minsS (enter=no, 1=yes): ')
%     if y==1
%         do_rec=1;
%     end
% end
if do_rec%~isfield(SM,'minsS') || isnan(SM.minsS)
    %     if 0 && isfield(Flat.p,'spec_min')
    %         for k=1:length(P.X)
    %             P.(P.method)(k).minsS=Flat.p.spec_min(k);
    %         end
    %     else
    P.X.imics = 1;
    P.imicsN = 1;
    
    Itmp=num2cell(inf(1,length(P.X)));
    [P.(P.method).minsS]=Itmp{:};
    h=inf;
    for i=1:get_number_of_recordings(Flat)
        for k=1:length(P.X)
%             if isfield(Flat.DAT,'datas')
%                 h=min(min(Flat.DAT.datas{i}{k}));
%             else
                h=min(min(Flat.DAT.data{i}));
%             end
            if P.(P.method)(k).minsS>h
                P.(P.method)(k).minsS=h;
            end
        end
    end
    
    %    end
end

%   for k=1:length(P.imics)
%          P.(P.method)(k).thets=40;%double(P.(P.method)(k).minsS+.2*abs(P.(P.method)(k).minsS));
%          P.(P.method)(k).thetsU=80;%double(P.(P.method)(k).thets+.2*abs(P.(P.method)(k).thets));
%   end

Flat.v.import_DAT = S;
% SM.thets=80;
% SM.thetsU=120;
%
thet=SM.thets;
thetU=SM.thetsU;


%  for i=1:length(P.imics)
%         P.(P.method)(i).thets=80;
%         P.(P.method)(i).thetsU=120;
%   end

[s,D,L]=func_pseudo_RMS_from_spec(Flat,S,P,0,Flat.p.channel_mic_display);
if isfield(fcH,'hf_import_DAT')
    
    % XXXX change green lines too
    
    % line plot top pannel
    try
        set(fcH.hf_import_DAT.plots,'ydata',log(s),'xdata',1:length(s));
    catch
        disp(' you may need to recompute Flat.p.syllable.sum.minsS');
    end
    set(fcH.hf_import_DAT.plot_thet,'xdata',[1 length(s)]);
    set(fcH.hf_import_DAT.plot_thetU,'xdata',[1 length(s)]);
    set(fcH.hf_import_DAT.plot_thet,'ydata',log(SM.thets)*[1 1]);
    set(fcH.hf_import_DAT.plot_thetU,'ydata',log(SM.thetsU)*[1 1]);
    
    set(fcH.hf_import_DAT.range1,'ydata',[P.els(1) P.els(1)]);
    set(fcH.hf_import_DAT.range2,'ydata',[P.els(end) P.els(end)]);
    set(fcH.hf_import_DAT.range1,'xdata',[1 L]);
    set(fcH.hf_import_DAT.range2,'xdata',[1 L]);
    if ~isempty(s)
        set(fcH.hf_import_DAT.axes1,'XLim',[1 length(s)],'Ylim',[min(min(log(s)),min(log(thet))) 10*eps+max(max(log(s)),max(log(thet)))]);
    end
    set(fcH.hf_import_DAT.title,'String',['Channel ',num2str(P.X(P.imicsN).imics),' file ' num2str(S.fileN) ':   ' num2str(min(100,ceil((S.i0+S.ncols)/L*100))) ' Percent, smooth: ' num2str(P.(P.method)(P.X(P.imicsN).imics).smoothingWidth)]);
    %axis tight
    
    set(fcH.hf_import_DAT.spec,'CData',D');
    if isfield(Flat.p,'ImportCustom') && Flat.p.ImportCustom==1 && ~isempty(Flat.v.DATstack) && Flat.v.DATstack>0
        str=Flat.DAT.stack(Flat.v.DATstack).name;
    else
        str='Sound ampl';
    end
    set(fcH.hf_import_DAT.legend,'Location','Best','String',{str,'Thresh','Upper Thresh'});
    set(fcH.hf_import_DAT.title,'Position',[.5 1.05]);
    
    if Flat.v.DATstack & isfield(Flat.DAT,'STACK');
        stacki=Flat.v.DATstack;
        
        out=func_stack_extract2(Flat,stacki,S.fileN);
        
        %% rich insert
        if 0
            X=Flat.DAT.STACK(Flat.v.stack.sparse,Flat.v.import_DAT_backpack_segm.file(1));
            %X=func_stack_extract3(Flat,Flat.v.stack.sparse,S.fileN);
            XX=cellfun(@(x) x',X,'UniformOutput',false);
            Flat.v.out=Flat.v.mdl.predict(double([XX{:}]));
        end
        %   out=Flat.v.out;
        
        v=[S.i0 S.i0+S.ncols-1]; V=ceil(v*Flat.p.nonoverlap/Flat.DAT.stack(stacki).nonoverlap);
        z=out(V(1):min(length(out),V(2)));
        mi=Flat.DAT.stack(stacki).min; ma=Flat.DAT.stack(stacki).max;
        if ~isempty(Flat.DAT.stack(stacki).comment)
            mult=Flat.DAT.stack(stacki).comment; %Flat.p.SdrStackMultiplier;b
            %        zscale=(z*mult-mi)*128/(ma-mi)-(mean(z*mult)-(ma+mi)/2)*128/(ma-mi);
            %        zscale=min(max(zscale,1),128);
            ma=ma-(ma-max(z))*(mult-1)/mult; 
            mi=mi+(min(z)-mi)*(mult-1)/mult;
        end
        zscale=(z-mi)/(ma-mi)*127;
        set(fcH.hf_import_DAT.curve,'xdata',[1:length(z)]/length(z)*length(s),'ydata',zscale,'visible','on','color','r') % =plot([1 size(D,1)],[1 1],'w','Visible','off');
        set(fcH.hf_import_DAT.legend2,'String',{'Range',Flat.DAT.stack(stacki).name},'Location','northeast');
        
    end
    
    
else
    if SM.thets==80
        Flat=func_DAT_threshold(Flat,P);
        thet=Flat.p.syllable.(P.method)(P.imicsN).thets;
        P.(P.method)(P.imicsN).thets=thet;
    end
    fcH.hf_import_DAT.fig=figure(16);clf;
    fcH.hf_import_DAT.title=title(['Channel ',num2str(P.X(P.imicsN).imics),' file ' num2str(S.fileN) ':   ' num2str(min(100,ceil((S.i0+S.ncols)/L*100))) ' Percent, smooth: ' num2str(P.(P.method)(P.X(P.imicsN).imics).smoothingWidth)]);
    set(fcH.hf_import_DAT.fig,'position',[30 Flat.v.Screensize(2)-680 Flat.v.Screensize(1)-135 600],...
        'Name','import elements from DAT','KeyPressFcn', {@evalkey_import_el_from_DAT, },...
        'CloseRequestFcn', 'close_import_DAT_figs;','color',[1 1 1]);
    set(gca,'ycolor',[1 1 1])
    
    
    
    %% spec plot
    fcH.ha_import2=axes;
    set(fcH.ha_import2,'Visible','off','position',[0 0 1 .5],'DrawMode','fast','box','off');
    %    subplot(212);
    %     fcH.ha_import2=axes;
    hold on;
    fcH.hf_import_DAT.spec=imagesc([D']);
    fcH.hf_import_DAT.range1=plot([1 size(D,1)],[P.els(1) P.els(1)],'g');
    fcH.hf_import_DAT.curve=plot([1 size(D,1)],[1 1],'r','Visible','off');
    fcH.hf_import_DAT.range2=plot([1 size(D,1)],[P.els(end) P.els(end)],'g');
    fcH.hf_import_DAT.legend2=legend('Range','');
    axis xy
    
    %% curve plot
    fcH.hf_import_DAT.axes1=axes;
    set(fcH.hf_import_DAT.axes1,'Visible','off','position',[0 .5 1 .45],'DrawMode','fast','box','off');
    
    linkaxes([fcH.hf_import_DAT.axes1,fcH.ha_import2],'x')
    axes(fcH.hf_import_DAT.axes1);
    hold on;
    fcH.hf_import_DAT.plots=plot(log(s));
    fcH.hf_import_DAT.plot_thet=plot([1 length(s)],log(thet)*[1 1],'r');
    fcH.hf_import_DAT.plot_thetU=plot([1 length(s)],log(thetU)*[1 1],'c');%,'visible','off');
    if isfield(Flat.p,'ImportCustom') && Flat.p.ImportCustom==1 && Flat.v.DATstack>0
        str=Flat.DAT.stack(Flat.v.DATstack).name;
    else
        str='Sound ampl';
    end
    fcH.hf_import_DAT.legend=legend(str,'Thresh','Upper Thresh');
    
    axis tight
end
Flat.p.syllable=P;
figure(16);
end

