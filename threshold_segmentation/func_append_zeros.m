function X=func_append_zeros(X, num_Tags_old, num_Tags)

%if num_Tags_old==0 && num_Tags==1
%    fprintf('You are about to add a single element to an empty archive \nwhich is not allowed (due to bad programming)\nLet Richard know about it.\n');
%    clc;
%    error('func_append_zeros: error.');
%end
fn=fieldnames(X);

for i=1:length(fn)
    l1=size(X.(fn{i}),1);
    %  if size(X.(fn{i}),2)==num_Tags_old && ~strcmp(fn{i},'templates')
    if size(X.(fn{i}),2)==num_Tags_old %&& ~strcmp(fn{i},'templates')
        if iscell(X.(fn{i}))
            % if strcmp(fn{i},'Tags')
            %if size(X.(fn{i}),2)>0 && ischar(X.(fn{i}){1,1})
            %                  X.(fn{i})(:,end+1:end+num_Tags)=cellfun(@(x) '',h,'UniformOutput',false);
            %% Tags:    X.(fn{i})(:,end+1:end+num_Tags)={''};
            % else
            X.(fn{i})(:,end+1:end+num_Tags) =cell(l1,num_Tags);
            %  end
            % else
            %  X.(fn{i})=[X.(fn{i}) num2cell(zeros(l1,num_Tags))];
            %              end
        else
            X.(fn{i})=[X.(fn{i}) zeros(l1,num_Tags)];
        end
    else
        error(['func_append_zeros: Problem with field "' fn{i} '" !!']);
    end
end

end
%% EOF