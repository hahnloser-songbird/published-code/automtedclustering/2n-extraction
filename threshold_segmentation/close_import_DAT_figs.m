%%
%skype_handles={'hf_skype','hf_skype_plotf','hf_skype_plott','hf_skype_spec'};

if exist('fcH','var')
    if isfield(fcH,'hf_import_DAT')
        
        all_fields = fieldnames(fcH.hf_import_DAT);
        
        for i = 1 : size(all_fields, 1)
            this_handle = fcH.hf_import_DAT.(all_fields{i});
            if ishandle(this_handle)
                delete(this_handle);
            end
        end
        fcH=rmfield(fcH,'hf_import_DAT');
    end
end
clear this_handle;
% 
% if exist('fcH','var')
%     for i = 1 : size(skype_handles, 2)
%         if isfield(fcH,skype_handles{i})
%             this_handle = fcH.(spyke_handles{i});
%             if  ishandle(this_handle)
%                 delete(this_handle);
%             end
%         end
%     end
% end
% 
