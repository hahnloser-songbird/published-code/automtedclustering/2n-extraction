function [s,D,L,s_noises]=func_pseudo_RMS_from_spec(Flat,S,P,entire_file,channel)

if length(channel)>1
    disp('must select single channel - aborting');
    s=[]; D=[]; L=[]; 
    return
end

imic=channel;
method=P.method;

%% skype: subtract dynamic part of threshold
do_skype=0;
if isfield(Flat.p,'skype') && isfield(Flat.p.skype,'which')
    %chan_i=find(ismember(Flat.p.skype.which.filtered,imic));
    %chan_no=imic;
    if Flat.p.skype.mode(imic)==1 % && ~isempty(chan_no)
        play_no=Flat.p.skype.which.playback(Flat.v.skype.suppress.chan_no);
        play_no=find(ismember(Flat.p.CHANNEL_MIC,play_no));
        t1=Flat.p.skypeCUT.thresh1(imic);
        do_skype=1;
    end
end

%% get specgrams
if nargin>4 && entire_file
    if isfield(Flat.DAT,'datas') && (size(Flat.DAT.datas,1)>1 || (iscell(Flat.DAT.datas{1}) && length(Flat.DAT.datas{1})>1))
        D=Flat.DAT.datas{S.fileN}{imic};
        if do_skype
            SP=Flat.DAT.datas{S.fileN}{play_no};
        end
        L=size(Flat.DAT.datas{S.fileN}{imic},1);
    else
        D=Flat.DAT.data{S.fileN}';
        L=size(Flat.DAT.data{S.fileN},2);
    end
else
    if isfield(Flat.DAT,'datas') && (size(Flat.DAT.datas,1)>1 || (iscell(Flat.DAT.datas{1}) && length(Flat.DAT.datas{1})>1))
        D=Flat.DAT.datas{S.fileN}{imic}(S.i0:min(S.i0+S.ncols-1,end),:);
        if do_skype
            SP=Flat.DAT.datas{S.fileN}{play_no}(S.i0:min(S.i0+S.ncols-1,end),:);
        end
        L=size(Flat.DAT.datas{S.fileN}{imic},1);
    else
        D=Flat.DAT.data{S.fileN}(:,S.i0:min(S.i0+S.ncols-1,end))';
        L=size(Flat.DAT.data{S.fileN},2);
    end
end
% RRR going from skype_suppress to impart_dat loses the thresholds!

switch method
    case 'sum'
        smoothW=P.(method)(P.X(P.imicsN).imics).smoothingWidth;
    %   smoothW=P.(method)(P.imics(P.imicsN)).smoothingWidth;
       if do_skype
            %    d=max(1,1-P.sum(P.imicsN).minsS+D(:,8:128)'-t1*(45+[-45*ones(2,121) ; SP(1:end-2,8:128)])');
            d=max(1,1-P.sum(P.imicsN).minsS+D(:,8:128)'-t1*(-P.sum(P.imicsN).minsS+SP(:,8:128))');
            %  d=max(1,1+D(:,8:128)'-t1*(SP(:,8:128))');
        else
            d=1-int16(P.sum(P.imicsN).minsS)+int16(D(:,P.els)');
        end
       % dsum=max(1,sum(d));
       dsum=mean(d);
       
       if isfield(Flat.p,'ImportCustom') && Flat.p.ImportCustom==1 && ~isempty(Flat.v.DATstack) && Flat.v.DATstack>0
           %s=double(Flat.DAT.STACK{Flat.v.DATstack,S.fileN}); 
           s=func_stack_extract2(Flat,Flat.v.DATstack,S.fileN);
           ma=Flat.DAT.stack(Flat.v.DATstack).max; mi=Flat.DAT.stack(Flat.v.DATstack).min;
           %dyns=Flat.DAT.stack(Flat.v.DATstack).dynamic_range;
           s=1+(((s-mi)/(ma-mi))); % scale from 1 to 2 (because of log)!! RRR
          % s=1+64*(s-dyns(1))/(diff(dyns));
           if ~entire_file
               v=[S.i0 S.i0+S.ncols-1]; V=ceil(v*Flat.p.nonoverlap/Flat.DAT.stack(Flat.v.DATstack).nonoverlap);
               s=s(V(1):min(length(s),V(2)));
               s=s(floor(1+(0:S.ncols-1)/S.ncols*length(s)));
               %length(s)
               %  (S.i0:min(S.i0+S.ncols-1,end));
           end
             %         dsum=exp(double(s)/1e8);
          dsum=s;
       end

        
       if isfield(Flat.p,'DATmasknoise') && Flat.p.DATmasknoise(imic)
           if isfield(Flat.DAT,'maskDiffNoise')
               masknoise=[1 Flat.DAT.maskDiffNoise{S.fileN}(imic,:) 1];
               %           masknoise=Flat.DAT.maskDiffNoise{S.fileN}(imic,:);
               if ~entire_file
                   masknoise=masknoise(S.i0:min(S.i0+S.ncols-1,end));
               end
               %   masknoise=smooth(double(masknoise),smoothW);
               h=min(dsum);
               dsum2=dsum;
               dsum(find(masknoise))=h;
               dsum2(~masknoise)=h;
           else
               Flat.p.DATmasknoise(imic)=0;
           end
       end
       
        if smoothW
            s=smooth(dsum,smoothW);
        else
            s=dsum;
        end
        
        if isfield(Flat.p,'DATmasknoise') && Flat.p.DATmasknoise(imic)
            s_noises=smooth(dsum2,smoothW);
        end
        
        %s=double(max(d))*20-double(min(d))*20;
        
    case 'max'
        % s=double(max(d))*20;
end

end