function        Flat=func_append_zeros_meta(Flat,num_old,num_new)
   

%%%%%%%%%%%%%

%% 3 functions: func_remove_elements, func_append_zeros_meta, func_reorder_by_indices_meta
if isfield(Flat,'X')
Flat.X=func_append_zeros(Flat.X,num_old,num_new);
end
if isfield(Flat,'V')
Flat.V=func_append_zeros(Flat.V,num_old,num_new);
end
%Flat=func_append_zeros_old(Flat,num_old,num_new);
%Flat.v=func_append_zeros_old(Flat.v,num_old,num_new);
if isfield(Flat,'Tags')
Flat.Tags(:,end+1:end+num_new)={''};   
end
%%%%%%%%%%%%%%%%%