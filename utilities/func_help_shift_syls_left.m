function Flat=func_help_shift_syls_left(Flat,seli,i,sel)
il=seli-1; slp=i;
if il>0 && il<=length(sel)
    sl=sel(il);
    while Flat.X.DATindex(sl)==Flat.X.DATindex(slp) && Flat.X.data_off(sl)+Flat.X.indices_all(sl)>= Flat.X.indices_all(slp)
        Flat.X.data_off(sl)=0;
        Flat.X.indices_all(sl)=min(Flat.X.indices_all(sl),Flat.X.indices_all(slp)-Flat.p.nonoverlap);
        slp=sl;
        il=il-1; sl=sel(il);
    end
end
end