function [FlatsInfo] = fci_defaultFlatsInfo(Flats)

    Flat = Flats{2};
    if isfield(Flat, 'birdname')
        FlatsInfo.birdname = Flat.birdname;
    else
        FlatsInfo.birdname = unique(Flat.birdnames);
    end
    FlatsInfo.annotations = struct('text', {}, 'timestamp', {}, 'color', {}, 'type', {});
    
end