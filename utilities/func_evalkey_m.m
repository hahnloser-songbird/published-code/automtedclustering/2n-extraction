function [Flat,move_elements]=func_evalkey_m(Flat, modifier, p, do_hide)

if nargin < 4
    do_hide = 0;
end
if p>length(Flat.v.order_sequence)
    return
end

if ~isfield(Flat.v,'move')
    Flat.v.move=zeros(4,1); % 1=nothing or shift, 2=shift+control, 3=2=shift+alt, 4=2=shift+control+alt
end

move_elements = Flat.v.order_sequence(p);
%%
switch size(modifier, 2)
    case 0
        if isfield(Flat.v, 'repeated_put_to')
            if Flat.v.repeated_put_to ==  Flat.v.curr_clust
                disp('This is the same cluster.');
                if ~Flat.p.do_smart_move
                    return
                end
            end
            clust_no=Flat.v.repeated_put_to;
            Flat.v.move(1)=Flat.v.move(1)+1;
            
        end
        
    case 1
        if strcmp(modifier,'shift')
            if Flat.v.all_clust_mode==1
                fprintf('Not possible in ALL_CLUST_MODE \n');
                return
            end
            clear options; options.Resize='on';
            clust_no=inputdlg('Move to cluster number','Move',1,{''},options);
            if isempty(clust_no)
                return;
            end
            
            if ~ismember(str2num(clust_no{1}),1:length(Flat.p.clust_names))
                disp(['cluster ',clust_no,' does not exist'])
                return;
            end
            
            clust_no=str2num(clust_no{1});
            if clust_no==Flat.v.curr_clust
                fprintf('Same cluster\n');
                %  return
                %           else
            end
            if clust_no==0 && Flat.p.do_smart_move
                disp('Remove prototype');
                Flat.X.prototypes(move_elements)=0;
                %  return
            else
                Flat.v.repeated_put_to=clust_no;
                Flat.v.move(1)=Flat.v.move(1)+1;
            end
            
        elseif strcmp(modifier, 'control')
            [status, Flat] = move_show_clust_name(Flat);
            clust_no = Flat.v.repeated_put_to;
            if status == 1
                return;
            end
            
        elseif strcmp(modifier,'alt')
            if ~(Flat.v.plot_ordered==4 || Flat.v.plot_ordered==5)
                return
            end
            
            clear options; options.Resize='on';
            clust_no=inputdlg('Move all context elements to cluster','Move',1,{''},options);
            if isempty(clust_no) || isempty(clust_no{:})
                return;
            end
            clust_no=str2num(clust_no{1});
            if clust_no==Flat.v.curr_clust
                fprintf('Same cluster, nothing happened\n');
                return
            else
                Flat.v.repeated_put_to=clust_no;
            end
            
            if Flat.v.plot_ordered==4
                leftmost_element_next=func_sequence_context_leftmost(Flat,Flat.v.leftmost_element,'pre',1);
                
            elseif Flat.v.plot_ordered==5
                leftmost_element_next=func_sequence_context_leftmost(Flat,Flat.v.leftmost_element,'post',1);
            end
            
            %  move_elements = Flat.v.sequence{Flat.v.plot_ordered,Flat.v.curr_clust}(1:leftmost_element_next-1);
            move_elements = Flat.v.sequence{Flat.v.plot_ordered,Flat.v.curr_clust}(Flat.v.leftmost_element:leftmost_element_next-1);
            %          [Flat.V.pre Flat.V.post]=func_sequence_context(Flat,1:Flat.num_Tags);
        end
        
    case 2
        if strcmp(modifier, {'shift', 'control'})
            % move all elements from currently selected one to the last one
            % in list.
            order_mode = Flat.v.plot_ordered;
            
            from_element = move_elements;
            start_index = find(Flat.v.sequence{order_mode, Flat.v.curr_clust} == from_element);
            move_elements = Flat.v.sequence{order_mode, Flat.v.curr_clust}(start_index:end);
            if isempty(move_elements)
                disp('something went wrong with moving items - no items selected');
                return
                %keyboard;
            end
            [status, Flat] = move_show_clust_name(Flat);
            clust_no = Flat.v.repeated_put_to;
            Flat.v.move(2)=Flat.v.move(2)+1;
            if status == 1
                return;
            end
            
        elseif strcmp(modifier, {'shift', 'alt'})
            % move all elements from 1st one to currently selected one.
            order_mode = Flat.v.plot_ordered;
            to_element = move_elements;
            end_index = find(Flat.v.sequence{order_mode, Flat.v.curr_clust} == to_element);
            move_elements = Flat.v.sequence{order_mode, Flat.v.curr_clust}(1:end_index);
            if isempty(move_elements)
                disp('something went wrong with moving items - no items selected');
                return
                %keyboard;
            end
            [status, Flat] = move_show_clust_name(Flat);
            Flat.v.move(3)=Flat.v.move(3)+1;
            if status == 1
                return;
            end
        end
        
    case 3
        if strcmp(modifier, {'shift', 'control', 'alt'})
            move_elements = Flat.v.order_sequence;
            [status, Flat] = move_show_clust_name(Flat);
            Flat.v.move(4)=Flat.v.move(4)+1;
            if status == 1
                return;
            end
        end
        
end

%% DO IT!

% move elements
if do_hide == 0
    
    if isfield(Flat.v,'repeated_put_to')
        % disable moving TSNE-out elements
        %         if isfield(Flat.v.tags,'TSNE') & ismember({'Out'},Flat.Tags(Flat.v.tags.TSNE,move_elements))
        %             disp('You are trying to move a TSNE OUT element - aborted');
        %             return
        %         end
        % regular move
        for i=1:length(move_elements)
            Flat=func_sequence_cleanup(Flat,move_elements(i));
            Flat.X.clust_ID(move_elements(i))=Flat.v.repeated_put_to;   % put the element into the last cluster
            Flat.v.leftmost_element=max(1,Flat.v.leftmost_element-1);
            
            if isfield(Flat.p,'umap') & isfield(Flat,'Z')
                if Flat.v.repeated_put_to==1
                    Flat.Z.stencil(Flat.X.custom(1,move_elements(i)):Flat.X.custom(2,move_elements(i)))=0;
                else
                    Flat.Z.stencil(Flat.X.custom(1,move_elements(i)):Flat.X.custom(2,move_elements(i)))=Flat.v.repeated_put_to;
                    if strcmp(Flat.Tags{4,move_elements(i)}(1),'C')
                        if isequal(str2num(Flat.Tags{4,move_elements(i)}(2:end)),Flat.v.repeated_put_to)
                            Flat.Tags{4,move_elements(i)}(1)='S';
                        else
                            disp('Unconventional cluster');
                             Flat.Tags{4,move_elements(i)}=['S' num2str(Flat.v.repeated_put_to)];
                        end
                    end
                end
            end
            if isfield(Flat.X,'clust_ID0')
                Flat.X.clust_ID0(move_elements)=Flat.v.repeated_put_to;
                Flat.Tags(4,move_elements)={['C' num2str(Flat.v.repeated_put_to)]};    
                s=find(ismember(Flat.v.select,move_elements));
                Flat.umap.X.color(s)=int8(Flat.v.repeated_put_to);
            end
            %             if isfield(Flat.p,'umap') & isfield(Flat,'Y')
%                 if Flat.v.repeated_put_to==1
%                     Flat.Y.umapCtrWP(1,Flat.X.custom(1,move_elements(i)):Flat.X.custom(2,move_elements(i)))=0;
%                 else
%                     Flat.Y.umapCtrWP(1,Flat.X.custom(1,move_elements(i)):Flat.X.custom(2,move_elements(i)))=Flat.v.repeated_put_to;     
%                     Flat.Tags{4,move_elements(i)}(1)='S';
%                 end
%              end
        end
        
    end
    
    
    % hide elements
else
    
    hide_tag = fc.find_tag_id(Flat, 'is_hidden');
    Flat.Tags(hide_tag, move_elements) = {'1'};
    
    % update elements for figure 24
    Flat.v.leftmost_element = max(1, Flat.v.leftmost_element-numel(move_elements));
    
    % update elements for figure 20
    for i = 1 : numel(move_elements)
        Flat = func_sequence_cleanup(Flat, move_elements(i));
    end
    
end

if Flat.p.do_smart_move && ~(clust_no==0)%&& isfield(Flat.v,'repeated_put_to')
    Flat=func_smart_move(Flat,move_elements);
    Flat=func_clust_scal_prod_all_move(Flat);
end


end

%% HELPERFUNCTION

function [status, Flat] = move_show_clust_name(Flat)

status = 0;

% make sure user has named the clusters
nbr_clusters = size(Flat.p.clust_names, 2);
unnamed_clusters = sum(cellfun(@(x) isempty(x), Flat.p.clust_names));
% no names, let's create default names.
if unnamed_clusters == nbr_clusters
    for i = 1 : nbr_clusters
        Flat.p.clust_names{i} = ['cluster ' num2str(i)];
    end
end

% show user the list of all clusters and move item to selected cluster
if ~isfield(Flat.v, 'repeated_put_to')
    Flat.v.repeated_put_to = 1;
end

[s, OK] = listdlg( 'Name','Move', 'ListString', Flat.p.clust_names, ...
    'SelectionMode', 'single', ...
    'InitialValue', Flat.v.repeated_put_to,'ListSize',[200 600]);
drawnow;    % Bugfix to prevent blocking on Windows; Important!
if OK == 0
    status = 1;
    return;
end

if isempty(Flat.p.clust_names{s}) == 1
    disp('You can not select a cluster without name,');
    status = 1;
    return;
end

if Flat.v.curr_clust == s
    disp('This is the same cluster.');
    status = 1;
    return;
end

%%
Flat.v.repeated_put_to = s;
Flat.v.plot_clust_fast = 1;

end
%% EOF