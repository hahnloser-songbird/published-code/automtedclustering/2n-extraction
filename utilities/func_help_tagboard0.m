function [Tags,tagboard,tagboardi]=func_help_tagboard0(Tags,tagnames,timestamp_end,max_num_diff_tags)
%Tags = Flat.Tags;
num_tags=size(Tags,2);
% if isfield(Flat.v,'tags_quantized') && ge(length(Flat.v.tags_quantized),tagsi) && ~isempty(Flat.v.tags_quantized{tagsi})
%     [u,I,J]=unique(Flat.v.tags_quantized{tagsi});
% else
%     [u,I,J]=unique(Tags(tagsi,:));
% end
L=size(Tags,1);
tagboard=cell(1,L);
tagboardi=cell(1,L);
userID = evalin('base', 'cfg.userID');
%do_fast=1;
for tagsi=1:L
    if ~isempty([Tags{tagsi,:}]) % ~all(cellfun(@isempty,Tags(tagsi,:))) %  
        [u,I,J]=unique(Tags(tagsi,:));
    else
        u = {''};
        I = [];    
    
%     if do_fast
%         [u,I,J]=unique(Tags(tagsi,:));
%     else
%         if ~isempty([Tags{tagsi,:}])
%             [u,I,J]=unique(Tags(tagsi,:));
%         else
%             u = {''};
%             I = [];
%         end
     end
    %[u,I,J]=unique(Tags);
    u={u{:},'||'};
    tagboard{tagsi}=u;
    
    tagboardi{tagsi}=cell(1,length(tagboard{tagsi}));
  %  t=tagboardi{tagsi};
   % ismember(
  % if length(I)<10
    for i=1:min(100000000000,length(I))
        tagboardi{tagsi}{i}=uint32(find(J==i));
%        tagboardi{tagsi}{i}=uint32(find(ismember(J,i)));
    end
%   else
%    for i=1:length(J)
%    tagboardi{tagsi}{
%    end
%   end
    tagboardi{tagsi}(end)={[]};
    
    nbr_elements = numel(u);
    do_quantize = 0;
    
    %% check whether we need to quantize the tags
    if nbr_elements > max_num_diff_tags
        do_quantize = 1;
        % for some users / categories it makes no sense to quantize
        if userID == 2
            bla = input(['quantize Tags for category "' tagnames{tagsi} '" [0] / 1: ']);
            if isempty(bla) || bla == 0
                do_quantize = 0;
            end
        end
    end
    
    %% no need to quantize. resort tags by numeric value (if found).
    if do_quantize == 0
        
        % PLEASE, PLEASE STOP BREAKING THIS CODE!! I've been fixing it like 
        % 5 or 6 times already. thanks, AK.
        
        strgs = isnan(str2double(u(1:end-1)));
        % strings only - everything is already sorted
        if sum(strgs) == nbr_elements - 1 % this is wrong because numeric values will not be resorted! || sum(strgs)==0 (AK)
            continue;
        % mix of strings and numbers, sort numbers first and then append the
        % strings
        else
            u_num = u(~strgs); 
            u_num_which = find(~strgs);
            [sorted, indices] = sort((str2double(u_num)), 'ascend'); %#ok<ASGLU>
            u = [u_num(indices) u(strgs) u(end)];
            % write re-ordered sequence back to tagboard
            tagboard{tagsi} = u;
            tagboardi{tagsi}(1:end-1) = {tagboardi{tagsi}{u_num_which(indices)},tagboardi{tagsi}{strgs}};
            continue;
        end
    end
    
    %% do quantize
    if now<ceil(timestamp_end) % still today
        num_tags_shown=20;
        u=u(1:num_tags_shown);
        fprintf('Only first %d tags shown for %s\n',num_tags_shown,tagnames{tagsi});
        continue
    end
    
    u_num=str2num(str2mat(u));
    if isempty(u_num)
        isnumTag=0;
    else
        isnumTag=1;
    end
    
    if isnumTag % quantize
        range_u=range(u_num);
        fact= range_u/(max_num_diff_tags-1);
        digit=ceil(log10(fact/100));
        fact=quant(fact,10^digit);
        
        h=find(strcmp(Tags(tagsi,:),''));
        %   h=find(strcmp(Tags,''));
        Tags(tagsi,h)={'0'};
        %   Tags(h)={'0'};
        h=str2num(str2mat(Tags(tagsi,:)));
        %  h=str2num(str2mat(Tags));
        h2=num2str(fact*round(str2num(str2mat(Tags(tagsi,:)))/fact));
        %  h2=num2str(fact*round(str2num(str2mat(Tags))/fact));
        for i=1:num_Tags
            Tags{tagsi,i}=h2(i,:);
            %  Tags{i}=h2(i,:);
        end
        
    else % cut evenly
        du=floor(linspace(1,length(u),max_num_diff_tags));
        curr=u(1); ck=2;
        for k=1:length(u)
            if le(ck,length(du)) && ge(k,du(ck))
                curr=u(k);
                ck=ck+1;
            end
            which=(J==k);
            Tags(tagsi,which)=curr;
            %       Tags(which)=curr;
        end
    end
    fprintf('%s quantized\n',tagnames{tagsi});
    
    %Flat.v.tags_quantized{tagsi}=Tags(tagsi,:);
    %Flat.Tags(tagsi,:)=Tags(tagsi,:);
    [u,I,J]=unique(Tags(tagsi,:));
    %[u,I,J]=unique(Tags);
    u={u{:},'||'};
    tagboard{tagsi}=u;
    
    tagboardi{tagsi}=cell(1,length(tagboard{tagsi}));
    for i=1:length(I)
        tagboardi{tagsi}{i}=uint32(find(J==i));
    end
    tagboardi{tagsi}(end)={[]};
end
