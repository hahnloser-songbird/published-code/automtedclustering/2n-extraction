function Flat=func_make_compatible_V1(Flat)
  % Make Compatible to Version 1
    if ~isfield(Flat,'templates')
        Flat.templates=ones(1,Flat.p.num_clust);
    end
    
    if ~isfield(Flat,'net')
        Flat.net=cell(Flat.p.num_clust,1);
    end
    
    if ~isfield(Flat.p,'channel_no')
        Flat.p.channel_no=1;
    end
    
    %% Spectrogram
    if ~isfield(Flat.p,'spec_num')
        Flat.p.spec_winls=1; % in seconds, amount of sound to show in figure 24
        Flat.p.spec_winrs=1;
        Flat.p.spec_num=16;
        Flat.specdata=cell(1,Flat.num_Tags);
    end
    
   
    if (~isfield(Flat,'dynamic_range') && Flat.num_Tags>0) || (~isfield(Flat,'dynamic_range_spec') && Flat.num_Tags>0)
        if evalin('base','exist(''output_data'')')
            output_data=evalin('base','output_data');
            [Flat.dynamic_range Flat.dynamic_range_spec]=func_get_dynamic_range(Flat,output_data,Flat.p.spec_log_min);
        else
            fprintf('You are loading old data, help me compress it by giving me ''output_data''\n');
        end
    end
        
    Flat.spec_names={'Cutout win','Syllable win','neural net win','PCA win','Clust win'};
    
    if ~isfield(Flat,'spec')
        Flat.v.numbuffs=func_numbuffs(Flat.p.samp_left+Flat.p.samp_right,Flat.p.nfft,Flat.p.nonoverlap);
        Flat.v.spec_plot_IDs=[];
        Flat.v.spec_new_clust_win=0;
        Flat.v.spec_data_mismatch=0;
        Flat.v.spec_resize=0;
        Flat.spec.samp_left=cellfun(@(x) x*Flat.p.samp_left*ones(Flat.p.num_clust,1),{1,1,1,1,1},'UniformOutput',false);
        Flat.spec.samp_right=cellfun(@(x) x*Flat.p.samp_right*ones(Flat.p.num_clust,1),{1,1,1,1,1},'UniformOutput',false);
        Flat.spec.firstbuff=cellfun(@(x) x*1*ones(Flat.p.num_clust,1),{1,1,1,1,1},'UniformOutput',false);
        Flat.spec.numbuffs=cellfun(@(x) x*Flat.v.numbuffs*ones(Flat.p.num_clust,1),{1,1,1,1,1},'UniformOutput',false);
    end
    
    if ~isfield(Flat.v,'numbuffs_mouseclick')
        Flat.v.numbuffs_mouseclick=Flat.v.numbuffs; % hack
    end
    
    if length(Flat.spec.samp_left)<4
        Flat.spec_names={'Delimit cutout win','Delimit Syllable','Delimit neural net','Delimit PCA window'};
        Flat.spec.samp_left{4}= Flat.spec_samp_left{2};
        Flat.spec.samp_right{4}= Flat.spec_samp_right{2};
    end
    if length(Flat.spec.firstbuff)<4
        Flat.spec.numbuffs{4}= Flat.spec_numbuffs{2};
        Flat.spec.firstbuff{4}= Flat.spec_firstbuff{2};
    end
    
    if length(Flat.spec_names)<5 || length(Flat.spec.numbuffs)<5
        Flat.spec_names={'Delimit cutout win','Delimit Syllable','Delimit neural net','Delimit PCA win','Delimit clust win'};
        Flat.spec.samp_left{5}= Flat.p.samp_left*ones(Flat.p.num_clust,1);
        Flat.spec.samp_right{5}= Flat.p.samp_right*ones(Flat.p.num_clust,1);
        Flat.spec.numbuffs{5}= func_numbuffs(Flat.p.samp_left+Flat.p.samp_right,Flat.p.nfft,Flat.p.nonoverlap)*ones(Flat.p.num_clust,1);
        Flat.spec.firstbuff{5}=-Flat.p.samp_left/Flat.p.nonoverlap*ones(Flat.p.num_clust,1);
    end
    
    if ~isfield(Flat,'v')
        Flat.v.leftmost_element=Flat.leftmost_element;
        Flat.v.view_compressed=Flat.view_compressed;
        Flat.v.plot_ordered=Flat.plot_ordered;
        Flat.v.zoom_level=Flat.zoom_level;
        Flat.v.curr_clust=Flat.curr_clust;
        Flat.v.plot_templates=1;%Flat.plot_templates;
        Flat.v.Screensize=scrsz(3:4);
        Flat.v.max_elements_on_screen=Flat.max_elements_on_screen;
       
        Flat.v.sequence=cell(25,Flat.p.num_clust);
        
        if isfield(Flat,'order_sequence')
            Flat.v.order_sequence=Flat.order_sequence;
        end
    end
    if ~isfield(Flat.v,'templates')
        Flat.v.templates=[];
    end
    
     
    %% temporary fix to get rid of useless fields
    if isfield(Flat,'leftmost_element');Flat = rmfield(Flat, 'leftmost_element');end
    if isfield(Flat,'view_compressed');Flat = rmfield(Flat, 'view_compressed');end
    if isfield(Flat,'plot_ordered');Flat = rmfield(Flat, 'plot_ordered');end
    if isfield(Flat,'suggest_clustering');Flat = rmfield(Flat, 'suggest_clustering');end
    if isfield(Flat,'zoom_level');Flat = rmfield(Flat, 'zoom_level');end
    if isfield(Flat,'curr_clust');Flat = rmfield(Flat, 'curr_clust');end
    if isfield(Flat,'plot_templates');Flat = rmfield(Flat, 'plot_templates');end
    if isfield(Flat,'Screensize');Flat = rmfield(Flat, 'Screensize');end
    if isfield(Flat,'max_elements_on_screen');Flat = rmfield(Flat, 'max_elements_on_screen');end
    if isfield(Flat,'neighbor_sequence');Flat = rmfield(Flat, 'neighbor_sequence');end
    if isfield(Flat,'clustered_sequence');Flat = rmfield(Flat, 'clustered_sequence');end
    if isfield(Flat,'order_sequence');Flat = rmfield(Flat, 'order_sequence');end
    
    if isfield(Flat,'spec_samp_left')
        Flat.spec.samp_left=Flat.spec_samp_left;
        Flat.spec.samp_right=Flat.spec_samp_right;
    end
    if isfield(Flat,'spec_firstbuff')
        Flat.spec.firstbuff=Flat.spec_firstbuff;
        Flat.spec.numbuffs=Flat.spec_numbuffs;
        %         Flat = rmfield(Flat, {'plot_IDs' 'spec_samp_left' 'spec_samp_right' 'spec_firstbuff' 'spec_numbuffs'});
    end
    
    
    %% temporary fix to get rid of useless fields
    if isfield(Flat,'spec_samp_left');Flat = rmfield(Flat, 'spec_samp_left');end
    if isfield(Flat,'spec_samp_right');Flat = rmfield(Flat, 'spec_samp_right');end
    if isfield(Flat,'spec_firstbuff');Flat = rmfield(Flat, 'spec_firstbuff');end
    if isfield(Flat,'spec_numbuffs');Flat = rmfield(Flat, 'spec_numbuffs');end
    
    if isfield(Flat,'plot_IDs');Flat = rmfield(Flat, 'plot_IDs');end
    
    if ~isfield(Flat,'spec_curr_ID')
        Flat.spec_curr_ID=Flat.spec_samp_curr_ID;
    end
    if isfield(Flat,'spec_samp_curr_ID');Flat = rmfield(Flat, 'spec_samp_curr_ID');end
    
    if ~isfield(Flat,'spec_names')
        Flat.spec_names=Flat.spec_samp_names;
    end
    if isfield(Flat,'spec_samp_names');Flat = rmfield(Flat, 'spec_samp_names');end
    
    if isfield(Flat,'syl')
        Flat=rmfield(Flat,'syl');
    end
    
    if isfield(Flat,'net') && all(cellfun(@isempty,Flat.net)) && isfield(Flat,'net_samp')
        Flat=rmfield(Flat,'net_samp');
    end
    
    if ~isfield(Flat.p,'perceptron_ignore')
        Flat.p.perceptron_ignore=6;
    end
    
    if ~isfield(Flat.v,'spec_new_clust_win')
        Flat.v.spec_new_clust_win=0;
    end
    if ~isfield(Flat.p,'perceptron_num_pcs')
        Flat.p.perceptron_num_pcs=20;
    end
    
    if ~isfield(Flat.p,'spec_raw_scale')
        Flat.p.spec_raw_scale=1;
    end
    
    if ~isfield(Flat.p,'spec_raw_offset')
        Flat.p.spec_raw_offset=0;
    end
    if ~isfield(Flat.v,'spec_data_mismatch')
        Flat.v.spec_data_mismatch=0;
    end    
    
    if ~isfield(Flat.p,'clust_names') %names of the clusters.
        Flat.p.clust_names=cell(Flat.p.num_clust,1);
        %default names:
        which_empty=1:Flat.p.num_clust;
    else
        which_empty=find(cellfun(@isempty,Flat.p.clust_names(1:Flat.p.num_clust)));
    end
    
    if ~isempty(which_empty)
        for j=which_empty
            Flat.p.clust_names{j}=['C' num2str(j)];
        end
    end
    
    if isfield(Flat.v,'firstbuff_clust')
        Flat.v=rmfield(Flat.v,'firstbuff_clust');
    end
    
    if isfield(Flat,'firstbuff_clust')
        Flat=rmfield(Flat,'firstbuff_clust');
    end
    
    if isfield(Flat,'firstbuff')
        Flat=rmfield(Flat,'firstbuff');
    end
    
    if ~isfield(Flat.v,'numbuffs')
        Flat.v.numbuffs=Flat.spec.numbuffs{5}(1);
    end
    
    if ~isfield(Flat.v,'pcs_num_display')
        Flat.v.pcs_num_display=4;
    end
    
    if ~isfield(Flat.p,'do_sam');
        Flat.p.do_sam=0;
    end
    
    
    %% transition matrix
    if ~isfield(Flat.p,'graph_clusts')
        Flat.p.graph_clusts=1:4;
    end
    if ~isfield(Flat.p,'graph_thr_percent')
        Flat.p.graph_thr_percent=1;
    end
    if ~isfield(Flat.p,'graph_max_gap_ms')
        Flat.p.graph_max_gap_ms=500;
    end
    if ~isfield(Flat.p,'graph_compressed')
        Flat.p.graph_compressed=0;
    end
    
    if ~isfield(Flat.v,'post')
        Flat.v.post=int8(zeros(1,Flat.num_Tags));
        Flat.v.pre=int8(zeros(1,Flat.num_Tags));
    end
    
    %     Flat.v.post = int8(Flat.v.post);
    %     Flat.v.pre = int8(Flat.v.pre);
    
    
    Flat.p.perceptron_spec_thresh=[];
    if ~isfield(Flat.p,'perceptron_within_ms')
        Flat.p.perceptron_within_ms=50;
    end
    
    if ~isfield(Flat.v,'show_str')
        Flat.v.spec_show_str=0;
    end
    
    if ~isfield(Flat.v,'spec_coupled')
        Flat.v.spec_coupled=1;
        Flat.v.spec_rlines=[Flat.p.samp_left Flat.p.samp_right];
    end
    
    if ~isfield(Flat.v,'spec_resize')
        Flat.v.spec_resize=0;
    end
    
    if ~isfield(Flat.v,'plot_clust_fast')
        Flat.v.plot_clust_fast=0;
    end
    
    if ~isfield(Flat.v,'sequence')
        Flat.v.sequence = cell(25,Flat.p.num_clust);
    end
    
    % old archives are missing this field and can therefore not be saved.
    if ~isfield(Flat.v,'DAT_size_at_load_time')
        Flat.v.DAT_size_at_load_time = 0;
    end    
    
    %% transfer old tags to new Tags
    
    if isfield(Flat,'tags')
        if Flat.num_Tags>0
            Flat.Tags=cellfun(@num2str,Flat.tags,'uniformoutput',false);
            Flat.v.tags_curr_ij=Flat.Tags(:,1);
        end
        Flat=rmfield(Flat,'tags');
    end
    
    
    new_tagnames = {'is_classified', 'is_hidden', 'Site', 'Mark'};
    if isfield(Flat.p, 'tagnames')
        not_string=~cellfun(@isstr,Flat.p.tagnames);
        if ~isempty(not_string)
            Flat.p.tagnames(not_string)={''};
        end
        %% Flat.p.tagnames(1:4) different ?
        if ~isequal(Flat.p.tagnames(1:4),new_tagnames)
            Tags=Flat.Tags;
            for j=3:4
                h=find(ismember(Flat.p.tagnames,new_tagnames{j}));
                if ~isempty(h)
                    tmp = conv_old_tag(Flat.Tags(h,:));
                    Tags(j,:) = tmp;
                    Flat.p.tagnames{j}=new_tagnames{j};
                end
            end
            Tags(1:2,:)={''};
            Flat.Tags=Tags;
        end
    end
    Flat.p.tagnames(1:2) = new_tagnames(1:2);
    
    %%% XXX this should go into write_to_Flats....
    if strcmp(Flat.p.tagnames{2},'is_hidden')
        empty_string=cellfun(@isempty,Flat.Tags(2,:));
        Flat.Tags(2,empty_string)={'0'};
    end
    
    if ~isfield(Flat.p,'tagparameters');
        Flat.p.tagparameters=cellfun(@(x) '',Flat.p.tagnames,'Uniformoutput',false);
    end
    
    
    if ~isfield(Flat,'Tags') || isempty(Flat.Tags)
        Flat.Tags=cell(length(Flat.p.tagnames),Flat.num_Tags);
        Flat.Tags(:)={''};
    end
    
    h=~cellfun(@ischar,Flat.Tags);
    if any(h(:))
        Flat.Tags(h)={''};
        disp('Flat.Tags corrected');
    end
        
    if ~isfield(Flat.v,'grow_leftmost')
        Flat.v.grow_leftmost=1;
    end
    
    if ~isfield(Flat.v,'add_str')
        Flat.v.add_str='';
        Flat.v.end_str='';
    end
    
    if ~isfield(Flat.v,'redtext')
        Flat.v.redtext.templates=[];
        Flat.v.redtext.grow_examples=[];
    end
    
    if ~isfield(Flat, 'grow_examples')
        Flat.grow_examples = [];
    end
    
    if ~isfield(Flat.v,'marktext_ypos')
        Flat.v.marktext_ypos=ceil(length(Flat.p.nfft_i)*28/32);
    end
    
    if ~isfield(Flat.p,'display_marks')
        Flat.p.display_marks=0;
    end
        
    if ~isfield(Flat.p,'allow_empty_clusters')
        Flat.p.allow_empty_clusters=(i>1);
    end
    
    if ~isfield(Flat.p,'savename')
        Flat.p.savename='Archives';
    end
    
    if isfield(Flat,'user_data')
        Flat.user.data=Flat.user_data;
        Flat=rmfield(Flat,'user_data');
    end
        
    %% func_get_data_new
    if ~isfield(Flat.p,'exec_get_fcn');
        Flat.p.exec_get_fcn='';
    end
    
    if ~isfield(Flat.v,'select_tagboard_on')
        Flat.v.select_tagboard_on=0;
    end
    
    if ~isfield(Flat.v,'select')
        Flat.v.select=[];
    end
       
    if ~isfield(Flat.v,'select_clust')
        Flat.v.select_clust=[];
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% DAT
    if ~isfield(Flat.p,'DATplugin');
        Flat.p.DATplugin={'fb_plugin_pitch_HPS','fb_plugin_RMS_500_7K','','','','','','','',''};
    end
    
    if ~isfield(Flat.p,'Channel_names');
        Flat.p.Channel_names={'microphone','electrode'};
    end
       
    if ~isfield(Flat.p,'DATwinl_s')
        Flat.p.DATwinl_s=.3;  % in seconds
        Flat.p.DATwinr_s=.7;
    end
    
    if ~isfield(Flat.v,'DATcurr_chan')
        Flat.v.DATcurr_chan=[];
    end
    
    
    if ~isfield(Flat.p,'DATnum_lines')
        Flat.p.DATnum_lines=500;
    end
    
    if ~isfield(Flat.v,'plot_strs') || length(Flat.v.plot_strs)>2
        Flat.v.plot_strs={'-','D'};
    end
    if ~isfield(Flat.v,'DATplot_last_clust')
        Flat.v.DATplot_last_clust=0;
    end
    
    if ~isfield(Flat.p,'DATplugin_nonoverlap')
        Flat.p.DATplugin_nonoverlap=128;
    end
    if ~isfield(Flat.p,'DATfilt_lowpass')
        Flat.p.DATfilt_lowpass = 6000;
    end
    if ~isfield(Flat.p,'DATfilt_highpass')
        Flat.p.DATfilt_highpass = 300;
    end
    if ~isfield(Flat.p,'DATfilt_order')
        Flat.p.DATfilt_order = 4;
    end
    if ~isfield(Flat.v,'DATColorScale')
        Flat.v.DATColorScale=1;
    end
    
    if ~isfield(Flat.p,'DATthresh_low_lim')
        Flat.p.DATthresh_low_lim = 75; % in muV
    end
    
    if ~isfield(Flat.p,'DATthresh')
        Flat.p.DATthresh = 150; % in muV
    end
    
    if ~isfield(Flat.v,'DATstack')
        Flat.v.DATstack=0; % 0=plugin, 1=spikes
    end
    
    if ~isfield(Flat.v,'DATwinl')
        Flat.v.DATwinl=16000; %
    end
    
    if ~isfield(Flat.v,'DATwinr')
        Flat.v.DATwinr=16000; %
    end
      
    if ~isfield(Flat.v,'all_clust_mode')
        Flat.v.all_clust_mode=1;
    end
    
    %% needs to be reprogrammed if needed
    %     if ~isfield(Flat.DAT,'CHANNELS_SAVED') && isfield(Flat,'CHANNELS_SAVED')
    %         Flat.DAT.CHANNELS_SAVED=zeros(size(Flat.CHANNELS_SAVED,1),length(Flat.DAT.filename));
    %         func_to_list(Flat,'filenames');
    %         [u,I,J]=unique(Flat.filenames);
    %         for j=1:length(u)
    %             which=find(ismember(Flat.DAT.filename,u{j}));
    %             Flat.DAT.CHANNELS_SAVED(:,which)=Flat.CHANNELS_SAVED(:,I(j));
    %         end
    %     end
    %
    %
    %       if ~isfield(Flat.DAT,'gain')
    %           Flat.DAT.gain=zeros(1,Flat.num_Tags);
    %       end
    %
    %       if ~isfield(Flat.DAT,'buffersize')
    %           Flat.DAT.buffersize=zeros(1,Flat.num_Tags);
    %       end
    %
    %     if ~isfield(Flat.p,'bootstrap_from_sound_RMS')
    %         Flat.p.bootstrap_from_sound_RMS=0;
    %     end
    %
    %if ~isfield(Flat.p,'do_sound_RMS_plug')
    % Flat.p.do_sound_RMS_plug=0;
    %end
        
    
    if ~isfield(Flat.v,'order_sequence_DAT')
        Flat.v.order_sequence_DAT=[];
    end
    
    
    if isfield(Flat.p,'tagpointer_sort')
        Flat.v.tagpointer_sort=Flat.p.tagpointer_sort;
        Flat.p=rmfield(Flat.p,'tagpointer_sort');
    end
    
    if ~isfield(Flat.v,'tagpointer_sort')
        Flat.v.tagpointer_sort=1;
    end
        
    if ~isfield(Flat.v,'DATshow_single_unit')
        Flat.v.DATshow_single_unit=0;
    end
    
    if size(Flat.templates,2)>size(Flat.templates,1)
        Flat.templates=Flat.templates';
    end
     
    
    if ~isfield(Flat.v,'plot_dayhist')
        Flat.v.plot_dayhist=0;
    end
    
    if ~isfield(Flat.v,'plot_frate')
        Flat.v.plot_frate=0;
    end
    
    if ~isfield(Flat.v,'dat_max_text_lines')
        Flat.v.dat_max_text_lines=50;
    end
    
    if ~isfield(Flat.v,'dat_text_cleaned_up')
        Flat.v.dat_text_cleaned_up=1;
    end
    
    if ~isfield(Flat.v,'DATlimits')
        Flat.v.DATlimits='DATwin';
    end
    
    if ~isfield(Flat.v,'DATshow_axes')
        Flat.v.DATshow_axes=0;
    end
    
    if ~isfield(Flat.v,'order_sequence_DAT_with_esc')
        Flat.v.order_sequence_DAT_with_esc=[];
    end
    
      %% Board
    if ~isfield(Flat,'BOARD')
        Flat.BOARD.row=[]; Flat.BOARD.col=[]; Flat.BOARD.SYL=[]; 
    end
    
    if ~isfield(Flat.BOARD,'tagboard') || length(Flat.BOARD.tagboard)<length(Flat.p.tagnames)
        Flat.BOARD.tagboard=cell(length(Flat.p.tagnames),1);
    end
    
    if ~isfield(Flat,'BOARDLIST')
        Flat.BOARDLIST=struct('title','','tagboard',cell(length(Flat.p.tagnames),1),'row',[],'col',[],'SYL',[],'Plot',[],'Data',[],'Param',[],'parent',0,'created',0,'modified',0);
    end
    
    if ~isempty(Flat.BOARDLIST) &&  ~isfield(Flat.BOARDLIST(1),'created')
        for j=1:length(Flat.BOARDLIST)
            Flat.BOARDLIST(j).created=now;
            Flat.BOARDLIST(j).modified=Flat.BOARDLIST(j).created;
            Flat.BOARDLIST(j).parent=0;
        end
        Flat.BOARD.created=now;
        Flat.BOARD.modified=0;
        Flat.BOARD.parent=0;
    end

    
    if ~isfield(Flat.BOARD,'row')
        Flat.BOARD.row=[];
    end
    
    if ~isfield(Flat.BOARD,'col')
        Flat.BOARD.col=[];
    end
    
    if ~isfield(Flat.BOARD,'SYL')
        Flat.BOARD.SYL=[];
    end
       
    if ~isfield(Flat.v,'board_num')
        Flat.v.board_num=1;
    end
    
    
    if ~isfield(Flat.v,'plot_numbutton')
        Flat.v.plot_numbutton=0;
    end
    
    if ~isfield(Flat.v,'plot_tagboard')
        Flat.v.plot_tagboard=0;
    end
    
    %% Table
    if ~isfield(Flat.v,'PLOT')
        Flat.v.PLOT={};
    end
    
    if ~isfield(Flat.v,'table_curr')
        Flat.v.table_curr='table_frate';
    end
    
    if ~isfield(Flat.p,'Tables')
        Flat.p.Tables={'frate','dprime','','','','',''};
    end
    
    if ~isfield(Flat.p,'Table_plot')
        Flat.p.Table_plot={'hist'};
    end
    
    if ~isfield(Flat.p,'Table_min_n')
        Flat.p.Table_min_n=2;
    end
    
    if ~isfield(Flat.p,'Table_frate_limit')
        Flat.p.Table_frate_limit='data_off';
    end
    
    
    %% PLOT
    if ~isfield(Flat.v.PLOT,'table')
        Flat.v.PLOT.table=0;
    end
    
    if ~isfield(Flat.v.PLOT,'DAT')
        Flat.v.PLOT.DAT=0;
    end
    
    if ~isfield(Flat.v.PLOT,'spec')
        Flat.v.PLOT.spec=0;
        Flat.v.spec_skip_plot=0;
    end
    
    if ~isfield(Flat.v,'plot_table')
        Flat.v.plot_table=0;
    end
    
    
    %% ANDREAS' stuff
    if ~isfield(Flat.v,'load_fixed_position')
        switch userID
            case 2
                Flat.v.load_fixed_position = 0;
              otherwise
                Flat.v.load_fixed_position = 0;
        end
    end
    
    % used in figure 24, evalkey_spec, "case 'j'"
    if ~isfield(Flat.p,'add_new_element_to_cluster')
        Flat.p.add_new_element_to_cluster = 0;
    end
    
    if ~isfield(Flat.p,'save_spectrogram')
        Flat.p.save_spectrogram = 0;
    end
    
    if ~isfield(Flat.p,'tag_master')
        Flat.p.tag_master=5;
    end
    if ~isfield(Flat.p,'tag_master_str')
        Flat.p.tag_master_str=[];
    end
    
    if ~isfield(Flat.v,'tags')
        Flat.v.tags=struct;
    end
    
    if ismember('activator',Flat.p.tagnames)
        act=find(ismember(Flat.p.tagnames,'activator'));
        Flat.v.tags.activator=act;
    end
    
    
    if ~isfield(Flat.p,'CHANNELS_PLUGIN_DATA')
        if isfield(Flat.p,'DATplugin_CHANNELS')
            Flat.p.CHANNELS_PLUGIN_DATA= Flat.p.DATplugin_CHANNELS;
            Flat.p=rmfield(Flat.p,'DATplugin_CHANNELS');
        else
            Flat.p.CHANNELS_PLUGIN_DATA=zeros(size(Flat.p.DATplugin));
        end
    end
    
    if ~isfield(Flat.p,'CHANNELS_ELECTRODES')
        if isfield(Flat.v,'ELECTRODE_CHANNELS')
            Flat.p.CHANNELS_ELECTRODES= Flat.v.ELECTRODE_CHANNELS;
            Flat.v=rmfield(Flat.v,'ELECTRODE_CHANNELS');
        else
            Flat.p.CHANNELS_ELECTRODES=[];
        end
    end
    
    if ~isempty(Flat.p.CHANNELS_ELECTRODES)  && (isempty(Flat.v.DATcurr_chan) || isempty(find(Flat.v.DATcurr_chan==Flat.p.CHANNELS_ELECTRODES)))
        Flat.v.DATcurr_chan=Flat.p.CHANNELS_ELECTRODES(1);
    end
    
    if ~isempty(Flat.p.CHANNELS_ELECTRODES) && max(Flat.p.CHANNELS_ELECTRODES)>length(Flat.p.DATthresh_low_lim)
        Flat.p.DATthresh_low_lim(end+1:max(Flat.p.CHANNELS_ELECTRODES))=Flat.p.DATthresh_low_lim(end);
    end
    
    if ~isfield(Flat.p,'CHANNEL_MIC') || ~isnumeric(Flat.p.CHANNEL_MIC)
        if isfield(Flat.v,'CHANNEL_MIC')
            Flat.p.CHANNEL_MIC= Flat.v.CHANNEL_MIC;
            Flat.v=rmfield(Flat.v,'CHANNEL_MIC');
        else
            if Flat.num_Tags>0
                Flat.p.CHANNEL_MIC = 1;
            end
        end
    end
    
    
    if ~isfield(Flat.p,'CHANNELS_KEEP_RAW')
        if isfield(Flat.p,'DATkeep_raw')
            Flat.p.CHANNELS_KEEP_RAW=Flat.p.DATkeep_raw;
            Flat.p=rmfield(Flat.p,'DATkeep_raw');
        else
            Flat.p.CHANNELS_KEEP_RAW=[];
        end
    end
    if ~isfield(Flat.p,'CHANNELS_FILTER')
        if isfield(Flat.v,'DATfilt_channels')
            Flat.p.CHANNELS_FILTER=Flat.v.DATfilt_channels;
            Flat.v=rmfield(Flat.v,'DATfilt_channels');
        else
            Flat.p.CHANNELS_FILTER=[];
        end
    end
    
    if ~isfield(Flat.p,'spec_full_mode')
        Flat.p.spec_full_mode=0;
    end
    
    Flat.v.plot_clust_white_lines=1;
    
    if ~isfield(Flat.p,'SPARSE_compute')
        Flat.p.SPARSE_compute=0;
        Flat.p.SPARSE_apply=0;
        Flat.p.SPARSE_nFeatures=100;
        Flat.p.SPARSE_nIterations=2000;
        Flat.p.SPARSE_filt_width=10;
        Flat.p.SPARSE_dynamic_range=32;
        Flat.p.SPARSE_buffersize=512;
        Flat.p.SPARSE_startsample=512;
        Flat.p.SPARSE_nonoverlap=128;
        Flat.p.SPARSE_name='SPARSE';
        Flat.p.SPARSE_colorscale=10;
    end
    
    if ~isfield(Flat.p,'PCAColorScale')
        Flat.p.PCAColorScale=1;
    end
    
    if ~isfield(Flat.v,'DATspec')
        Flat.v.DATspec=cell(Flat.p.num_clust,1);
    end
        
      
    if ~isfield(Flat.v,'DATxlabel')
        Flat.v.DATxlabel='';
        Flat.v.DATylabel='';
    end
    
    if ~isfield(Flat.v,'DATnonlinearity')
        Flat.v.DATnonlinearity=0;
    end
    
    if ~isfield(Flat.v,'import') || ~isfield(Flat.v.import,'sam')
        Flat.v.import=func_help_import_define;
    end
    
    if ~isfield(Flat.p,'save_appendix')
        Flat.p.save_appendix={'BOARDLIST','SPARSE','',''};
    end
    
    if ~isfield(Flat.v,'save')
        Flat.v.save.DAT_data=1;
        Flat.v.save.DAT_multiunit=1;
        Flat.v.save.DAT_STACK=1;
        Flat.v.save.BOARDLIST=1;
    end
            
    
    if isfield(Flat.v,'sequence') && size(Flat.v.sequence,1)<25
        Flat.v.sequence(end+1:25,:)={[]};
    end
    
    
    if ~isfield(Flat.p,'tagnames_per_file')
        Flat.p.tagnames_per_file=zeros(length(Flat.p.tagnames),1);
        Flat.p.tagnames_per_file(3)=1; % 'Site'
    end
    
    if length(Flat.p.tagnames_per_file)<length(Flat.p.tagnames)
        Flat.p.tagnames_per_file(end+1:length(Flat.p.tagnames))=0;
    end
    
    
    if ~isfield(Flat.p,'importI_function')
        Flat.p.importI_function='';
    end
    
    if ~isfield(Flat.v,'DATplot_yaxis')
        Flat.v.DATplot_yaxis=0;
    end
    
    if ~iscell(Flat.p.DATplugin)
        Flat.v.import.plugin_data=zeros(1,10);
        if isempty(Flat.p.DATplugin)
            Flat.p.DATplugin={'','','','','','','','','',''};
            Flat.p.CHANNELS_PLUGIN_DATA=zeros(1,10);
        else
            Flat.p.DATplugin={Flat.p.DATplugin,'','','','','','','','',''};
            Flat.p.CHANNELS_PLUGIN_DATA=[Flat.p.CHANNELS_PLUGIN_DATA(1) zeros(1,9)];
        end
        if isfield(Flat.v,'stack') && isfield(Flat.v.stack,'plugin')
            h=Flat.v.stack.plugin;
            Flat.v.stack.plugin=h(find(h));
        end
        if isfield(Flat.DAT,'curr_plug')
            curr_plug=Flat.DAT.curr_plug;
            Flat.DAT=rmfield(Flat.DAT,'curr_plug');
            Flat.DAT.curr_plug{1}=curr_plug;
        end
    end
        
    
    if ~isfield(Flat.p,'import_cluster')
        if strcmp(Flat.p.importI_function,'func_importI_tutoring_josh')
            Flat.p.import_cluster=2;
        else
            Flat.p.import_cluster=0;
        end
    end
    
    if ~isfield(Flat.p,'import_is_song')
        Flat.p.import_is_song=1;
    end
    
    if ~isfield(Flat, 'annotations')
        Flat.annotations = struct('text', {}, 'color', {}, 'timestamp', {});
    end
    
    if ~isfield(Flat.v,'set_spec_consistency')
        Flat.v.set_spec_consistency=0;
    end
    
    if ~isfield(Flat.v,'DAT_crossings_hist_done')
        Flat.v.DAT_crossings_hist_done=0;
    end
    
    if ~isfield(Flat.v,'tagboard0_recompute')
        Flat.v.tagboard0_recompute=1;
    end
    
    if ~isfield(Flat.v,'tagboard0_replot')
        Flat.v.tagboard0_replot=1;
    end
    
    if ~isfield(Flat.v,'DATspk_done')
        Flat.v.DATspk_done=zeros(1,Flat.num_Tags);
        Flat.v.DATspk=cell(1,Flat.num_Tags);
    end
        
    if ~isfield(Flat.p,'Tagboard_execute')
        Flat.p.Tagboard_execute={'','','','',''};
    end
        
    if ~isfield(Flat.p,'tags_quantize_threshold')
        Flat.p.tags_quantize_threshold=1e9;
    end
    
    if ~isfield(Flat.p,'verbose')
        Flat.p.verbose=1;
    end
    
    if ~isfield(Flat,'X')
        Flat.X = [];
    end
        
    if ~isfield(Flat.v,'rms_offset')
        Flat.v.rms_offset=false;
        Flat.v.rms_offset_done=0;
    end
    
    if ~isfield(Flat.p,'ignore_noise_rms')
        Flat.p.ignore_noise_rms=0;
    end
    
    if ~isfield(Flat.v,'DATmark_lines_pos')
        Flat.v.DATmark_lines_last_changed=1;
        Flat.v.DATmark_lines_pos=zeros(2);
        Flat.v.DATmark_lines=[false false];
        for j=1:length(Flat.BOARDLIST)
            Flat.BOARDLIST(j).v.DATmark_lines_pos=zeros(2);
            Flat.BOARDLIST(j).v.DATmark_lines=[false false];
        end
        Flat.BOARD.v.DATmark_lines_pos=zeros(2);
        Flat.BOARD.v.DATmark_lines=[false false];
    end
    
    
    if isfield(Flat,'raw')
        Flat=rmfield(Flat,'raw');
    end
    
    if isfield(Flat,'raw_pre')
        Flat=rmfield(Flat,'raw_pre');
    end
    if isfield(Flat,'filetype')
        Flat=rmfield(Flat,'filetype');
    end
    
    if isfield(Flat.v,'DATspk_do')
        Flat.v=rmfield(Flat.v,'DATspk_do');
    end
    
    if isfield(Flat,'Effector_notes')
        Flat=rmfield(Flat,'Effector_notes');
    end
    
    
    if isfield(Flat,'scanrate') && iscell(Flat.scanrate) %&& numel(Flat.scanrate) > 1)
        Flat.scanrate = Flat.scanrate{1};
    end
    
    if ~isfield(Flat.p,'DAT_frate_smooth_ms')
        Flat.p.DAT_frate_smooth_ms=30;
    end
    
   if ~isfield(Flat.p,'DAT_frate_signrank')
        Flat.p.DAT_frate_signrank=0;
    end
    
   if ~isfield(Flat.p,'DAT_frate_tagpoint_pair_sig')
        Flat.p.DAT_frate_tagpoint_pair_sig=0;
   end
    
    
    if ~isfield(Flat.p,'spec_log_min')
        Flat.p.spec_log_min=0.1;
    end
    
    if isfield(Flat,'data') && Flat.p.spec_full_mode==1
        Flat=rmfield(Flat,'data');
    end
    
    if isfield(Flat.X,'data') && Flat.p.spec_full_mode==1
        Flat.X=rmfield(Flat.X,'data');
    end
             
    if ~isfield(Flat.v,'DATspec_i')
        Flat.v.DATspec_i=zeros(Flat.p.num_clust,1);
    end
    
    %% to DAT: data_eof, timestamp, filename, subdir
    %% example Flat.DAT.eof(Flat.x)=Flat.data_eof
    %% Flat.DAT.timestamp(Flat.DATindex)=Flat.timestamp
    %% remove fields in make_compatible
    %%
    %% to int: v.select, v.select_clust, graph_context, indices_all, DATspk_do, DATspk_done net_samp, grow_examples, DAT.CHANNELS_SAVED (8), DAT.GAIN (16), indices_all, ParseActIndex, DAT.eof, DATindex,
    %% rich: rm specdata, rm data,  v.post, v.pre, scanrate, birdname, rm net_samp,
    
       
    %% real consistency checks
    if length(Flat.v.DATspec)<Flat.p.num_clust
        h=Flat.v.DATspec;
        Flat.v.DATspec=cell(Flat.p.num_clust,1);
        Flat.v.DATspec(1:length(h))=h;
    end

    %% DAT checks  %YR%
    if ~isfield(Flat,'DAT')
        Flat.DAT.subdir={};
        Flat.DAT.filename={};
        Flat.DAT.timestamp=[];
        Flat.DAT.eof=[];
        Flat.DAT.data={};
    end
    
    % check for missing DAT fieldnames
    fieldnames_to_check = {'subdir', 'filename', 'timestamp', 'eof', 'data'};
    preallocate_values = {{}, {}, [], [], {}};
    
    for j = 1 : numel(fieldnames_to_check)
        if ~isfield(Flat.DAT, fieldnames_to_check{j})
            Flat.DAT.(fieldnames_to_check{j}) = preallocate_values{j};
        end
    end
    
    
    
    %%  %%%%%%%%%% Archives without DATindex
    if Flat.num_Tags>0 &&  ~isfield(Flat.X,'DATindex') && isfield(Flat.X,'subdir')
        dirfiles = cell(1, Flat.num_Tags);
        subdir=Flat.X.subdir;
        filename=Flat.X.filenames;
        for j = 1 : Flat.num_Tags
            dirfiles{j} = [subdir{j} filename{j}];
        end
        
        %% make an old Flat compatible
        [u,I,J]=unique(dirfiles);
        if isempty(Flat.DAT) || isempty(Flat.DAT.filename)
            num_dummy_channels=6;
            fprintf('DAT field in Flat empty - filling now\n');
            Flat.DAT.subdir=cellfun(@(x) x(1:10),u,'UniformOutput',false);
            Flat.DAT.filename=cellfun(@(x) x(11:end),u,'UniformOutput',false);
            Flat.DAT.timestamp=Flat.timestamp(I);
            Flat.DAT.eof=Flat.data_eof(I);
        end
        Flat.X.DATindex=J;
        
        if isfield(Flat,'filenames')
            Flat.X=rmfield(Flat.X,'filenames');
        end
        if isfield(Flat,'subdir')
            Flat.X=rmfield(Flat.X,'subdir');
        end
        %    Flat.X=rmfield(Flat.X,'data_eof');
    end
    
    % why do we remove 'data_eof' here if we add it at the top?
    if isfield(Flat,'data_eof') && ~any(Flat.DAT.eof==0)
        Flat.X.data_eof=Flat.data_eof;
        Flat=rmfield(Flat,'data_eof');
    end
    
    %% remove backup subdir and filenames into X
    if isfield(Flat,'subdir')
         Flat=rmfield(Flat,'subdir');
    end
    
    if isfield(Flat,'filenames')
        Flat=rmfield(Flat,'filenames');
    end
    
    %% MOVE fields
    Flat = func_move_X_fields(Flat);
    Flat = func_move_V_fields(Flat);
    
    if isfield(Flat,'birdnames')
        Flat=rmfield(Flat,'birdnames');
    end
    
    if isfield(Flat,'timestamp')
        Flat=rmfield(Flat,'timestamp');
    end
    
    
    %%
    
    %% The following requires DAT
    if ~isfield(Flat.DAT,'GAIN') && isfield(Flat.DAT,'CHANNELS_SAVED')% && isfield(Flat.DAT,'GAIN')
        Flat.DAT.GAIN=uint16(Flat.REC.gain)*ones(size(Flat.DAT.CHANNELS_SAVED),'uint16');
    end
    
    if isfield(Flat.DAT,'stack')
        for j=1:length(Flat.DAT.stack)
            if length(Flat.DAT.stack(j).dynamic_range)<2
                Flat.DAT.stack(j).dynamic_range=[-Flat.DAT.stack(j).dynamic_range Flat.DAT.stack(j).dynamic_range];
            end
        end
    end
    
    if ~isfield(Flat.v,'tagboard0i') && Flat.num_Tags>0
        [Flat.Tags,Flat.v.tagboard0,Flat.v.tagboard0i]=func_help_tagboard0(Flat.Tags,Flat.p.tagnames,Flat.DAT.timestamp(end),Flat.p.tags_quantize_threshold);
    end
    
    if isfield(Flat.DAT,'stack') && ~isfield(Flat.DAT.stack,'comment')
        for j=1:length(Flat.DAT.stack)
            Flat.DAT.stack(j).comment='';
        end
    end
    
    if isfield(Flat.DAT,'thresh') && ~isempty(Flat.p.CHANNELS_ELECTRODES) && size(Flat.DAT.thresh,1)<max(Flat.p.CHANNELS_ELECTRODES)
        Flat.DAT.thresh(end+1:max(Flat.p.CHANNELS_ELECTRODES),:)=ones(max(Flat.p.CHANNELS_ELECTRODES)-size(Flat.DAT.thresh,1),1)*Flat.DAT.thresh(1,:);
    end
    
    if isfield(Flat.DAT,'song_on_off');
        Flat.DAT.Song_on_off=Flat.DAT.song_on_off;
        Flat.DAT=rmfield(Flat.DAT,'song_on_off');
    end
    
    if isfield(Flat.DAT,'playback_on_off');
        Flat.DAT.Playback_on_off=Flat.DAT.playback_on_off;
        Flat.DAT=rmfield(Flat.DAT,'playback_on_off');
    end
    
    if isfield(Flat.DAT,'spk') && ~isfield(Flat.DAT,'spk_esc')
        if isfield(Flat.p,'CHANNELS_ELECTRODES') && ~isempty(Flat.p.CHANNELS_ELECTRODES) 
        l=max(Flat.p.CHANNELS_ELECTRODES)+1;
        else
            l=5;
        end
        Flat.DAT.spk_esc=cell(1,length(Flat.DAT.spk));
        Flat.DAT.spk_esc(:)={cell(l,1)};
    end
    
    if isfield(Flat,'buffersizeREC') &&  Flat.num_Tags>0
        Flat.DAT.buffersizeREC=uint16(Flat.buffersizeREC(1)*ones(1,length(Flat.DAT.filename))); %% cheap
        Flat=rmfield(Flat,'buffersizeREC');
    end 
    
    if ~isfield(Flat.v,'DATstack_sort') && isfield(Flat.DAT,'stack')
        Flat.v.DATstack_indices=[0 1:length(Flat.DAT.stack)];
        Flat.v.DATstack_sort=1;
    end
    
    if any(Flat.X.clust_ID==0)
        h=find(Flat.X.clust_ID==0);
        fprintf('Set %d elements with clust_ID==0 to 1\n',length(h));
        Flat.X.clust_ID(h)=1;
    end
    
    
    if isfield(Flat,'custom')
        Flat.X.custom=Flat.custom;
        Flat=rmfield(Flat,'custom');
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% END PART 1
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %% Correct external playback offset, is now done in parse2flats
if Flat.num_Tags>0  && (~isfield(Flat.v,'help') || ~isfield(Flat.v.help,'external_playback_correct'))
    if isfield(Flat.p, 'Activators') == 1
        ext_pb=find(ismember(Flat.p.Activators,'activator_external_playback'));
        if ~isempty(ext_pb)
            which_elements=find(strcmp(Flat.Tags(Flat.v.tags.activator,:),num2str(ext_pb)));
            bufrec=double(Flat.DAT.buffersizeREC(Flat.X.DATindex(which_elements)));
            Flat.X.indices_all(which_elements)=Flat.X.indices_all(which_elements)+bufrec+64*(bufrec==128)+1; % Josh's formula
            Flat.v.help.external_playback_correct=1;
            fprintf('Corrected the onsets (indices_all) of %d external_activator_playback tags\n',length(which_elements));
        end
    end
end
end



function new_tags = conv_old_tag(old_tags)

% convert everything into doubles. old archives have tags like '01',
% '02', etc.
tmp = str2double(old_tags);
new_tags = arrayfun(@(x) converter(x), tmp, 'UniformOutput', 0);

    function element = converter(element)
        
        % NAN element needs to be converted into a ''
        if isnan(element) == 1
            element = '';
            % everything else gets converted into a string.
        else
            element = num2str(element);
        end
    end
end
%% fix old user archives for kotowicz
%
% for i=1:60
%     if isfield(Flat,'user')
%         h=Flat.user.data;
%         Flat.user.data=cell(1,Flat.num_Tags);
%         Flat.user.data(1:length(h))=h;
%
%         h=Flat.user.is_silence;
%         Flat.user.is_silence=zeros(1,Flat.num_Tags);
%         Flat.user.is_silence(1:length(h))=h;
%     end
% end

% function status=func_help_num_tagstrings_check(Flat,max_num_diff_tags)
% status=0;
% for j=1:length(Flat.p.tagnames)
%      u=unique(Flat.Tags(j,:));
%    if length(u)>max_num_diff_tags
%        status=j;
%    end
% end
% end



%
% function Tags=func_help_num_tagstrings_correct(Flat,tagsi,max_num_diff_tags)
%     Tags=Flat.Tags;
% [u,I,J]=unique(Tags(tagsi,:));
%
% u_num=str2num(str2mat(u));
% if isempty(u_num)
%     isnumTag=0;
% else
%     isnumTag=1;
% end
%
%      if isnumTag % quantize
%          range_u=range(u_num);
%          fact= range_u/(max_num_diff_tags-1);
%          digit=ceil(log10(fact/100));
%          fact=quant(fact,10^digit);
%
%          h=find(strcmp(Tags(tagsi,:),''));
%          Tags(tagsi,h)={'0'};
%          h=str2num(str2mat(Tags(tagsi,:)));
%          h2=num2str(fact*round(str2num(str2mat(Tags(tagsi,:)))/fact));
%          for i=1:Flat.num_Tags
%          Tags{tagsi,i}=h2(i,:);
%          end
%
%      else % cut evenly
%          du=floor(linspace(1,length(u),max_num_diff_tags));
%         curr=u(1); ck=2;
%        for k=1:length(u)
%            if le(ck,length(du)) && ge(k,du(ck))
%                curr=u(k);
%                ck=ck+1;
%            end
%            which=(J==k);
%             Tags(tagsi,which)=curr;
%        end
%      end
% end
% %% EOF
