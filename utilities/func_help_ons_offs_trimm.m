function [ons,offs,success]=func_help_ons_offs_trimm(sd,thet)
success=0;

ons=find(sd(2:end)>thet & sd(1:end-1)<=thet);
offs=find(sd(2:end)<=thet & sd(1:end-1)>thet);
if size(ons,1)>size(ons,2)
    ons=ons';
end
if size(offs,1)>size(offs,2)
    offs=offs';
end
if isempty(ons) || isempty(offs)
     ons=[]; offs=[];
%    disp('no onsets or offsets');
    return
end

% weird threshold crossings:

if length(offs)==1
    if length(ons)==1 && ons>offs % off-on
        return
    elseif length(ons)>1 % on-off-on
        ons=ons(1);
    end
end
if length(offs)>1 && ons(1)>offs(1)
    offs=offs(2:end);
end

if length(ons)>1 && offs(end)<ons(end)
    ons=ons(1:end-1);
end

% if length(offs)*length(ons)==0
%     ons=[]; offs=[];
%     return
% end
if ~(length(offs)==length(ons)) || any(offs-ons<0)
    disp('Problem');
    keyboard
end
success=1;
end