function Flat=func_sequence_cleanup(Flat,which_del)

    for i=1:size(Flat.v.sequence,1)
        if ~isempty(Flat.v.sequence{i,Flat.v.curr_clust})
            if length(which_del)>1
                hi2=find(ismember(Flat.v.sequence{i,Flat.v.curr_clust},which_del));
            else
                hi2=find(Flat.v.sequence{i,Flat.v.curr_clust}==which_del);
            end
            if ~isempty(Flat.v.sequence{i,Flat.v.curr_clust})
            Flat.v.sequence{i,Flat.v.curr_clust}(hi2)=[];
            end
        end
    end
    
 %   hi3=find(Flat.v.select_clust==which_del);
    hi3=ismember(Flat.v.select_clust,which_del);
    Flat.v.select_clust(hi3)=[];

end
%% EOF
