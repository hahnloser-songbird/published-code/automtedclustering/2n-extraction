% make old Flat variables compatible with new one
function Flats=func_make_compatible(Flats)
%% XXX todo: ask Nicolas to check that all v and p fields are defined in func_default_Flat_values

userID = 1;
scrsz = get(0,'ScreenSize');
% this is probably a dual screen setup - only use one screen.
if scrsz(3) > 2560
    scrsz(3) = scrsz(3)/2;
end
%disp('hallo du');
names=cellfun(@(x) x.name,Flats,'UniformOutput',false);


for i=1:length(Flats)
    
    
    %%  Version 0
    if ~isfield(Flats{i}.p,'Ver')
        Flats{i}=func_make_compatible_V1(Flats{i});
        Flats{i}=func_finalize_make_compatibe(Flats{i});
        
        Flats{i}.p.Ver=1;
    end
    
    %% Version 1
    if  Flats{i}.p.Ver==1
        
        %% code comes here
        if isfield(Flats{i}.p,'umap') && isfield(Flats{i}.p.umap,'brute_force') && ~isfield(Flats{i}.p.umap.brute_force,'buff_span')
            Flats{i}.p.umap.brute_force.buff_span=2;
        end
        
        if isfield(Flats{i}.p,'umap') & ~isfield(Flats{i}.p.umap,'MinNumTimeSegments')
            Flats{i}.p.umap.MinNumTimeSegments=4;
        end
        
        if ~isfield(Flats{i}.p,'STACKscaled')
            Flats{i}.p.STACKscaled=0;
        end
        
        if ~isfield(Flats{i}.X,'tsne_manually_moved')
            Flats{i}.X.tsne_manually_moved=zeros(1,Flats{i}.num_Tags);
        end
        
        %  if ~isfield(Flats{i}.v.tsne,'manual_move_counter')
        Flats{i}.v.tsne.manual_move_counter=0;
        %  end
        
        if ~isfield(Flats{i}.DAT,'Tags')
            Flats{i}=func_help_tags_per_file_to_DAT(Flats{i});
        end
        
        if isfield(Flats{i},'dynamic_range') && ~isfield(Flats{i}.p,'dynamic_range')
            Flats{i}.p.dynamic_range=Flats{i}.dynamic_range;
            Flats{i}.p.dynamic_range_spec=Flats{i}.dynamic_range_spec;
        end
        
        if ~isfield(Flats{i}.v,'chanell_mic_consistent')
            Flats{i}.v.chanell_mic_consistent=1;
        end
        
        if ~isfield(Flats{i}.p,'DAT_curve_smooth')
            Flats{i}.p.DAT_curve_smooth=1;
        end
        if ~isfield(Flats{i}.p,'DAT_curve_std')
            Flats{i}.p.DAT_curve_std=0;
        end
        
        if ~isfield(Flats{i}.v,'display_multi')
            Flats{i}.v.display_multi=0;
        end
        
        if isfield(Flats{i}.v,'import_DAT')
            if  isfield(Flats{i}.p,'syllable') && ~isfield(Flats{i}.p.syllable,'opt')
                if isfield(Flats{i}.p.syllable,'fileN')
                    Flats{i}.p=rmfield(Flats{i}.p,'fileN');
                end
                if isfield(Flats{i}.p.syllable,'i0')
                    Flats{i}.p=rmfield(Flats{i}.p,'i0');
                end
                if isfield(Flats{i}.p.syllable,'ncols')
                    Flats{i}.p=rmfield(Flats{i}.p,'ncols');
                end
                Flats{i}.p.syllable.imics=1:length(Flats{i}.p.CHANNEL_MIC);%Flat.p.CHANNEL_MIC; % Microphone channels to be used for syllable detection (index into Flat.p.Channel_names)
                Flats{i}.p.syllable.imicsN = Flats{i}.p.channel_mic_display;
                for j=1:length(Flats{i}.p.syllable.sum)
                    Flats{i}.p.syllable.sum(j).smoothingWidth=6;
                end
                Flats{i}.p.syllable.multi=0;
                Flats{i}.p.syllable.opt=0;
                
                h=Flats{i}.v.import_DAT;
                Flats{i}.v=rmfield(Flats{i}.v,'import_DAT');
                Flats{i}.v.import_DAT.fileN=h.fileN;
                Flats{i}.v.import_DAT.i0=h.i0;
                Flats{i}.v.import_DAT.ncols=h.ncols;
            end
        end
        if  ~isfield(Flats{i}.p,'syllable')
            Flats{i}.p.syllable=[];
        end
        
        
        
        if  isfield(Flats{i}.p,'CHANNEL_MIC')
            
            
%             if  ~isfield(Flats{i}.p.syllable,'imicsN')
%                 if isfield(Flats{i}.p, 'channel_mic_display')
%                     Flats{i}.p.syllable.imicsN=Flats{i}.p.channel_mic_display;
%                 else
%                     Flats{i}.p.syllable.imicsN=1;
%                 end % until here replaced by Anna
%             end
            
            if ~isfield(Flats{i}.p,'Channel_syl_tagstring') || length(Flats{i}.p.Channel_syl_tagstring)<length(Flats{i}.p.CHANNEL_MIC)
                for j=1:length(Flats{i}.p.CHANNEL_MIC)
                    if length(Flats{i}.p.Channel_names)>=Flats{i}.p.CHANNEL_MIC(j)
                        Flats{i}.p.Channel_syl_tagstring{j}=Flats{i}.p.Channel_names{Flats{i}.p.CHANNEL_MIC(j)};
                    end
                end
            end
            
            if ~isfield(Flats{i}.v,'RMS') || ~ismember(Flats{i}.v.RMS,Flats{i}.p.Channel_syl_tagstring)
                if  length(Flats{i}.p.CHANNEL_MIC)==1
                    Flats{i}.v.RMS='RMSfilt';
                    [Flat,sel]=func_select_tagboard(Flats{i},[],Flats{i}.p.tagnames{Flats{i}.v.tags.activator},Flats{i}.p.Channel_syl_tagstring(1));
                    if ~isempty(sel)
                        Flats{i}.Tags(Flats{i}.v.tags.activator,sel)={'RMSfilt'};
                    end
                    Flats{i}.p.Channel_syl_tagstring{1}='RMSfilt';
                    Flats{i}.p.Channel_names{Flats{i}.p.CHANNEL_MIC}='RMSfilt';
                else
                    Flats{i}.v.RMS=Flats{i}.p.Channel_syl_tagstring{1};
                    Flats{i}.p.channel_mic_display=1;
                end
            end
            
            if ~isfield(Flats{i}.p,'spec_min')
                if length(Flats{i}.p.CHANNEL_MIC)==1 && isfield(Flats{i}.DAT,'data')
                    Flats{i}.p.spec_min=double(min(min(cell2mat(Flats{i}.DAT.data(cellfun(@isempty,Flats{i}.DAT.data)==0)))));
                elseif length(Flats{i}.p.CHANNEL_MIC)>1 && isfield(Flats{i}.DAT,'datas')
                    for j=1:length(Flats{i}.p.CHANNEL_MIC)
                        Flats{i}.p.spec_min(j)=double(min(min(cell2mat(Flats{i}.DAT.datas(j,:)))));
                    end
                else
                    Flats{i}.p.spec_min=[];
                end
            end
            
            if ~isfield(Flats{i}.p.syllable,'X')
                try
                    for j=1:length(Flats{i}.p.CHANNEL_MIC)
                        Flats{i}.p.syllable.X(j)=struct('imics',Flats{i}.p.syllable.imics(j),'numbuffs',Flats{i}.p.syllable.numbuffs(j),'samp_left',Flats{i}.p.syllable.samp_left(j),'IsPCcoeffs',Flats{i}.p.syllable.IsPCcoeffs(j),'DistCcoeffs',Flats{i}.p.syllable.DistCoeffs(j),'PCs',Flats{i}.p.syllable.PCs(j),'PCmean',Flats{i}.p.syllable.PCmean(j));
                    end
                catch
                    for j=1:length(Flats{i}.p.CHANNEL_MIC)
                        Flats{i}.p.syllable.X(j)=struct('imics',j,'numbuffs',60,'samp_left',0,'IsPCcoeffs',1,'DistCcoeffs',[],'PCs',[],'PCmean',[]);
                    end
                end
            end
            
            if  ~isfield(Flats{i}.p.syllable,'sum')
                Flats{i}.p.syllable.method='sum';
                Flats{i}.p.syllable.multi=0;
                Flats{i}.p.syllable.opt=0;
                P=Flats{i}.p.syllable;
                for ii=1:length(Flats{i}.p.syllable.X)
                    Flats{i}.p.syllable.(P.method)(ii).smoothingWidth=1;
                    Flats{i}.p.syllable.(P.method)(ii).thets=80;
                    Flats{i}.p.syllable.(P.method)(ii).thetsU=120;
                    Flats{i}.p.syllable.(P.method)(ii).low=100;
                    Flats{i}.p.syllable.(P.method)(ii).high=128*Flats{i}.scanrate/Flats{i}.p.nfft-100;
                end
            end
            
            for ii=1:length(Flats{i}.p.CHANNEL_MIC)
                if  isfield(Flats{i}.V,['PCcoeffs' num2str(ii)])
                    Flats{i}.V=rmfield(Flats{i}.V,['PCcoeffs' num2str(ii)]);
                end
            end
        end
        
        if  ~isfield(Flats{i}.p,'spec_syll_dur')
            Flats{i}.p.spec_syll_dur=0;
        end
        if ~isfield(Flats{i}.v,'tsne')
            Flats{i}.v.tsne.clust_dense_mode=0;
        end
        if ~isfield(Flats{i}.v.tsne,'clust_dense_mode')
            Flats{i}.v.tsne.clust_dense_mode=0;
        end
        
        if Flats{i}.p.num_clust>length(Flats{i}.p.clust_names)
            for j=length(Flats{i}.p.clust_names)+1:Flats{i}.p.num_clust
                Flats{i}.p.clust_names{j}=['C' num2str(j)];
            end
            for j=1:length(Flats{i}.spec.samp_left)
                Flats{i}.spec.samp_left{j}(end+1:Flats{i}.p.num_clust)=Flats{i}.spec.samp_left{j}(end);
                Flats{i}.spec.samp_right{j}(end+1:Flats{i}.p.num_clust)=Flats{i}.spec.samp_right{j}(end);
                Flats{i}.spec.numbuffs{j}(end+1:Flats{i}.p.num_clust)=Flats{i}.spec.numbuffs{j}(end);
                Flats{i}.spec.firstbuff{j}(end+1:Flats{i}.p.num_clust)=Flats{i}.spec.firstbuff{j}(end);
            end
        end
        
        if Flats{i}.p.num_clust>length(Flats{i}.templates)
            Flats{i}.templates(end+1:Flats{i}.p.num_clust)=0;
        end
        
        if ~isfield(Flats{i}.p,'import_INTAN')
            Flats{i}.p.import_INTAN.do=0;
        end
        
        if isfield(Flats{i}.p,'CHANNEL_MIC') && length(Flats{i}.p.CHANNEL_MIC)>1 && ~isfield(Flats{i}.DAT,'datas') && isfield(Flats{i}.DAT,'data')
            Flats{i}.DAT.datas=Flats{i}.DAT.data;
            Flats{i}.DAT=rmfield(Flats{i}.DAT,'data');
            try
                Flats{i}.DAT.data=func_DAT_help_data_from_datas(Flats{i}.DAT.datas,1);
                %            for j=1:length(Flats{i}.DAT.filename)
                %                Flats{i}.DAT.data{j}=Flats{i}.DAT.datas{j}{1}';
                %            end
            end
        end
        
        if ~isfield(Flats{i}.p,'stereotypy_win_ms')
            Flats{i}.p.stereotypy_win_ms=1;
        end
        
        if isfield(Flats{i}.p,'do_dspecgram')
            Flats{i}.p.DAT_dspecgram=Flats{i}.p.do_dspecgram;
            Flats{i}.p=rmfield(Flats{i}.p,'do_dspecgram');
        end
        if ~isfield(Flats{i}.p,'DAT_dspecgram')
            Flats{i}.p.DAT_dspecgram=0;
        end
        try
            FlatsInfo = evalin('base', 'FlatsInfo');
        catch %#ok<CTCH>
            FlatsInfo = [];
        end
        
        if ~isstruct(FlatsInfo) || isempty(fieldnames(FlatsInfo))
            % This code might be better placed somewhere else..
            FlatsInfo = fci_importFlatsInfo(Flats, '', '');
            assignin('base', 'FlatsInfo', FlatsInfo);
        end
        
        if ismember(userID, [40, 1000:1002])
            Flats{i}.p.autoCorrectFileOverlaps = true;
        end
        if ~isfield(Flats{i}.p,'DAT_colormap')
            Flats{i}.p.DAT_colormap=0; % 0= normal (hot), 1=gray, 2=red
        end
        
        %% careful, if-clause coming up
        if ~isfield(Flats{i}.spec,'offset_corrected')
            Flats{i}.spec.offset_corrected=true;
            for j=1:length(Flats{i}.spec.firstbuff)
                Flats{i}.spec.samp_left{j}=Flats{i}.spec.samp_left{j}-Flats{i}.p.nfft;
                Flats{i}.spec.samp_right{j}=Flats{i}.spec.samp_right{j}+Flats{i}.p.nfft;
            end
        end
        
        if ~isfield(Flats{i}.v,'plot_DAT_marklines')
            Flats{i}.v.plot_DAT_marklines=1;
        end
        if ~isfield(Flats{i}.p,'DATyax')
            Flats{i}.p.DATyax=0.8;
        end
        if ~isfield(Flats{i}.v,'last_clicked')
            Flats{i}.v.last_clicked = -1;
        end
        
        if ~isfield(Flats{i}.v,'last_clickOccurred')
            Flats{i}.v.last_clickOccurred = false;
        end
        
        
        if ~isfield(Flats{i}.v,'DATspec_num')
            Flats{i}.v.DATspec_num=1;
        end
        if ~isfield(Flats{i}.p,'draw_scrollbars')
            Flats{i}.p.draw_scrollbars=1;
        end
        
        
        if ~isfield(Flats{i}.p,'channel_mic_display') %|| isempty(Flats{i}.p.channel_mic_display)
            Flats{i}.p.channel_mic_display=1;
        end
        
        if ~isfield(Flats{i}.p,'do_smart_move')
            Flats{i}.p.do_smart_move=0;
            Flats{i}.X.prototypes= false(1,Flats{i}.num_Tags);
        end
        
        if ~isfield(Flats{i}.v,'DATcurr_chanV')
            Flats{i}.v.DATcurr_chanV=1;
        end
        
        if ~isfield(Flats{i}.v,'spk_virtual') || isempty(Flats{i}.v.spk_virtual)
            Flats{i}.v.spk_virtual=zeros(1,max(Flats{i}.p.CHANNELS_ELECTRODES));
        end
        
        if ~isfield(Flats{i}.p,'spk_burst_lim_ms')
            Flats{i}.p.spk_burst_lim_ms=0;
        end
        if isfield(Flats{i}.X,'data') && Flats{i}.p.spec_full_mode==1
            Flats{i}.X=rmfield(Flats{i}.X,'data');
        end
        if ~isfield(Flats{i}.p,'DATstepsize_marklines_ms')
            Flats{i}.p.DATstepsize_marklines_ms=4;
        end
        
        if ~isfield(Flats{i}.p,'DAT_tagpoint_pair_sig')
            Flats{i}.p.DAT_tagpoint_pair_sig=0;
        end
        if ~isfield(Flats{i}.p,'plot_ordered_names')
            Flats{i}.p.plot_ordered_names={'Neighbor','Time','Duration','Left','Right','Template','Rand','Custom','Tagpoint','Stackpeak','Frate'};
        end
        
        if ~isfield(Flats{i}.p,'channels_filter_type')
            Flats{i}.p.channels_filter_type=1; % 1= butter, 2=fft
        end
        %Flats{i}.p.Ver=2;
    end
    
    
    Flats{i}.v.ind=i;
    Flats{i}.p.names=names;
    
    %% screen
    Flats{i}.v.Screensize=scrsz(3:4);
    Flats{i}.v.max_elements_on_screen=floor(Flats{i}.v.Screensize(1)/Flats{i}.v.numbuffs);
    
    if ~isfield(Flats{i}, 'birdname')
        if isfield(Flats{i},'birdnames')
            Flats{i}.birdname=Flats{i}.birdnames{1};
        else
            birdname = evalin('base', 'cfg.birdname');
            Flats{i}.birdname = birdname;
        end
    end
    
    if isfield(Flats{i},'BOARD')
        Flats{i}=rmfield(Flats{i},'BOARD');
    end
    Flats{i}.BOARD.row=[]; Flats{i}.BOARD.col=[]; Flats{i}.BOARD.SYL=[];
    Flats{i}.BOARD.tagboard=cell(length(Flats{i}.p.tagnames),1);
    
    
    %% v fields
    Flats{i}.v.DATplot_init=0;
    Flats{i}.v.plot_ordered=2;
    Flats{i}.v.plot_tagboard = 0;
    Flats{i}.v.plot_clust=1;
    Flats{i}.v.PLOT.DAT=0; Flats{i}.v.PLOT.eotFigure=0;  Flats{i}.v.PLOT.spec=0; Flats{i}.v.PLOT.table=0;
    Flats{i}.v.plot_grow =0;
    Flats{i}.v.plot_spec=0;
    Flats{i}.v.plot_dat=0;
    Flats{i}.v.plot_templates=0;
    Flats{i}.v.plot_clust_fast=0;
    Flats{i}.v.prepare_plot=0;
    Flats{i}.v.spec_resize=1;
    Flats{i}.v.DATspec=cell(Flats{i}.p.num_clust,1);
    Flats{i}.v.sequence=cell(25,Flats{i}.p.num_clust);
    
    Flats{i}.v.run_select=1;
    Flats{i}.v.run_select_clust=1;
    Flats{i}.v.run_set_consistency=1;
    Flats{i}.v.suggest_clustering=0;
    
    Flats{i}.v.tags_curr_i=1; %% XXX what is this ???
    Flats{i}.v.tags_curr_ij=cell(length(Flats{i}.p.tagnames),1);
    Flats{i}.v.tags_curr_ij(:)= {''};
    
    
    %% p fields
    Flats{i}.p.import_clustspec=0;
    Flats{i}.p.import_electrode=0;
    Flats{i}.p.import_plugin_import=0;
    if isfield(Flats{i}.p,'CHANNEL_PLUGIN_DATA')
        Flats{i}.p.import_plugin_data=zeros(size(Flats{i}.p.CHANNELS_PLUGIN_DATA));
    end
    Flats{i}=func_help_Parameter_menu(Flats{i});
    
end

end


