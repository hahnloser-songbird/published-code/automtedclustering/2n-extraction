function h5_flag = is_from_h5(Flat)
% check if Flats were created from hdf5 files


% write and list here which file formats correspond to hdf5:
% 12 for .kwd
h5_fformats = 12; % 
cfg = evalin('base', 'cfg');

h5_flag = any(h5_fformats == cfg.file_format);

end