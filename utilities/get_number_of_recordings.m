function n = get_number_of_recordings(Flat)
% get the number of recordings in Flat
%
% for hdf5 check Flat.DAT.recording
% otherwise check Flat.DAT.filenames

if is_from_h5(Flat)
    n = length(Flat.DAT.recording);
else 
    n = length(Flat.DAT.filename);
end

end




    
