function [Flat,Flats]=func_default_Flat_values(num_Tags)

num_archs = 120; % number of archives

Flat.p.STACKscaled=1;
%% Sizes
Flat.p.num_train=400; % maximum number of Tags used for training
Flat.p.var_explain=0.85; % amount of variance to be captured by principal comonents
Flat.p.max_elementsPC=100000; % maximum number of elements for principal component analysis
%(increasing this number may give more acurate
%results, but will be slower

%% clustering parameters
Flat.p.num_clust=40; % number of clusters
Flat.p.cluster_algorithm=0; % 0=clusterdata, 1=kmeans
Flat.p.clusterdata_linkage='ward';  % if 'single' not OK, choose 'ward' if first cluster is too large
Flat.p.clusterdata_distance='mahalanobis'; %'cityblock' or 'mahalanobis'

%% group names
Flat.p.groupnames={'Syllable','Call','Noise','Playback','Other','','','',''};

%% Flats names  ('Train' is reserved!)
Flat.p.names=cellfun(@(x) ['Archive ' num2str(x)],num2cell(1:num_archs),'UniformOutput',false);
Flat.p.names{1}='Train';
Flat.p.tagnames={'is_classified','is_hidden','Site','Mark'};
Flat.p.tagparameters=Flat.p.tagnames;
Flat.p.tagparameters(:)={''};
%cellfun(@(x) '',Flat.p.tagnames,'Uniformoutput',false);

%% import from output_data
Flat.p.nfft=512;
Flat.p.nfft_i=1:round(Flat.p.nfft/4);
Flat.p.nonoverlap=128;
Flat.p.samp_left=0; % Can be smaller than output_data.samp_left!
Flat.p.samp_right=6*512; % Can be smaller than output_data.samp_right!
%Flat.p.channel_no=1;

%% growing, viewing, importing, extracting
Flat.p.num_grow=24; % number of elements to search (grow cluster)
Flat.p.max_elements_grow_search=16; % maximum number of elements in cluster to be used for growing the cluster
Flat.p.num_nearest_neighbors_for_order_sequence=8; % for ordered plotting of cluster elements
Flat.p.num_nearest_neighbors_for_clust=1;
Flat.p.import_method=0; % 0: nearest neighbor, 1: mahalanobis
Flat.p.grow_method=0; % 0: nearest neighbor, 1: mahalanobis
Flat.p.max_num_mahalanobis=8; % number of PCs to be used for mahalanobis method, only clusters larger than this number can be
% processed!
%  Flat.p.extract_method=2; % 0: random over all clusters (num_train over all)
% 1: random for each cluster (Flat.p.extract_per_clust per clust)
% 2: extract complete files only (at least Flat.p.extract_per_clust per clust)
%  Flat.p.extract_per_clust=40;

%% Perceptron
Flat.p.perceptron_per_clust=10; % minimum number of elements for training a perceptron
Flat.p.perceptron_num_hidden=3;
Flat.p.perceptron_ignore=6;  % ignore number of buffers around target
Flat.p.perceptron_num_pcs=20;

%% Spectrogram
Flat.p.spec_winls=1; % in seconds, amount of sound to show in figure 24
Flat.p.spec_winrs=1;
Flat.p.spec_num=16;
Flat.p.spec_raw_scale=1;
Flat.p.spec_raw_offset=0;

Flat.p.do_sam=0;

%% transition matrix
Flat.p.graph_max_gap_ms=500;
Flat.p.graph_thr_percent=1;
Flat.p.graph_clusts=1:5;
Flat.p.graph_compressed=1;

%% marks
Flat.p.display_marks=0;

Flat.p.allow_empty_clusters=0;

Flat.p.do_raw=0;

%Default cluster names
for j=1:Flat.p.num_clust
    Flat.p.clust_names{j}='';
end

Flat.p.savename='Archives';

%% func_get_data_new
Flat.p.exec_get_fcn='';

%% DAT
Flat.p.DATplugin={'fb_plugin_pitch_HPS','fb_plugin_RMS_500_7K','','','','','','','',''};

%    Flat.p.channels_load='electrode';
%    Flat.p.channel_mic='';
Flat.p.DATwinl_s=.3;  % in seconds
Flat.p.DATwinr_s=.7;
Flat.p.DATnum_lines=100;
% Flat.p.DATkeep_raw=[];

% DAT filter
Flat.p.DATplugin_nonoverlap=128;
Flat.p.DATfilt_highpass = 300;
Flat.p.DATfilt_order = 4;
% Flat.p.DATfilt_channels='electrode';

Flat.p.DATthresh_low_lim = 75; % in muV
Flat.p.DATthresh=150; %
Flat.p.spec_full_mode=0;





%% initialize changing variables
Flat.v.pcs_num_display=4;
Flat.v.leftmost_element=1; % cluster element shown at the left of the screen
Flat.v.view_compressed=0; % Shows data as projected on PCs
Flat.num_classified=0;
Flat.name='Train';  % or 'Train'
Flat.v.plot_ordered=2; % default plotting mode, 7=random, 1=nearest neighbor, 2=timestamp, 3=autoclust
Flat.v.suggest_clustering=0; % suggest which cluster the elements to the right of the clicked point belong to
Flat.v.zoom_level=1;  % 0=minimum resolution, 1=highest resolution, 2-3=lower
Flat.v.curr_clust=1; % default cluster on screen
Flat.templates=zeros(Flat.p.num_clust,1);

Flat.v.plot_templates=1;
Flat.v.plot_clust=0;
Flat.v.plot_grow =0;
Flat.v.plot_spec=0;
Flat.v.run_set_consistency=0;
Flat.v.run_select=1;
Flat.v.run_select_clust=1;
Flat.v.plot_clust_fast=0;
Flat.v.prepare_plot=0;
Flat.v.all_clust_mode=1;
Flat.v.add_str=' ';
Flat.v.end_str=' ';
Flat.v.redtext.templates=[];
Flat.v.redtext.grow_examples=[];
%Flat.v.tagview=0;

Flat.spec_curr_ID=1;
Flat.grow_examples = [];
%Flat.select.n={'data_good','is_classified','is_hidden'};
%Flat.select.i=[-1 -1 -1]; % -1=ignore, 0=off, 1=on
%% Do not change
Flat.v.numbuffs=func_numbuffs(Flat.p.samp_left+Flat.p.samp_right,Flat.p.nfft,Flat.p.nonoverlap);

Flat.v.spec_plot_IDs=0;
Flat.v.spec_new_clust_win=0;
%Flat.v.spec_data_mismatch=0;
Flat.v.spec_resize=0;

Flat.v.rms_offset=false;
Flat.v.rms_offset_done=0;
Flat.v.templates=ones(1,Flat.p.num_clust);
Flat.v.load_fixed_position = 0;
Flat.v.set_spec_consistency=0;

Flat.spec_names={'Cutout win','Syllable win','neural net win','PCA win','Clust win'};
Flat.spec.samp_left=cellfun(@(x) x*Flat.p.samp_left*ones(Flat.p.num_clust,1),{1,1,1,1,1},'UniformOutput',false);
Flat.spec.samp_right=cellfun(@(x) x*Flat.p.samp_right*ones(Flat.p.num_clust,1),{1,1,1,1,1},'UniformOutput',false);
Flat.spec.firstbuff=cellfun(@(x) x*1*ones(Flat.p.num_clust,1),{1,1,1,1,1},'UniformOutput',false);
Flat.spec.numbuffs=cellfun(@(x) x*Flat.v.numbuffs*ones(Flat.p.num_clust,1),{1,1,1,1,1},'UniformOutput',false);
Flat.spec.offset_corrected=true;
Flat.num_Tags=num_Tags;
Flat.v.sequence=cell(25,Flat.p.num_clust); % number of plot_ordered=7;
%Flat.v.clustered_sequence=cell(1,Flat.p.num_clust);
Flat.X.data=cellfun(@int8,cell(1,num_Tags),'uniformoutput',false);%int8([]);% Flat.raw=[];
Flat.X.specdata=cell(1,num_Tags);


Flat.group=cell(9,1);
Flat.pcs_per_clust=cell(Flat.p.num_clust,1);
Flat.net=cell(Flat.p.num_clust,1);
%Flat.fileindex=zeros(1,num_Tags);
Flat.X.indices_all=zeros(1,num_Tags);
%Flat.buffersizeREC=zeros(1,num_Tags);
Flat.X.Parse_act_index=zeros(1,num_Tags);
%Flat.net_samp=-ones(1,num_Tags); % -1 should not be a possible value, but 0 is
%Flat.X.data_good=boolean(zeros(1,num_Tags));
%Flat.DAT.eof(Flat.X.DATindex)=zeros(1,num_Tags);
Flat.X.tag_threshold=zeros(1,num_Tags);
Flat.X.data_off=zeros(1,num_Tags);
%Flat.raw=cell(1,Flat.num_Tags);
%Flat.raw_pre=cell(1,Flat.num_Tags);

% pre allocate all DAT fields %YR
Flat.DAT.filename = {};
Flat.DAT.subdir = {};
Flat.DAT.timestamp = [];
Flat.DAT.eof = [];
Flat.DAT.data = {};
Flat.DAT.phds=[];

Flat.birdname='';
%Flat.filetype=cell(1,Flat.num_Tags);
%Flat.marked=zeros(1,num_Tags);
%Flat.is_classified=boolean(zeros(1,num_Tags));
%Flat.is_hidden=boolean(zeros(1,num_Tags));
%Flat.v.firstbuff_clust=int16(ones(1,num_Tags));
Flat.V.post=int8(zeros(1,num_Tags));
Flat.V.pre=int8(zeros(1,num_Tags));
Flat.v.spec_show_str=0;
Flat.v.spec_coupled=1;
Flat.v.spec_rlines=[Flat.p.samp_left Flat.p.samp_right];
Flat.v.plot_clust_fast=0;

Flat.v.grow_leftmost=1;
Flat.v.marktext_ypos=ceil(length(Flat.p.nfft_i)*28/32);

if ~isfield(Flat.p,'tagnames')
    Flat.p.tagnames={'is_classified','is_hidden','Site','Mark'};
    Flat.p.tagparameters=cellfun(@(x) '',Flat.p.tagnames,'Uniformoutput',false);
end
h=cell(length(Flat.p.tagnames),num_Tags);
Flat.Tags=cellfun(@(x) '',h,'uniformoutput',false);
h2=cell(length(Flat.p.tagnames),1);
Flat.v.tags_curr_ij= cellfun(@(x) '',h2,'UniformOutput',false);
Flat.v.tags_curr_i=1;

Flat.v.select_tagboard_on=0;
Flat.v.select=[];
Flat.v.select_clust=[];
Flat.BOARD.tagboard=cell(length(Flat.p.tagnames),1);

%% Plot in figures 24 and 25
%Flat.v.plot_strs={'-','D'};
%Flat.v.plot_str_i=1;
Flat.v.DATplot_last_clust=0;

%Flat.v.DATPluginVSSpikes=0; % 0=plugin, 1=spikes

%Flat.class_ID=zeros(1,num_Tags);
Flat.X.clust_ID=uint8(zeros(1,num_Tags));
Flat.X.PCcoeffs=zeros(1,num_Tags);
Flat.PCs=0;
Flat.PCmean=zeros(1,num_Tags);
%Flat.DAT.timestamp(Flat.X.DATindex)=zeros(1,num_Tags);
Flat.X.DATindex=zeros(1,num_Tags);
Flat.num_PCs=20;
Flat.scanrate=0;

%% DAT
% we do not define Flat.CHANNELS_SAVED and Flat.Channel_names because we do not know the number of
% saved channels
%Flat.v.ELECTRODE_CHANNELS=[];
Flat.v.order_sequence_DAT=[];

%% DAT keypress
Flat.v.DATColorScale=1;

if length(Flat.p.names)<120
    pnanmes=cellfun(@(x) ['Archive ' num2str(x)],num2cell(1:num_archs),'UniformOutput',false);
    lpn=length(Flat.p.names);
    Flat.p.names(lpn+1:120)=cellfun(@(x) ['Archive ' num2str(x)],num2cell(lpn+1:num_archs),'UniformOutput',false);
    Flat.p.names{1}='Train';
end
for i=1:num_archs
    Flats{i}=Flat;
    Flats{i}.name=Flat.p.names{i};
    Flats{i}.v.ind=i;
    Flats{i}.p.allow_empty_clusters=(i>1);
end




