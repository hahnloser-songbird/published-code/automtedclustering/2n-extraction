function      Flats=func_help_tags_write(Flats)

%% write Tags
for j=1:length(Flats)
    if isfield(Flats{j},'Tags') ||  (isfield(Flats{j},'Tags') && Flats{j}.num_Tags==0) || ~isfield(Flats{j}.v,'tagboard0')
        continue
    end
    Flath=Flats{j};
    Flath.Tags=cell(length(Flath.p.tagnames),Flath.num_Tags);
    
    for k=1:length(Flath.p.tagnames)
        t0=Flath.v.tagboard0{k};
        h=find(cellfun(@isempty,t0));
        if ~isempty(h)
        t0{h}='[]';
        end
        Flath.v.tagboard0{k}=t0;
%         [Flath.v.tagboard0{k},sort_inds]=sort(t0);
%         Flath.v.tagboard0i{k}=Flath.v.tagboard0i{k}(sort_inds);
       % all=unique([Flath.v.tagboard0i{k}{:}]);
       % missing=setdiff(1:Flath.num_Tags,all);
       Flath.Tags(k,:)={'[]'};
         for l=1:length(t0)-1
            Flath.Tags(k,Flath.v.tagboard0i{k}{l})=t0(l);
        end
        
    end
    [Flath.Tags,Flath.v.tagboard0,Flath.v.tagboard0i]=func_help_tagboard0(Flath.Tags,Flath.p.tagnames,Flath.DAT.timestamp(Flath.X.DATindex(end)),Flath.p.tags_quantize_threshold);
    Flats{j}=Flath;
end
end