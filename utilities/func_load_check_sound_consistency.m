function Flats=func_load_check_sound_consistency(Flats,do_ask)

if nargin<2
    do_ask=1;
end

% Check that all sound processing parameters are identicalFlat

nonempty=cellfun(@(x) x.num_Tags,Flats,'Uniformoutput',false);
nonempty=[nonempty{:}];
I=find(nonempty);
l=length(I);
if l<2
    return
end
if do_ask
Pfields={'syllable','spec_log_min','nfft','nonoverlap','nfft_i','spec_raw_scale','spec_raw_offset','dynamic_range','dynamic_range_spec'};
Ps=cellfun(@(x) x.p,Flats,'Uniformoutput',false);
for a=1:length(I)-1
    i=I(a); ii=I(a+1);
    for b=1:length(Pfields)
        if ~(isfield(Ps{i},Pfields{b}) && isfield(Ps{ii},Pfields{b}) && isequal(Ps{i}.(Pfields{b}),Ps{ii}.(Pfields{b})))
            fprintf('Flat.p.%s different in archive %d\n',Pfields{b},ii);
            try
                fprintf('%s vs %s\n',Ps{i}.(Pfields{b}),Ps{ii}.(Pfields{b}));
            end
            disp('Type dbcont to continue'); 
           % keyboard
        end
    end
end
end
end