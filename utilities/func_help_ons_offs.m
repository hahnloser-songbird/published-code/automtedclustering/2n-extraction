function   [ons,offs,maxs]=func_help_ons_offs(sd,thet)
 

[ons,offs,success]=func_help_ons_offs_trimm(sd,thet);
maxs=zeros(size(ons));
if ~success
    return
end

% linear interpolation
for kk=1:length(ons)
    maxs(kk)= max(sd(ons(kk):offs(kk))); % special line
    ons(kk)=ons(kk)+(thet-sd(ons(kk)))/(sd(ons(kk)+1)-sd(ons(kk)));
end
for kk=1:length(offs)
    offs(kk)=offs(kk)+(sd(offs(kk))-thet)/(sd(offs(kk))-sd(offs(kk)+1));
end

if any(offs-ons<0)
    disp('problem with interpolation')
    keyboard
end
end