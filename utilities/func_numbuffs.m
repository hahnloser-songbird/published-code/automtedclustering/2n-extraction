function numbuffs = func_numbuffs(samples, window, nonoverlap)
    numbuffs = 1 + max(0,floor((samples-window)/nonoverlap));
end
%% EOF