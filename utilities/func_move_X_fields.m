function Flat = func_move_X_fields(Flat)

    fieldTokens = {'data','data_off','indices_all','specdata','Parse_act_index','tag_threshold', 'clust_ID','PCcoeffs','DATindex','stack_mean','stack_median'...
        'stack_var','detector_levels','IsSong','pitch'};
    
    for i = 1 : numel(fieldTokens)
        
        if isfield(Flat,fieldTokens{i})
            
            Flat.X.(fieldTokens{i}) = Flat.(fieldTokens{i});
            Flat = rmfield(Flat,fieldTokens{i});
            
        end
        
    end
    
   
end

