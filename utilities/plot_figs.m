function [Flat,fcH]=plot_figs(Flat,fcH,skip_axes)
if nargin<3
    skip_axes=0;
end

if ~isfield(Flat.v,'times')
    Flat.v.times=now;
else
    Flat.v.times(end+1)=now;
end

[Flat,Flat.v.order_sequence,Flat.v.select_clust]=...
    func_update_order_sequence(Flat,Flat.v.plot_ordered,Flat.v.curr_clust);

fcH.ha_spcgrm.YAxis.Direction = 'reverse';


if Flat.v.prepare_plot==1 || (isfield(Flat.v.PLOT,'spec') && Flat.v.PLOT.spec==1 )
    [Flat,fcH]=func_spec_prepare_plot(Flat,fcH);
end

if Flat.v.plot_spec==1 || (isfield(Flat.v.PLOT,'spec') && Flat.v.PLOT.spec==1 )
    [Flat,fcH]=plot_spec(Flat,fcH);
    Flat.v.plot_spec=0;
end


[Flat,fcH]=func_spec_keyrelease2(Flat,fcH);

end
% EOF


function [timevec,cv,max_all]=func_help_plot_curve_cv(Flat,blocks,binsize)

nwins=floor((Flat.v.DATwinl+Flat.v.DATwinr)/binsize);
binsize2=(size(Flat.v.DATI,1)/nwins);
timevec=linspace(-Flat.v.DATwinl,Flat.v.DATwinr,nwins)/Flat.scanrate*1000;
cv=cell(length(blocks)-1,1);
max_all=cell(length(blocks)-1,nwins);
for j=1:length(cv)
    cv{j}=zeros(1,nwins);
    ci=0;
    for i=1:binsize2:binsize2*(nwins-0)
        ci=ci+1;
        %% with overlap !!
        x0=max(Flat.v.DATI(max(1,round(i)-3):min(round(i+binsize2)+2,size(Flat.v.DATI,1)),blocks(j):blocks(j+1)-1));
        max_all{j,ci}=x0;
        % x0=max(Flat.v.DATI(round(i):round(i+binsize2)-1,blocks(j):blocks(j+1)-1));
        cv{j}(ci)=std(x0);
    end
end
end