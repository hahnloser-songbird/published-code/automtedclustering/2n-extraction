function FlatsInfo = fci_importFlatsInfo(Flats, birdPath, archivePath)

Flat = Flats{2};
if isfield(Flat, 'birdname')
    birdname = Flat.birdname;
else
    birdname = unique(Flat.birdnames);
end

% Check whether a BIRDdata file is present in the birdpath and load
% events
try
    BIRDdataPath = [birdPath filesep 'BIRDdata.mat'];
    load(BIRDdataPath);
    BIRD = BIRDdata{find(cellfun(@(x) strcmp(x.name, birdname), BIRDdata))};
catch
    BIRD = [];
end

try
    load([archivePath filesep 'FlatsInfo.mat']);
catch
    FlatsInfo = fci_defaultFlatsInfo(Flats);
end

if isempty(FlatsInfo)
    FlatsInfo.annotations = [];
    FlatsInfo.birdname = birdname;
    j = 0;
    for ai = 1:length(BIRD.events)
        j = j + 1;
        if iscell(BIRD.events(ai).tag)
            BIRD.events(ai).tag = BIRD.events(ai).tag{1};
        end
        FlatsInfo.annotations(j).text = BIRD.events(ai).tag;
        FlatsInfo.annotations(j).timestamp = datenum(BIRD.events(ai).datevec);
        FlatsInfo.annotations(j).color = [0.7, 0.3, 0.2];
        FlatsInfo.annotations(j).type = 1;
    end
    j = j + 1;
    FlatsInfo.annotations(j).text = 'birthday';
    FlatsInfo.annotations(j).timestamp = datenum(BIRD.birthday);
    FlatsInfo.annotations(j).color = [0.7, 0.3, 0.2];
    FlatsInfo.annotations(j).type = -1;
    
    j = j + 1;
    FlatsInfo.annotations(j).text = 'tutoring onset';
    FlatsInfo.annotations(j).timestamp = datenum(BIRD.tutoringOnset);
    FlatsInfo.annotations(j).color = [0.7, 0.3, 0.2];
    FlatsInfo.annotations(j).type = -1;
    
    % delete doubles
    doubles = [];
    for i = 1:length(FlatsInfo.annotations)
        for j =i+1:length(FlatsInfo.annotations)
            if strcmp(FlatsInfo.annotations(j).text, FlatsInfo.annotations(i).text) &&...
                    FlatsInfo.annotations(j).timestamp == FlatsInfo.annotations(i).timestamp
                doubles = [doubles, i];
            end
        end
    end
    doubles = unique(doubles);
    FlatsInfo.annotations(doubles) = [];
end

[~, sidx] = sort([FlatsInfo.annotations.timestamp]);
FlatsInfo.annotations = FlatsInfo.annotations(sidx);

end