function Flat=func_help_shift_syls_right(Flat,seli,i,sel)

ir=min(length(sel),seli+1); srp=i; sr=sel(ir);
while Flat.X.DATindex(sr)==Flat.X.DATindex(i) && Flat.X.indices_all(sr)<=Flat.X.data_off(srp)+Flat.X.indices_all(srp) && ir<length(sel)
    Flat.X.indices_all(sr)=Flat.X.data_off(srp)+Flat.X.indices_all(srp)+Flat.p.nonoverlap;%Flat.X.indices_all(sr)+Flat.X.data_off(sr);
    Flat.X.data_off(sr)=max(0,Flat.X.data_off(sr)-Flat.p.nonoverlap);
    srp=sr;
    ir=ir+1; sr=sel(ir);
end
end