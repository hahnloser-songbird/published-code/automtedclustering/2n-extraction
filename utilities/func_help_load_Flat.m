function [Flath,Lfilt]= func_help_load_Flat(path_arch,arch_str,decoupled_exist,Lfilt)
% arch_str ends with '.mat'
% if nargin<4
%     thin_load=0;
% end
thin_load=0;
directoryDelimiter =filesep;
cfg=evalin('base','cfg');

disp(['Loading ' path_arch directoryDelimiter arch_str ' ...']);
if ~exist([path_arch directoryDelimiter arch_str])
    Flath=[];
    disp('Archive not found');
    return
end
load([path_arch directoryDelimiter arch_str]);
if ~exist('Flath')
    Flath=[]; Lfilt=[];
    disp('Invalid archive')
    return
end

if isfield(Flath.p,'CHANNEL_MIC') && length(Flath.p.CHANNEL_MIC)>1
    Lfilt.datas=Lfilt.data;
    %Lfilt.data=0; %this prevents loading of datas field in the next call of func_help_load_Flat
    %(when multiple archives are loaded) --> bug
end
%% load decoupled fields
%if thin_load==0 && decoupled_exist==7 && ~ismember(cfg.userID, [16])
if  decoupled_exist==7 && ~ismember(cfg.userID, [16])
    if ismember(arch_str(1),{'@','#'})
        arch_str=arch_str(2:end);
    end
    d=dir([path_arch directoryDelimiter 'Decoupled' directoryDelimiter arch_str(1:end-4) '=*.mat']);
    if ~isempty(d)
        for k=1:length(d)
            h=findstr(d(k).name,'DAT_');
            str=d(k).name(h+4:end-4);
            if isfield(Lfilt,str) && ~Lfilt.(str)
                continue
            end
%             if strcmp(d(k).name(1),'@')
%                 loadstr=d(k).name(2:end);
%             else
                loadstr=d(k).name;
%           end
            A=load([path_arch directoryDelimiter 'Decoupled' directoryDelimiter loadstr]);
            fprintf('%s loaded.\n',[path_arch directoryDelimiter 'Decoupled' directoryDelimiter loadstr]);
            
            if isfield(A,'DAT') %&& Lfilt.(A)
                fn=fieldnames(A.DAT);
              if ~isempty(fn)
                    for kk=1:length(fn)
                        Flath.DAT.(fn{kk})=A.DAT.(fn{kk});
                    end
                else
                    fn=fieldnames(A);
                    Flath.(fn{1})=A.(fn{1});
                end
            end
        end
    end
end

%% load appendix files
if exist([path_arch directoryDelimiter 'Appendix'],'dir')
    if ismember(Flath.name(1),{'@','#'})
 %       Flath.namebase='g17y2';
        dapp=dir([path_arch directoryDelimiter 'Appendix' directoryDelimiter Flath.namebase '=*.mat']);       
    else
        dapp=dir([path_arch directoryDelimiter 'Appendix' directoryDelimiter Flath.name '=*.mat']);
     end
    if ~isempty(dapp)
        for k=1:length(dapp)
            if ismember(Flath.name(1),{'@','#'})% strcmp(Flath.name(1),'@')
                fieldname=dapp(k).name(length(Flath.namebase)+2:end-4);
            else
                fieldname=dapp(k).name(length(Flath.name)+2:end-4);
            end
            load([path_arch directoryDelimiter 'Appendix' directoryDelimiter dapp(k).name]);
            Flath.(fieldname)=X;
            fprintf('%s loaded.\n',[path_arch directoryDelimiter 'Appendix' directoryDelimiter dapp(k).name]);
        end
    end
end
