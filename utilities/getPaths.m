function [paths, home_dir] = getPaths()

fdir = uigetdir(cd, 'select the (bird) folder that contains the "Data" folder and subfolders with .wav files');


%     fdir = 'C:\Users\Corinna\Dropbox\repos\2nbasedextraction\g17y2\';

paths.bird_path = [fdir filesep];
paths.load_path_default = [fullfile(fdir, 'Data'), filesep];

home_dir = strsplit(fdir, filesep); % data directory


end

