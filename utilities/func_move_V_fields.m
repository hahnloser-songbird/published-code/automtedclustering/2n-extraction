function Flat = func_move_V_fields(Flat)

    fieldTokens = {'pre','post'};
    
    for i = 1 : numel(fieldTokens)
        
        if isfield(Flat,'v') && isfield(Flat.v,fieldTokens{i})
            Flat.V.(fieldTokens{i}) = Flat.v.(fieldTokens{i});
            Flat.v = rmfield(Flat.v,fieldTokens{i});
            
        end
        
    end
    
   
end

