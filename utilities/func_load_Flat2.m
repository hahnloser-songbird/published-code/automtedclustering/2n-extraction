function [Flats, first_loaded, FlatsInfo] = func_load_Flat2(Flats, paths, auto_load, archiveColl, Lfilt, archives)
% if you change this function, make sure you also adopt
% "func_load_Flat2_fixed_position" !!


% Lfilt: load filter:
% Lfilt.data=1;
% LFilt.multiunit=1;
% LFilt.STACK=1

first_loaded=[];
FlatsInfo = [];

if nargin<6
    archives={};
end
if nargin<5 || isempty(Lfilt)
    cfg=evalin('base','cfg');
    if isfield(cfg,'flatclust')
        Lfilt=cfg.flatclust;
    else
        Lfilt.data=1;
        Lfilt.multiunit=1;
        Lfilt.STACK=1;
    end
end

if ~isfield(Lfilt,'do_ask')
    Lfilt.do_ask=0;
end

%Lfilt.data=0; % JuGa: uncomment this if you want to load Flats without
% spectrograms

if nargin < 4
    archiveColl=[];
end

if nargin < 3
    auto_load = 1;
end

directoryDelimiter=filesep;
coll_lst=dir(fullfile(paths.bird_path, 'ArchiveCollection' )) ;
coll_lst=coll_lst(3:end);
coll_lst=coll_lst([coll_lst.isdir]);

if isempty(coll_lst)
    disp('No Archive found in folder ArchiveCollection. Please use different bird.');
    Flats = [];
    return;
end

str = {coll_lst.name};
if auto_load == 0
    [s,v]=listdlg('PromptString','Which Collection do you want to load ?','SelectionMode','single','ListString',str,'Name', 'Load Flat','ListSize', [800 200]);
    drawnow;    % Bugfix for Windows, prevents hanging; Important!
else
    s = 1; v = 1;
    if ~isempty(archiveColl)
        str{s}=archiveColl;
    end
    
    disp(['Using the following collection: ' str{s}]);
end

%%
if v
    path_arch=[paths.bird_path 'ArchiveCollection' directoryDelimiter str{s}];
    if isempty(archives)
        arch_lst=dir([path_arch  directoryDelimiter '*.mat']);
        arch_lst=setdiff({arch_lst.name},'CFG.mat');
    else
        arch_lst=archives;
    end
    user_i=cellfun(@(x) findstr(x,'#USER#'),arch_lst,'UniformOutput',false);
    user_i=find([user_i{:}]);
    
    arch_lst_full=arch_lst;  user_lst=arch_lst(user_i);
    arch_lst(user_i)=[];
    
    arch_lst(cellfun(@(s) strcmp(s, 'FlatsInfo.mat'), arch_lst)) = [];
    
    if auto_load == 0
        [which,status] = listdlg('PromptString','Load ?', 'SelectionMode','multiple','ListString',arch_lst);
        drawnow;    % Bugfix for Windows, prevents hanging; Important!
    else
        status = 1;
        which = 1:size(arch_lst, 2);
        disp('Loading the following archiveColl: ' );
        disp({arch_lst{which}}');
    end
    
    if status==0
        return
    end
    
    %% check if archiveColl to be loaded already exists in Flats
    file_names={};
    for i=which
        file_names{end+1}=arch_lst{i}(1:end-4);
    end
    
    if auto_load == 0
        flat_names=cellfun(@(x) x.name,Flats,'Uniformoutput',false);
        [which_present which_present_where]=ismember(file_names,flat_names);
        
        replace_existing=0;
        if sum(which_present)>0
            disp('replace existing archiveColl ? (yes=1) ');
            y=input('');
            replace_existing=(y==1);
            if isempty(y)
                replace_existing = 0;
            end
        end
    else
        replace_existing = 0;
        which_present = 0;
    end
    
    answer='';
    if ~isempty(user_lst)
        load_user=0;
        for i=which
            if max(ismember(user_lst,['#USER#' arch_lst{i}]))
                load_user=1;
                break
            end
        end
        if load_user
            if auto_load == 0
                answer=questdlg('Load User Data (archive changes not permitted if not loaded)?','No');
                if strcmp(answer,'Cancel')
                    return
                end
            else
                % auto load user data
                answer = 'Yes';
            end
        end
    end
    tic
    %   if auto_load == 0
    if ~isempty(Flats)
        num_non_empty_archiveColl=max(find(cellfun(@(x) x.num_Tags, Flats)));
        if isempty(num_non_empty_archiveColl)
            ci=1;          % skip TrainS
        else
            ci=num_non_empty_archiveColl;
        end
    else
        ci = 1;
    end
    
    decoupled_exist=exist([path_arch directoryDelimiter 'Decoupled'],'dir');
    X=struct('file','','var','');
    for i=1:length(which)
        ci=ci+1;
        %% loading Flath
        [Flath,Lfilth]=func_help_load_Flat(path_arch,arch_lst{which(i)},decoupled_exist,Lfilt);
        Flath.p.Lfilt=Lfilth;
        
        if isempty(Flath) % need this code to run SAP analysis on Josh's birds
            Flath=Flats{ci-1};
            Flath.name=arch_lst{which(i)};
            Flath.v.delete=1;
        else
            Lfilt=Lfilth;
            if isfield(Lfilt,'datas')
                Flath.p.channel_mic_display=Flath.p.channel_mic_display(1);
                if isfield(Flath.DAT,'datas')
                    Flath=func_help_datas_to_data(Flath);
                else
                    Flath.DAT.data=cell(1,length(Flath.DAT.filename));
                    Flath.DAT.datas=cell(1,length(Flath.DAT.filename));
                end
            elseif isfield(Lfilt,'data') && Lfilt.data==0
                Flath.DAT.data={};
            end
        end
        
        %  end
        
        %% set the correct savename
        Flath.p.savename=str{s};
        if strcmp(answer,'Yes') && max(ismember(user_lst,['#USER#' arch_lst{which(i)}]))
            % inform user that we are loading user data (which might take
            % forever)
            disp(['Loading user data: #USER#' arch_lst{which(i)} ]);
            load([path_arch directoryDelimiter '#USER#' arch_lst{which(i)}]);
            Flath.user=user;
        end
        
        try
        if ~isfield(Flath.p,'DATmasknoise')
            Flath.p.DATmasknoise=zeros(1,length(Flath.p.CHANNEL_MIC));
        end
        Flath.v.DAT_size_at_load_time=length(Flath.DAT.filename);
        catch
            disp('Problem with this archive...');
        end
        %
        %           if ~isfield(Flath.DAT,'data')  % JuGa
        %               Flath.DAT.data=cell(1,length(Flath.DAT.filename)); % JuGa
        %           end % JuGa
        % %
        %% variable position
        if strcmp(arch_lst{which(i)},'Train.mat')
            ci=ci-1;
            Flats{1}=Flath;
            Flats{1}.v.ind=1;
            if isempty(first_loaded)
                first_loaded=1;
            end
            
        else
            if replace_existing && which_present_where(i)>0
                ciold=ci;
                ci=which_present_where(i);
                Flats{ci}=Flath;
                Flats{ci}.v.ind=ci;
                if isempty(first_loaded)
                    first_loaded=ci;
                end
                ci=ciold-1;
                
            else
                Flats{ci}=Flath;
                Flats{ci}.v.ind=ci;
                if isempty(first_loaded)
                    first_loaded=ci;
                end
            end
        end
    end
end

%%
disp('Finished loading files, will run "func_make_compatible" now');

%% write names
Flats{1}.name='Train';
if ~isfield(Flats{1},'num_Tags')
    Flats{1}.num_Tags=0;
    Flats{1}.p.nfft=256;
end

for i=ci+1:length(Flats)
    Flats{i}.name=['Archive ' num2str(i-1)];
end

%% double check we don't have empty entries - substitute them with '' strings
%     for i = 1 : size(Flats, 2)
%         if isfield(Flats{i},'Tags')
%             bla = cellfun(@(x) isempty(x), Flats{i}.Tags);
%             Flats{i}.Tags(bla) = {''};
%         end
%     end

% If function crashes in the function below, use
% [dummy,Flats]=func_default_Flat_values(0); %to create Flats and pass Flats
% as an input variable to this function...
Flats=func_load_check_sound_consistency(Flats,Lfilt.do_ask);
% check that all thresholds are identical in case of func_import_el_from_DAT
%     for a=1:length(Flats)
%         if Flats{a}.num_Tags==0
%             continue
%         end
%         if isfield(Flats{a}.p,'syllable') %|| ~isequal(Flats{a}.p.syllable,Flat.p.syllable)
%           if exist('syls','var')
%               if ~isequal(syls,Flats{a}.p.syllable)
%                   fprintf('Syllable segmentation parameters different in archive%d\n!!rerun func_import_elements_from_DAT \n',a);
%                   pause
%               end
%           else
%             syls=Flats{a}.p.syllable;
%           end
%         end
%     end


%%
% Flats=func_make_compatible(Flats);
% Load the FlatsInfo file and BIRDdata if it exists
FlatsInfo = fci_importFlatsInfo(Flats, paths.bird_path, path_arch);

toc
end
%% EOF
