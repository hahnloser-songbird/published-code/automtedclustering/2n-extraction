function clust=func_help_umap_select_from_I(Flat,PIX,M)


clust=zeros(1,length(Flat.umap.X.select));

do_parfor=0;
if do_parfor
    clusts=cell(1,M);
    parfor i=1:M
    %    j=clustSize(i);
      %  h=find(ismember(map_index,PIX{j}));
        h=find(ismember(map_index,PIX{i}));
        clusts{i}=h;
    end
%    for i=1:M
%        clust(clusts{i})=i;
%    end
else
    GD2=Flat.p.umap.GPUdevice2;
    for i=1:M
       % j=clustSize(i);
        if GD2 %&& Lh(j)>10
%            PIXj=gpuArray(PIX{j});
    %        h=find(ismember(Flat.umap.X.map_index,PIXj));
             PIXi=gpuArray(PIX{i});
        h=find(ismember(Flat.umap.X.map_index,PIXi));
        else
%            h=find(ismember(Flat.umap.X.map_index,PIX{j}));
           h=find(ismember(Flat.umap.X.map_index,PIX{i}));
        end
        clust(h)=i;
    end
end
end