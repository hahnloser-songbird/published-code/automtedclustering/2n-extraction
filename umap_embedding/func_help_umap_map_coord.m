function [map_coord,map_index]=func_help_umap_map_coord(map,r,n)
%map_index=zeros(1,size(map,1));
%map_coord=zeros(ndims,size(map,1));

o=ones(size(map,1),1);
%m=n*(map-range(1,:))./(range(2,:)-range(1,:));
map_coord=min(n,max(1,round(n*(map-o*r(1,:))./(o*(r(2,:)-r(1,:))))))';
map_index=(map_coord(2,:)-1)*n+map_coord(1,:);

% for i=1:size(map,1)
%     xy=func_help_tsne_Ipos(map(i,:),r,n);
%     m=round(xy); map_coord(:,i)=m;
%     if ndims==2
%         I(m(1),m(2))=1;
%     else
%         I(m(1),m(2),m(3))=1;
%     end
%     map_index(i)=(m(2)-1)*n+m(1);
% end
end