function [Flat,order_sequence,select_clust_sorted]=func_update_order_sequence(Flat,plot_ordered,curr_clust)
%hi=func_select_elements(Flat,curr_clust,Flat.v.tagview,Flat.select);
hi=Flat.v.select_clust;
if ~isfield(Flat.v,'sequence')
    Flat.v.sequence=cell(25,Flat.p.num_clust);
end
po=plot_ordered;

if isempty(hi)
    order_sequence=[];
    select_clust_sorted=[];
  %  Flat.v.sequence{po,curr_clust}=[];
  %  return
end

update_n=0;
if isempty(Flat.v.sequence{po,curr_clust}) || length(hi)~=length(Flat.v.sequence{po,curr_clust})
    update_n=1;
elseif max(Flat.v.sequence{po,curr_clust}) > Flat.num_Tags
    update_n=1;
elseif  Flat.v.all_clust_mode==0 && any(diff(Flat.X.clust_ID(Flat.v.sequence{po,curr_clust})))
    update_n=1;
end
%Flat.v.pca_recompute=0;
if update_n && isfield(Flat.v,'pca_recompute') && Flat.v.pca_recompute && length(Flat.p.channel_mic_display)==1
    Flat=func_Flat_PCA(Flat,0);
end
switch po
%     case 1
%         add_str=['Neighbor(' num2str(Flat.p.num_nearest_neighbors_for_order_sequence) ') sorted || '];
%         if update_n
%             
%             Flat.v.sequence{po,curr_clust}=func_sequence_neighbor(Flat,curr_clust);
%             %fprintf('new neighbor sequence\n');
%         end
        
    case {1,12,13} % timestamp & inverse timestamp & timestamp+tagpoint
        t1=datestr((Flat.DAT.timestamp(Flat.X.DATindex(min(hi)))),'mm-dd-HH-MM');
        t2=datestr((Flat.DAT.timestamp(Flat.X.DATindex(max(hi)))),'mm-dd-HH-MM');
        if isempty(t1)
            add_str='';
        else
            if po == 12
                add_str = [t2 ' until ' t1 '|| '];
            else
                add_str = [t1 ' until ' t2 '|| '];
            end
        end
        if update_n
                % [dummy timestamp_sorted_i]=sort(Flat.DAT.timestamp(Flat.X.DATindex(hi)),'ascend');
            [dummy timestamp_sorted_i]=sort(hi,'ascend');
            if po == 12
                Flat.v.sequence{po,curr_clust}=hi(timestamp_sorted_i(end:-1:1));
            else
                Flat.v.sequence{po,curr_clust}=hi(timestamp_sorted_i);
            end
        end
        
    case 2 % by length of syllable
        add_str='Duration sorted || ';
        if update_n
            [dummy length_sorted_i]=sort(Flat.X.data_off(hi),'ascend');
            Flat.v.sequence{po,curr_clust}=hi(length_sorted_i);
        end
        
%     case 4 % by Left context
%         add_str='Left context sorted || ';
%         if update_n
%             [Flat.V.pre Flat.V.post]=func_sequence_context(Flat,hi);
%             u=sort(unique(Flat.V.pre(hi)),'ascend');
%             hseq=[];
%             for i=1:length(u)
%                 hii=hi(Flat.V.pre(hi)==u(i));
%                 hseq=[hseq hii];
%             end
%             Flat.v.sequence{po,curr_clust}=hseq;
%         end
%         
%     case 5 % by right context
%         add_str='Right context sorted || ';
%         if update_n
%             [Flat.V.pre Flat.V.post]=func_sequence_context(Flat,hi);
%             u=sort(unique(Flat.V.post(hi)),'ascend');
%             hseq=[];
%             for i=1:length(u)
%                 hii=hi(Flat.V.post(hi)==u(i));
%                 hseq=[hseq hii];
%             end
%             Flat.v.sequence{po,curr_clust}=hseq;
%         end
%         
%     case 6
%         add_str='Template sorted || ';
%         if update_n
%             if ~isempty(hi)
%                % template=func_extract_data(Flat,5,hi(1));
%                 Flat.v.sequence{po,curr_clust}=func_clustered_sequence(Flat,hi(1));
%             else
%                 template=[];
%                 Flat.v.sequence{po,curr_clust}=[];
%                 
%             end
%         end
        
    case {3,7,0}
        add_str='Randomly sorted || ';
        if update_n
            Flat.v.sequence{po,curr_clust}=hi(randperm(length(hi)));
        end
        
    case 8
        add_str='Custom sorted || ';
        if update_n
            if isfield(Flat.X,'custom')
                [dummy custom_sorted_i]=sort(Flat.X.custom(end,hi),'ascend');
                Flat.v.sequence{po,curr_clust}=hi(custom_sorted_i);
            else
                fprintf('No custom field in Flats\n');
                Flat.v.plot_ordered=2;
            end
        end
%         
%     case 9
%         add_str=['Tagpoint ' num2str(Flat.p.tagnames{Flat.v.tagpointer_sort}) ' || '];
%         if update_n
%             %  all=combine(Flat.v.tagboard0i{Flat.v.tagpointer_sort}{:});
%             try
%                 all=[Flat.v.tagboard0i{Flat.v.tagpointer_sort}{:}];
%             catch
%                 all=cat(1,Flat.v.tagboard0i{Flat.v.tagpointer_sort}{:});
%             end
%             [h,i,j]=intersect(all,hi);
%             [sorti,indi]=sort(i);
%             Flat.v.sequence{po,curr_clust}=all(sorti);
%             
%             %    h=[Flat.v.tagpointer_indices{:}];
%             %  Flat.v.sequence{po,curr_clust}=h(Flat.X.clust_ID(h)==Flat.v.curr_clust);
%         end
        
        %% old, slower
        %              [dummy tagpointer_sorted_i]=sort(Flat.Tags(Flat.v.tagpointer_sort,hi));
        %             %% correction
        %             current_user = evalin('base', 'cfg.userID');
        %             switch current_user
        %                 % thanks for breaking stuff for everyone except you, Sepp.
        %                 case 23
        %                     h=zeros(1,length(hi));
        %                     h1=str2num(str2mat(dummy));
        %                     filled=~cellfun(@isempty,dummy);
        %                     h(filled)=h1;
        %                     if ~isempty(h)
        %                         [h,h2]=sort(h);
        %                         tagpointer_sorted_i=tagpointer_sorted_i(h2);
        %                     end
        %                     hi=hi(tagpointer_sorted_i);
        %                     Flat.v.sequence{po,curr_clust}=hi;
        %
        %                 otherwise
        %                     h=str2num(str2mat(dummy));
        %                     if ~isempty(h)
        %                         [h,h2]=sort(h);
        %                         tagpointer_sorted_i=tagpointer_sorted_i(h2);
        %                     end
        %                     Flat.v.sequence{po,curr_clust}=hi(tagpointer_sorted_i);
        
        %             end
        %          end
        
    case 10 % hack
        add_str=['Local stackpeak || '];
        %  if update_n
        try
            %   if isfield(Flat.v,'plot_ordered_tagpoint_cv') && Flat.v.plot_ordered_tagpoint_cv==1 && length(Flat.v.DATcurves)==1
            if any(Flat.v.DATmark_lines)
                which_mark=find(Flat.v.DATmark_lines,1,'first');
                posl=Flat.v.DATmark_lines_pos(which_mark,1);
                posr=Flat.v.DATmark_lines_pos(which_mark,2);
                out=func_stack_extract(Flat,Flat.v.DATstack,hi,[-posl posr]);
                h=max(out');
            else
                [h,h2]=max(Flat.v.DATcurves{1});
                h=Flat.v.help_cv_max_all{h2};
            end
            [hsort,hsorti]=sort(h,'descend');
            tagp_sorted=hi(hsorti);
            tagp_sorted=[tagp_sorted setdiff(hi,tagp_sorted)];
        catch
            tagp_sorted=hi;
            fprintf('Problem local stackpeak sort\n');
        end
        Flat.v.sequence{po,curr_clust}=tagp_sorted;
        %  end
        
        
    case 11 %% hack
        add_str=['Firing rate || '];
        try
            if any(Flat.v.DATmark_lines)
                which_mark=find(Flat.v.DATmark_lines,1,'first');
                % relevantCL
                posl=Flat.v.DATmark_lines_pos(which_mark,1);
                posr=Flat.v.DATmark_lines_pos(which_mark,2);
                
                frate=func_help_DAT_spk_extract2(Flat,hi,[-posl posr]);
                [h,hsorti]=sort(cellfun(@length,frate),'descend');
                
                %                 z=zeros(length(hi),-posl+posr);
                %                 for jj=1:length(frate)
                %                  for    kk=1:length(frate{jj})
                %                    z(jj,-posl+frate{hsorti(jj)}(kk))=z(jj,-posl+frate{hsorti(jj)}(kk))+1;
                %                         end
                %                 end
                %
                tagp_sorted=hi(hsorti);
            else
                [h,hsorti]=sort(sum(Flat.v.DATIs,1));
                tagp_sorted=hi(hsorti);
                tagp_sorted=[tagp_sorted setdiff(hi,tagp_sorted)];
            end
        catch
            tagp_sorted=hi;
            fprintf('Problem firing rate sort\n');
        end
        Flat.v.sequence{po,curr_clust}=tagp_sorted;
        
    case 14 %% hack
        add_str='pitch ascend || ';
        if update_n
            if isfield(Flat.X,'pitch')
                [dummy custom_sorted_i]=sort(Flat.X.pitch(hi),'ascend');
                Flat.v.sequence{po,curr_clust}=hi(custom_sorted_i);
            else
                fprintf('No pitch field in Flat.X! (created by activator_pitch_shift)\n');
            end
        end
        
    case 15 %% hack
        add_str='syldur ascend || ';
        if update_n
            if isfield(Flat.X,'syldur')
                [dummy custom_sorted_i]=sort(Flat.X.syldur(hi),'ascend');
                Flat.v.sequence{po,curr_clust}=hi(custom_sorted_i);
            else
                fprintf('No syldur field in Flat.X! (created by activator_syllable_stretch)\n');
            end
        end
        
    case 16 %% hack
        add_str='sylgapdur ascend || ';
        if update_n
            if isfield(Flat.X,'sylgapdur')
                [dummy custom_sorted_i]=sort(Flat.X.sylgapdur(hi),'ascend');
                Flat.v.sequence{po,curr_clust}=hi(custom_sorted_i);
            else
                fprintf('No sylgapdur field in Flat.X! (created by activator_syllable_gap_stretch)\n');
            end
        end
        
    case 17 %% smart move
        add_str='smart move || ';
        if update_n
            %              [i_s,I_s,s_s,s_t]=func_clust_scal_prod_all_move(Flat,Flat.v.curr_clust,[]);
            %              [h,hi]=sort(s_s,'descend');
            %              Flat.v.sequence{Flat.v.plot_ordered,Flat.v.curr_clust}=[I_s i_s(hi)];
            Flat=func_clust_scal_prod_all_move(Flat);
            %disp('did');
        end
end

order_sequence=Flat.v.sequence{po,curr_clust};
select_clust_sorted=order_sequence;

%% check leftmost_element within bounds (if current cluster and order_type)
if Flat.v.last_clickOccurred
    
    orderSeqIdx = find(order_sequence == Flat.v.last_clicked, 1);
    Flat.v.leftmost_element = orderSeqIdx; %max(1, orderSeqIdx-floor(length(Flat.v.spec_plot_IDs)/2));
    Flat.v.last_clickOccurred = false;

elseif curr_clust==Flat.v.curr_clust && po==Flat.v.plot_ordered
    
    if  Flat.v.leftmost_element>length(order_sequence)
        %  Flat.v.leftmost_element=1;
        Flat.v.leftmost_element=max(1,length(order_sequence)-Flat.v.max_elements_on_screen);
    end
    
end

%% narrow down order sequence given leftmost element
if plot_ordered<20   %&& Flat.v.zoom_level>0
    hh=min(length(order_sequence),Flat.v.leftmost_element+Flat.v.max_elements_on_screen-1);
    order_sequence=order_sequence(Flat.v.leftmost_element:hh);
    end_str=[' ' num2str(Flat.v.leftmost_element) ' to ' num2str(hh) ' / ' num2str(length(hi)) '.'];
else
    end_str=[' 1 to end.'];
end


%% write add and end strings
if curr_clust==Flat.v.curr_clust && po==Flat.v.plot_ordered
    Flat.v.add_str=add_str;
    Flat.v.end_str=end_str;
end
