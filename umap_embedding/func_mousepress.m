function func_mousepress(src, evnt) %#ok<INUSD>

Flat=evalin('base','Flat');
fcH=evalin('base','fcH');
Flat.v.plot_clust_fast=0;

if src==24  % context figure
%     return
%     if isfield(Flat,'umap') && isfield(Flat.umap,'brute_force')
%         if ~isfield(Flat.umap.brute_force,'Gpress')
%             Flat.umap.brute_force.Gpress=0;
%         end
%         Flat.umap.brute_force.Gpress=max(0,Flat.umap.brute_force.Gpress-1);
%     end
    p = func_mouseclick_element(Flat, 'ha_spcgrm');
    for i=1:length(Flat.v.spec_plot_IDs)
        if length(fcH.hf_spcgrm_text)>=i
            set(fcH.hf_spcgrm_text{i},'color','w');
        else
            fcH.hf_spcgrm_text{i}=text(0.01,(i-1)*Flat.scanrate/4+6000,num2str(Flat.p.spec_num+1-i),'color','w','fontsize',11, 'BackgroundColor', 'k', 'FontWeight', 'bold','linewidth',1);
        end
    end
%     if ~isfield(Flat.v,'key_mod_spec') || isempty(Flat.v.key_mod_spec)
%         Flat.v.fig24_range=[];
%         Flat.v.fig24_shiftclick=false;
%         Flat.v.spec_redtext_i=p;
%     elseif strcmp('shift', Flat.v.key_mod_spec)
%         if ~isfield(Flat.v,'fig24_shiftclick') || ~Flat.v.fig24_shiftclick
%             Flat.v.fig24_shiftclick=true;
%             Flat.v.fig24_range=[];
%             Flat.v.spec_redtext_i=p;
%             disp('Shift+click on the other end of the range');
%         else
%             Flat.v.fig24_shiftclick=~Flat.v.fig24_shiftclick;
%             i1=find(Flat.v.select_clust==Flat.v.last_clicked);
%             i2=find(Flat.v.select_clust==Flat.v.spec_plot_IDs(p));
%             if i1>i2
%                 h=i1; i1=i2; i2=h;
%             end
%             Flat.v.fig24_range=Flat.v.select_clust(i1:i2);
%             Flat.v.spec_redtext_i=find(ismember(abs(Flat.v.spec_plot_IDs),Flat.v.fig24_range));
%             disp('Range defined');
%         end
%         
%     end
%     set(fcH.hf_spcgrm_text{p},'color','r');
%     Flat.v.redtext.grow_examples=[];
%     Flat.v.redtext.templates=[];
%     Flat.v.plot_templates=1;
%     Flat.v.plot_grow=1;
%     if p <= numel(Flat.v.spec_plot_IDs)
%         Flat.v.last_clicked = Flat.v.spec_plot_IDs(p);
%     end
%     
%     if isfield(fcH,'hf_tsne') && ishandle(fcH.hf_tsne) && Flat.v.curr_clust==Flat.p.num_clust
%         h=find(Flat.v.last_clicked==Flat.v.select_clust);
%         sel=Flat.v.select_clust(h+1:end);
%         sel=find(ismember(Flat.v.tsne.select,sel));
%         set(fcH.hf_tsneScatter,'XData',fcH.hf_tsneVar.map_coord(1,sel),'YData',fcH.hf_tsneVar.map_coord(2,sel),'color','w','visible','on');
%     end
%     
%     
%     %% fig 28
elseif isfield(Flat.p, 'f28')  && isfield(fcH, 'f28') && (src == fcH.f28.ha_img ||...
        any(cellfun(@(x) src == x, fcH.f28.segments1_rct)))
    Flat.v.plot_ordered=2; % only timestamp ordering is allowed in fig 28
    
    pos = get(fcH.f28.ha_spec, 'CurrentPoint');
    xp = pos(1, 1);
    yp = pos(1, 2);
    
    if ~isfield(Flat.p.f28, 'specList')
        return;
    end
    
    fstart = Flat.p.f28.specList.fstart;
    fend = Flat.p.f28.specList.fend;
    fy = Flat.p.f28.specList.fy;
    fidx = Flat.p.f28.specList.fidx;
    pi = find(fstart < xp & fend > xp);
    [~, mdx] = min(abs(fy(pi) - yp));
    pi = pi(mdx);
    newIdx = fidx(pi);
    
    if ~isempty(newIdx)
        [Flat, fcH] = fci_setFocussedElement(Flat,fcH, newIdx);
    end
    
    
    
elseif isfield(Flat.p, 'f28') && isfield(fcH, 'f28') && (src == fcH.f28.ha_plot || src == fcH.f28.ha_overview)
    pos = get(fcH.f28.ha_overview, 'CurrentPoint');
    xp = pos(1);
    Flat.p.f28.cursorLeft_ms = xp;
        
    %% UMAP
elseif src==32 % UMAP
    if strcmp(Flat.v.RMS(1),'@')
        disp('Already clustered, clicking on this figure will erase everything - Aborting');
        return
    end
    p=get(fcH.hf_umap_ax,'currentpoint');
    x=ceil(p(1)); y=ceil(p(3));
    
    % move back syllables from last clust
    h=Flat.X.clust_ID(Flat.umap.X0.select)==Flat.p.num_clust;
    if strcmp(Flat.v.RMS(1),'#')
        if any(h)
            Flat.v.last_clust=mode(Flat.X.clust_ID0(Flat.umap.X0.select(h)));
        end
        Flat.X.clust_ID(Flat.umap.X0.select(h))=Flat.X.clust_ID0(Flat.umap.X0.select(h)); % rrr
    else
        Flat.X.clust_ID(Flat.umap.X0.select(h))=1; % rrr
    end
    
    n=Flat.p.umap.NumPixels;
    r=Flat.p.umap.RadiusKernel;
    ind=(x-1)*n+y;
    I=get(fcH.hf_umapIT,'CData'); % density distribution at the current time slice
    if ind<1 || ind>n^2 || I(ind)==0 % if the mouse click was outside a blob
        Flat.umap.curr_clust=0;
        if isfield(Flat.v,'last_clust')
            Flat.v.curr_clust=Flat.v.last_clust;
        else
            Flat.v.all_clust_mode=1;
            h=setdiff(1:Flat.p.num_clust,unique(Flat.X.clust_ID(Flat.umap.X.select)));
            Flat.v.curr_clust=h(1); %rrr
        end
        Flat.v.run_select_clust=1;
        Flat.p.spec_syll_dur=1;
        Flat.v.plot_clust=1;
        Flat.v.sequence{Flat.v.plot_ordered,Flat.v.curr_clust}=[];
        Flat.v.prepare_plot=1;
        [Flat, Flat.v.select, Flat.v.select_clust]=func_select_mode(Flat);
        assignin('base','fcH',fcH);
        assignin('base','Flat',Flat);
        return
    else
        Flat.v.all_clust_mode=0;
    end
    this_clust=find(I==I(ind));
    Flat.v.umap.Imousepress_ind=ind; % index into image
    Flat.v.umap.IclustID=this_clust;   % clicked blob in image
    
    clust=Flat.umap.NumClust+1-I(ind); % reverse blob numbering (Note: cluster is here a synonym for blob)
    Flat.umap.curr_clust=clust;
    inds=find(func_help_umap_select_from_I(Flat,Flat.umap.PIX(clust),1));
    
    Flat.umap.XC.select=inds; % clicked elements (relative)
    Flat.umap.XC.Select=Flat.umap.X.select(inds); % clicked elements (absolute)
    
    
    if strcmp(Flat.v.RMS(1),'#')
        aa=histogram(Flat.X.clust_ID0(Flat.umap.XC.Select),1:Flat.p.num_clust);
        af=find(aa);
        for i=1:length(af)
            fprintf('C%d: %d, ',af(i),aa(af(i)));
        end
        fprintf('\n');
    end
    Flat.X.clust_ID(Flat.umap.XC.Select)=Flat.p.num_clust; % for displaying purposes in figs 20 and 24
    Flat.v.curr_clust=Flat.p.num_clust;
    
    % find closest elements
    Z=Flat.umap.X.map';
    M=Flat.umap.Maprange;
    n=Flat.p.umap.NumPixels;
    
    m1=x*(M(2,1)-M(1,1))/n+M(1,1);
    m2=y*(M(2,2)-M(1,2))/n+M(1,2);
    
    [dd,d]=sort(sum((ones(size(Z,1),1)*[m1 m2]-Z).^2,2),'ascend');
    els=Flat.umap.X.select(d);
    
    
    if ~isfield(Flat.X,'custom')
        Flat.X.custom=(length(els)+1)*ones(1,Flat.num_Tags);
    end
    if strcmp(Flat.v.RMS(1),'#')
        Flat.X.custom(3,els)=(1:length(els));%:-1:1;
    else
        % relevant section for plotting the results in ascending order
        Flat.X.custom(end,:)= ones(size(Flat.X.custom))*size(Flat.X.custom,2);
        Flat.X.custom(end,els)=(1:length(els));%:-1:1;
    end
    set(fcH.hf_umapTit,'String',...
        sprintf('r=%.1f || th=%.2f || Nclust=%d || t=%d || n = %d', ...
        r, Flat.p.umap.theta, Flat.p.umap.MaxClust, Flat.p.umap.time, length(inds)), ...
        'fontsize',9);
    
    if  isempty(Flat.v.umap.evnt_modifier)
        Flat.v.plot_ordered=8;
    elseif strcmp(Flat.v.umap.evnt_modifier{1},'shift')
        Flat.v.plot_ordered=8; % random ordering
        
        % Select blob for clustering
    elseif strcmp(Flat.v.umap.evnt_modifier{1},'control')
        Flat.v.plot_ordered=8;%7;
        Flat=func_umap_mouseclick_control(Flat); % this is where it all happens
        set(fcH.hf_umapScatterCurrBlob,'XData',Flat.umap.fcH.ScatterCurrBlob(1,:),'YData',Flat.umap.fcH.ScatterCurrBlob(2,:),'color','m','Visible','On');
    end
    Flat.v.plot_clust=1;
    Flat.v.sequence{Flat.v.plot_ordered,Flat.v.curr_clust}=[];
    Flat.v.leftmost_element=1;
    Flat.v.prepare_plot=1;
    Flat.v.run_select_clust=1;
    [Flat, Flat.v.select, Flat.v.select_clust]=func_select_mode(Flat);
    Flat.v.plot_spec = 1;
end

[Flat,fcH]=plot_figs(Flat,fcH);

%% write back to workspace
assignin('base','Flat',Flat);
assignin('base','fcH',fcH);

end
