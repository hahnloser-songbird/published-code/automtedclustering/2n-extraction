function Flat = evalkey_umap_q(Flat)
% close umap and save results in Flat

ui=0;
disp('This will finalize your umap clustering - I hope you saved the archive!');
disp('press 1+enter to continue');
y=input('');
if isempty(y) || ~(y==1)
    disp('Aborted');
    return
end

appendix=Flat.p.nonoverlap*(Flat.p.umap.MinNumBuffSyl-Flat.p.umap.MinNumTimeSegments);
L=length(Flat.DAT.filename);

% clear some variables
Flat.X.umapCtrWP(1,:)=0; Flat.X.umapCtrWN(1,:)=0;

if ~isfield(Flat,'Z')
    Flat=func_umap_Z_generate(Flat,0);
end
Flat.Z.stencil=int8(zeros(1,length(Flat.Z.buffer)));

% extract syllables
WP=Flat.X.umapCtrWP(1:end,Flat.umap.X0.select);
WN=Flat.X.umapCtrWN(1:end,Flat.umap.X0.select);
TimeSL=Flat.X.umap_time(:,Flat.umap.X0.select);
datinds=Flat.X.DATindex(Flat.umap.X0.select);
inds=Flat.X.indices_all(Flat.umap.X0.select);

% initialize new element insertion
tagvec=cell(length(Flat.p.tagnames),1); tagvec(:)={''}; tagvec{5}=['@' Flat.v.RMS];
Ons=cell(1,L); Offs=Ons; ClustIDs=Ons; Tags=Ons; Custs=Ons;
Cinsert=zeros(1,L);

multibuff=0;% multibuffrand=0; % 'rand' variables are to inspect behavior for studying onset or offset only clusterings
% jother=setdiff(2:Flat.umap.XC.ClustNo,j);
for f=1:L
    file_i=find(datinds==f);
    indsF=inds(file_i);  datindsF=datinds(file_i);
    sameS=int8(zeros(size(file_i))); clusts=sameS;
    for j=2:Flat.umap.XC.ClustNo
        pos=WP(j,file_i);
        if isfield(Flat.v,'umap.do_rand') && Flat.v.umap.do_rand==1
            neg=pos;
        else
            neg=-WN(j,file_i);
        end
        h=(pos==neg & pos);
        sameS=sameS + int8(h);
        clusts(h)=j;
    end
    multibuff=multibuff+sum(sameS>1);
    
    c=1; onoff=[];  cmiss=1; zis=[]; timeSLon=[]; timeSLoff=[];
    sameSi=find(sameS);
    if isempty(sameSi)
        continue
    end
    on=sameSi(1); clustsON=[];
    for ii=2:length(sameSi)
        i=sameSi(ii);
        if i==sameSi(end)
            continue
        end
        %  new blob or new file or gap => new offset or miss
        if  (~isequal(clusts(i),clusts(on)) || ~isequal(datindsF(i),datindsF(on)) || indsF(i)-indsF(sameSi(ii-1))>Flat.p.nonoverlap)
            
            % syllable sufficiently long and gap large enough
            if indsF(sameSi(ii-1))-indsF(on)>=Flat.p.nonoverlap*Flat.p.umap.MinNumTimeSegments	&& ...
                    (indsF(i)-indsF(sameSi(ii-1))>appendix ||  ~isequal(datindsF(i),datindsF(on)))
                onoff(1,c)=on;
                onoff(2,c)=sameSi(ii-1);
                clustsON(c)=clusts(sameSi(ii-1));
                if clustsON(c)<2
                    keyboard
                end
                WP(1,file_i(onoff(1,c):onoff(2,c)))=clustsON(c);
                
                zi=Flat.Y.Zindex(file_i(onoff(1,c):onoff(2,c)));
                zi=[zi (zi(end)+1):(zi(end)+Flat.umap.appendixB)];
                zis(1,c)=zi(1); zis(2,c)=zi(end);
                timeSLon(c)=TimeSL(1,file_i(onoff(1,c)));
                timeSLoff(c)=TimeSL(2,file_i(onoff(2,c)));
                
                if ~isequal(Flat.X.indices_all(Flat.umap.X0.select(file_i(onoff(1,c)))),Flat.Z.indices_all(zi(1)))
                    keyboard
                end
                
                if any(Flat.Z.stencil(zi))
                    keyboard
                end
                if any(diff(Flat.Z.buffer(zi))>1)
                    keyboard
                end
                Flat.Z.stencil(zi)=clustsON(c);
                c=c+1;
                on=i;
            else
                cmiss=cmiss+1;
                on=i;
            end
        end
    end
    
    % last offset
    if indsF(i)-indsF(on)>Flat.p.nonoverlap*Flat.p.umap.MinNumBuffSyl
        onoff(1,c)=on; onoff(2,c)=i;  clustsON(c)=clusts(i);
        WP(1,file_i(onoff(1,c):onoff(2,c)))=clustsON(c);
        zi=Flat.Y.Zindex(file_i(onoff(1,c):onoff(2,c)));
        
        timeSLon(c)=TimeSL(1,file_i(onoff(1,c)));
        timeSLoff(c)=TimeSL(2,file_i(onoff(2,c)));
        
        zi=[zi (zi(end)+1):(zi(end)+Flat.umap.appendixB)];
        zis(1,c)=zi(1); zis(2,c)=zi(end);
        
        if ~isequal(Flat.X.indices_all(Flat.umap.X0.select(file_i(onoff(1,c)))),Flat.Z.indices_all(zi(1)))
            disp('@CL something is wrong')
            keyboard
        end
        if any(Flat.Z.stencil(zi))
            keyboard
        end
        %   Flat.Z.stencil(zi:zi+diff(onoff(:,c))-1+Flat.umap.appendixB)=clustsON(c);
        Flat.Z.stencil(zi)=clustsON(c);
    else
        c=c-1;
    end
    if isempty(onoff)
        continue
    end
    on=onoff(1,:); off=onoff(2,:);
    
    for i=1:length(on)
        Di=datindsF(on(i));
        Cinsert(Di)=Cinsert(Di)+1;
        Ons{Di}(Cinsert(Di))=indsF(on(i));
        Offs{Di}(Cinsert(Di))=indsF(off(i))+appendix;
        ClustIDs{Di}(Cinsert(Di))=clustsON(i);
        Tags{Di}(:,Cinsert(Di))=tagvec;
        Custs{Di}(:,Cinsert(Di))=[zis(1,i) ; zis(2,i); timeSLon(i); timeSLoff(i)];
        if zis(1,i)==0 || zis(2,i)==0
            keyboard
        end
    end
end
Flat.umap.Z_doneQ=1;
Flat.Y.umap=Flat.X.umap(:,Flat.umap.X0.select);
Flat.Z.stencil0=Flat.Z.stencil;  % for computation of precision and recall
fprintf('number of multiply defined buffers: %d\n',multibuff);

Flat.X.umapCtrWP(1,Flat.umap.X0.select)=WP(1,:);


% remove RMSfilt elements
[Flat,sels,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS});
if ~isempty(sels)
    fprintf('Now removing old %s elements (and storing them in Flat.Y)\n',Flat.v.RMS);
    fn={'indices_all','data_off','DATindex','PCcoeffs','umapCtrWP','umapCtrWN'};
    Flat.p.umap.Ygen=fn;
    for i=1:length(fn)
        Flat.Y.(fn{i})=Flat.X.(fn{i})(:,sels);
    end
    Flat=func_remove_elements(Flat,sels,0,1);
end

% remove old umap elements
[Flat,sels,board] = func_select_tagboard(Flat,[],'activator',{['@' Flat.v.RMS]});
if ~isempty(sels)
    disp('Now removing old UMAP elements');
    Flat=func_remove_elements(Flat,sels,0,1);
end

Flat.X=rmfield(Flat.X,'custom');
Flat=func_write_imported_elements_to_Flat(Flat,Ons,Offs,ClustIDs,Custs,[],Tags,[],[]);
if ~strcmp(Flat.v.RMS(1),'@')
    Flat.umap.RMSold=Flat.v.RMS;
end
Flat.v.RMS=['@' Flat.v.RMS]; Flat.p.Channel_syl_tagstring{Flat.p.channel_mic_display}=Flat.v.RMS;
Flat.X.umap_time=Flat.X.custom(3:4,:);
Flat.X.custom(3:4,:)=[];
Flat.Z.custom0=Flat.X.custom;


Flat.name=['@' Flat.name];
Flat.v.ind=Flat.v.ind+1;
Flat.p.names{Flat.v.ind}=Flat.name;

Flat.v.umap.track_stencil=1;
Flat.v.save.DAT_data=1;
Flat.v.save.BOARDLIST=1;
Flat.v.save.DAT_multiunit=1;
Flat.v.save.DAT_STACK=1;
Flats=evalin('base','Flats');
Flats{Flat.v.ind}=Flat;
assignin('base','Flats',Flats);

Flat.v.all_clust_mode=0;
Flat.v.plot_numbutton=1;
Flat.v.plot_tagboard=1;
Flat.v.curr_clust=2;
Flat.v.plot_ordered=3;
Flat.v.repeated_put_to=1;

Flat.v.tagboard0_recompute=1;
Flat.v.tagboard0_replot=1;
Flat.BOARD.tagboard{5}={Flat.v.RMS};
[Flat, Flat.v.select, Flat.v.select_clust]=func_select_mode(Flat);
Flat.v.plot_clust=1;
Flat.v.plot_spec=0; % previously 1
Flat.v.PLOT.spec = 0;

Flat.v.select = find(strcmp(Flat.Tags(5,:), '@RMSfilt'));
Flat.v.select_clust = Flat.v.select;

end

