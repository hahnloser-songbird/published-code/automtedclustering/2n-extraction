function [data, numbuffs] = func_extract_data(Flat,spec_id,hi,do_double,Frange,usenoisemask)
% extract data: number 5 is the cluster data

data=[];
numbuffs=0;
%noisemask=[];
 
if nargin<4
    do_double=0;
end

if nargin<5
    Frange=[];
else
    % if isfield(Flat.v,'import_DAT')
    if isfield(Flat.p,'syllable')
        %      S=Flat.v.import_DAT;
        P=Flat.p.syllable;
    else
        Frange=[];
    end
end
if nargin>5 && nargout>2 && isfield(Flat.DAT,'maskDiffNoise')
  %  usenoisemask=1;
else
     usenoisemask=0;
end

if isempty(hi)
    return
end

Ldisp=length(Flat.p.channel_mic_display);
if length(Flat.p.CHANNEL_MIC)>1 && Ldisp>1
    numbuffs=Flat.spec.numbuffs{spec_id}(Flat.X.clust_ID(hi(1)));
    samp_left=Flat.spec.samp_left{spec_id}(Flat.X.clust_ID(hi(1))); % shorthand
    l=numbuffs*length(Flat.p.nfft_i);
    data=zeros(Ldisp*l,length(hi));
    if do_double
        hh=find(hi);
        if size(hh,1)>size(hh,2)
            hh=hh';
        end
        for i=hh
            i0=Flat.X.indices_all(hi(i));
            %           on=round((i0-samp_left)/Flat.p.nonoverlap)+1; % in func_stack_which_buffer it is ceil istead of round!
            on=max(0,round((i0-samp_left-Flat.p.nfft)/Flat.p.nonoverlap))+1;
            if on>0 && on+numbuffs<size(Flat.DAT.datas{Flat.X.DATindex(hi(i))}{1},1)+2
                % the next line without the apostrophe after (:,on:on+numbuffs-1) does not work
                % for 'v' in evalkey_clust
                %               if Flat.v.display_multi==0
                a=[Flat.DAT.datas{Flat.X.DATindex(hi(i))}{Flat.p.channel_mic_display}];
                data(:,i)=reshape(double(a(on:on+numbuffs-1,:)')/128*Flat.dynamic_range_spec,l*Ldisp,1);
                %             else
                %                data(1:end/2,i)=reshape(double(a(on:on+numbuffs-1,1:end/2)')/128*Flat.dynamic_range_spec,l*Ldisp,1);
                %          end
            end
        end
    end
%      if isfield(Flat.spec,'cap')
%         data=min(data,Flat.v.spec_cap);
%     end
    return
else
    % use try / catch here because we run into problems if user selects
    % invalid elements.
    %   try
    
    %%
    if Flat.p.spec_full_mode
        
        numbuffs=Flat.spec.numbuffs{spec_id}(Flat.X.clust_ID(hi(1)));
        
        if do_double
            try
            data=zeros(numbuffs*length(Flat.p.nfft_i),length(hi));
            catch ME
                disp('Error: maybe try reducing the size of the clust win');
                rethrow(ME)
            end
        else
            data=zeros(numbuffs*length(Flat.p.nfft_i),length(hi),'int8');
        end
        %         if usenoisemask
        %             noisemask=zeros(numbuffs,length(hi));
        %         end
        l=size(data,1);
        samp_left=Flat.spec.samp_left{spec_id}(Flat.X.clust_ID(hi(1))); % shorthand
        todo=find(hi);
        
        not_loaded=cellfun(@isempty,Flat.DAT.data(Flat.X.DATindex(todo)));
        hi(todo(not_loaded))=[];
        
        if ~isempty(hi)% do_double
            hh=find(hi);
            if size(hh,1)>size(hh,2)
                hh=hh';
            end
            for i=hh
                i0=Flat.X.indices_all(hi(i));
                on=max(0,round((i0-samp_left-Flat.p.nfft)/Flat.p.nonoverlap))+1;
                %           on=round((i0-samp_left)/Flat.p.nonoverlap)+1; % in func_stack_which_buffer it is ceil istead of round!
                if on>0 && on+numbuffs<size(Flat.DAT.data{Flat.X.DATindex(hi(i))},2)+2
                    % the next line without the apostrophe after (:,on:on+numbuffs-1) does not work
                    % for 'v' in evalkey_clust
                    if do_double
                        h=double(Flat.DAT.data{Flat.X.DATindex(hi(i))}(:,on:on+numbuffs-1));
                    else
                        h=Flat.DAT.data{Flat.X.DATindex(hi(i))}(:,on:on+numbuffs-1);
                    end
                    if usenoisemask % we do this because we may get some noise between two syllables in Flat.v.tsne.clust_dense_mode with joining syllables mode
                        h2=[1 Flat.DAT.maskDiffNoise{Flat.X.DATindex(hi(i))}(Flat.p.channel_mic_display,:) 1];
                        noisemask=h2(on:on+numbuffs-1)';
%                         if sum(sum(noisemask(:,i)))
%                             keyboard
%                         end
                        h(:,find(noisemask))=Flat.p.syllable.sum(Flat.p.channel_mic_display).minsS;
%                        h(:,find(noisemask))=-45;  
                    end
                    if  isfield(Flat.v,'tsne') && isfield(Flat.v.tsne,'zeropad') && Flat.v.tsne.zeropad == 1
                        ioff=Flat.X.data_off(hi(i));
                        off=min(numbuffs,round(ioff/Flat.p.nonoverlap))+1;
                        h(:,off:numbuffs) = Flat.v.tsne.smin;
                    end
                    if  isfield(Flat.v,'tsne') && isfield(Flat.v.tsne,'clust_dense_mode') && (Flat.v.tsne.clust_dense_mode==1 || Flat.v.tsne.clust_dense_mode==3)
                        if ~isfield(Flat.v,'pca_segmented')
                            Flat.v.pca_segmented=1;
                        end
                        if Flat.v.pca_segmented
                            if ~isfield(Flat.v,'pca_min')
                             	Flat.v.pca_min=min(min(Flat.DAT.data{1}));
                            end
                            h(:,min(end+1,round(Flat.X.data_off(hi(i))/Flat.p.nonoverlap-Flat.spec.firstbuff{5}(1))+1):end)=Flat.v.pca_min;
                        end
                    end
                    if isempty(Frange) || ~isfield(P,'els')
                        if do_double
                            data(:,i)=reshape(h/128*Flat.dynamic_range_spec,l,1);
                        else
                            data(:,i)=reshape(h,l,1);
                        end
                    else
                        h2=zeros(size(h));
                        h2(P.els,:)=h(P.els,:);
                        if do_double
                            data(:,i)=reshape(h2/128*Flat.dynamic_range_spec,l,1);
                           % data(:,i)=reshape(ceil(h2/128*Flat.dynamic_range_spec/16)*16,l,1);
                        else
                            data(:,i)=reshape(h2,l,1);
                        end
                    end
                end
            end
        end
        %%
    else
        
        firstbuffs=(+Flat.spec.firstbuff{spec_id}(Flat.v.curr_clust)-Flat.spec.firstbuff{1}(Flat.v.curr_clust))*ones(size(hi));
        numbuffs=Flat.spec.numbuffs{spec_id}(Flat.v.curr_clust);
        
        data=zeros(numbuffs*length(Flat.p.nfft_i),length(hi));
        todo=find(hi);
        not_loaded=cellfun(@isempty,Flat.X.data(todo));
        hi(todo(not_loaded))=0;
        if do_double
            for i=find(hi)
                datah=Flat.dynamic_range_spec/128*double([Flat.X.data{hi(i)}]);
                data(:,i)=datah(firstbuffs(i)*length(Flat.p.nfft_i)+1:(firstbuffs(i)+numbuffs)*length(Flat.p.nfft_i));
            end
        end
    end
    
    
    %%
    %     catch me
    %         fprintf('Problem: maybe try importing with I - func_get_data_new\n');
    %         %        keyboard
    %         custom_db_stack(me.message, me.stack, 'func_extract_data', '   ', me.identifier);
    %     end
    
end
% if isfield(Flat.spec,'cap')
%         data=min(data,Flat.v.spec_cap);
%     end
end
%end
%% EOF
