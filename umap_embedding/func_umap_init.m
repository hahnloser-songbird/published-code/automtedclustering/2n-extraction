function [fcH,Flat]=func_umap_init(fcH,Flat, ask, do_umap)
% this function sets up umap clustering
% Flat.umap.X contains the data for the current mic channel
% Flat.umap.XT contains the time slice data (index into Flat.umap.X)
% Flat.umap.XC containes the clicked cluster (index into Flat.umap.X)
%
% select is the index into Flat.umap.X.select,
% Select is the index into Flat.X

if nargin<3
    ask=1;
    do_umap=0;
elseif nargin < 4
    do_umap=0;
end

passed=func_umap_some_tests(Flat);
if ~passed
    return
end
Flat=func_some_initializations(Flat);
disp('Type - to lower threshold until you see clusters....')

%% some boring stuff
if ~Flat.v.all_clust_mode
    disp('You are not in all_clust_mode, press enter to fix');
    pause
    Flat.v.all_clust_mode=1;
    [Flat, Flat.v.select, Flat.v.select_clust]=func_select_mode(Flat);
end

[Flat,sels,board] = func_select_tagboard(Flat,[],'activator',{['@' Flat.v.RMS]});
if ~isempty(sels) || strcmp(Flat.v.RMS(1),'@')
    fprintf('Now removing %s elements\n',['@' Flat.v.RMS]);
    Flat=func_remove_elements(Flat,sels);
end

umap_init=1; % remove blob clustering

if ~isfield(Flat,'umap') || ~isfield(Flat.umap,'X0') || ~isfield(Flat.umap.X0,'select') || ~(length(Flat.v.select_clust)==length(Flat.umap.X0.select)) %~isequal(sort(Flat.v.select_clust),sort(Flat.umap.X0.select)) && length(Flat.v.select_clust)==length(Flat.umap.X0.select)
    disp('Flat.umap.X0.select corrected');
    Flat.umap.X0.select=sort(Flat.v.select_clust);
    do_umap=1;
end

if isfield(Flat,'umap') && isfield(Flat.umap,'RMS') && ~strcmp(Flat.umap.RMS,Flat.v.RMS)
    fprintf('Wrong mic channel! Current umap field was created with %s\n',Flat.umap.RMS);
    disp('Press enter to continue (or ctr+c to abort)');
end
remove_blobs=1;
if do_umap==0
    
    if ask==1
        disp('Reuse umap data (yes=1, default)? ');
        reuse=input('');
        if isempty(reuse)
            reuse=1;
        end
    else
        reuse=1;
    end
    
    if reuse==1
        if ~isempty(setdiff(Flat.v.select_clust,Flat.umap.X0.select))
            disp('Some elements changed position, I hope this will work out');
            Flat.umap.X0.select=sort(Flat.v.select_clust);
        end
        
        do_umap=0;
        
        if ask==1
            disp('Remove clustered blobs (no=0, default)? ');
            remove_blobs=input('');
        else
            remove_blobs=1;
        end
        
        Flat.umap.X.map=Flat.X.umap(:,Flat.umap.X.select);
    else
        do_umap=1;
        remove_blobs=1;
    end
    
elseif Flat.v.all_clust_mode==0 && Flat.v.curr_clust==1
    Flat.umap.X.select=sort(Flat.v.select_clust);
    [lia,locb]=ismember(Flat.umap.X.select,Flat.umap.X0.select);
    Flat.umap.X.map=Flat.umap.X0.map(locb,:);
    do_umap=0;
end

if remove_blobs
    Flat.umap.X.select=Flat.umap.X0.select;
    if isfield(Flat.X, 'umap')
        Flat.umap.X.map=Flat.X.umap(:,Flat.umap.X.select);
    end
    if isfield(Flat.umap,'ContClickHist')
        Flat.umap=rmfield(Flat.umap,'ContClickHist');
    end
    Flat.umap.fcH.init=1;
end

%% RUN UMAP

[Flat, Flat.v.select, Flat.v.select_clust]=func_select_mode(Flat); % added by CL for Lisa's Data
select=sort(Flat.v.select_clust);
if do_umap
    fprintf('Cluster \n1: Flat.X.PCcoeffs (PCA, default) or \n2: Flat.X.DistCoeffs (expert, 2) \n');
    if ask
        y=input('? ');
    else
        y=1;
    end
    
    if y==1
        X=Flat.X.PCcoeffs';
    elseif y==2
        X=Flat.X.Distcoeffs';
    end
    
    %% define map and select
    
    X=X(select,:);
    pca_use=min(size(X,2),Flat.p.umap.pcaDims);
    fprintf('Now performing dimensionality reduction using %d Princ. components... \n',pca_use);
    
    %%
    a=ver;
    if datenum(a(1).Date)>datenum('01-Jan-2018')
        tic
        if length(select)>Flat.p.umap.Umap_train_max_size%max_num_umap
            paths=evalin('base','paths');
            nn=length(select);          r=randperm(nn);
            l=1:length(select);  [dummy,inv_inds]=sort(r(l));
            
            X1=X(r(1:Flat.p.umap.Umap_train_max_size),:);
            pnames=cell(1,size(X,2));
            for i=1:size(X,2)
                pnames{i}=['p' sprintf('%.4d',i)];
            end
            [map1,model1]=run_umap(double(X1),'parameter_names',pnames,'save_template_file',[paths.bird_path 'UmapModel.mat']);
            %        save([paths.bird_path 'UmapModel'],'model1']);
            fprintf('model1 saved\n');
            X2=X(r(Flat.p.umap.Umap_train_max_size+1:end),:);
            
            [map2,model]=run_umap(double(X2),'parameter_names',pnames,'template_file',[paths.bird_path 'UmapModel.mat']);
            map=[map1 ; map2];
            map=map(inv_inds,:);
        else
            [map,model]=run_umap(double(X));
        end
        toc
    else
        disp('install newer matlab');
        return
    end
    if ~isfield(Flat.X,'umap')
        Flat.X.umap=zeros(2,Flat.num_Tags);
    end
    
    % write some important stuff
    Flat.X.umap(:,select)=map'; % the master map
    Flat.umap.RMS=Flat.v.RMS; % the name
    
    %    Flat.umap.X0.map=map;
    Flat.umap.X.map=map'; % the current work map
    Flat.umap.X.select=select;
    
    Maprange=[min(map); max(map)];
    %   Flat.v.umap.nExamples=size(X,2);
    Flat.umap.Maprange=Maprange;
    
end


%% finishing up
Flat.umap.X0.select=select; % the indices of the master map
if ~strcmp(Flat.v.RMS(1),'#')
    Flat.X.clust_ID(Flat.umap.X0.select)=1;
end
Flat.p.umap.time=1; % time slice to onset

if isfield(Flat.umap,'ContClick')
    Flat.umap=rmfield(Flat.umap,'ContClick');
end
Flat.umap.ContClickCount=0;

Flat.umap.XC.selectScatterCurrBlob=[];
if ~isfield(Flat.umap.fcH,'ScatterClusteredBlobs')
    Flat.umap.fcH.ScatterClusteredBlobs=[]; % remove previous cXblustering
end
Flat.umap.fcH.ScatterCurrBlob=[];

if Flat.umap.fcH.init==1
    [fcH,Flat]=func_help_umap_init(fcH,Flat);
end

Flat.v.umap.evnt_modifier='';

[fcH,Flat]=func_help_umap_map_to_I(fcH,Flat);
[fcH,Flat]=func_umap_select(fcH,Flat,Flat.umap.X);

%% percent unique points
L=length(Flat.umap.X.map_index); LU=length(unique(Flat.umap.X.map_index));
Flat.umap.UniquePointsPercent=round(100*LU/L);
fprintf('%d percent unique points in %dx%d image \n',Flat.umap.UniquePointsPercent,Flat.p.umap.NumPixels,Flat.p.umap.NumPixels);
figure(32);

end

function Flat=func_some_initializations(Flat)

if ~isfield(Flat.p,'umap') || ~isfield(Flat.p.umap,'Umap_train_max_size')
    Flat.p.umap.Umap_train_max_size=2e5; 
end

if ~isfield(Flat.p.umap,'ParentMaxSize')
    Flat.p.umap.ParentMaxSize=1e6; % for running umap
end

if ~isfield(Flat.p.umap,'MaxClust')
    Flat.p.umap.MaxClust=30;
end

if ~isfield(Flat.p.umap,'pcaDims')
    Flat.p.umap.pcaDims=1000;
end

if ~isfield(Flat.p.umap,'NumPixels') || Flat.p.umap.NumPixels==0
    Flat.p.umap.NumPixels=min(2000,ceil(1200+0.5*sqrt(length(Flat.v.select_clust))));
end


if ~isfield(Flat.p.umap,'RadiusKernel')
    Flat.p.umap.RadiusKernel=8; % radius of smoothing circle (for cluster formation)
    Flat.p.umap.new_kernel=1;
end

if ~isfield(Flat.p.umap,'theta')
    Flat.p.umap.theta=0.05;
end
try
    g=gpuDevice;
catch
    g=[];
end
if ~isempty(g)
    am=[g(:).AvailableMemory]; [~,picki]=max(am);
    Flat.p.umap.GPUdevice=picki;
else
    Flat.p.umap.GPUdevice=0; % for convolution
end
Flat.p.umap.GPUdevice2=0; % for ismember
end

function passed=func_umap_some_tests(Flat)
passed=1;
if ~(Flat.v.tsne.clust_dense_mode==2) % everything mode
    disp('only works in clust_dense mode! aborting');
    passed=0; return
end

if isfield(Flat.v,'umap') && isfield(Flat.v.umap,'ranPC') && Flat.v.umap.ranPC==0
    disp('First run PCA! aborting. (or set Flat.v.umap.ranPC to 1)');
    passed=0; return
end

if isfield(Flat.v,'chanell_mic_consistent') &&  ~Flat.v.chanell_mic_consistent
    disp('Channel selection inconsistent - aborting');
    passed=0; return
end

end