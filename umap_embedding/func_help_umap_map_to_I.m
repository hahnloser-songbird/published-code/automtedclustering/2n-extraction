function [fcH,Flat]=func_help_umap_map_to_I(fcH,Flat) % ,map)
% computes Flat.umap.X and Flat.umap.XT and Flat.umap.IT

map=Flat.umap.X.map';

n=Flat.p.umap.NumPixels;
Maprange=Flat.umap.Maprange;
r=Flat.p.umap.RadiusKernel;


t=Flat.p.umap.time;
if t>0
    %    h=Flat.X.umap_time(1,Flat.umap.X.select)==Flat.p.umap.time; % time slice of original data, onset
    h=ismember(Flat.X.umap_time(1,Flat.umap.X.select),[t t+1]); % time slice of original data, onset
elseif t<0
    %   h=Flat.X.umap_time(2,Flat.umap.X.select)==Flat.p.umap.time; % time slice of original data, offset
    h=ismember(Flat.X.umap_time(2,Flat.umap.X.select),[t t-1]); % time slice of original data, offset
else
    h=[];
end
select_i=find(h);
Flat.umap.XT.select=select_i; % is an index into Flat.umap.X.select!
Flat.umap.XT.map=map(Flat.umap.XT.select,:); % map of time slice T
[Flat.umap.XT.map_coordT,Flat.umap.XT.map_indexT]=func_help_umap_map_coord(Flat.umap.XT.map,Maprange,n);
IMul=zeros(n); %IT(Flat.umap.XT.map_indexT)=1;
IMul(Flat.umap.XT.map_indexT)=Flat.umap.X.multiplicity(Flat.umap.XT.select);

if Flat.p.umap.GPUdevice
    Flat.umap.IMul=gpuArray(IMul);
else
    Flat.umap.IMul=IMul;
end

if ~isfield(fcH,'hf_umap') | ~ishandle(fcH.hf_umap)
    fcH.hf_umap=figure(32); clf;
    set(fcH.hf_umap,'Name','UMAP','color','k');
end
if ~isfield(fcH,'hf_umap_ax') | ~ishandle(fcH.hf_umap_ax)
    fcH.hf_umap_ax=axes;
    hold on;
end
set(fcH.hf_umap_ax,'position',[0 0 1 1],'Visible','off','box','off');

set(fcH.hf_umap,'KeyPressFcn', {@evalkey_umap, },'BusyAction','cancel',...
    'WindowScrollWheelFcn',{@func_mousewheel,}, ...
    'WindowButtonDownFcn',{@func_mousepress, }, ...
    'keyReleaseFcn','func_umap_keyrelease;');
if ~isfield(fcH,'hf_umapIT') | ~ishandle(fcH.hf_umapIT)
    fcH.hf_umapIT=imagesc(zeros(n)); colormap(jet);
    axis tight
end

if ~isfield(fcH,'hf_umapTit') | ~ishandle(fcH.hf_umapTit)
     fcH.hf_umapTit=text(20,20,['radius ' num2str(r)],'color','w');
end


fcH=func_umap_help_Scatterplot(Flat,fcH);

if ~isfield(fcH,'hf_umapScatterCurrBlob') | ~ishandle(fcH.hf_umapScatterCurrBlob)
    fcH.hf_umapScatterCurrBlob=plot(n/2,n/2,'c.','linewidth',1,'markersize',1,'visible','off');
end
if ~isfield(fcH,'hf_umapScatterClusteredBlobs') | ~ishandle(fcH.hf_umapScatterClusteredBlobs)
    fcH.hf_umapScatterClusteredBlobs=plot(n/2,n/2,'c.','linewidth',1,'markersize',1,'visible','off','color','k');
end
if Flat.umap.fcH.init
    set(fcH.hf_umapScatterCurrBlob,'XData',[],'YData',[],'visible','off');
end

FfcH=Flat.umap.fcH;

if isfield(FfcH,'ScatterClusteredBlobs') && ~isempty(FfcH.ScatterClusteredBlobs)
    set(fcH.hf_umapScatterClusteredBlobs,'XData',FfcH.ScatterClusteredBlobs(1,:),'YData',FfcH.ScatterClusteredBlobs(2,:),'visible','on','color','k');
else
    set(fcH.hf_umapScatterClusteredBlobs,'XData',[],'YData',[],'visible','off');
end


end