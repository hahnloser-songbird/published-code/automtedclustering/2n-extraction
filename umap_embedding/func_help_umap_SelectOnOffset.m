function [Selectonoffset,remove_elements,selectonoffset,onoffset]=func_help_umap_SelectOnOffset(Flat,sel,timeslice)

if ~issorted(sel)
    disp('not sorted');
    keyboard
end
Sel=Flat.umap.X.select(sel); % the same as Flat.umap.XC.Select
if isempty(Sel)
    Selectonoffset=[];
    selectonoffset=[];
    remove_elements=[];
    return
end

% direction of search
% -1 if starting from offset
% 1 if starting from onset
direction=-1+2*(timeslice>0); 

% forward == 1, if starting from onset
% backward == 2, if starting from offset
forw_or_backw=1+(timeslice<1);

%% UMAP Compute voted time slice (majority or time slice)
% onset time is determined by majority vote
%timesC=Flat.X.umap_time(forw_or_backw,Sel);
%voted_time=mode(timesC); % majority
voted_time=timeslice;%Flat.p.umap.time;

times=Flat.X.umap_time(forw_or_backw,Flat.umap.X.select);
datinds=Flat.X.DATindex(Flat.umap.X.select);
inds=Flat.X.indices_all(Flat.umap.X.select);

%% UMAP Find onsets and offsets of selected syllables
% Flat.umap.XC.selectonoffset and Flat.umap.XC.Selectonoffset

clip_duration=0; % to avoid double syllable detections (can induce syllable cropping, which is worse)
% positive time slice: onset
if direction==1
    Sel_h=0*Sel;  Sel_h(1)=Sel(1); % just a temporary pointer
    onset_insert=0*sel; % onset_insert(1)=sel(1)+1-last_shift;
    onset_to_offset=0*sel; % distance to offset in buffers
    
    c=0; first_round=1;
    for i=2:length(sel)
        % find first element that could be in a different syllable
        % (more than Flat.p.umap.MinNumBuffSyl away)
        if  ~first_round
            different_file=~(Flat.X.DATindex(Sel(i))==Flat.X.DATindex(Sel_h(c)));
            %far_enough=Flat.X.indices_all(Sel(i))-Flat.X.indices_all(Sel_h(c))>Flat.p.nonoverlap*Flat.p.umap.MinNumBuffSyl;
            % at least MinNumBuffSyl buffers further than previous onset
            onsetS=Flat.X.indices_all(Flat.umap.X.select(onset_insert(c)));
            far_enough=Flat.X.indices_all(Sel(i))+Flat.p.nonoverlap*(voted_time-1)-onsetS>Flat.p.nonoverlap*onset_to_offset(c);%Flat.p.umap.MinNumBuffSyl;
        end
        if first_round || different_file || far_enough
            first_round=0;
            c=c+1; Sel_h(c)=Sel(i);

            last_shift=min(Flat.X.umap_time(1,Sel(i)),voted_time); % only shift onset by voted_time if admissible!
            onset_insert(c)=sel(i)+1-last_shift;
            
            % find end
            j=onset_insert(c)+direction;
            % forward in same direction until next syl
            while j<length(times)+1 && datinds(j)==datinds(j-direction) && inds(j)-inds(j-direction)==Flat.p.nonoverlap 
                j=j+direction;
            end
            % look for end
            j=j-direction;
          
            SylDurS=Flat.X.indices_all(Flat.umap.X.select(j))-Flat.X.indices_all(Flat.umap.X.select(onset_insert(c)));
            onset_to_offset(c)=ceil(SylDurS/Flat.p.nonoverlap);
            if onset_to_offset(c)<0
                keyboard
            end
        end
    end

    onset_insert=onset_insert(1:c);
    onset_to_offset=onset_to_offset(1:c);
    selectonoffset=onset_insert;
    Selectonoffset=Flat.umap.X.select(onset_insert);
    
    if clip_duration
        slices=Flat.X.umap_time((Flat.p.umap.time<0)+1,Selectonoffset);
        in_slice=find(slices==sign(Flat.p.umap.time) | slices==sign(Flat.p.umap.time)+direction);
        
        qq = quantile(onset_to_offset(in_slice),[.25 .50 .75]);
        onset_to_offsetLIM=round(qq(2)+2*max(1,(qq(3)-qq(2)))); % 2 quartiles away from median
        onset_to_offset=min(onset_to_offset,onset_to_offsetLIM);
    end
    onoffset=onset_to_offset;
    
    onset_to_offset=ceil(onset_to_offset/2)+1;
    remove_elements=0*sel;
    c2=0;
    for i=1:c
        remove_elements(c2+1:c2+onset_to_offset(i))=onset_insert(i):(onset_insert(i)+onset_to_offset(i)-1);
        c2=c2+onset_to_offset(i);
    end
    remove_elements=remove_elements(1:c2);
    
   
else % negative time slice: offset
    Sel_h=0*Sel;  Sel_h(end)=Sel(end); % just a temporary pointer
    
    %last_shift=max(Flat.X.umap_time(2,Sel(end)),voted_time);
    offset_insert=0*sel; %offset_insert(end)=sel(end)-1-last_shift;
    offset_to_onset=0*sel; % distance to offset in buffers
    
    c=length(Sel)+1;
    first_round=1;
    
    for i=length(sel)-1:-1:1
        if ~first_round
            different_file=~(Flat.X.DATindex(Sel(i))==Flat.X.DATindex(Sel_h(c)));
            % at least MinNumBuffSyl buffers further than previous offset
            offsetS=Flat.X.indices_all(Flat.umap.X.select(offset_insert(c)));
            far_enough=offsetS-Flat.X.indices_all(Sel(i))+Flat.p.nonoverlap*(1+voted_time)>Flat.p.nonoverlap*offset_to_onset(c);% Flat.p.umap.MinNumBuffSyl;
        end
        
        if  first_round || different_file || far_enough % new syllable
            first_round=0;
            c=c-1; Sel_h(c)=Sel(i);
            last_shift=max(Flat.X.umap_time(2,Sel(i)),voted_time); % only shift offset by voted_time if admissible!
            offset_insert(c)=sel(i)-1-last_shift;
            if offset_insert(c)==0
                keyboard
            end
            % find start
            j=offset_insert(c)+direction;
            while j>0 && datinds(j)==datinds(j-direction) && inds(j-direction)-inds(j)==Flat.p.nonoverlap
                j=j+direction;
            end
            j=j-direction;
            % look for start
            SylDurS=Flat.X.indices_all(Flat.umap.X.select(offset_insert(c)))-Flat.X.indices_all(Flat.umap.X.select(j));
            offset_to_onset(c)=ceil(SylDurS/Flat.p.nonoverlap);
            if offset_to_onset(c)<0
                keyboard
            end
            
        end
    end
    offset_insert=offset_insert(c:end);
    offset_to_onset=offset_to_onset(c:end);
    selectonoffset=offset_insert;
    Selectonoffset=Flat.umap.X.select(offset_insert);
    
    if clip_duration
        slices=Flat.X.umap_time((Flat.p.umap.time<0)+1,Selectonoffset);
        in_slice=find(slices==sign(Flat.p.umap.time) | slices==sign(Flat.p.umap.time)+direction);
        qq = quantile(offset_to_onset(in_slice),[.25 .50 .75]);
        offset_to_onsetLIM=round(qq(2)+2*max(1,(qq(3)-qq(2)))); % 2 quartiles away from median
        offset_to_onset=min(offset_to_onset,offset_to_onsetLIM);
    end
    onoffset=offset_to_onset;
    
    offset_to_onset=ceil(offset_to_onset/2)+1;
    remove_elements=0*sel;
    c=length(offset_insert);
    c2=0;
    for i=1:c
        remove_elements(c2+1:c2+offset_to_onset(i))=(offset_insert(i)-offset_to_onset(i)+1):offset_insert(i);
        c2=c2+offset_to_onset(i);
    end
    remove_elements=remove_elements(1:c2);
    
end
end