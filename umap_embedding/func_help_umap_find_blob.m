function  [Flat,plot_flatclust]=func_help_umap_find_blob(Flat)
plot_flatclust=0;

I=Flat.umap.IT;
h=find(I(Flat.umap.X.map_index(Flat.umap.XC.select)),1,'first');
if ~isempty(h)
    hclust=(I(Flat.umap.X.map_index(Flat.umap.XC.select(h(1)))));
    clust=Flat.umap.NumClust+1-hclust;
    inds=find(func_help_umap_select_from_I(Flat,Flat.umap.PIX(clust),1));
    %  inds=find(func_help_umap_select_from_I(Flat,Flat.umap.PIX(i),1));
    %  h=intersect(Flat.umap.XC.select,inds);
    
    Flat.umap.XC.select=inds;
    Flat.umap.XC.Select=Flat.umap.X.select(inds);
    Flat.X.custom=(length(h)+1)*ones(1,Flat.num_Tags);
    Flat.X.custom(Flat.umap.XC.Select)=1:length(h);
    
    Flat.v.curr_clust=1;
    Flat.v.sequence{Flat.v.plot_ordered,Flat.v.curr_clust}=[];
    Flat.v.plot_clust=1;
    Flat.v.leftmost_element=1;%length(Flat.v.tsne.select)-Flat.v.max_elements_on_screen+1;
    Flat.v.prepare_plot=1;
    Flat.v.run_select_clust=1;
    [Flat Flat.v.select Flat.v.select_clust]=func_select_mode(Flat);
    plot_flatclust=1;
end

end