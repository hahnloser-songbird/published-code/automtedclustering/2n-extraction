function [Flat, PCcoeffs] = func_Flat_PCA(Flat, pcas, pcasm, dontask, h, num_PCs)
% h = selected elements
if nargin < 4
    dontask = 0;
end
if nargin < 6
    num_PCs = 20;
end

if nargin < 3
    pcasm = 0;
end

Flat.v.pca_recompute=0;

if Flat.num_Tags<3
    fprintf('Too few elements in archive: func_Flat_PCA returns\n')
    return
end
if nargin<5
 %   h=1:Flat.num_Tags;
    h=Flat.v.select_clust;
    disp('Recommendation: select RMSfilt and all_clust ');
    %    if false %Flat.num_Tags > Flat.p.max_elementsPC
    %        h=randperm(Flat.num_Tags);
    %        h=h(1:Flat.p.max_elementsPC);
    %    else
    %        h=1:Flat.num_Tags;
    %    end
end
if isempty(h)
    return
end

maxnum=4e5;
if length(h)>maxnum
    fprintf('thinning pca down to %d samples\n',maxnum);
    horig=h;
    h=h(ceil((1:maxnum)/maxnum*length(h)));
else
    horig=[];
end

%p_channel_mic_display=Flat.p.channel_mic_display;
%% new TRAINING archive?
if pcas == 0
    disp('Please wait while we calculate the PCA components ...');
    disp('Or set Flat.v.pca_recompute = 0 ; ');
    
%  if 0 %~dontask && length(Flat.p.channel_mic_display)>1
%         str=mat2str(Flat.p.channel_mic_display);
%         fprintf('Mic Channels: %s (enter = all channels)\n',str);
%         which_chan=input('','s');
%         if ~isempty(which_chan)
%             Flat.p.channel_mic_display=zeros(1,length(which_chan));
%             for j=1:length(which_chan)
%                 Flat.p.channel_mic_display(j)=str2num(which_chan(j));
%             end
%         end
%     end

    h2 = single(get_spec(Flat, h)); 
    if size(h2,2)<num_PCs
        disp('Must have more elements than principal components!!');
        pause
    end
    
    if isfield(Flat.v,'pca_segmented') & Flat.v.pca_segmented==1
        h2=max(h2,single(Flat.v.pca_min));
    end
    
%     try
%     S=Flat.v.import_DAT;
%     h3=zeros(size(h2));
%     h3(S.els,:)=h2(S.els,:);
%     h2=h3; clear h3;
%     end
    Flat.PCmean=mean(h2,2);
    h2=h2-repmat(Flat.PCmean,1,size(h2,2));
    
    
    %     if 0
    %     %% DO THE PCA
    %     %  Flat.PCs = processpca(h2',1-Flat.p.var_explain);
    %     [coeff,score,latent] = princomp(h2','econ');
    %     Flat.PCs=coeff';
    %     Flat.PCmean=mean(h2,2);
    %     cumlat=cumsum(latent);
    %     cumlat=cumlat/cumlat(end);
    %     numPCs=find(ge(cumlat,Flat.p.var_explain));
    %     numPCs=max(numPCs,2); % otherwise we run into problems in the grow figure
    %     Flat.PCs=Flat.PCs(1:numPCs(1),:);
    %     var_explain=Flat.p.var_explain;
    %
    %     else
    
    
    % if a big window is used, we split it up into smaller windows
    
    
    useFastPCAApproximation = true;
    if useFastPCAApproximation
        Flat.v.isSplitPCA = false;
        % only compute first 20 pcas
        if dontask
            y=min(size(h2,2),num_PCs);
        else
            if length(Flat.p.CHANNEL_MIC)>1 && length(Flat.p.channel_mic_display)==1
                fprintf('%s, ',Flat.p.Channel_names{Flat.p.CHANNEL_MIC(Flat.p.channel_mic_display)});
            end
            fprintf('specify the number of PCs: (enter=20 ) \n');
            y=input('');
            if isempty(y)
                y=20;
            end
        end
        
        [u, s, v] = pcaCust(h2, y);
        fprintf('%d principal components used (approximative PCA)\n',y);
        Flat.PCs = u';
        
    else
        
        Flat.splitPCA.maxwndSize = 2688;
        Flat.splitPCA.splitSize = 1024;
        Flat.splitPCA.nSplitPC = 10;
        maxwndSize = Flat.splitPCA.maxwndSize;
        splitSize = Flat.splitPCA.splitSize;
        nSplitPC = Flat.splitPCA.nSplitPC;
        
        if size(h2, 1) > maxwndSize
            Flat.PCs = {};
            Flat.PCmean = {};
            hsplit = func_splitUpSpectrograms(h2, splitSize);
            numPCs=100;
            for k = 1:length(hsplit)-1
                cMat=hsplit{k}*hsplit{k}'/size(hsplit{k},2);
                [E,L] = eigs(cMat, min(size(hsplit{k}, 1), numPCs));
                var_explain=trace(L)/trace(cMat);
                Flat.PCs{k} = E';
                Flat.PCmean{k} = mean(hsplit{k}, 2);
                Flat.v.isSplitPCA = true;
            end
        else
            numPCs=100;
            cMat=h2*h2'/size(h2,2);
            [E,L] = eigs(cMat, numPCs);
            var_explain=trace(L)/trace(cMat);
            %   Flat.PCs = abs(L)^-.5 * E';
            Flat.PCs=E';
            Flat.v.isSplitPCA = false;
        end
    end
    
    if ~Flat.v.isSplitPCA && ~useFastPCAApproximation
        
        fprintf('%d principal components cover %.2f percent of the total variance\n',size(Flat.PCs, 1),100*var_explain);
        if size(Flat.PCs,1) > 20
            if dontask == 0
                disp('That is too many (threshold = 20).');
                y=input('specify the number of PCs (more than 20 usually does not help!): (enter=20 ) ');
                if isempty(y)
                    y=20;
                end
            else % automatic mode - use 12.
                y = 12;
            end
            Flat.PCs=Flat.PCs(1:y,:);
        end
        
        if strcmp(Flat.p.clusterdata_distance,'mahalanobis') && Flat.num_PCs > Flat.p.max_num_mahalanobis
            fprintf('using only first %d principal components for the mahalanobis distance...\n',Flat.p.max_num_mahalanobis);
        end
    end
    %% importing a new day using the PCs from the training archive
else
    Flat.PCs=pcas;
    Flat.PCmean=pcasm;
end
clear h2;

%% CALC 'PCcoeffs'

if isfield(Flat.v,'isSplitPCA') && Flat.v.isSplitPCA
    Flat.num_PCs = nSplitPC * (length(Flat.PCs));
else
    Flat.num_PCs = size(Flat.PCs, 1);
end

if ~all(size(Flat.X.PCcoeffs)==[Flat.num_PCs Flat.num_Tags])
    Flat.X.PCcoeffs=single(zeros(Flat.num_PCs,Flat.num_Tags));
end
if ~isempty(horig)
    h=horig;
end
block_size = 3000;
if length(h)>block_size % read in block-wise, solves out-of-memory problems
    for i=1:block_size:length(h)
        l=h(i:(min(length(h),i+block_size)));
        if isfield(Flat.v,'isSplitPCA') && Flat.v.isSplitPCA
            spec_data = get_spec(Flat, l);
            splitSpecs = func_splitUpSpectrograms(spec_data, splitSize);
            for k = 1:length(splitSpecs)-1
                Flat.X.PCcoeffs(nSplitPC*(k-1)+1:nSplitPC*k ,l) = Flat.PCs{k} * (splitSpecs{k} - repmat(Flat.PCmean{k}, 1, size(splitSpecs{k},2)));
            end
        else
            spec_data = get_spec(Flat, l);
            Flat.X.PCcoeffs(:,l) = Flat.PCs * (spec_data-repmat(Flat.PCmean,1,size(spec_data,2)));
        end
    end
else
    if isfield(Flat.v,'isSplitPCA') && Flat.v.isSplitPCA
        spec_data = get_spec(Flat, h);
        splitSpecs = func_splitUpSpectrograms(spec_data, splitSize);
        for k = 1:length(splitSpecs)-1
            Flat.X.PCcoeffs(nSplitPC*(k-1)+1:nSplitPC*k ,h) = Flat.PCs{k}(1:nSplitPC, :) * (splitSpecs{k} - repmat(Flat.PCmean{k}, 1, size(splitSpecs{k},2)));
        end
    else
        spec_data = get_spec(Flat, h);
        Flat.X.PCcoeffs(:,h) = Flat.PCs * (spec_data-repmat(Flat.PCmean,1,size(spec_data,2))); % coefficients (compressed data)
    end
end
Flat.X.PCcoeffs=single(Flat.X.PCcoeffs);
PCcoeffs = Flat.X.PCcoeffs(:,h);

% if Flat.num_Tags>10000 % read in block-wise, solves out-of-memory problems
%     Flat.X.PCcoeffs=zeros(Flat.num_PCs,Flat.num_Tags);
%     for i=1:1000:Flat.num_Tags
%         l=i:min(Flat.num_Tags,i+1000);
%
%         if isfield(Flat.v,'isSplitPCA') && Flat.v.isSplitPCA
%             spec_data = get_spec(Flat, l);
%             splitSpecs = func_splitUpSpectrograms(spec_data, splitSize);
%             for k = 1:length(splitSpecs)-1
%                 Flat.X.PCcoeffs(nSplitPC*(k-1)+1:nSplitPC*k ,l) = Flat.PCs{k} * (splitSpecs{k} - repmat(Flat.PCmean{k}, 1, size(splitSpecs{k},2)));
%             end
%         else
%             spec_data = get_spec(Flat, l);
%             Flat.X.PCcoeffs(:,l) = Flat.PCs * (spec_data-repmat(Flat.PCmean,1,size(spec_data,2)));
%         end
%     end
% else
%     if isfield(Flat.v,'isSplitPCA') && Flat.v.isSplitPCA
%         spec_data = get_spec(Flat, 1:Flat.num_Tags);
%         splitSpecs = func_splitUpSpectrograms(spec_data, splitSize);
%         for k = 1:length(splitSpecs)-1
%             Flat.X.PCcoeffs(nSplitPC*(k-1)+1:nSplitPC*k ,:) = Flat.PCs{k}(1:nSplitPC, :) * (splitSpecs{k} - repmat(Flat.PCmean{k}, 1, size(splitSpecs{k},2)));
%         end
%     else
%         spec_data = get_spec(Flat, 1:Flat.num_Tags);
%         Flat.X.PCcoeffs = Flat.PCs * (spec_data-repmat(Flat.PCmean,1,size(spec_data,2))); % coefficients (compressed data)
%     end
% end
%%
%Flat.p.channel_mic_display=p_channel_mic_display;
if isfield(Flat.v,'umap') && isfield(Flat.v.umap,'ranPC') && Flat.v.umap.ranPC==0
    Flat.v.umap.ranPC=1;
end

if length(Flat.p.channel_mic_display)==1% length(Flat.p.CHANNEL_MIC)>1
   % Flat.v.PCcoeffs{Flat.p.channel_mic_display}=Flat.X.PCcoeffs;
  Flat.V.(['PCcoeffs' num2str(Flat.p.channel_mic_display)])=Flat.X.PCcoeffs;
    if ~isempty(Flat.p.syllable)
        Flat.p.syllable.X(Flat.p.channel_mic_display).PCmean=Flat.PCmean;
        Flat.p.syllable.X(Flat.p.channel_mic_display).PCs=Flat.PCs;
    end
end
 
end

%% HELPERFUNCTIONS


function spec_data = get_spec(Flat, IDs)

%spec_data = func_extract_data_fullspec(Flat, 5, IDs); %Alexei enables 08.07.2013
spec_data = func_extract_data(Flat, 5, IDs,1,1,1); %Alexei disabled 08.07.2013
%spec_data(:,find(noisemask))=-45;
%spec_data=spec_data';
%
% cMat=spec_data*spec_data';
% [E,L] = eigs(cMat, 24);
% Ws = abs(L)^-.5 * E';

% %% monkey people keep the original data
% if ~isfield(Flat, 'not_monkey')
%     return;
% end
%
% %% non monkeys
% spec_data_mean = mean(spec_data, 2);
%
% % subtract the mean value from each pixel
% %spec_data = spec_data - (spec_data_mean * ones(1, size(spec_data, 2)));
%
% % this works the best for me; (Kotowicz)
% spec_data = spec_data - mean(spec_data_mean);

end

%% EOF