function fcH=func_redraw_markings(Flat,fcH,numbuffs)

%fcH=evalin('base','fcH');
%% white lines in Fig 20
if Flat.v.plot_clust_white_lines
    
    updateXData(fcH, 'hf_clust_wl', numbuffs);
    updateXData(fcH, 'hf_grow_wl', numbuffs);
    
    updatePos(fcH, 'hf_grow_text', numbuffs);
    updatePos(fcH, 'hf_clust_text', numbuffs);
    
    % check if figure is open.
    if isfield(fcH, 'hf_Templ') && ishandle(fcH.hf_Templ)
        for i=1:length(fcH.hf_templ_wl)
            set(fcH.hf_templ_wl{i},'XData',.5+Flat.v.numbuffs*i*[1 1]);
            set(fcH.hf_templ_text{i},'position',[Flat.v.numbuffs*(i-.5) ceil(length(Flat.p.nfft_i)/32) ]);
            set(fcH.hf_templ_text_top1{i},'position',[Flat.v.numbuffs*(i-.9) ceil(33*length(Flat.p.nfft_i)/32) ]);
            set(fcH.hf_templ_text_top2{i},'position',[Flat.v.numbuffs*(i-.9) ceil(30*length(Flat.p.nfft_i)/32) ]);
        end
    end
end

%% bottom text in Fig 20

%% size cluster figure
% set(fcH.ha_clust,'YLim',...
%     [-3.5 length(Flat.p.nfft_i)*length(Flat.p.channel_mic_display)-.5]);


%% top text in Fig 20
% for i=1:length(fcH.hf_spec_text)
%     hpos=get(fcH.hf_spec_text{i},'position');
%     set(fcH.hf_spec_text{i},'position',[(i-.8)*numbuffs hpos(2:3)]);
% end

end


function updateXData(fcH, field, numbuffs)

if isfield(fcH, field)
    
    for i=1:length(fcH.(field))
            set(fcH.(field){i},'XData',.5+numbuffs*i*[1 1]);
    end
end
end


function updatePos(fcH, field, numbuffs)

if isfield(fcH, field)
    for i=1:length(fcH.(field))
        hpos=get(fcH.(field){i},'position');
        set(fcH.(field){i},'position',[numbuffs*(i-.5) hpos(2:3)]);
    end
end
end