function func_show_help(path)
    % Extracts and displays help content from m-file
    %   path: Path of the m-file to parse for help content
    %
    % Note: This version is based on java classes. The path to those classes
    % must be set.
    
    %% Initialization:
    
    % Path of the java classes
    currentPath = mfilename('fullpath');
    dd=filesep;
    slashLocations = strfind(currentPath,dd);
    directoryPath = currentPath(1:slashLocations(end));
    helpExtractorPath = [directoryPath 'HelpExtractor'];
    javaclasspath(helpExtractorPath);
    % javaclasspath('/media/Storage/SongBird_Repository/songbird-work/FlatClust/evalkey1/HelpExtractor');
    
    % For quick testing
    % if (nargin < 1)
    % path = '/Volumes/Data HD/Work/Programming/Java/FlatClust/data/evalkey_clust.m';
    % end
    
    
    %% Extracting Help Content:
    
    % Create a Java HelpExtractor Object
    helpExtractor = HelpExtractor();
    % Use the object to extract help content and return it in a java ArrayList
    helpLines = helpExtractor.extractHelp(path);
    % Note: Each help entry is stored as a java HelpEntry object
    
    %% Style of the Help Figure
    
    y_extent=18;
    titleOffset = 10;
    rowSpace = 8;
    colSpace = 20;
    
    %% Size and Position of the Help Figure
    screenCoveredX = 0.95;      % What fraction of the active screen should the figure cover
    screenCoveredY = 0.8;      %
    
    screenDims = get(0,'ScreenSize');
    screenWidth = screenDims(3);
    screenHeight =screenDims(4);
    figDims = [screenWidth*(1-screenCoveredX)/2, screenHeight*(1-screenCoveredY)/2, ...
        screenWidth*screenCoveredX, screenHeight*screenCoveredY];
    rowWidth = figDims(3)/2-colSpace;
    
    
    %% Old Size and Position of the Help Figure
    % screenDims = get(0,'ScreenSize');
    % figDims = [0 0 1000,600];
    % figDims=screenDims;
    % figDims(4) = round(0.8 * screenDims(4));
    % figDims(1) = round((screenDims(3) - figDims(3))/2);
    % figDims(2) = round((screenDims(4) - figDims(4))/2);
    % rowWidth = screenDims(3)/2-colSpace;
    
    
    keyDescriptionSeparation = 0;%0.8;
    numCols = floor(figDims(3)/rowWidth);
    %round(helpLines.size()/numCols)
    
    posIndex = 1; rowPos = colSpace;
    
    %% Create New or Clear Existing Figure
    %f = figure(1);
    f = figure;
    clf;
    set(f, 'Name','Help | Move the mouse over a help line to see a more detailed description.',...
        'Position', figDims, 'Resize','off', 'KeyPressFcn',@evalKey, 'Color', [1, 1, 1]);
    % Create New Every Time
%     %f = figure('Visible','off','Name','Help | Move the mouse over a help line to see a more detailed description.','Position', figDims,'Resize','off', 'KeyPressFcn',@evalKey);
%     
    % Create panel
    panel = uipanel('Parent', f,'BackgroundColor',[1, 1, 1],'BorderType','none');
    % old background color: [0.8, 0.8, 0.8]
    
    % Add a label for each help entry
    for i = 1 : helpLines.size() - 1
        if (posIndex == 1)
            %         % Create a text label for the table header
            %         uicontrol('Parent', panel, 'Style','text', 'Position',[rowPos,figDims(4)-titleOffset,rowWidth,15],...
            %             'String',['Key:' getSpacer('Key::') 'Brief description.'], 'HorizontalAlignment','left',...
            %             'BackgroundColor',[0.8 0.8 0.8],'FontSize',10,'FontWeight','bold','FontName', 'Monospaced');
        end
        if ~strcmp(char(helpLines.get(i-1).getBriefDescription()),'No Description Given')
            % The (i-1) argument of the get function is because Java is zero indexed
            yPosKey = figDims(4)-posIndex*rowSpace-titleOffset;
            if yPosKey<0 %% RRR rich fix
                posIndex = 1;
                rowPos = rowPos + rowWidth + colSpace;
                yPosKey = figDims(4)-posIndex*rowSpace-titleOffset;
            end
            
            if strcmp(helpLines.get(i-1).getKey(), '') || strcmp(helpLines.get(i-1).getBriefDescription(), '')
                colonStr = '';
            else
                colonStr = ':';
            end
            
            %% New Text Style
            keys(i,1) = uicontrol('Parent', panel, 'Style','text', 'Position',[rowPos,yPosKey,rowWidth,y_extent],...
                'String', [char(helpLines.get(i-1).getKey()) colonStr],...
                'TooltipString',paragraphize(char(helpLines.get(i-1).getDetailedDescription())),...
                'HorizontalAlignment','left', 'BackgroundColor',[1, 1, 1],...
                'FontName', 'Sans','FontSize', 10, 'ForegroundColor', [0.1 0 0.6], 'fontweight', 'bold');
            %'ForegroundColor',[0.8 0 0]);
            
            yPosDescription = figDims(4)-(posIndex+keyDescriptionSeparation)*rowSpace-titleOffset;
            %         keys(i,2) = uicontrol('Parent', panel, 'Style','text', 'Position',[rowPos,yPosDescription,rowWidth,15],...
            %             'String', [' ' char(helpLines.get(i-1).getBriefDescription())],...
            %             'TooltipString',paragraphize(char(helpLines.get(i-1).getDetailedDescription())),...
            %             'HorizontalAlignment','left','BackgroundColor',[0.8 0.8 0.85], 'FontName', 'Monospaced','FontSize',9);
            %
            keys(i,2) = uicontrol('Parent', panel, 'Style','text', 'Position',[rowPos,yPosDescription,rowWidth,y_extent],...
                'String', [' ' char(helpLines.get(i-1).getBriefDescription())],...
                'TooltipString',paragraphize(char(helpLines.get(i-1).getDetailedDescription())),...
                'HorizontalAlignment','left', 'BackgroundColor', [1, 1, 1],...
                'FontName', 'Sans','FontSize',9);
            
            %% Old Style
            %         keys(i,1) = uicontrol('Parent', panel, 'Style','text', 'Position',[rowPos,yPosKey,rowWidth,y_extent],...
            %             'String', [char(helpLines.get(i-1).getKey()) ':'],...
            %             'TooltipString',paragraphize(char(helpLines.get(i-1).getDetailedDescription())),...
            %             'HorizontalAlignment','left','BackgroundColor',[0.8 0.8 0.85], 'FontName', 'Monospaced','FontSize',9, 'ForegroundColor',[0.8 0 0]);
            %
            %         yPosDescription = figDims(4)-(posIndex+keyDescriptionSeparation)*rowSpace-titleOffset;
            % %         keys(i,2) = uicontrol('Parent', panel, 'Style','text', 'Position',[rowPos,yPosDescription,rowWidth,15],...
            % %             'String', [' ' char(helpLines.get(i-1).getBriefDescription())],...
            % %             'TooltipString',paragraphize(char(helpLines.get(i-1).getDetailedDescription())),...
            % %             'HorizontalAlignment','left','BackgroundColor',[0.8 0.8 0.85], 'FontName', 'Monospaced','FontSize',9);
            % %
            %        keys(i,2) = uicontrol('Parent', panel, 'Style','text', 'Position',[rowPos,yPosDescription,rowWidth,y_extent],...
            %             'String', [' ' char(helpLines.get(i-1).getBriefDescription())],...
            %             'TooltipString',paragraphize(char(helpLines.get(i-1).getDetailedDescription())),...
            %             'HorizontalAlignment','left','BackgroundColor',[0.8 0.8 0.85], 'FontName', 'Monospaced','FontSize',9);
            
            %
            extent=get(keys(i,1),'Extent');
            set(keys(i,2),'Position',[rowPos+extent(3),yPosDescription,rowWidth,y_extent]);
            posIndex = posIndex + 2;
            
        end
%         if (mod(i,round(helpLines.size()/numCols)) == 0) %% RRR change      rich
%             posIndex = 1;
%             rowPos = rowPos + rowWidth + colSpace;
%         end
    end
    
    % The initial position of the panel
    panelPos = get(panel,'Position');
    
    % Create slider. Current position = slider position * original panel position
    sliderPos = [figDims(3)-20,0,20,figDims(4)];
    hSlider = uicontrol('Style','slider','Position',sliderPos,'Callback',{@sliderAction,panel,panelPos});
    set(hSlider,'Value',1);
    
    % Align components
    %align(keys,'Center','None');
    
    % Make GUI visible
    set(f,'Visible','on');
    
    
end

function spacer = getSpacer(key)
    spacer = '               ';
    spacer = spacer(1:numel(spacer)-numel(char(key)));
end

function sliderAction(hObject, eventData, panel, pos)
    
    % Get the current value of the slider position
    sliderPosition = get(hObject,'Value');
    % Move the panel according to: New position = slider position * original panel position
    set(panel,'position',[pos(1),pos(2)-sliderPosition+1,pos(3),pos(4)]);
    
end

function evalKey(src,evnt)
    
    if strcmp(evnt.Key,'h')
        close(gcf)
    end
    
end

function paragraph = paragraphize(line)
    
    spacePositions = strfind(line, ' ');
    lineWidth = 50;
    
    hop = spacePositions(spacePositions > lineWidth);
    if (~isempty(hop))
        hop = hop(1);
    end
    
    lastPos = 1;
    currentPos = hop;
    
    paragraph = '<html>';
    
    while (currentPos < numel(line))
        
        paragraph = [paragraph line(lastPos:currentPos) '<br>'];
        
        lastPos = currentPos;
        
        hop = spacePositions(spacePositions > currentPos+lineWidth);
        if (~isempty(hop))
            hop = hop(1);
        end
        currentPos = hop;
        
    end
    
    paragraph = [paragraph line(lastPos:numel(line)) '</html>'];
    
end
