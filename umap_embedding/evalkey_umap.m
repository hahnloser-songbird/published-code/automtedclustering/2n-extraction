function evalkey_umap(src, evnt)
modifier = evnt.Modifier;
%% read in from workspace
Flat = evalin('base', 'Flat');
% cfg = evalin('base', 'cfg');
fcH=evalin('base','fcH');
%map=evalin('base','map');

plot_flatclust=0;
dR=.5;

% move back syllalbes from last cluster
in_last=find(Flat.X.clust_ID(Flat.umap.X.select)==Flat.p.num_clust);
Flat.X.clust_ID(Flat.umap.X.select(in_last))=1;

%% switch keypress
switch evnt.Key
    
    %$ Move blob to cluster ID. $%
    %* Quick way to write a blob to a cluster. *%
    case {'1','2','3','4','5','6','7','8','9'} %group it %^ 0 - 9 ^%
        if ~strcmp(Flat.v.RMS(1),'#')
            disp('Only during post-processing - aborting');
            return
        end
        
        if isempty(modifier)
            target_clust=uint8(str2double(evnt.Key));
            
            %$ Move blob to cluster ID+10. $%
            %* Quick way to write a blob to a clusters 10-19. *%
        elseif strcmp(modifier,'shift')
            y=input('Move to cluster number: ');
            target_clust=uint8(y);
        end
        Flat.X.clust_ID(Flat.v.select_clust)=target_clust;
        Flat.v.run_select=0;
        
            if isfield(Flat.X,'clust_ID0')
                Flat.X.clust_ID0(Flat.v.select_clust)=target_clust;
                for i=1:length(Flat.v.select_clust)
                    Flat.Z.stencil(Flat.X.custom(1,Flat.v.select_clust(i)):Flat.X.custom(2,Flat.v.select_clust(i)))=target_clust;
                end
                Flat.Tags(4,Flat.v.select_clust)={['C' num2str(target_clust)]};
                s=find(ismember(Flat.v.select,Flat.v.select_clust));
                Flat.umap.X.color(s)=int8(target_clust);
            end
            
            fprintf('moved %d elements to cluster %d\n',length(Flat.v.select_clust),target_clust);
            
    %$ Increase clustering threshold (factor 1.1).  $%
    %* Changes the cluster selection threshold. *%
    case 'hyphen'
        if isempty(modifier)
            Flat.p.umap.theta=Flat.p.umap.theta/1.2;
            [fcH,Flat]=func_umap_select(fcH,Flat,Flat.umap.X);
            
            %$ Decrease clustering threshold (factor 1.2). Do not plot $%
            %* This function increses processing speed. *%
        elseif strcmp(modifier,'shift')
            Flat.p.umap.theta=Flat.p.umap.theta/1.2;
            
            %$ Decrease clustering threshold (factor 1.02).  $%
            %* Changes the cluster selection threshold. *%
        elseif strcmp(modifier,'control')
            Flat.p.umap.theta=Flat.p.umap.theta/1.02;
            [fcH,Flat]=func_umap_select(fcH,Flat,Flat.umap.X);
        end
        
        %$ Decrease clustering threshold (factor 1.1).  $%
        %* Changes the cluster selection threshold. *%
    case 'equal'
        if isempty(modifier)
            Flat.p.umap.theta=Flat.p.umap.theta*1.2;
            [fcH,Flat]=func_umap_select(fcH,Flat,Flat.umap.X);
            
            %$ Increases clustering threshold (factor 1.2). Do not plot. $%
            %* This function increses processing speed. *%
        elseif strcmp(modifier,'shift')
            Flat.p.umap.theta=Flat.p.umap.theta*1.2;
            
            %$ Decrease clustering threshold (factor 1.01).  $%
            %* Changes the cluster selection threshold. *%
        elseif strcmp(modifier,'control')
            Flat.p.umap.theta=Flat.p.umap.theta*1.02;
            [fcH,Flat]=func_umap_select(fcH,Flat,Flat.umap.X);
        end
        
        
        %$ Decrease radius of convolution kernel.  $%
        %* *%
    case 'downarrow'
        if isempty(modifier)
            Flat.p.umap.RadiusKernel=max(1,Flat.p.umap.RadiusKernel-dR);
            Flat.v.tsne.did_fill=0;
            Flat.p.umap.new_kernel=1;
            [fcH,Flat]=func_umap_select(fcH,Flat,Flat.umap.X);
            
            
            %$ Increase radius of convolution kernel. Do not plot. $%
            %* This function speeds up processing. *%
        elseif strcmp(modifier,'shift')
            Flat.p.umap.RadiusKernel=Flat.p.umap.RadiusKernel-dR;
            
            
            %$ Decrease NumClust.  $%
            %* *%
        elseif strcmp(modifier,'control')
            Flat.p.umap.MaxClust=max(2,Flat.p.umap.MaxClust-1);
            [fcH,Flat]=func_umap_select(fcH,Flat,Flat.umap.X);
        end
        
        %         if isfield(Flat.v.tsne,'map_reduced') && Flat.v.tsne.map_reduced
        %             Flat.v.tsne.map=Flat.v.tsne.map0;
        %             Flat.v.tsne.select=Flat.v.tsne.select0;
        %             fcH=func_help_tsne_map_to_I(fcH,Flat);
        %         end
        % fcH=func_tsne_select(fcH,Flat.v.tsne.map);
        
        %$ Increase radius of convolution kernel.  $%
        %* *%
    case 'uparrow'
        if isempty(modifier)
            Flat.p.umap.RadiusKernel=Flat.p.umap.RadiusKernel+dR;
            Flat.v.tsne.did_fill=0;
            Flat.p.umap.new_kernel=1;
            [fcH,Flat]=func_umap_select(fcH,Flat,Flat.umap.X);
            
            
            %$ Increase radius of convoluation kernel. Do not plot. $%
            %* This function speeds up processing. *%
        elseif strcmp(modifier,'shift')
            Flat.p.umap.RadiusKernel=Flat.p.umap.RadiusKernel+dR;
            
            
            %$ Increase NumClust.  $%
            %* *%
        elseif strcmp(modifier,'control')
            Flat.p.umap.MaxClust=Flat.p.umap.MaxClust+1;
            [fcH,Flat]=func_umap_select(fcH,Flat,Flat.umap.X);
        end
        
        %          if isfield(Flat.v.tsne,'map_reduced') && Flat.v.tsne.map_reduced
        %             Flat.v.tsne.map=Flat.v.tsne.map0;
        %             Flat.v.tsne.select=Flat.v.tsne.select0;
        %             fcH=func_help_tsne_map_to_I(fcH,Flat);
        %          end
        % fcH=func_tsne_select(fcH,Flat.v.tsne.map);
        
        %         %$ Compute boundary elements used for clustering.  $%
        %         %* Set radius (up&down arrow keys) and number of TSNE clusters first.*%
        %     case 'b'
        %         if isempty(modifier)
        %             if Flat.v.tsne.did_fill==0
        %                 fcH=func_tsne_select(fcH,Flat.v.tsne.map,1);
        %                 Flat.v.tsne.did_fill=1;
        %             end
        %             disp('computing boundary...');
        %         Flat=func_help_tsne_boundary_elements_all(Flat,fcH);
        %         Flat.p.tsne{Flat.p.channel_mic_display}=Flat.v.tsne;
        %         Flat.Tags(4,Flat.v.tsne.boundary_elements)={'B'};
        %         Flat.Tags(4,Flat.v.tsne.outlier_elements)={'O'};
        %
        %         end
        
        %$ Increase umap time slice by 1 bin (Flat.umap.X.Param.time).  $%
        %* Sets the element grouping to a larger time since threshold crossing. *%
    case 'rightarrow'
        
        if isempty(modifier)
            Flat.p.umap.time=Flat.p.umap.time+1;
            
            %$ Increase umap time by 1 bin (Flat.umap.X.Param.time) and plot context (Fig. 24)$%
            %* This function is to speed up the blobs in the umap figure. *%
        elseif strcmp(modifier,'control')
            Flat.p.umap.time=Flat.p.umap.time+10;
            
            %$ Increase umap time by 1 bin (Flat.umap.X.Param.time) and plot context (Fig. 24)$%
            %* This function allows visual tracking of the slice in Fig. 24. *%
        elseif strcmp(modifier,'shift')
            Flat.p.umap.time=Flat.p.umap.time+1;
            [Flat,plot_flatclust]=func_help_umap_find_blob(Flat);
        end
        [fcH,Flat]=func_help_umap_map_to_I(fcH,Flat);
        [fcH,Flat]=func_umap_select(fcH,Flat,Flat.umap.X);
        
        
        %$ Decrease umap time slice by 1 bin Flat.umap.X.Param.time). DO NOT PLOT. $%
        %* Sets the element grouping to a smaller time since syllable onset. This function is to speed up work because plotting is omitted. *%
    case 'leftarrow'
           
        if isempty(modifier)
        Flat.p.umap.time=Flat.p.umap.time-1;
           
            
            %$ Decrease umap time slice by 10 bin (Flat.umap.X.Param.time).$%
            %* This function is to speed up the blobs in the umap figure. *%
        elseif strcmp(modifier,'control')
            Flat.p.umap.time=Flat.p.umap.time-10;
   
            %$ Increase umap time by 1 bin (Flat.umap.X.Param.time) show spectrogram context by updating context fig 24. $%
            %* This function allows visual tracking of the slice in Fig. 24. *%
        elseif strcmp(modifier,'shift')
            [Flat,plot_flatclust]=func_help_umap_find_blob(Flat);
        end
        [fcH,Flat]=func_help_umap_map_to_I(fcH,Flat);
        [fcH,Flat]=func_umap_select(fcH,Flat,Flat.umap.X);
      
        %$ Set umap time to 0 (Flat.umap.X.Param.time).  $%
        %* Sets the element grouping to time zero. *%
    case '0'
        Flat.p.umap.time=0;
        if isempty(modifier)
            [fcH,Flat]=func_help_umap_map_to_I(fcH,Flat);
            [fcH,Flat]=func_umap_select(fcH,Flat,Flat.umap.X);
        end
        
        %$ Delete Elements.  $%
        %* Do not delete overlapping elements. *%
    case 'delete'
        if isempty(modifier)
            if Flat.v.tsne.clust_dense_mode
                Flat.Tags(Flat.v.tags.TSNE,Flat.v.tsne.select(fcH.hf_tsneVar.selected_clust_inds))={'Out'};
                Flat.v.tagboard0_recompute=1;
                [Flat,fcH]=func_tagboard_define(Flat,fcH);
                Flat.v.plot_numbutton=1;
                Flat.v.plot_tagboard=1;
                plot_flatclust=1;
            end
            [Flat,fcH]=func_tsne_remove_elements(Flat,fcH,fcH.hf_tsneVar.selected_clust_inds);
        end
        %
        %             %$ Delete Elements including overlapping ones.  $%
        %             %* *%
        %         elseif strcmp(modifier,'shift')
        %             if Flat.v.tsne.clust_dense_mode
        %                 this_clust=Flat.v.tsne.select(fcH.hf_tsneVar.selected_clust_inds);
        %                 Flat.Tags(Flat.v.tags.TSNE,[this_clust,Flat.v.tsne.select(fcH.hf_tsneVar.OVLPcross_ind)])={'Out'};
        %                 [Flat,fcH]=func_tsne_remove_elements(Flat,fcH,[fcH.hf_tsneVar.selected_clust_inds,fcH.hf_tsneVar.OVLPcross_ind]);
        %                 Flat.v.tagboard0_recompute=1;
        %                 [Flat,fcH]=func_tagboard_define(Flat,fcH);
        %                 Flat.v.plot_numbutton=1;
        %                 Flat.v.plot_tagboard=1;
        %                 %[Flat,fcH]=func_tsne_remove_elements(Flat,fcH,[fcH.hf_tsneVar.selected_clust_inds,fcH.hf_tsneVar.cross_select]);
        %                 disp('Select Out on tagboard (TSNE field)');
        %             else
        %                 disp('only in clust_dense_mode! aborting.');
        %                 return
        %             end
        %         end
        
        
        %$  Display help.$%
        %* You're looking at it. *%
    case 'h'
        if isempty(modifier)
            currentPath = [mfilename('fullpath') '.m'];
            func_show_help(currentPath);
        end
        
        
        %$ Delete last Control-clicked blob (undo operation).  $%
        %* *%
    case 'c'
        if isempty(modifier)
            if isfield(Flat.umap.XC,'selectScatterCurrBlob')
                Flat.umap.XC=rmfield(Flat.umap.XC,'selectScatterCurrBlob');
            end
            Flat.umap.fcH.ScatterCurrBlob=[];
            set(fcH.hf_umapScatterCurrBlob,'Visible','Off','XData',[],'YData',[]);
            
            [fcH,Flat]=func_umap_select(fcH,Flat,Flat.umap.X);
            disp('Current blob erased');
            
            Flat.umap.ContClickCount=0;
            if isfield(Flat.umap,'ContClick')
%                 if isfield(Flat.umap,'ContClickHist')
%                     Flat.umap.ContClickHist(end-length(Flat.umap.ContClick):end)=[];
%                 end
                Flat.umap=rmfield(Flat.umap,'ContClick');
            end
            
            %$ Delete all Control-clicked blobs (undo operation).  $%
            %* *%
        elseif   strcmp(modifier,'shift')
            
            Flat.umap.X.select=Flat.umap.X0.select;
            Flat.umap.X.map=Flat.X.umap(:,Flat.umap.X.select);
            %           Flat.umap.X.color=Flat.X.clust_ID0(Flat.umap.X.select);
            if isfield(Flat.umap,'ContClickHist')
                Flat.umap=rmfield(Flat.umap,'ContClickHist');
            end
            Flat.umap.fcH.init=1;
            [fcH,Flat]=func_help_umap_init(fcH,Flat);
            [fcH,Flat]=func_help_umap_map_to_I(fcH,Flat);
            [fcH,Flat]=func_umap_select(fcH,Flat,Flat.umap.X);
            
            %% CHILD
            if isfield(Flat,'GEN') && Flat.GEN.is_parent
                disp('Now initializing CHILD');
                %                 Flat.umap.CHILD.ContClickCount=0;
                %                 if isfield(Flat.umap.CHILD,'ContClick')
                %                     Flat.umap.CHILD=rmfield(Flat.umap.CHILD,'ContClick');
                %                 end
                Flats=evalin('base','Flats');
                FlatC=Flats{Flat.GEN.child_ID};
                FlatC=func_help_umap_init_GEN(FlatC);
                Flats{Flat.GEN.child_ID}=FlatC;
                assignin('base','Flats',Flats);
                disp('Done');
            end
            
            
        end
        
        %$ Write all selected blobs to Flat.X.ClustID  $%
        %* *%
    case 'o'
        if isempty(modifier)
            auto_fix=0;
            
            %$ Write all selected blobs to Flat.X.ClustID and auto fix issues. $%
            %* *%
        elseif strcmp(modifier,'shift')
            auto_fix=1;
            disp('Auto fix');
        end
        
        disp('This will finalize your umap clustering - I hope you saved the archive!');
        disp('press 1+enter to continue');
        y=input('');
        if isempty(y) || ~(y==1)
            disp('Aborted');
            return
        end
        if isfield(Flat,'GEN')
            disp('This will erase PARENT and CHILD archives and create FAMILY archive');
            disp('Did you save PARENT AND CHILD? Press 1 to continue');
            y=input('');
            if isempty(y) || ~(y==1)
                disp('Aborted');
                return
            end
        end
        
        Flat=func_umap_evalkey_o(Flat);
        Flat.p.display_marks=1;
        if auto_fix
            Flat=func_umap_auto_fix(Flat);
        end
        
        %% now CHILD
        if isfield(Flat,'GEN')
            disp('Now finalizing child');
            Flats=evalin('base','Flats');
            FlatC=Flats{Flat.GEN.child_ID};
            FlatC=func_umap_evalkey_o(FlatC);
            FlatC.p.display_marks=1;
            FlatC=func_umap_auto_fix(FlatC);
            
            %% create Family (source=PARENT)
            [Flat,FlatC,success]=f_move_selection_to_flats(Flat,FlatC,1:Flat.num_Tags,0,0,0);
            Flats{Flat.v.ind}=Flat;
            FlatC.name=strrep(FlatC.name,'CHILD','FAMILY');
            Flats{Flat.GEN.child_ID}=FlatC;
            Flat=FlatC;
            assignin('base','Flats',Flats);
            disp('Done');
        else
            Flat.name=['@' Flat.name];
            Flat.v.ind=Flat.v.ind+1;
            Flat.p.names{Flat.v.ind}=Flat.name;
            
            Flat.v.save.DAT_data=1;
            Flat.v.save.BOARDLIST=1;
            Flat.v.save.DAT_multiunit=1;
            Flat.v.save.DAT_STACK=1;
            
            Flats=evalin('base','Flats');
            Flats{Flat.v.ind}=Flat;
            assignin('base','Flats',Flats);
        end
        
        Flat.v.tagboard0_recompute=1;
        Flat.v.tagboard0_replot=1;
        Flat.BOARD.tagboard{5}={Flat.v.RMS};
        [Flat, Flat.v.select, Flat.v.select_clust]=func_select_mode(Flat);
        Flat.v.plot_clust=1;
        Flat.v.plot_spec=1;
        [Flat,fcH]=plot_figs(Flat,fcH);
        
        
    case 'q'
        ui=0;
        disp('This will finalize your umap clustering - I hope you saved the archive!');
        disp('press 1+enter to continue');
        y=input('');
        if isempty(y) || ~(y==1)
            disp('Aborted');
            return
        end
        
        appendix=Flat.p.nonoverlap*(Flat.p.umap.MinNumBuffSyl-Flat.p.umap.MinNumTimeSegments);
        L=length(Flat.DAT.filename);
        
        % clear some variables
        Flat.X.umapCtrWP(1,:)=0; Flat.X.umapCtrWN(1,:)=0;
        
        if ~isfield(Flat,'Z')
            Flat=func_umap_Z_generate(Flat,0);
        end
        Flat.Z.stencil=int8(zeros(1,length(Flat.Z.buffer)));
        
        % extract syllables
        WP=Flat.X.umapCtrWP(1:end,Flat.umap.X0.select);
        WN=Flat.X.umapCtrWN(1:end,Flat.umap.X0.select);
        TimeSL=Flat.X.umap_time(:,Flat.umap.X0.select);
        datinds=Flat.X.DATindex(Flat.umap.X0.select);
        inds=Flat.X.indices_all(Flat.umap.X0.select);
        
        % initialize new element insertion
        tagvec=cell(length(Flat.p.tagnames),1); tagvec(:)={''}; tagvec{5}=['@' Flat.v.RMS];
        Ons=cell(1,L); Offs=Ons; ClustIDs=Ons; Tags=Ons; Custs=Ons;
        Cinsert=zeros(1,L);
   
             multibuff=0;% multibuffrand=0; % 'rand' variables are to inspect behavior for studying onset or offset only clusterings
            % jother=setdiff(2:Flat.umap.XC.ClustNo,j);
            for f=1:L
                file_i=find(datinds==f);
                indsF=inds(file_i);  datindsF=datinds(file_i);
                sameS=int8(zeros(size(file_i))); clusts=sameS;
                for j=2:Flat.umap.XC.ClustNo
                    pos=WP(j,file_i);
                    if isfield(Flat.v,'umap.do_rand') & Flat.v.umap.do_rand==1
                        neg=pos;
                    else
                        neg=-WN(j,file_i);
                    end
                    %posR=sum(WP(jother,file_i)); negR=sum(WN(jother,file_i));
                    %  h=(pos==neg & pos & ~posR & ~negR);
                    h=(pos==neg & pos);
                    sameS=sameS + int8(h);% & ~posR & ~negR);
                    
                   % hrand=(pos==pos & pos);
                   %sameSrand=sameS+int8(hrand);
                    clusts(h)=j;
                end
                multibuff=multibuff+sum(sameS>1);
               % multibuffrand=multibuffrand+sum(sameSrand>1);
                %sameS=sameS>0;
                if 0 %isfield(Flat.p.umap,'brute_force') & isfield(Flat.p.umap.brute_force,'NoiseClusts') & ~isempty(Flat.p.umap.brute_force.NoiseClusts)
                    for i=1:length(Flat.p.umap.brute_force.NoiseClusts)
                        ni=Flat.p.umap.brute_force.NoiseClusts(i);
                        pot_noise=(WP(ni,file_i) | WN(ni,file_i)) & ~sameS;
                       % pot_noiserand=(WP(ni,file_i) | WN(ni,file_i)) & ~sameSrand;
                        on=find(diff(pot_noise)>0); off=find(diff(pot_noise)<0);
                       % onrand=find(diff(pot_noiserand)>0); offrand=find(diff(pot_noiserand)<0);
                        if isempty(on) | isempty(off)
                            continue
                        end
                        if on(1)>off(1)
                            off=off(2:end);
                        end
%                         if onrand(1)>offrand(1)
%                             offrand=offrand(2:end);
%                         end
                        for ii=1:length(on)-1
                            if on(ii+1)-off(ii)<Flat.p.umap.MinNumTimeSegments
                                pot_noise(off(ii):on(ii+1))=1;
                            end
                        end
%                         for ii=1:length(onrand)-1
%                             if onrand(ii+1)-offrand(ii)<Flat.p.umap.MinNumTimeSegments
%                                 pot_noiserand(offrand(ii):onrand(ii+1))=1;
%                             end
%                         end
                        
%                         if ~isequal(size(sameS),size(pot_noise))
%                             keyboard
%                         end
                        sameS=sameS+int8(pot_noise);
                      %  sameSrand=sameSrand+int8(pot_noiserand);
                        clusts(pot_noise)=ni;
                    end
                end
                c=1; onoff=[];  cmiss=1; zis=[]; timeSLon=[]; timeSLoff=[];
                sameSi=find(sameS);
                if isempty(sameSi)
                    continue
                end
                on=sameSi(1); clustsON=[];
                for ii=2:length(sameSi)
                    i=sameSi(ii);
                    if i==sameSi(end)
                        continue
                    end
                    %  new blob or new file or gap => new offset or miss
                    if  (~isequal(clusts(i),clusts(on)) | ~isequal(datindsF(i),datindsF(on)) | indsF(i)-indsF(sameSi(ii-1))>Flat.p.nonoverlap)
                        
                        % syllable sufficiently long and gap large enough
                        if indsF(sameSi(ii-1))-indsF(on)>=Flat.p.nonoverlap*Flat.p.umap.MinNumTimeSegments	& (indsF(i)-indsF(sameSi(ii-1))>appendix |  ~isequal(datindsF(i),datindsF(on)))
                            onoff(1,c)=on;
                            onoff(2,c)=sameSi(ii-1);
                            clustsON(c)=clusts(sameSi(ii-1));
                            if clustsON(c)<2
                                keyboard
                            end
                            WP(1,file_i(onoff(1,c):onoff(2,c)))=clustsON(c);
                            
                            zi=Flat.Y.Zindex(file_i(onoff(1,c):onoff(2,c)));
                            zi=[zi (zi(end)+1):(zi(end)+Flat.umap.appendixB)];
                            zis(1,c)=zi(1); zis(2,c)=zi(end);
                            timeSLon(c)=TimeSL(1,file_i(onoff(1,c)));
                            timeSLoff(c)=TimeSL(2,file_i(onoff(2,c)));
                            
                            if ~isequal(Flat.X.indices_all(Flat.umap.X0.select(file_i(onoff(1,c)))),Flat.Z.indices_all(zi(1)))
                                keyboard
                            end
                            
                            if any(Flat.Z.stencil(zi))
                                keyboard
                            end
                            if any(diff(Flat.Z.buffer(zi))>1)
                                keyboard
                            end
                            Flat.Z.stencil(zi)=clustsON(c);
                            c=c+1;
                            on=i;
                        else
                            cmiss=cmiss+1;
                            on=i;
                        end
                    end
                end
                
                % last offset
                if indsF(i)-indsF(on)>Flat.p.nonoverlap*Flat.p.umap.MinNumBuffSyl
                    onoff(1,c)=on; onoff(2,c)=i;  clustsON(c)=clusts(i);
                    WP(1,file_i(onoff(1,c):onoff(2,c)))=clustsON(c);
                    zi=Flat.Y.Zindex(file_i(onoff(1,c):onoff(2,c)));
 
                    timeSLon(c)=TimeSL(1,file_i(onoff(1,c))); 
                    timeSLoff(c)=TimeSL(2,file_i(onoff(2,c)));
                    
                    zi=[zi (zi(end)+1):(zi(end)+Flat.umap.appendixB)];
                    zis(1,c)=zi(1); zis(2,c)=zi(end);
                      
                    if ~isequal(Flat.X.indices_all(Flat.umap.X0.select(file_i(onoff(1,c)))),Flat.Z.indices_all(zi(1)))
                        keyboard
                    end
                    if any(Flat.Z.stencil(zi))
                        keyboard
                    end
                 %   Flat.Z.stencil(zi:zi+diff(onoff(:,c))-1+Flat.umap.appendixB)=clustsON(c);
                Flat.Z.stencil(zi)=clustsON(c);
                else
                    c=c-1;
                end
                if isempty(onoff)
                    continue
                end
                on=onoff(1,:); off=onoff(2,:);
                
                for i=1:length(on)
                    Di=datindsF(on(i));
                    Cinsert(Di)=Cinsert(Di)+1;
                    Ons{Di}(Cinsert(Di))=indsF(on(i));
                    Offs{Di}(Cinsert(Di))=indsF(off(i))+appendix;
%                     if ~isequal(Ons{Di}(Cinsert(Di)),Flat.Z.indices_all(zi(1)))
%                         keyboard
%                     end
                    %                     if off(i)-on(i)<4
                    %                         keyboard
                    %                     end
                    ClustIDs{Di}(Cinsert(Di))=clustsON(i);
                    Tags{Di}(:,Cinsert(Di))=tagvec;
                     Custs{Di}(:,Cinsert(Di))=[zis(1,i) ; zis(2,i); timeSLon(i); timeSLoff(i)];
               % Custs{Di}(:,Cinsert(Di))=[zis(1,i) ; zis(2,i)];
                    if zis(1,i)==0 | zis(2,i)==0
                        keyboard
                    end
                end
            end
            Flat.umap.Z_doneQ=1; 
            Flat.Y.umap=Flat.X.umap(:,Flat.umap.X0.select);
            Flat.Z.stencil0=Flat.Z.stencil;  % for computation of precision and recall
            fprintf('number of multiply defined buffers: %d\n',multibuff);
        
            Flat.X.umapCtrWP(1,Flat.umap.X0.select)=WP(1,:);
        
        
        % remove RMSfilt elements
        [Flat,sels,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS});
        if ~isempty(sels)
            fprintf('Now removing old %s elements (and storing them in Flat.Y)\n',Flat.v.RMS);
            %fn=fieldnames(Flat.X);
            fn={'indices_all','data_off','DATindex','PCcoeffs','umapCtrWP','umapCtrWN'};
            Flat.p.umap.Ygen=fn;
            for i=1:length(fn)
                Flat.Y.(fn{i})=Flat.X.(fn{i})(:,sels);
            end
            Flat=func_remove_elements(Flat,sels,0,1);
        end
        
        % remove old umap elements
        [Flat,sels,board] = func_select_tagboard(Flat,[],'activator',{['@' Flat.v.RMS]});
        if ~isempty(sels)
            disp('Now removing old UMAP elements');
            Flat=func_remove_elements(Flat,sels,0,1);
        end
        
        Flat.X=rmfield(Flat.X,'custom');
        Flat=func_write_imported_elements_to_Flat(Flat,Ons,Offs,ClustIDs,Custs,[],Tags,[],[]);
        if ~strcmp(Flat.v.RMS(1),'@')
            Flat.umap.RMSold=Flat.v.RMS;
        end
        Flat.v.RMS=['@' Flat.v.RMS]; Flat.p.Channel_syl_tagstring{Flat.p.channel_mic_display}=Flat.v.RMS;
        %         if ~isfield(Flat.X,'custom')
        %             Flat.X.custom=round(Flat.num_Tags/2)*ones(3,Flat.num_Tags);
        %         end
        Flat.X.umap_time=Flat.X.custom(3:4,:);
        Flat.X.custom(3:4,:)=[];
        Flat.Z.custom0=Flat.X.custom;
        
        
        Flat.name=['@' Flat.name];
        Flat.v.ind=Flat.v.ind+1;
        Flat.p.names{Flat.v.ind}=Flat.name;
        
        Flat.v.umap.track_stencil=1;
        Flat.v.save.DAT_data=1;
        Flat.v.save.BOARDLIST=1;
        Flat.v.save.DAT_multiunit=1;
        Flat.v.save.DAT_STACK=1;
        Flats=evalin('base','Flats');
        Flats{Flat.v.ind}=Flat;
        assignin('base','Flats',Flats);
        
        Flat.v.all_clust_mode=0;
        Flat.v.plot_numbutton=1;
        Flat.v.plot_tagboard=1;
        Flat.v.curr_clust=2;
        Flat.v.plot_ordered=3;
        Flat.v.repeated_put_to=1;
        
        Flat.v.tagboard0_recompute=1;
        Flat.v.tagboard0_replot=1;
        Flat.BOARD.tagboard{5}={Flat.v.RMS};
        [Flat, Flat.v.select, Flat.v.select_clust]=func_select_mode(Flat);
        Flat.v.plot_clust=1;
        Flat.v.plot_spec=1;
        [Flat,fcH]=plot_figs(Flat,fcH);
        
        %$ Show/Unshow raw scatterplot.  $%
        %* *%
    case 'space'
        if isempty(modifier)
            if ~isfield(Flat.v.umap,'space_mode')
                Flat.v.umap.space_mode=0;
            end
            Flat.v.umap.space_mode=rem(Flat.v.umap.space_mode+1,3);
            switch Flat.v.umap.space_mode
                case 0
                    for i=1:length(fcH.hf_umapScatterAll)
                        if ishandle(fcH.hf_umapScatterAll{i})
                         set(fcH.hf_umapScatterAll{i},'Visible','off');
                        end
                    end
                    set(fcH.hf_umapIT,'Visible','On');
                case 1
                    for i=1:length(fcH.hf_umapScatterAll)
                        if ishandle(fcH.hf_umapScatterAll{i})
                         set(fcH.hf_umapScatterAll{i},'Visible','on','markersize',1,'linewidth',1);
                        end
                            
                    end
                case 2
                    set(fcH.hf_umapIT,'Visible','Off');
            end
            [fcH,Flat]=func_help_umap_map_to_I(fcH,Flat);
            [fcH,Flat]=func_umap_select(fcH,Flat,Flat.umap.X);
            
            %             h=get(fcH.hf_umapScatterAll,'Visible');
%             switch h
%                 case 'on'
%                     set(fcH.hf_umapScatterAll,'Visible','off');
%                 case 'off'
%                     set(fcH.hf_umapScatterAll,'Visible','on','markersize',1,'linewidth',1);
%             end
        end
        
        %$  Cluster previous control-clicked blob (either onset, offset, or both).  $%
        %*  *%
    case 'w'
        if ~isfield(Flat.umap,'ContClick')
            disp('You may need to press c before running this function');
            return
        end
        if length(unique(sign([Flat.umap.ContClick.timeslice])))==1
            disp('Only one 1 slice used, this is useful for defining noise...');
            y=input('Is this noise ? (yes=1) ');
            if y==1
                if isfield(Flat.p.umap,'brute_force') & isfield(Flat.p.umap.brute_force,'NoiseClusts')
                    Flat.p.umap.brute_force.NoiseClusts(end+1)=Flat.umap.XC.ClustNo+1;
                else
                    Flat.p.umap.brute_force.NoiseClusts=Flat.umap.XC.ClustNo+1;
                end
            end
        end
        if Flat.umap.ContClickCount>0
            Flat.umap.XC.ClustNo=Flat.umap.XC.ClustNo+1;
        else
            return
        end
        if ~isfield(Flat.X,'umapCtrWP')
            Flat.X.umapCtrWP=int8(zeros(1,Flat.num_Tags));
            Flat.X.umapCtrWN=int8(zeros(1,Flat.num_Tags));
        end
        Flat.X.umapCtrWP(Flat.umap.XC.ClustNo,:)=0;
        Flat.X.umapCtrWN(Flat.umap.XC.ClustNo,:)=0;
        
        num_written=zeros(2,1);
        for i=1:Flat.umap.ContClickCount
            is_neg_time=Flat.umap.ContClick(i).timeslice<0;
            on_vs_off=1+double(is_neg_time);
            direction=1-2*is_neg_time;
            %   Flat.X.umapOnOffClust(on_vs_off,Flat.umap.ContClick(i).Selectonoffset)=Flat.umap.XC.ClustNo;
            seli=Flat.umap.ContClick(i).selectonoffset;
            %which_short=Flat.umap.ContClick(i).onoffset<Flat.p.umap.MinNumTimeSegments;
            %which_long=Flat.umap.ContClick(i).onoffset>=Flat.p.umap.MinNumTimeSegments;
            % Flat.Tags(8,Flat.umap.X.select(seli(which_short)))={num2str(Flat.umap.XC.ClustNo*direction)};
            % Flat.Tags(8,Flat.umap.X.select(seli(which_long)))={'0'};
            for j=1:length(seli)
                s=Flat.umap.X.select(seli(j));
                if direction>0
                    se=Flat.umap.X.select(min(end,seli(j)+Flat.umap.ContClick(i).onoffset(j)));
                    Flat.X.umapCtrWP(Flat.umap.XC.ClustNo,s:se)=Flat.umap.XC.ClustNo;
                else
                    sa=Flat.umap.X.select(max(1,seli(j)-Flat.umap.ContClick(i).onoffset(j)));
                    Flat.X.umapCtrWN(Flat.umap.XC.ClustNo,sa:s)=direction*Flat.umap.XC.ClustNo;
                end
            end
            num_written(on_vs_off)=num_written(on_vs_off)+length(Flat.umap.ContClick(i).Selectonoffset);
        end
        
        fprintf('clust %d: Written %d on and %d off elements into Flat.X.umapCtrWP+N\n',Flat.umap.XC.ClustNo,num_written(1),num_written(2));
        Flat.umap.fcH.ScatterClusteredBlobs=[Flat.umap.fcH.ScatterClusteredBlobs Flat.umap.fcH.ScatterCurrBlob];
        set(fcH.hf_umapScatterClusteredBlobs,'XData',Flat.umap.fcH.ScatterClusteredBlobs(1,:),'YData',Flat.umap.fcH.ScatterClusteredBlobs(2,:),'color','k','Visible','On');
        
        
        Flat.umap.X = func_remove_routine(Flat.umap.X, Flat.umap.XC.selectScatterCurrBlob, length(Flat.umap.X.select)); % RRR
        Flat.umap.XC.selectScatterCurrBlob=[];
        Flat.umap.ContClickCount=0;
        if isfield(Flat.umap,'ContClick')
            if isfield(Flat.umap,'ContClickHist')
                Flat.umap.ContClickHist(end+1:end+length(Flat.umap.ContClick))=Flat.umap.ContClick;
            else
                Flat.umap.ContClickHist=Flat.umap.ContClick;
            end
            Flat.umap=rmfield(Flat.umap,'ContClick');
        end
        
        
        %set(fcH.hf_umapScatterAll,'XData',Flat.umap.X.map_coord(1,:),'YData',Flat.umap.X.map_coord(2,:),'color','w','Visible','On');
        fcH=func_umap_help_Scatterplot(Flat,fcH);
        
        Flat.umap.fcH.ScatterCurrBlob=[];
        set(fcH.hf_umapScatterCurrBlob,'XData',[],'YData',[],'color','m','Visible','Off');
        
        
        [fcH,Flat]=func_help_umap_map_to_I(fcH,Flat);
        
        %% update CHILD
        if isfield(Flat,'GEN') && Flat.GEN.is_parent
            Flats=evalin('base','Flats');
            FlatC=Flats{Flat.GEN.child_ID};
            
            remove_elements=cell(1,Flat.umap.ContClickCount);
            for i=1:Flat.umap.ContClickCount
                PIX={Flat.umap.ContClick(i).PIX};
                is_XC_select=func_help_umap_select_from_I(FlatC,PIX,1);
                sel=find(is_XC_select);
                timeslice=Flat.umap.ContClick(i).timeslice;
                [Selectonoffset,remove_elements{i},selectonoffset,onoffset]=func_help_umap_SelectOnOffset(FlatC,sel,timeslice);
                %                     if timeslice>0
                %                         FlatC.X.umapOnOffClust(1,Selectonoffset)=Flat.umap.ContClick(i).ClustNo;
                %                     else
                %                         FlatC.X.umapOnOffClust(2,Selectonoffset)=Flat.umap.ContClick(i).ClustNo;
                %                     end
            end
            remove_elements=[remove_elements{:}];
            FlatC.umap.X = func_remove_routine(FlatC.umap.X, remove_elements, length(FlatC.umap.X.select));
            Flats{Flat.GEN.child_ID}=FlatC;
            assignin('base','Flats',Flats);
        end
        
        
        
    case 'click'
        if isempy(modifier)
            % do nil
            
            %$ show random sorting.  $%
            %* *%
        elseif strcmp(modifier,'shift')
            Flat.v.umap.evnt_modifier=modifier;
        end
        
    case {'shift','control'}
        if isfield(Flat.v.umap,'evnt_modifier')
            Flat.v.umap.secondlastmodifier= Flat.v.umap.evnt_modifier;
        end
        Flat.v.umap.evnt_modifier=modifier;
end

if plot_flatclust
    Flat.v.run_select_clust=1;
    Flat.p.spec_syll_dur=1;
    Flat.v.plot_clust=1;
    Flat.v.sequence{Flat.v.plot_ordered,Flat.v.curr_clust}=[];
    Flat.v.prepare_plot=1;
    [Flat Flat.v.select Flat.v.select_clust]=func_select_mode(Flat);
    [Flat,fcH]=plot_figs(Flat,fcH);
end
if isfield(Flat.v.umap,'lastkey')
    Flat.v.umap.secondlastkey=Flat.v.umap.lastkey;
end

Flat.v.umap.lastkey=evnt.Key;
assignin('base','fcH',fcH);
assignin('base','Flat',Flat);



%fprintf('current threshold: %1.5f \n', fcH.hf_tsneVar.theta); % added by CL to see the threshold
%fprintf('current threshold: %1.5f \n', Flat.p.umap.theta); % added by CL to see the threshold

end
%% EOFF
