function Flat=func_umap_ZumapCtr(Flat)
% write Flat.Z.umapCtrWP to Flat.Z.umapCtrWN

%sel=func_select_mic(Flat,Flat.p.channel_mic_display);
sel=find(Flat.Z.custom0(1,:));
Flat.Z.umapCtrWP=zeros(size(Flat.Y.umapCtrWP,1),size(Flat.Z.DATindex,2));
Flat.Z.umapCtrWN=Flat.Z.umapCtrWP;
for i=1:length(sel)
    j=sel(i);
%     x1=Flat.X.custom(1,j); x2=Flat.X.custom(2,j)-Flat.umap.appendixB;
    x1=Flat.Z.custom0(1,j); x2=Flat.Z.custom0(2,j)-Flat.umap.appendixB;
    if x2<0
        fprintf('issue\n');
        x2=x1;
    end
    y1=Flat.Z.Yindex(x1);
    y2=Flat.Z.Yindex(x2);

    Flat.Z.umapCtrWP(:,x1:x2)=Flat.Y.umapCtrWP(:,y1:y2);
    Flat.Z.umapCtrWP(:,x2+1:x2+Flat.umap.appendixB)=Flat.Z.umapCtrWP(:,x2)*ones(1,Flat.umap.appendixB);
    
    Flat.Z.umapCtrWN(:,x1:x2)=Flat.Y.umapCtrWN(:,y1:y2);
    Flat.Z.umapCtrWN(:,x2+1:x2+Flat.umap.appendixB)=Flat.Z.umapCtrWN(:,x2)*ones(1,Flat.umap.appendixB);
end
end