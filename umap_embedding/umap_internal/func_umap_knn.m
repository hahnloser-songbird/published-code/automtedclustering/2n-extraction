function Flat=func_umap_knn(Flat,writeFlat)
%% find kNN
% Flat.X.custom: 1 = onset buffer in Flat.Y,
%                2 = offset buffer in Flat.Y
%                3 = Similarity Value
if nargin<3
    verbose=0;
end

if nargin<2
    writeFlat=0;
end

k=Flat.v.curr_clust;

knN=10;
min_nN=2; % at least this number of nN required to proceed
do_rank=Flat.p.umap.brute_force.do_rank;

%do_rank=0;

Flat.p.umap.brute_force.knN=knN;
nclusts=Flat.umap.brute_force.nclusts;
%No=Flat.umap.brute_force.No;

CosM=Flat.umap.brute_force.CosM;
Maxis=Flat.umap.brute_force.Maxis;
NbuffsC=Flat.umap.brute_force.NbuffsC;
selsC=Flat.umap.brute_force.selsC;

CosD=Flat.umap.brute_force.CosD;
CselfSim=Flat.umap.brute_force.CselfSim;

Cosm0=Flat.umap.brute_force.Cosm0;
NM0=Flat.umap.brute_force.NbuffsM0;
selsM0=Flat.umap.brute_force.selsM0;

selsM=find(Flat.Y.umapCtrWP(1,:)==0);
%minDurC=min(Flat.umap.brute_force.DursC{k});
DursC=Flat.umap.brute_force.DursC;
minDurC=cellfun(@min,DursC,'UniformOutput',false); minDurC=min([minDurC{2:end}]);

[selsM,NM,PCcoeffsM]=func_umap_help_Nbuffs(Flat,selsM,[],minDurC,1);
No=size(NM,2);
% compute Cosm from Cosm0 and Flat.Y.umapCtrWP(1,:)
%Cosm=Flat.umap.brute_force.Cosm0;
N=length(selsM);
Cosm=cell(1,N);
c0=1; % index into Cosm0
[s,si]=sort(selsM(1,:),'ascend');
s23=selsM(2:3,si);
[s0,s0i]=sort(selsM0(1,:),'ascend');
s023=selsM0(2:3,s0i);
for i=1:N 
    i0=c0-1+find(s(i)>=s0(c0:end),1,'last');
    c0=i0;
    i0=s0i(i0);
   % don=1+NM(1,si(i))-NM0(1,i0);
   % Cosm{si(i)}=Cosm0{k,i0}(:,don:min(end,don+NM(2,si(i))-NM(1,si(i))-1));
   don=1+s23(1,i)-s023(1,c0);
    Cosm{si(i)}=Cosm0{k,i0}(:,don:min(end,don+s23(2,i)-s23(1,i)));
end
    

%% Compute average knN similarity as a function of buffer number, for each omit region j

Durs=Flat.umap.brute_force.DursC{k};
minDur=min(Durs);
%eat.Cosm=Cosm(k,:);
eat.Cosm=Cosm;
%eat.NM=NM;
eat.selsM=selsM;

eat.C=cell(1,No);
CmsAll=cell(1,No);
eat.whereCms=cell(1,No);
for j=1:No
%    [CmsAll{j},eat.whereCms{j}]=sort(Cosm{k,j},1,'descend'); % for each buffer sort by similarity    
    [CmsAll{j},eat.whereCms{j}]=sort(eat.Cosm{j},1,'descend'); % for each buffer sort by similarity    
    if isempty(CmsAll{j})
        CmsAll{j}=-2;
    else
        cms=mean(CmsAll{j}(1:min(end,knN),:),1); % top knN similarities for each buffer
    end
    eat.C{j}=CmsAll{j};
   % eat.C{j}=cms;
end

if writeFlat
    if isfield(Flat.umap.brute_force,'Clast')
        [Flat,sels,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS},'Mark',{Flat.umap.brute_force.Clast});
        if ~isempty(sels)
            fprintf('Now removing Mark=%s syllables\n',Flat.umap.brute_force.Clast);
            Flat=func_remove_elements(Flat,sels,0,1);
        end
    end
    Rstr=['C' num2str(k)];
    Flat.umap.brute_force.Clast=Rstr;
    [Flat,sels,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS});
    Flat.Tags(4,sels)={'[]'};
    
    L=length(Flat.DAT.filename);
    tagvec=cell(length(Flat.p.tagnames),1); tagvec(:)={''}; tagvec{5}=Flat.v.RMS; tagvec{4}=Rstr;
    Ons=cell(1,L); Offs=Ons; ClustIDs=Ons; Custs=Ons; Tags=Ons;
    Cinsert=zeros(1,L); 
end

%% eat NM away until nothing is left
to_remove=[];
for i=1:No
    if eat.selsM(3,i)-eat.selsM(2,i)<minDur
        to_remove=[to_remove i];
    end
end
if ~isempty(to_remove)
    eat.selsM(:,to_remove)=[];
    eat.C=eat.C(to_remove(end)+1:end); 
    eat.Cosm=eat.Cosm(to_remove(end)+1:end);
    eat.whereCms=eat.whereCms(to_remove(end)+1:end);
end

while diff(eat.selsM(2:3,1))>=minDur
    if size(eat.C{1},1)<min_nN
        eat.C=eat.C(2:end);  eat.Cosm=eat.Cosm(2:end);   eat.whereCms=eat.whereCms(2:end);   eat.selsM=eat.selsM(:,2:end);
        continue
    end
 %  cms=mean(eat.C{1}(1:min(end,knN),:),1);
    cms=sum(eat.C{1}(1:min(end,knN),:),1)/knN;
    [Cmax,Cmaxi]=max(cms); % max across buffers, Cmaxi(k)=buffer position
  % [Cmax,Cmaxi]=max(eat.C{1}); % max across buffers, Cmaxi(k)=buffer position
  
    inds=eat.whereCms{1}(1:min(end,knN),Cmaxi);
    sylhdur=diff(NbuffsC{k}(:,inds));
    [maxdur,maxdurpos]=max(sylhdur);
    if size(eat.whereCms{1},1)>10
  %      keyboard
    end
    
    %    [dummy,syli]=max(eat.Cosm{1}(:,Cmaxi)); % most similar syllable
    syli=inds(maxdurpos); % longest syllable
    son=eat.selsM(1,1)+Cmaxi-1;  %  selsM an index into Flat.DAT (1=index into Flat.Y, 2,3=on&offset in buffers)
    dur0=diff(NbuffsC{k}(:,syli));
    dur=dur0-Flat.p.umap.brute_force.nbuffsL-Flat.p.umap.brute_force.nbuffsR;

    Di=Flat.Y.DATindex(son);
    onoff(1)=Flat.Y.indices_all(son);
    onoff(2)=onoff(1)+Flat.p.nonoverlap*dur;

    if writeFlat        
        Cinsert(Di)=Cinsert(Di)+1;
        Ons{Di}(Cinsert(Di))=onoff(1);
        Offs{Di}(Cinsert(Di))=onoff(2)+Flat.p.nonoverlap*(Flat.p.umap.MinNumBuffSyl-Flat.p.umap.MinNumTimeSegments);
        ClustIDs{Di}(Cinsert(Di))=1;
        Custs{Di}(:,Cinsert(Di))=[son ; son+diff(NbuffsC{k}(:,syli)) ; Cmax];
        Tags{Di}(:,Cinsert(Di))=tagvec;
    end
    
%     h123=[eat.selsM(1) eat.selsM(1) ; eat.selsM(2,1)  eat.selsM(2,1)+dur+1 ; Cmaxi-2 eat.selsM(3,1)];
%     eat.selsM(:,1)=[]; eat.selsM=[h123 eat.selsM];
%     
     % space at offset
     if eat.selsM(3,1)-eat.selsM(2,1)>Cmaxi+dur0+minDur
        hoff.C=eat.C{1}(:,Cmaxi+dur0+1:end);
        hoff.selsM=[eat.selsM(1)  ; eat.selsM(2,1)+Cmaxi+dur0+1 ; eat.selsM(3,1)];
        hoff.Cosm=eat.Cosm{1}(:,Cmaxi+dur0+1:end);
        hoff.whereCms=eat.whereCms{1}(:,Cmaxi+dur0+1:end);
    else
        hoff=[];
     end
     
    % space at onset
    if Cmaxi>minDur 
        hon.C=eat.C{1}(:,1:Cmaxi-1);
        hon.selsM=[eat.selsM(1) ; eat.selsM(2,1) ; Cmaxi-1];
        hon.Cosm=eat.Cosm{1}(1:Cmaxi-1);
        hon.whereCms=eat.whereCms{1}(:,1:Cmaxi-1);
    else
        hon=[];
    end
    
    if ~isempty(hoff) && ~isempty(hon)
        %eat.C=[hon.C hoff.C eat.C(2:end)];   
        eat.C(3:end+1)=eat.C(2:end); eat.C(1)={hon.C}; eat.C(2)={hoff.C};         
        eat.Cosm(3:end+1)=eat.Cosm(2:end); eat.Cosm(1)={hon.Cosm}; eat.Cosm(2)={hoff.Cosm};        
        eat.whereCms(3:end+1)=eat.whereCms(2:end); eat.whereCms(1)={hon.whereCms}; eat.whereCms(2)={hoff.whereCms};        
        %eat.Cosm=[hon.Cosm hoff.Cosm eat.Cosm(2:end)];  
        eat.selsM=[hon.selsM hoff.selsM eat.selsM(:,2:end)];
 
    elseif ~isempty(hoff)  
        eat.C(1)={hoff.C};   eat.Cosm(1)={hoff.Cosm};  eat.whereCms(1)={hoff.whereCms};   eat.selsM(:,1)=hoff.selsM;
    elseif  ~isempty(hon)
        eat.C(1)={hon.C};   eat.Cosm(1)={hon.Cosm};  eat.whereCms(1)={hon.whereCms};   eat.selsM(:,1)=hon.selsM;
      else
        eat.C=eat.C(2:end);  eat.Cosm=eat.Cosm(2:end);   eat.whereCms=eat.whereCms(2:end);   eat.selsM=eat.selsM(:,2:end);
    end
end
Flat=func_write_imported_elements_to_Flat(Flat,Ons,Offs,ClustIDs,Custs,[],Tags,[],[]);

Flat.v.all_clust_mode=0;
Flat.BOARD.tagboard{4}={Rstr};
Flat.v.plot_numbutton=1;
Flat.v.plot_tagboard=1;
Flat.v.curr_clust=1;
Flat.v.plot_ordered=8;
Flat.v.run_select=1;
Flat.v.run_select_clust=1;
[Flat,select]=func_select_tagboard(Flat,[],'Mark','[]');
select=select(Flat.X.clust_ID(select)==k);
[h,hi]=sort(Flat.X.data_off(select),'descend');
Flat.umap.brute_force.specID_examples=select(hi(1:2))
Flat.v.repeated_put_to=k; % ready for move

Flat.v.leftmost_element=max(1,1+sum(Cinsert)-Flat.p.spec_num);


return
%%
C2=cell(nclusts,No);
for k=2:nclusts
    %  i0=I0s(k,i)
    for j=1:No
        cms=sort(Cosm{k,j},1,'descend'); % for each buffer sort by similarity
        if isempty(cms)
            cms=-2;
        else
        cms=mean(cms(1:min(end,knN),:),1); % top knN similarities for each buffer
        end
        C2{k,j}=cms;   
    end
end

if writeFlat
    if isfield(Flat.umap.brute_force,'Round')
        r=str2num(Flat.umap.brute_force.Round(2:end));
        r=r+1;
        Rstr=['R' num2str(r)];
        Flat.umap.brute_force.Round=Rstr;
    else
        Rstr='R1';
        % remove old Round elements R1, R2, etc
        [Flat,sels,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS},'Mark',{'R1','R2','R3','R4','R5'});
        if ~isempty(sels)
            disp('Now removing old Round syllables');
            Flat=func_remove_elements(Flat,sels,0,1);
        end
        [Flat,sels,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS});
        Flat.Tags(4,sels)={'[]'};
    end
    L=length(Flat.DAT.filename);
    tagvec=cell(length(Flat.p.tagnames),1); tagvec(:)={''}; tagvec{5}=Flat.v.RMS; tagvec{4}=Rstr;
    Ons=cell(1,L); Offs=Ons; ClustIDs=Ons; Custs=Ons; Tags=Ons;
    Cinsert=zeros(1,L);
end

% C2{k,i}= knN similarity across buffers
for i=1:No
    Cmax=-2*ones(1,nclusts); Cmaxi=2*ones(1,nclusts);
    for k=2:nclusts
        if ~isempty(C2{k,i})
            [Cmax(k),Cmaxi(k)]=max(C2{k,i}); % max across buffers, Cmaxi(k)=buffer position
        end
    end
    [maxval,clust_no]=max(Cmax);

    [dummy,syli]=max(Cosm{clust_no,i}(:,Cmaxi(clust_no)));
    son=selsM(1,i)+Cmaxi(clust_no)-1;
    dur=diff(NbuffsC{clust_no}(:,syli))-Flat.p.umap.brute_force.nbuffsL-Flat.p.umap.brute_force.nbuffsR;
 

    Di=Flat.Y.DATindex(son);
    onoff(1)=Flat.Y.indices_all(son);
    onoff(2)=onoff(1)+Flat.p.nonoverlap*dur;

    if writeFlat
        
        Cinsert(Di)=Cinsert(Di)+1;
        Ons{Di}(Cinsert(Di))=onoff(1);
        Offs{Di}(Cinsert(Di))=onoff(2)+Flat.p.nonoverlap*(Flat.p.umap.MinNumBuffSyl-Flat.p.umap.MinNumTimeSegments);
        ClustIDs{Di}(Cinsert(Di))=clust_no;
        Custs{Di}(:,Cinsert(Di))=[son ; son+diff(NbuffsC{clust_no}(:,syli)) ; i ; maxval];
         Tags{Di}(:,Cinsert(Di))=tagvec;
    end
end
Flat=func_write_imported_elements_to_Flat(Flat,Ons,Offs,ClustIDs,Custs,[],Tags,[],[]);

Flat.v.all_clust_mode=0;
Flat.v.plot_numbutton=1;
Flat.v.plot_tagboard=1;
Flat.v.curr_clust=2;
Flat.v.plot_ordered=3;

return
%%
CM=cell(1,nclusts); whereM=CM;
C=zeros(nclusts,No);
for k=2:nclusts
%     if do_rank
%         CosD{k}=sort(CosD{k},'ascend');
%     end
     [CM{k},whereM{k}]=sort(CosM{k},'descend'); % CosM{k}=syllables vs omit region number
    % C(1+(k-2)*kNN:(k-1)*kNN,:)=CM(1:kNN,:);
    C(k,:)=mean(CM{k}(1:knN,:));
end

if do_rank % transform C to rank
    CosDi=Flat.umap.brute_force.CosDi;
    for k=2:nclusts
        L=length(CosDi{k});
        if  ~isequal(knN,Flat.umap.brute_force.knN) % recompute self-sim
            Flat.umap.brute_force.knN=knN;
            CselfSim{k}=zeros(1,L);
            for ii=1:L
                CosDs=CosD{k}(ii,CosDi{k}(ii):end);
                h=sort(CosDs,'descend');
                CselfSim{k}(ii)=mean(h(1:min(end,knN)));
            end
            CselfSim{k}=sort(CselfSim{k},'ascend');
        end
        for i=1:No
            %     h=round(100*find(C(k,i)<CosD{k},1,'first')/length(CosD{k}));
            h=round(100*find(C(k,i)<CselfSim{k},1,'first')/max(L,length(CselfSim{k}))); % we take to max to not favor clusters with few syllables!
            if isempty(h)
               C(k,i)=100;
            else
                C(k,i)=h;
            end
        end
    end
    % h=find(~cellfun(@isempty,arrayfun(@(x) find(x>CosD{2},1,'first'),C(2,:),'UniformOutput',false)));
end
[Cmax,Cmaxi]=max(C);

[CmaxSort,CmaxSorti]=sort(Cmax,'descend');

if writeFlat
    if isfield(Flat.umap.brute_force,'Round')
        r=str2num(Flat.umap.brute_force.Round(2:end));
        r=r+1;
        Rstr=['R' num2str(r)];
        Flat.umap.brute_force.Round=Rstr;
    else
        Rstr='R1';
        % remove old Round elements R1, R2, etc
        [Flat,sels,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS},'Mark',{'R1','R2','R3','R4','R5'});
        if ~isempty(sels)
            disp('Now removing old Round syllables');
            Flat=func_remove_elements(Flat,sels,0,1);
        end
        [Flat,sels,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS});
        Flat.Tags(4,sels)={'[]'};
    end
    L=length(Flat.DAT.filename);
    tagvec=cell(length(Flat.p.tagnames),1); tagvec(:)={''}; tagvec{5}=Flat.v.RMS; tagvec{4}=Rstr;
    Ons=cell(1,L); Offs=Ons; ClustIDs=Ons; Custs=Ons; Tags=Ons;
    Cinsert=zeros(1,L);
end

for i=1:No
    if Cmax(CmaxSorti(i))==0
        disp('zero')
     continue
    end
    if Cmaxi(CmaxSorti(i))==6 % noise
        %       continue
    end
    
    clust_no=Cmaxi(CmaxSorti(i)); % top cluster (kNN syllables)
    
    % kNN syllables
    sylsi=whereM{clust_no}(1:knN,CmaxSorti(i)); % top knN syllables
    durs=diff(NbuffsC{clust_no}(:,sylsi));
    [maxdur,maxduri]=max(durs);
    
    % chosen syllable is the longest one!
    syli=whereM{clust_no}(maxduri,CmaxSorti(i)); % top syllable
    buff_offset=Maxis{clust_no}(syli,CmaxSorti(i))-1; % offset
    dur=diff(NbuffsC{clust_no}(:,syli));
    
    if Flat.p.umap.brute_force.doPCcoeffs
        son=selsM(NM(1,CmaxSorti(i)))+buff_offset; soff=son+dur;
    else
        dur=dur-Flat.p.umap.brute_force.nbuffsL-Flat.p.umap.brute_force.nbuffsR;
        son=selsM(1,CmaxSorti(i))+buff_offset; soff=son+dur;
    end
    Di=Flat.Y.DATindex(son);
    onoff(1)=Flat.Y.indices_all(son);
    onoff(2)=onoff(1)+Flat.p.nonoverlap*dur;
   % onoff=Flat.Y.indices_all([son soff]);
    
    if writeFlat
        
%        % rankC=find(Cmax(CmaxSorti(i))>CosD{clust_no},1,'first');
%         rankC=find(Cmax(CmaxSorti(i))>CselfSim{clust_no},1,'first');
%         if isempty(rankC)
%             rankC=0;
%         end
        Cinsert(Di)=Cinsert(Di)+1;
        Ons{Di}(Cinsert(Di))=onoff(1);
        Offs{Di}(Cinsert(Di))=onoff(2)+Flat.p.nonoverlap*(Flat.p.umap.MinNumBuffSyl-Flat.p.umap.MinNumTimeSegments);
        ClustIDs{Di}(Cinsert(Di))=clust_no;
        Custs{Di}(:,Cinsert(Di))=[son ; son+diff(NbuffsC{clust_no}(:,syli)) ; i ; Cmax(CmaxSorti(i))];
      % Custs{Di}(:,Cinsert(Di))=[son ; son+diff(NbuffsC{clust_no}(:,syli)) ; 100*rankC/length(CosD{clust_no})];
        Tags{Di}(:,Cinsert(Di))=tagvec;
    end
    
    
    if verbose
        figure(43); clf; hold on;
        bonoff=func_numbuffs(onoff,Flat.p.nfft,Flat.p.nonoverlap); % RRR correct!!!
         
        % Omit
        S=Flat.DAT.data{Di}(:,bonoff(1):bonoff(2));
        h1=subplot('position',[1/(knN+4) 0.05 1/(knN+4) .9]);
        imagesc(S);axis xy; axis off
                
        sons=selsC{clust_no}(NbuffsC{clust_no}(1,sylsi));   soffs=sons+durs;
        Snn=[];
        for j=1:knN
            Di=Flat.Y.DATindex(sons(j));
            onoff=Flat.Y.indices_all([sons(j) soffs(j)]);
            bonoff=func_numbuffs(onoff,Flat.p.nfft,Flat.p.nonoverlap); % RRR correct!!!
            S=Flat.DAT.data{Di}(:,bonoff(1):bonoff(2));
            Snn=[Snn S 10*ones(128,1) ];
        end
        h2=subplot('position',[3/(knN+4) 0.05 (knN)/(knN+4) .9]);imagesc(Snn);axis xy; axis off
        pause
    end
    
    
    
end
Flat=func_write_imported_elements_to_Flat(Flat,Ons,Offs,ClustIDs,Custs,[],Tags,[],[]);

Flat.v.all_clust_mode=0;
Flat.v.plot_numbutton=1;
Flat.v.plot_tagboard=1;
Flat.v.curr_clust=2;
Flat.v.plot_ordered=3;

end