function   Flat=func_umap_mouseclick_control(Flat)
% this function creates Flat.umap.XC.Selectonset and Flat.umap.XC.Selectoffset


timeslice=Flat.p.umap.time;

[Selectonoffset,remove_elements,selectonoffset,onoffset]=...
    func_help_umap_SelectOnOffset(Flat,Flat.umap.XC.select,timeslice);

if ~isfield(Flat.umap.XC,'selectScatterCurrBlob')
    Flat.umap.XC.selectScatterCurrBlob=remove_elements;
else
    Flat.umap.XC.selectScatterCurrBlob=[Flat.umap.XC.selectScatterCurrBlob remove_elements];
end
Flat.umap.fcH.ScatterCurrBlob=[Flat.umap.fcH.ScatterCurrBlob Flat.umap.X.map_coord(:,remove_elements)];

if ~isfield(Flat.umap,'ContClickCount')
    Flat.umap.ContClickCount=1;
else
    Flat.umap.ContClickCount=Flat.umap.ContClickCount+1;
end
direction=-1+2*(timeslice>0);
slices=Flat.X.umap_time((Flat.p.umap.time<0)+1,Selectonoffset);


h = figure(32);
p = h.Position;

h2 = figure(44);
h2.Position = [p(1) p(2)- 0.75*p(4) p(3) p(4)/2];
clf;
binW=min(slices):max(slices);
if length(binW)>1
    [y,x]=hist(slices,binW); bar(x,y); 
%     set(gca,'Yscale','log'); set(gca,'Xscale','log');
    xlabel('Time slice'); ylabel('Count');
    set(gca,'box','off');
    drawnow
end
figure(32);

h=find(slices==sign(Flat.p.umap.time) | slices==sign(Flat.p.umap.time)+direction);
Slice_percentile=100*length(h)/length(Selectonoffset);

% fprintf('%d percent in chosen slice\n',Slice_percentile);
fprintf('%2.2f percent of chosen time slice in selected blob\n',Slice_percentile);

c=Flat.umap.ContClickCount;
Flat.umap.ContClick(c).ClustNo=Flat.umap.XC.ClustNo+1;
Flat.umap.ContClick(c).PIX=Flat.umap.PIX{Flat.umap.curr_clust};
Flat.umap.ContClick(c).timeslice=Flat.p.umap.time;
Flat.umap.ContClick(c).remove_elements=remove_elements';
Flat.umap.ContClick(c).Selectonoffset=Selectonoffset;
Flat.umap.ContClick(c).selectonoffset=selectonoffset;
Flat.umap.ContClick(c).onoffset=onoffset;
Flat.umap.ContClick(c).slices=slices;

end
