function Flat=func_umap_evalkey_o(Flat)
% XXX obsolete
% all syllables longer than twice the median are 'LONG' and thus divided
sel=Flat.umap.X0.select;
OnOffClust=Flat.X.umapOnOffClust(:,sel);


%% create ons and offs of syllables
offset_delay=Flat.p.umap.MinNumBuffSyl-1;
% 1=indicess_all, 2=offset, 3=clustID, 4=Datindex, 5=sel
onoff=zeros(5,length(sel));
C=0; % counter of processed syllables
last=1; % 0=on, 1=off
for i=1:length(sel)
    s=sel(i);
%     if C==376 && any(OnOffClust(:,i))
%         keyboard
%     end
    % collision, but offset agrees with previous onset
    % just write offset
    if OnOffClust(1,i) && OnOffClust(2,i) && last==0 && Flat.X.DATindex(s)==onoff(4,C) && OnOffClust(2,i)==onoff(3,C) % collision
        onoff(2,C)=Flat.X.indices_all(s)+offset_delay*Flat.p.nonoverlap;
        C=C+1;
        onoff(1,C)=Flat.X.indices_all(s); onoff(3,C)=OnOffClust(1,i);
        onoff(4,C)=Flat.X.DATindex(s);
        onoff(5,C)=s;
        last=0; continue
        %       last=1; continue
    end
    %     % collision, but last was offset
    %     % just take onset
    %     if OnOffClust(1,i) && OnOffClust(2,i) && last==1 && Flat.X.DATindex(s)==onoff(4,C) && OnOffClust(2,i)==onoff(3,C) % collision
    %         C=C+1;
    %         onoff(1,C)=Flat.X.indices_all(s); onoff(3,C)=OnOffClust(1,i);
    %         onoff(4,C)=Flat.X.DATindex(s);
    %         last=0; continue
    %     end
    %
    if OnOffClust(1,i)% && last==1% new onset
        C=C+1; onoff(1,C)=Flat.X.indices_all(s);
        onoff(3,C)=OnOffClust(1,i); onoff(4,C)=Flat.X.DATindex(s); onoff(5,C)=s;
        last=0;
 
    elseif OnOffClust(2,i) %&& last==0 % new offset
        insert_new=1;
        if C>0 && Flat.X.DATindex(s)==onoff(4,C) && OnOffClust(2,i)==onoff(3,C) % same file and cluster
            % check if connected
            if onoff(1,C) && length(onoff(1,C):Flat.X.indices_all(s))<Flat.p.nonoverlap*length(find(sel<=sel(i) & sel>=onoff(5,C))) % contiguous
                onoff(2,C)=Flat.X.indices_all(sel(i))+offset_delay*Flat.p.nonoverlap; %Flat.X.indices_all(s)+offset_delay*Flat.p.nonoverlap;
                onoff(3,C)=OnOffClust(2,i);
                last=1;
                insert_new=0;
            else
                insert_new=1;
            end
        end
        if insert_new % new syllable in same file
            C=C+1;
            onoff(2,C)=Flat.X.indices_all(s)+offset_delay*Flat.p.nonoverlap;
            onoff(4,C)=Flat.X.DATindex(s);
            onoff(3,C)=OnOffClust(2,i);
            last=1;
        end
    end
end
onoff=onoff(:,1:C);

%% compute typical (median) syllable duratons
[Flat,mediandurs,QQs]=func_help_umap_mediandurs(Flat,[],onoff);
Flat.umap.medianDurs=mediandurs;
MaxClust=max(onoff(3,:));
ClustMedZero=find(mediandurs(2:MaxClust)==0);


%% define new elements for insertion into Flat
L=length(Flat.DAT.filename);
tagvec=cell(length(Flat.p.tagnames),1); tagvec(:)={''}; tagvec{5}=['@' Flat.v.RMS];
Ons=cell(1,L); Offs=Ons; ClustIDs=Ons; Tags=Ons; 
Cinsert=zeros(1,L);
for i=1:C
    Di=onoff(4,i);
    Cinsert(Di)=Cinsert(Di)+1;
    if onoff(1,i)
        Ons{Di}(Cinsert(Di))=onoff(1,i);
    else
        Ons{Di}(Cinsert(Di))=max(1,onoff(2,i)-mediandurs(onoff(3,i)));  % correct
    end
        
    % now process offset
    if onoff(2,i)
        Offs{Di}(Cinsert(Di))=onoff(2,i);
    else
        Offs{Di}(Cinsert(Di))=min(onoff(1,i)+mediandurs(onoff(3,i)),Flat.DAT.eof(Di));  % correct
        %         if mediandurs(onoff(3,i))==0
        %             correct_offset=[Di Cinsert(Di)];
        %         end
    end
    ClustIDs{Di}(Cinsert(Di))=onoff(3,i);
    if Offs{Di}(Cinsert(Di))-Ons{Di}(Cinsert(Di))<0
        keyboard
    end
    
    if onoff(1,i) & onoff(2,i)
        onset_to_offset=onoff(2,i)-onoff(1,i);
        qq = QQs(:,onoff(3,i));
        %  onset_to_offsetLIM=qq(2)+3*max(1,(qq(3)-qq(2)));
        %    if onset_to_offset<onset_to_offsetLIM
        if onset_to_offset<2.5*qq(2) % 2.5 times the median
            tagvec{4}='OK';
        else
            tagvec{4}='OKL';
            % fix
            Offs{Di}(Cinsert(Di))=min(onoff(1,i)+mediandurs(onoff(3,i)),Flat.DAT.eof(Di));  % correct
            Tags{Di}(:,Cinsert(Di))=tagvec;
            
            if Offs{Di}(Cinsert(Di))-Ons{Di}(Cinsert(Di))<0
                keyboard
            end
            
            Cinsert(Di)=Cinsert(Di)+1;
            Offs{Di}(Cinsert(Di))=onoff(2,i);
            ClustIDs{Di}(Cinsert(Di))=onoff(3,i);
            Ons{Di}(Cinsert(Di))=max(1,onoff(2,i)-mediandurs(onoff(3,i)));  % correct
            
            if Offs{Di}(Cinsert(Di))-Ons{Di}(Cinsert(Di))<0
                keyboard
            end
            
        end
    elseif onoff(1,i)
        tagvec{4}='mOFF';
        if mediandurs(onoff(3,i))==0
            tagvec{4}='mOFF0';
        end
    elseif onoff(2,i)
        tagvec{4}='mON';
        if mediandurs(onoff(3,i))==0
            tagvec{4}='mON0';
        end
    else
        dsakfj
    end
    Tags{Di}(:,Cinsert(Di))=tagvec;
end

%% correct mediandur=0 syllables (added with only on or offset defined but not both)
Ms=Flat.p.nonoverlap*Flat.p.umap.MinNumBuffSyl;
%Ons2=Ons; Offs2=Offs; ClustIDs2=ClustIDs; Cinsert2=Cinsert; Tags2=Tags;
hbar = waitbar(0,'Please wait...');
tstart=now;
for i=1:L
    waitbar(i/L,hbar,['Correct short sylables ' datestr((now-tstart)/i*(L-i),'HH:MM:SS')]); drawnow
    remove=[];
    seli=[];
    for j=1:Cinsert(i)
        if ismember(ClustIDs{i}(j),ClustMedZero) &&  Offs{i}(j)-Ons{i}(j)<Ms % too short
            %                 if Ons{i}(j)+Ms>Flat.DAT.eof(i)
            %                     Offs{i}(j)=Flat.DAT.eof(i);
            %                 end
            
            if j<Cinsert(i) && Ons{i}(j)+Ms>Ons{i}(j+1)
                remove=[remove j];
            else
                if isempty(seli)
                        seli=sel(Flat.X.DATindex(sel)==i);
                end
                s1=find(Flat.X.indices_all(seli)>=Ons{i}(j),1,'first');
                s2=find(Flat.X.umap_time(2,seli(s1):seli(end))==-1,1,'first');
                i_tillend=Flat.X.indices_all(seli(s1):seli(end)); %%% here RRR
                %                 s2=find(i_tillend-i_tillend(1)>Flat.p.nonoverlap*(0:(length(i_tillend)-1)),1,'first');
                %    Offs{i}(j)=Ons{i}(j)+Ms;
                if isempty(s2)
                    Offs{i}(j)=i_tillend(end);
                else
                    Offs{i}(j)=i_tillend(s2);
                end
            end
        end
    end
    if ~isempty(remove)
        Ons{i}(remove)=[]; Offs{i}(remove)=[]; ClustIDs{i}(remove)=[]; Cinsert(i)=Cinsert(i)-length(remove); Tags{i}(:,remove)=[];
    end
end

%% insert Omissions into cluster 1
tagvec{4}='OMT';
selRMS=func_select_mic(Flat,Flat.p.channel_mic_display);
for i=1:L
    waitbar(i/L,hbar,['Define omissions ' datestr((now-tstart)/i*(L-i),'HH:MM:SS')]); drawnow
    sfile=selRMS(Flat.X.DATindex(selRMS)==i);
    s=logical(zeros(1,Flat.DAT.eof(i))); t=s;
    for j=1:length(sfile)
        s(Flat.X.indices_all(sfile(j)):(Flat.p.umap.MinNumBuffSyl*Flat.p.nonoverlap+Flat.X.indices_all(sfile(j))+Flat.X.data_off(sfile(j))))=1;
    end
    for j=1:Cinsert(i)
        t(Ons{i}(j):Offs{i}(j))=1;
    end
    s(find(t))=0;
    dfon=find(diff(s)==1); dfoff=find(diff(s)==-1);
    if length(dfoff)<length(dfon)
        dfoff=[dfoff length(s)];
    end
    for j=1:length(dfon)
        if dfoff(j)-dfon(j)>Flat.p.umap.MinNumBuffSyl*Flat.p.nonoverlap
            Cinsert(i)=Cinsert(i)+1;
            ClustIDs{i}(Cinsert(i))=1;
            Ons{i}(Cinsert(i))=dfon(j);
            Offs{i}(Cinsert(i))=dfoff(j);
            Tags{i}(:,Cinsert(i))=tagvec;
        end
    end
end
close(hbar);

% remove RMSfilt elements
[Flat,sels,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS});
if ~isempty(sels)
    fprintf('Now removing old %s elements\n',Flat.v.RMS);
    Flat=func_remove_elements(Flat,sels,0,1);
end

% remove old umap elements
[Flat,sels,board] = func_select_tagboard(Flat,[],'activator',{['@' Flat.v.RMS]});
if ~isempty(sels)
    disp('Now removing old UMAP elements');
    Flat=func_remove_elements(Flat,sels,0,1);
end
Flat=func_write_imported_elements_to_Flat(Flat,Ons,Offs,ClustIDs,[],[],Tags,[],[]);

% renaming archive
if ~strcmp(Flat.v.RMS(1),'@')
    Flat.umap.RMSold=Flat.v.RMS;
end
Flat.v.RMS=['@' Flat.v.RMS]; Flat.p.Channel_syl_tagstring{Flat.p.channel_mic_display}=Flat.v.RMS;

%Flat=func_umap_overlap_stat(Flat);

Flat.v.all_clust_mode=0;
Flat.v.plot_numbutton=1;
Flat.v.plot_tagboard=1;
Flat.v.curr_clust=2;
Flat.v.plot_ordered=3;
end