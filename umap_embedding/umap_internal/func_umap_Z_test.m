function [Flat,OK]=func_umap_Z_test(Flat)
%% should probably not run this function when junk or dealer cluster not empty
OK=1;

[Flat,sels,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS});
h=find(Flat.X.data_off(sels)==0);
if ~isempty(h)
    fprintf('%d syllables with zero duration - now removing\n',length(h));
    Flat=func_remove_elements(Flat,sels(h),0,1);
    [Flat,sels,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS});
end
%Flat.X.indices_all(sels)=max(Flat.X.indices_all(sels),Flat.p.nfft);
h=find(Flat.X.custom(2,sels)==0 | Flat.X.custom(1,sels)==0);
if ~isempty(h)
    fprintf('%d syllables with onset/offset problems - now removing, press a key\n',length(h));
    pause
    Flat=func_remove_elements(Flat,sels(h),0,1);
    [Flat,sels,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS});
end

c=0; stencil=0*Flat.Z.stencil; sels_broken=[];
%  if ~isequal(Flat.X.indices_all(sels(i)),Flat.Z.indices_all(Flat.X.custom(1,sels(i))))
h=find(Flat.X.indices_all(sels)~=Flat.Z.indices_all(Flat.X.custom(1,sels)));
if ~isempty(h)
    fprintf('Inconsistency in your indices_all (onset) in %d vocalization(s)\n',length(h));
    keyboard
    % can try fixing with
    %[Flat,sels,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS});h=find(Flat.X.indices_all(sels)~=Flat.Z.indices_all(Flat.X.custom(1,sels)));Flat=func_remove_elements(Flat,sels(h),0,1);
end

h=find(Flat.X.indices_all(sels)+Flat.X.data_off(sels)~=Flat.Z.indices_all(Flat.X.custom(2,sels)));
if ~isempty(h)
    fprintf('Inconsistency in your indices_all (offset) in %d vocalization(s)\n',length(h));
    keyboard
    Flat.X.data_off(sels(h))=double(Flat.Z.indices_all(Flat.X.custom(2,sels(h))))-Flat.X.indices_all(sels(h));
    disp('fixed')
    % can try fixing with
    %[Flat,sels,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS});h=find(Flat.X.indices_all(sels)+Flat.X.data_off(sels)~=Flat.Z.indices_all(Flat.X.custom(2,sels)));Flat=func_remove_elements(Flat,sels(h),0,1);
end

if isfield(Flat.p.umap.brute_force,'buff_span')
    buff_span=Flat.p.umap.brute_force.buff_span;
else
    buff_span=2;
end
for i=1:length(sels)
    if any(Flat.Z.stencil(Flat.X.custom(1,sels(i)):Flat.X.custom(2,sels(i)))==0) ||  length(unique(Flat.Z.stencil(Flat.X.custom(1,sels(i)):Flat.X.custom(2,sels(i)))))>1 ||   max(diff(Flat.Z.buffer(Flat.X.custom(1,sels(i)):Flat.X.custom(2,sels(i)))))>buff_span-1
        c=c+1; sels_broken=[sels_broken sels(i)];
        %       max(diff(Flat.Z.buffer(Flat.X.custom(1,sels(i)):Flat.X.custom(2,sels(i)))))
    end
    stencil(Flat.X.custom(1,sels(i)):Flat.X.custom(2,sels(i)))=Flat.X.clust_ID(sels(i));
end
fprintf('%d syllables with stencil inconsistency\n',c);
if c>0
    y=input('Fix ? (yes=1) ');
    if y==1
        for i=1:length(sels_broken)
            Flat.Z.stencil(Flat.X.custom(1,sels_broken(i)):Flat.X.custom(2,sels_broken(i)))=0;
        end
        Flat=func_remove_elements(Flat,sels_broken,0,1);
    else
    end
    disp('Need to run again.');
    OK=0;
    return
end
if isequal(stencil,Flat.Z.stencil)
    disp('Stencil correctly reconstructed');
else
    disp('stencil wrongly reconstructed');
    h=find(stencil~=Flat.Z.stencil);
    %   keyboard
    y=input('Fix (yes=1)? ');
    if y==1
        Flat.Z.stencil(h)=0;
    else
        disp('Not fixed');
    end
    disp('Need to run again');
    OK=0;
    return
end
end