function Flat=func_umap_overlap_correct(Flat,verbose)
% this function assumes that 'OK' syllables have been inspected and
% corrected
%%
if nargin<2
    verbose=0;
end
maxN=400;

%nbuff=Flat.spec.numbuffs{5}(1);

Flat=func_help_tagname_add(Flat,'UMAP',0,'-');

[Flat,selsOK,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS},'Mark','OK');
[Flat,sels_missON,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS},'Mark','mON');
[Flat,sels_missOFF,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS},'Mark',{'mOFF','mOFF0'});

% remove far outliers by euclidean distance to OK syllables
nclust=max(Flat.X.clust_ID(selsOK));
DsAll=cell(1,nclust); SOKAll=cell(1,nclust);
remove_elements=[];
for i=2:nclust
    sOK=selsOK(Flat.X.clust_ID(selsOK)==i);
    if isempty(selsOK) || isempty(sOK)
        continue
    end
    maxNe=min(maxN,length(sOK));
    r=randperm(length(sOK)); sOK=sOK(r(1:maxNe));
    %SOK=func_extract_data(Flat,5,sOK);
    SOK=Flat.X.PCcoeffs(:,sOK);
    DS=dist(SOK); DS=sum(DS)/(size(DS,1)-1);
    %Ds=triu(dist(SOK)); Ds=Ds(find(Ds));
    Ds=DS;
    SOKAll{i}=SOK; DsAll{i}=Ds;
    maxDS=max(Ds);
    figure(39); clf;  [y,x]=hist(Ds);
    
    [Flat,sels_missON_out]=func_help_overlap_correct_this(Flat,i,x,y,maxDS,sels_missON,SOK,'mON','ro:',1);
    [Flat,sels_missOFF_out]=func_help_overlap_correct_this(Flat,i,x,y,maxDS,sels_missOFF,SOK,'mOFF','go:',2);
    
    sels_missON_out_and_overlap=sels_missON_out(ismember(Flat.Tags(2,sels_missON_out),'LargeOVLP'));
    Flat.Tags(2,sels_missON_out_and_overlap)={'OVLP&Out'};
    remove_elements=[remove_elements sels_missON_out_and_overlap];
    drawnow
    if verbose
        pause
    end
end
Flat.umap.SOKAll=SOKAll; Flat.umap.DsAll=DsAll;
Flat.v.tagboard0_recompute=1;

% eliminate low-rank bifurcations
%[Flat,sels,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS},'is_hidden',{'LargeOVLP'});
[Flat,sels,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS});
for s=1:length(sels)-1
    i=sels(s); j=sels(s+1);
    % same file and starts early
    if Flat.X.DATindex(i)==Flat.X.DATindex(j) && Flat.X.indices_all(j)<Flat.X.indices_all(i)+Flat.X.data_off(i)%Flat.p.umap.MinNumBuffSyl*Flat.p.nonoverlap
        %         if strcmp(Flat.Tags{Flat.v.tags.UMAP,i},'LowR') % already low
        %             Flat.Tags{Flat.v.tags.UMAP,j}='HighR';
        %         elseif  strcmp(Flat.Tags{Flat.v.tags.UMAP,i},'HighR')
        %             Flat.Tags{Flat.v.tags.UMAP,j}='LowR';
        %         else
        r1=func_umap_rank_in_OK(Flat,i);   r2=func_umap_rank_in_OK(Flat,j);
        if r1<r2
            Flat.Tags(Flat.v.tags.UMAP,j)={'LowR'}; Flat.Tags(Flat.v.tags.UMAP,i)={'HighR'};
        else
            Flat.Tags(Flat.v.tags.UMAP,i)={'LowR'}; Flat.Tags(Flat.v.tags.UMAP,j)={'HighR'};
        end
    end
    %    end
    Flat.v.tagboard0_recompute=1;
end
disp('Done');

end

function [Flat,smON_out]=func_help_overlap_correct_this(Flat,clustNo,x,y,maxDs,sels_missON,SOK,str,col_str,subNo)
smON=sels_missON(Flat.X.clust_ID(sels_missON)==clustNo);
%SmON=func_extract_data(Flat,5,smON);
SmON=Flat.X.PCcoeffs(:,smON);
DsON=dist(SOK',SmON); DsON=mean(DsON);

which_out=find(DsON>maxDs);
smON_out=smON(which_out);
Flat.Tags(Flat.v.tags.UMAP,smON_out)={'Out'};
if ~isempty(which_out)
    fprintf('Clust %d, %s: %d syllables out of range\n',clustNo,str,length(which_out));
end

Flat.X.custom(smON)=DsON;
subplot(2,1,subNo); plot(x,y/sum(y),'ko-','linewidth',2); hold on;
hold on; [y2,x2]=hist(DsON); plot(x2,y2/sum(y2),col_str,'linewidth',2);
set(gca,'box','off'); title(['Clust ' num2str(clustNo) ':  ' str]);
xlabel('Distance'); ylabel('Count');
end

function r=func_umap_rank_in_OK(Flat,i)
cl=Flat.X.clust_ID(i);
DsON=dist(Flat.umap.SOKAll{cl}',Flat.X.PCcoeffs(:,i)); DsON=mean(DsON);
Dsort=sort(Flat.umap.DsAll{cl},'ascend');
r=find(DsON<Dsort,1,'first');
r=r/length(Dsort);
end
