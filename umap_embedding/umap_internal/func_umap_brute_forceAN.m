function Flat=func_umap_brute_forceAN(Flat,verbose)
if nargin<2
    verbose=1;
end

% nDim=400; % number of principal components
% do_PCcoeffs=0; % COs distance of either Flat.X.PCcoeffs or Flat.DAT.data
% Flat.p.umap.brute_force.nDim=nDim;
% Flat.p.umap.brute_force.doPCcoeffs=do_PCcoeffs;

distM=2;  % 1= Cosince Distance,  2=Euler

if verbose
    %% do_clusts
    fprintf('\nCompute distance matrix (Cosm) of which clusters?\n');
    sclust=input(' Set a value (e.g. 3 5, empty=all): ','s');
else
    sclust='';
end

%% nP
if isfield(Flat.p.umap,'brute_force') & isfield(Flat.p.umap.brute_force,'nP')
    nP=Flat.p.umap.brute_force.nP;
else
    nP=100; % max number of syllables per clust
end
if verbose
    fprintf('\nMaximum number of templates per syllable: %d.\n',nP);
    s=input(' Set a value (e.g. 1000, empty=keep): ');
    if ~isempty(s)
        nP=s;
    end
end
Flat.p.umap.brute_force.nP=nP;

%% nL
if isfield(Flat.p.umap,'brute_force') & isfield(Flat.p.umap.brute_force,'nbuffsL')
    nbuffsL=Flat.p.umap.brute_force.nbuffsL;
else
    nbuffsL=3;
    if verbose
        fprintf('\nStart of brute-force search window %d buffers before syllable onset.\n',nbuffsL);
        s=input(' Set a value (e.g. 3, empty=keep): ');
        if ~isempty(s)
            nbuffsL=s;
        end
    end
end
Flat.p.umap.brute_force.nbuffsL=nbuffsL;

%% nR
if isfield(Flat.p.umap,'brute_force') & isfield(Flat.p.umap.brute_force,'nbuffsR')
    nbuffsR=Flat.p.umap.brute_force.nbuffsR;
else
    nbuffsR=3;
    if verbose
        fprintf('\nEnd of brute-force search window %d buffers after syllable offset.\n',nbuffsR);
        s=input(' Set a value (e.g. 3, empty=keep): ');
        if ~isempty(s)
            nbuffsR=s;
        end
    end
end
Flat.p.umap.brute_force.nbuffsR=nbuffsR;

%% noiseClusts
if isfield(Flat.p.umap,'brute_force') & isfield(Flat.p.umap.brute_force,'NoiseClusts')
    NoiseClusts=Flat.p.umap.brute_force.NoiseClusts;
else
    NoiseClusts=[];
end
if verbose
    fprintf('\nCurrent noise clusters: %s\n',num2str(NoiseClusts));
    s=input('Define your noise clusters, with a space in between (e.g. 4 11, empty=keep or 0=none): ','s');
    if ~isempty(s)
        if strcmp(s,'0')
            NoiseClusts=[];
        else
            NoiseClusts=str2num(s);
        end
    end
end
Flat.p.umap.brute_force.NoiseClusts=NoiseClusts;


%% check if new syllable
new_syl=0;
Flat.v.tagboard0_recompute=1;
[Flat,sels,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS});
Flat.Tags(2,sels)={'[]'};
nclusts=max(Flat.X.clust_ID(sels));
if ~isempty(sclust) & isfield(Flat.umap,'brute_force') & isfield(Flat.umap.brute_force,'nclusts') & Flat.umap.brute_force.nclusts<str2num(sclust)
    disp('New syllable! - now running');
    sclust=num2str(Flat.umap.brute_force.nclusts+1); % forcing to run new syllable
    new_syl=1;
end


%%  delete elements in Junk cluster 1 and dealer cluster nclusts+1
sels1=sels(Flat.X.clust_ID(sels)==1);
Flat.v.tagboard0_recompute=1;
if isfield(Flat,'umap') && isfield(Flat.umap,'brute_force') && isfield(Flat.umap.brute_force,'Clast')
    [Flat,selsD,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS},'Mark',{Flat.umap.brute_force.Clast});
else
    selsD=[];
end
%selsD=sels(ismember(Flat.Tags(4,sels),Flat.umap.brute_force.Clast));
selsJD=union(sels1,selsD);
if ~isempty(selsJD)
    fprintf('Now deleting %d syllables in junk and dealer clusters\n',length(selsJD));
    Flat.v.tagboard0_recompute=1;
    Flat=func_remove_elements(Flat,selsJD,0,1);
    if ~new_syl
        [Flat,OK]=func_umap_Z_test(Flat);
        if ~OK
            disp('Run brute_forceAN again');
            return
        end
    end
end

%% new syl write to dealer cluster
if new_syl
    Flat.v.tagboard0_recompute=1;
    [Flat,sels,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS});
    selsN=sels(Flat.X.clust_ID(sels)>Flat.umap.brute_force.nclusts+1);
    for i=1:length(selsN)
        Flat.X.clust_ID(selsN(i))=Flat.umap.brute_force.nclusts+1;
        Flat.Z.stencil(Flat.X.custom(1,selsN(i)):Flat.X.custom(2,selsN(i)))=Flat.umap.brute_force.nclusts+1;
    end
    Flat.v.tagboard0_recompute=1;
end

%% sels, nclust, clusts
[Flat,sels,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS});
nclusts=max(Flat.X.clust_ID(sels));
Flat.umap.brute_force.nclusts=nclusts;
clusts=setdiff(2:nclusts,Flat.p.umap.brute_force.NoiseClusts);
Flat.p.umap.brute_force.clusts=clusts;

if isempty(sclust)
    clusts_do=clusts;
    u=unique(Flat.X.clust_ID(sels));
    h=setdiff(clusts_do,u);
    if ~isempty(h)
        fprintf('cluster %d is empty - please fix\n',h);
        return
    end
else
    clusts_do=str2num(sclust);
end

try
    g=gpuDevice;
catch
    g=[];
end
Flat.p.umap.GPUdevice=~isempty(g);

%% if not first time
if ~isequal(clusts_do,clusts) & isfield(Flat.umap,'brute_force') & isfield(Flat.umap.brute_force,'NbuffsC')
    NbuffsC=Flat.umap.brute_force.NbuffsC;
    selsC=Flat.umap.brute_force.selsC;
    DursC=Flat.umap.brute_force.DursC;
    PCcoeffs=Flat.umap.brute_force.PCcoeffs;
else
    NbuffsC=cell(1,nclusts); selsC=NbuffsC; PCcoeffs=NbuffsC; NormPAll=NbuffsC; NormC=NbuffsC; DursC=NbuffsC; Nk=NbuffsC;
end

%% Extract PC data of OK syllables: Pccoeffs and NormOK (NbuffsC)
for k=clusts_do
    selsCkh=find(Flat.Z.stencil==k);
    [selsCk,NbuffsC{k},PCcoeffs{k}]=func_umap_help_Nbuffs(Flat,selsCkh,nP,0);
    selsC{k}=selsCk;
    DursC{k}=diff(selsCk(1:2,:));
    Nk{k}=size(NbuffsC{k},2); % number of syllables
    NormPAll{k}=sum(PCcoeffs{k}.*PCcoeffs{k});
    NormC{k}=zeros(1,Nk{k});
    for i=1:Nk{k}
        NormC{k}(i)=sum(NormPAll{k}(NbuffsC{k}(1,i):NbuffsC{k}(2,i)));
    end
    if distM==1 % cosine
        NormC{k}=sqrt(NormC{k});
    end
end
Flat.umap.brute_force.NbuffsC(clusts_do)=NbuffsC(clusts_do);
Flat.umap.brute_force.selsC(clusts_do)=selsC(clusts_do);
Flat.umap.brute_force.DursC(clusts_do)=DursC(clusts_do);
Flat.umap.brute_force.PCcoeffs(clusts_do)=PCcoeffs(clusts_do);

minDurCk=cellfun(@min,DursC,'UniformOutput',false);
minDurC=min([minDurCk{clusts}]);

%% adapt Cosm0 if not first time
if ~isequal(clusts_do,clusts)  & isfield(Flat.umap,'brute_force') & isfield(Flat.umap.brute_force,'selsM0')
    %    selsMM=find(Flat.Z.stencil==0);
    selsM0=Flat.umap.brute_force.selsM0;
    NbuffsM0=Flat.umap.brute_force.NbuffsM0;
    PCcoeffsM=Flat.umap.brute_force.PCcoeffsM;
    
    %   [s01,s01i]=sort(selsM0(1,:),'ascend');
    
    Cosm0=Flat.umap.brute_force.Cosm0;
    %   Cosm=Cosm0;
    
    %     for k=setdiff(clusts,clusts_do)
    %         minDurCk0=min([minDurCk{k}]);
    %         [selsM,NbuffsM0,PCcoeffsM]=func_umap_help_Nbuffs(Flat,selsMM,[],minDurCk0);
    %         [s1,s1i]=sort(selsM(1,:),'ascend'); % sort by time
    %         s2=selsM(2,s1i);
    %         N=length(selsM);
    %
    %         c0=1; % index into Cosm0
    %         for i=1:N
    %             i0=c0-1+find(s1(i)>=s01(c0:end),1,'last');
    %             c0=i0;
    %             i0=s01i(i0);
    %             don=1+s1(i)-s01(c0);
    %             durm=s2(i)-s1(i)-minDurCk0;
    %             Cosm{k,s1i(i)}=Cosm0{k,i0}(:,don:don+durm);
    %         end
    %     end
    %     selsM0=selsM;
else
    selsM0=find(Flat.Z.stencil==0);
    [selsM0,NbuffsM0,PCcoeffsM]=func_umap_help_Nbuffs(Flat,selsM0,[],minDurC);
    Cosm0=cell(nclusts,size(NbuffsM0,2));
end

%% Define OMIT regions
DursM=diff(selsM0(1:2,:));
NormMAll=sum(PCcoeffsM.*PCcoeffsM);
Flat.umap.brute_force.NbuffsM0=NbuffsM0;
Flat.umap.brute_force.selsM0=selsM0;
Flat.umap.brute_force.PCcoeffsM=PCcoeffsM;

PM=PCcoeffsM; NM=NbuffsM0;
if Flat.p.umap.GPUdevice
    PM=gpuArray(PM);
end
No=size(NbuffsM0,2); %  number of OMIT regions
Flat.umap.brute_force.No=No;

%% Search all omitted regions
hbar = waitbar(0,'Please wait...');

tgpu=0; tcpu=0;
for k=clusts_do
    P=PCcoeffs{k}; N=NbuffsC{k}; nk=size(N,2);
    if Flat.p.umap.GPUdevice
        P=gpuArray(P);
    end
    for j=1:No
        Cosm0{k,j}=-1e11*ones(nk,1+DursM(j)-minDurCk{k}); % multiplier should be smaller than -knN
    end
    tstart=now;
    for i=1:nk  % loop over syllables
        waitbar(i/nk,hbar,['Process OMIT clust ' num2str(k) ', ' datestr((now-tstart)/i*(nk-i),'HH:MM:SS')]); drawnow
        if i==1
            i0=find(DursM>=DursC{k}(i),1,'first');
        else
            i0=i0-1+find(DursM(i0:end)>=DursC{k}(i),1,'first');
        end
        %       I0s(k,i)=i0;
        tic
        A=PCcoeffs{k}(:,N(1,i):N(2,i));
        B=PM(:,NM(1,i0):end);
        d=(conv2(B,A(end:-1:1,end:-1:1),'valid'));
        if Flat.p.umap.GPUdevice
            d=gather(d);
        end
        tgpu=tgpu+toc;
        tic
        for j=i0:No
            on=NM(1,j)-NM(1,i0)+1;
            don=NM(2,j)-NM(1,j)-(N(2,i)-N(1,i));
            h=zeros(1,don+1);
            h(1)=(sum(NormMAll(NM(1,j):NM(1,j)+N(2,i)-N(1,i))));
            for ll=2:length(h)
                onl=NM(1,j)-2+ll;
                h(ll)=h(ll-1)-NormMAll(onl)+NormMAll(onl+1+N(2,i)-N(1,i));
            end
            switch distM
                case 1 % Cosine
                    Cosm0{k,j}(i,1:1+don)=d(on:on+don)./sqrt(h)/NormC{k}(i);
                case 2 % Euler
                    Cosm0{k,j}(i,1:1+don)=2*d(on:on+don)-h-NormC{k}(i);
            end
        end
        %         for j=i0:No
        %             [d1,d2]=maxk(Cosm0{k,j},10);
        %             d3=1:100:100*size(d2,2);
        %             Cosm0{k,j}(d2+d3)=-1e11;
        %             %             for ll=1:size(d2,2), Cosm0{k,j}(d2(:,ll),ll)=-1e11; end
        %         end
        tcpu=tcpu+toc;
    end
end
close(hbar);
fprintf('tGPU=%f, tCPU=%f\n' ,tgpu,tcpu);

for i=clusts_do
    Flat.umap.brute_force.Cosm0(i,1:size(Cosm0,2))=Cosm0(i,:);
    if size(Flat.umap.brute_force.Cosm0,2)>size(Cosm0,2)
        Flat.umap.brute_force.Cosm0(i,size(Cosm0,2)+1:end)={[]};
    end
end
SylOrder=cellfun(@mean,DursC(clusts)); %1= mean duration, 2=sorted index, 3=done

[SylOrder(1,:),SylOrder(2,:)]=sort(SylOrder,'descend');
SylOrder(2,:)=clusts(SylOrder(2,:));
SylOrder(3,:)=0;
h=find(isnan(SylOrder(1,:)));
if ~isempty(h)
    SylOrder(3,h)=1;
end
if isfield(Flat.umap.brute_force,'SylOrder') % transfer old jobs done
    donei=find(Flat.umap.brute_force.SylOrder(3,:));
    for i=donei
        h=find(SylOrder(2,:)==Flat.umap.brute_force.SylOrder(2,i));
        if ~ismember(SylOrder(2,i),clusts_do)
            SylOrder(3,h)=Flat.umap.brute_force.SylOrder(3,i);
        end
    end
end
syldo=1+find(SylOrder(3,2:end)==0,1,'first');
if isempty(syldo)
    syldo=1;
end
Flat.v.curr_clust=SylOrder(2,syldo);
SylOrder(3,syldo)=1;
Flat.p.umap.brute_force.finalized=0;

Flat=func_umap_brute_forcekN(Flat);
Flat.umap.brute_force.SylOrder=SylOrder;

Flat.v.umap.track_stencil=1;
Flat.p.display_marks=1;
Flat.v.tag_rename_i=2;
Flat.v.tag_rename_string='1';
end