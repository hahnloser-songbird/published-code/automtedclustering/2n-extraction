function [Flat,DATindex_orig]=func_umap_extract_files_remove(Flat,files,field,exception,DATindex_orig0)
if nargin<5
    DATindex_orig0=[];
end
hZ=find(ismember(Flat.(field).DATindex,files));
fnZ=fieldnames(Flat.(field));
if nargin==4 || ~isempty(exception)
    fnZ=setdiff(fnZ,exception);
end
for j=1:length(fnZ)
    Flat.(field).(fnZ{j})(:,hZ)=[];
end

DATindex_orig=unique(Flat.(field).DATindex);
h=1+[0 find(diff(Flat.(field).DATindex)) length(Flat.(field).DATindex)-1];
for i=1:length(h)-1
    if isempty(DATindex_orig0)
    Flat.(field).DATindex(h(i):h(i+1))=i;
    else
    Flat.(field).DATindex(h(i):h(i+1))=find(DATindex_orig(i)==DATindex_orig0);
    end
end
end
