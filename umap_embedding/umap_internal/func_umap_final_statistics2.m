%function Flat=func_umap_final_statistics2(Flat)
% this function computes the distribution of onset & offset times of
% vocalizations relative to threshold crossings
clear birds
% first FP, then gold standard
%birds{1}.name='tutMeta'; birds{1}.archDir={'ArchivesFP','ArchivesGS'}; birds{1}.arch={'@g17y2','@g17y2xy'};

% birds{1}.name='tutMeta'; birds{1}.archDir={'ArchivesFP','ArchivesFP'}; birds{1}.arch={'@g17y2','@g17y2FP'};
% birds{2}.name='ComplexSongZai'; birds{2}.archDir={'Archives','Archives'}; birds{2}.arch={'@2014-04-29','@2014-04-29xy'};
% birds{3}.name='ComplexSongZai'; birds{3}.archDir={'Archives','Archives'}; birds{3}.arch={'@2014-08-26-0230','@2014-08-26-0230xy'};
% %birds{1}.name='tutMeta'; birds{1}.archDir={'ArchivesFP','ArchivesFP'}; birds{1}.arch={'@g4p5','@g4p5FP2'};
% %birds{1}.name='b8p2male-b10o15female_aligned'; birds{1}.archDir={'ArchivesFP','ArchivesRich'}; birds{1}.arch={'@2018-08-22','@2018-08-22xy'};%,'@g17y2xy','@g19o3Trainxy','@g19o10Trainxy'};
% birds{4}.name='b8p2male-b10o15female_aligned'; birds{4}.archDir={'ArchivesFP','ArchivesFP'}; birds{4}.arch={'@2018-08-22','@2018-08-22FP'};%,'@g17y2xy','@g19o3Trainxy','@g19o10Trainxy'};


birds{1}.name='2NPaper';  birds{1}.archDir={'Archives','Archives'}; birds{1}.arch={'@g17y2','@g17y2FP'};
birds(2:5)=birds(1); birds{2}.arch={'@2014-04-29','@2014-04-29xy'};
birds{3}.arch={'@2014-08-26-0230','@2014-08-26-0230xy'}; birds{4}.arch={'@2018-08-22','@2018-08-22FP'};
birds{5}.arch={'@Meta','#META'};
%paths=evalin('base','paths');

figure(3); clf;     hss{1}=loglog([10 10],[100 100],'k--','linewidth',2); hold on;
figure(4); clf;     hss{2}=loglog([10 10],[100 100],'k--','linewidth',2); hold on;


titles={'Onset','Offset'};
doIs=[1 2]; onoffs=[1 -1];cols={'k','r'};
% Gold standard
sumSlices=cell(length(titles),2); sumSlices(:)={[]};
    slicesAll=cell(length(birds),2,2); % 1 = bird, 2 = tit, 3 = arch (2=GS)
for b=1:length(birds)
    for tit=1:length(titles)
        figure(tit); clf;
        hs=loglog([10 10],[100 100],'k--','linewidth',2); hold on;
        
        for a=1:length(birds{b}.arch)
            name=[birds{b}.name '  ' birds{b}.arch{a}];
            str=[paths.data_path birds{b}.name directoryDelimiter 'ArchiveCollection' directoryDelimiter birds{b}.archDir{a} directoryDelimiter];
            strA=[str birds{b}.arch{a}];
            strD=[str 'Decoupled' directoryDelimiter birds{b}.arch{a}(2:end) '=DAT_data'];
            load(strA); Flat=Flath; clear Flath;
            %h=load(strD);
            %Flat.DAT.data=h.DAT.data;
            %fprintf('%s: ',Flat.name)
            
            [Flat,sels,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS});
            lags=zeros(1,length(sels));
            for i=1:length(sels)
                z=Flat.X.custom(doIs(tit),sels(i))-1+doIs(tit);%+appendixB;
                t=0;
                while z>1 && z<=length(Flat.Z.buffer) && diff(Flat.Z.buffer(z-1:z))==1
                    t=t+1;
                    z=z-onoffs(tit);
                end
                lags(i)=t;
            end
            [y,x]=hist(lags,1:1001);  x=x(1:1000); y=y(1:1000);
            slices=y/sum(y);
            slices=slices*100;
            slicesAll{b,tit,a}=cumsum(slices);
            xs=1000/Flat.scanrate*Flat.p.nonoverlap*x;
            if isempty(sumSlices{tit,a})
                sumSlices{tit,a}=slicesAll{b,tit,a};
            else
                sumSlices{tit,a}=sumSlices{tit,a}+slicesAll{b,tit,a};
            end
            loglog(xs,slicesAll{b,tit,a},cols{a},'linewidth',2); hold on %[y,x]=hist(slice-H(i).timeslice,100); semilogy(x,y);
            xlabel('Time lag to threshold crossing (ms)'); ylabel('Percent Vocalizations');
        end
        set(hs,'xdata',[min(xs) max(xs)]);
        title([name ' ' titles{tit}]);
        axis tight; set(gca,'box','off','fontsize',20,'xtick',[10 100 1000]);
        legend({'amplitude threshold','2N extraction','gold standard'},'location','southeast');
        print([name ' ' titles{tit}],'-dsvg');
    end
    pause
end
%%
for tit=1:2
    figure(2+tit);
    loglog(xs,sumSlices{tit,1}/length(birds),cols{1},'linewidth',2);%[y,x]=hist(slice-H(i).timeslice,100); semilogy(x,y);
    loglog(xs,sumSlices{tit,2}/length(birds),cols{2},'linewidth',2); %[y,x]=hist(slice-H(i).timeslice,100); semilogy(x,y);
    xlabel('Time lag to threshold crossing (ms)'); ylabel('Percent Vocalizations');
    title(titles{tit});
    set(hss{tit},'xdata',[min(xs) max(xs)]);
    
    axis tight;set(gca,'box','off','fontsize',20,'xtick',[10 100 1000]);
    legend({'amplitude threshold','2N extraction','gold standard'},'location','southeast');
     print(titles{tit},'-dsvg');
end
%%



%fprintf('dt=0: %.2f, dt>0: %.2f\n',slices(1),sum(slices(2:end)));

%hold on; loglog(1000/Flat.scanrate*Flat.p.nonoverlap*x(1:100),cumsum(slices),'r','linewidth',2);
%Y=[Y;cumsum(slices)];
%x=x(1:100);

%end