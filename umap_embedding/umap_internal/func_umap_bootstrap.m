function Flat=func_umap_bootstrap(Flat,verbose)
if nargin<2
    verbose=1;
end

% this function prepares an arbitrary archive for brute force search
% go to Figure 16 and toggle DATmasknoise off (press 'n'), set the smoothing ('+/-') and set the
% threshold (press 'g', and possibly up/down-arrow)

fcH=evalin('base','fcH');
if verbose
if ~isfield(fcH,'hf_import_DAT') || ~isfield(fcH.hf_import_DAT,'fig') || ~ishandle(fcH.hf_import_DAT.fig)
    disp('First open figure 16, turn off noise masking (if needed) and set threshold!  Press enter');
    pause
    return
end
end
%orig_mic=Flat.p.channel_mic_display;
%winS=Flat.p.syllable.X(k).numbuffs;
Flat.p.umap.MinNumTimeSegments=5;
fprintf('Minimum number of buffers per vocalization: %d - press a key\n',Flat.p.umap.MinNumTimeSegments)
if verbose
pause
end
[Flat,sels,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS});
%Flat.X.custom(1:2,sels)=0;
%h=find(Flat.X.custom(1,sels)); Flat.X.custom(1,sels(h))=-1;
%h=find(Flat.X.custom(2,sels)); Flat.X.custom(2,sels(h))=-1;
h1=find(Flat.X.clust_ID(sels)==1);
if ~isempty(h1)
    disp('Now deleting elements in cluster 1, press a key to continue');
    pause
    Flat=func_remove_elements(Flat,sels(h1),0,1);
    [Flat,sels,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS});
end

% discretize indices_all, make at least Flat.p.nfft
Flat.X.indices_all(sels)=max(Flat.p.nfft,ceil(Flat.X.indices_all(sels)/Flat.p.nonoverlap)*Flat.p.nonoverlap);
Flat.X.data_off(sels)=ceil(Flat.X.data_off(sels)/Flat.p.nonoverlap)*Flat.p.nonoverlap;
Flat.X.custom=zeros(2,Flat.num_Tags);
% get rid of elements if duration nonpositive or overlap
[Flat,sels]=func_syl_check_overlap(Flat,sels);


% initialize stenciel margin stuff
clear Z
P=Flat.p.syllable; S=Flat.v.import_DAT;
k=P.imicsN;
thet=P.(P.method)(k).thets;
P.imicsN=k;
P.els=max(floor(P.(P.method)(k).low/Flat.scanrate*Flat.p.nfft),1):floor(P.(P.method)(k).high/Flat.scanrate*Flat.p.nfft);

t0=Flat.DAT.timestamp(1);
c=0; %stencilc=0;
L=length(Flat.DAT.filename);
seli=1;
for i=1:L % loop over files
    S.fileN=i;
    % from sound intensity
    sd=func_pseudo_RMS_from_spec(Flat,S,P,1,k);
   % [ons,offs,maxs]=func_help_ons_offs(sd,thet);
   % ons=round(ons); offs=round(offs);
    
    %from syllables
    selsF=sels(Flat.X.DATindex(sels)==i);
 %   sd=zeros(1+ceil((Flat.DAT.eof(i)-Flat.p.nfft)/Flat.p.nonoverlap),1);
    for jj=1:length(selsF)
        on=func_numbuffs(Flat.X.indices_all(selsF(jj)),Flat.p.nfft,Flat.p.nonoverlap);
        off=func_numbuffs(Flat.X.indices_all(selsF(jj))+Flat.X.data_off(selsF(jj)),Flat.p.nfft,Flat.p.nonoverlap);
        sd(on:off)=2*thet;
    end
    sd=[0; sd; 0];
    [ons,offs,maxs]=func_help_ons_offs(sd,thet);
    ons=ceil(ons)-1; offs=ceil(offs)-1;
    
    
    Bfile=Flat.scanrate/Flat.p.nonoverlap*(Flat.DAT.timestamp(i)-t0)*86400;
    %buffs=func_numbuffs(Flat.DAT.eof(i),Flat.p.nfft,Flat.p.nonoverlap);
%     if i==49
%         keyboard
%     end
    for jj=1:length(ons)
            sten=0;
%         if offs(jj)-ons(jj)<Flat.p.umap.MinNumTimeSegments
%             continue
%         end
% if min(ons)==0
%     keyboard
% end
        for j=ons(jj):offs(jj)
            % for j=1:buffs % full (all buffers)
            c=c+1;
            
            Z.DATindex(c)=i;
            Z.indices_all(c)=Flat.p.nonoverlap*int64(j)+Flat.p.nfft-Flat.p.nonoverlap; % XXX verify with umap code
            Z.buffer(c)=round(Bfile+j);
            s=sels(seli);
            while seli<length(sels) && Flat.X.DATindex(s)<i
                seli=seli+1;
                s=sels(seli);
            end
            while seli<length(sels) && Flat.X.DATindex(s)==i && (0*Flat.p.nonoverlap+Flat.X.indices_all(s)+Flat.X.data_off(s))<Z.indices_all(c)
                seli=seli+1;
                s=sels(seli);
            end
            xr=Flat.X.indices_all(s); yr=Flat.X.indices_all(s)+Flat.X.data_off(s);
            if Z.DATindex(c)==Flat.X.DATindex(s) && Z.indices_all(c)==xr
                Flat.X.custom(1,s)=c;
                sten=Flat.X.clust_ID(s); % =2
            end
            
            Z.stencil(c)=sten;
            
            if Z.DATindex(c)==Flat.X.DATindex(s) && Z.indices_all(c)==yr
                Flat.X.custom(2,s)=c;
                sten=0;
            end
        end
    end
end
Flat.Z=Z;
Flat.p.umap.brute_force.buff_span=2;
disp('Ready, now run func_umap_brute_forceAN');

end
