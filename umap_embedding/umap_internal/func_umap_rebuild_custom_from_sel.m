function  [X,stencil]=func_umap_rebuild_custom_from_sel(X,sel,Z)
% this function builds X.custom from X.DATindex and X.indices_all and
% X.data_off

lZ=length(Z.stencil);
stencil=zeros(1,lZ);
ci=1;
on=0;
wrongi=0;
for i=1:lZ
    if ci<=length(sel) &&  X.DATindex(sel(ci))<Z.DATindex(i)
        ci=ci+1;
        wrongi=wrongi+1;
    end
    if ci<=length(sel) && Z.DATindex(i)==X.DATindex(sel(ci))
        if Z.indices_all(i)==X.indices_all(sel(ci))
            X.custom(1,sel(ci))=i;
            on=1;
        end
        if Z.indices_all(i)==(X.indices_all(sel(ci))+X.data_off(sel(ci)))
            X.custom(2,sel(ci))=i;
            stencil(X.custom(1,sel(ci)):X.custom(2,sel(ci)))=X.clust_ID(sel(ci));
            ci=ci+1;
            on=0;
        end
    end
end
fprintf('%d mistakes corrected\n', wrongi);

end
