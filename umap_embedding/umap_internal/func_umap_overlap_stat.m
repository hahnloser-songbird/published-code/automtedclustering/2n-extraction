function Flat=func_umap_overlap_stat(Flat)

% 1) This function solves Small OVLP problems (less than
%    Flat.p.umap.MinNumBuffSyl buffers), we trust 'OK' syllables
%
% 2) The function marks overlapping elements in the is_hidden field on the Tagboard
%
% 3) Short syllables are marked SHRT in ExtClock
%

sel=func_select_mic(Flat,Flat.p.channel_mic_display);
Flat.Tags(2,:)={'[]'}; Flat.Tags(7,:)={'[]'}; 

% Put tag for too short syllables
minS=Flat.p.umap.MinNumBuffSyl*Flat.p.nonoverlap;
s=Flat.X.data_off(sel)<minS;
if any(s)
    Flat.Tags(7,sel(s))={'SHRT'};
end

% define OK tags
[Flat,selsOK,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS},'Mark','OK');
% compute median durations according to OK tags
%[Flat,mediandurs,QQs]=func_help_umap_mediandurs(Flat,selsOK);

%% if small overlap then try to fix
nOffOk=0; nOkOn=0; nOkOk=0; nRest=0;
c=0; nbuffOverlap=zeros(length(sel));
remove_elements=[];
fixed_els=0*sel;
for s=1:length(sel)-1
    i=sel(s); j=sel(s+1);
    if Flat.X.DATindex(i)<Flat.X.DATindex(j) % continue if different files
        continue
    end
    ovlp=Flat.X.indices_all(i)+Flat.X.data_off(i)-Flat.X.indices_all(j); %  with next

    if ovlp>0 && ovlp<Flat.p.umap.MinNumBuffSyl*Flat.p.nonoverlap
       Flat.Tags(2,i)={'SmallNotFixed'};

        % equal share
        if (strcmp(Flat.Tags(4,i),'OK') && strcmp(Flat.Tags(4,j),'OK')) || (ismember(Flat.Tags(4,i),{'mOFF','mOFF0'}) && strcmp(Flat.Tags(4,j),'mON'))
            minSov=Flat.p.umap.ClustWinSize*Flat.p.nonoverlap+ovlp/2;
            if Flat.X.data_off(i)>minSov && Flat.X.data_off(j)>minSov
                Flat.X.data_off(i)=Flat.X.data_off(i)-ovlp/2;
                Flat.X.data_off(j)=Flat.X.data_off(j)-ovlp/2;
                Flat.X.indices_all(j)=Flat.X.indices_all(j)+ovlp/2;
                Flat.Tags(2,i)={'OVLPsolvEQ'};
                fixed_els(s+1)=1;
            end         
           
            % trust this, fix follower
        elseif ismember(Flat.Tags(4,i),{'OK','mON'}) && ismember(Flat.Tags(5,j),{'mON'})
            Flat.X.data_off(j)=Flat.X.data_off(j)-ovlp;
            Flat.X.indices_all(j)=Flat.X.indices_all(j)+ovlp;
            if Flat.X.data_off(j)<minS
                remove_elements=[remove_elements j];
             end
            Flat.Tags(2,i)={'OVLPsolvOK'};
            fixed_els(s+1)=1;

            % trust follower, fix this one
        elseif ismember(Flat.Tags(4,j),{'OK','mOFF','mOFF0'}) && ~ismember(Flat.Tags(4,i),{'mON'}) && ~fixed_els(s)
            Flat.X.data_off(i)=Flat.X.data_off(i)-ovlp;
            if Flat.X.data_off(i)<minS
                remove_elements=[remove_elements i];
            end
            Flat.Tags(2,j)={'OVLPsolvOK'}; 
        end
    end
end
if ~isempty(remove_elements)
    Flat=func_remove_elements(Flat,remove_elements,0,1);
    fprintf('%d elements removed\n',length(remove_elements));
end

%% now large overlap, then mark but do not fix
sel=func_select_mic(Flat,Flat.p.channel_mic_display);
for s=1:length(sel)-1
    i=sel(s); j=sel(s+1);
    if Flat.X.DATindex(i)<Flat.X.DATindex(j) % continue if different files
        continue
    end
    ovlp=Flat.X.indices_all(i)+Flat.X.data_off(i)-Flat.X.indices_all(j); %  with next
    % large overlap
    if ovlp>=Flat.p.umap.MinNumBuffSyl*Flat.p.nonoverlap
        c=c+1;
        nbuffOverlap(c)=ovlp;
        if ismember(Flat.Tags(4,i),{'mOFF','mOFF0'}) && strcmp(Flat.Tags(4,j),'OK')
            nOffOk=nOffOk+1;
        elseif strcmp(Flat.Tags(4,i),'OK') && strcmp(Flat.Tags(4,j),'mON')
            nOkOn=nOkOn+1;
        elseif strcmp(Flat.Tags(4,i),'OK') && strcmp(Flat.Tags(4,j),'OK')
            nOkOk=nOkOk+1;
        else
            nRest=nRest+1;
        end
        Flat.Tags(2,i)={'LargeOVLP'};       
    end
end

nbuffOverlap=nbuffOverlap(1:c);

Flat.v.tagboard0_recompute=1;
fprintf('%d overlapping syllables, now marked with is_hidden=OVLP\n',c);
fprintf('%d OK-mON, %d mOFF-OK, %d OK-OK, %d rest\n',nOkOn,nOffOk,nOkOk,nRest);
disp('First correct all OK elements, then fix all others using Flat=func_umap_overlap_correct(Flat);');

figure(39);clf; hist(nbuffOverlap/Flat.scanrate*1000,1:500); xlabel('Overlap (ms)'); ylabel('Count');


end