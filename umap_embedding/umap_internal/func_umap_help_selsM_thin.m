function selsM0=func_umap_help_selsM_thin(Flat,selsM0)

appendixB=Flat.umap.brute_force.appendixB;
% remove all buffers less than appendixB away from a syllable
to_rem=[];
for i=1:length(selsM0)
    ii=selsM0(i);
%     if ii==473676
%     %    keyboard
%     end
    for j=ii-appendixB:ii-1
        if j>0 & Flat.Y.umapCtrWP(1,j) & Flat.Y.DATindex(j)==Flat.Y.DATindex(ii) &  Flat.Y.indices_all(ii)-Flat.Y.indices_all(j)<=Flat.umap.brute_force.appendix
            to_rem=[to_rem i];  
            
        end
    end
    for j=ii+1:ii+appendixB
        if j<=length(Flat.Y.DATindex) & Flat.Y.umapCtrWP(1,j)% & Flat.Y.DATindex(j)==Flat.Y.DATindex(ii) &  Flat.Y.indices_all(j)-Flat.Y.indices_all(ii)<=Flat.umap.brute_force.appendix
            to_rem=[to_rem i];               
        end
    end
end
to_rem=[to_rem length(selsM0)-appendixB:length(selsM0)];
if ~isempty(to_rem)
    fprintf('%d buffers to remove from omit\n',length(to_rem));
    selsM0(to_rem)=[];
%    pause
end
end