function fcH=func_umap_help_Scatterplot(Flat,fcH)
hjet=jet; %hjet(end,:)=[0 0 0];
if isfield(Flat.X,'clust_ID0')
    if rand(1)<.5
        aa=hjet;
    else
        aa=hjet;
    end
    %  a=hsv;
    clusts=unique(Flat.umap.X.color);
    cols=ceil((1:length(clusts))/length(clusts)*size(aa,1));
    for i=1:length(clusts)
        sel=find(Flat.umap.X.color==clusts(i));
        %  sel=sort(sel);
        if ~isfield(fcH,'hf_umapScatterAll') | ~iscell(fcH.hf_umapScatterAll)  | length(fcH.hf_umapScatterAll)<i | ~ishandle(fcH.hf_umapScatterAll{i})
            fcH.hf_umapScatterAll{i}=plot(Flat.umap.X.map_coord(1,sel),Flat.umap.X.map_coord(2,sel),'.','linewidth',1,'markersize',1,'color',aa(cols(i),:),'visible','on');
        else
            %             if i==5
            %                 figure(33); clf; hold on
            %                 ds=0*sel;
            %                 for k=1:length(sel)
            %                     d=32000;
            %                     s=Flat.v.select(sel(k));
            %                     for l=s+1:s+10
            %                         if Flat.X.DATindex(l)==Flat.X.DATindex(s) & Flat.X.clust_ID0(l)==3
            %                             d=Flat.X.indices_all(l)-Flat.X.indices_all(s)-Flat.X.data_off(s);
            %                             break
            %                         end
            %                     end
            %                     d=min(32000,d);
            %                     ds(k)=d;
            %                    plot(Flat.umap.X.map_coord(1,sel(k)),Flat.umap.X.map_coord(2,sel(k)),'.','linewidth',1,'markersize',2,'color',aa(ceil(d/32000*size(aa,1)),:),'visible','on','linewidth',2);
            %                 end
            %             figure(35); hist(ds);
            %             end
            set(fcH.hf_umapScatterAll{i},'XData',Flat.umap.X.map_coord(1,sel),'YData',Flat.umap.X.map_coord(2,sel),'color',aa(cols(i),:),'visible','on');
        end
    end
else
    if ~isfield(fcH,'hf_umapScatterAll') | ~iscell(fcH.hf_umapScatterAll) | ~ishandle(fcH.hf_umapScatterAll{1})
        fcH.hf_umapScatterAll{1}=plot(Flat.umap.X.map_coord(1,:),Flat.umap.X.map_coord(2,:),'w.','linewidth',1,'markersize',1,'visible','on');
    else
        set(fcH.hf_umapScatterAll{1},'XData',Flat.umap.X.map_coord(1,:),'YData',Flat.umap.X.map_coord(2,:));
    end
end

end