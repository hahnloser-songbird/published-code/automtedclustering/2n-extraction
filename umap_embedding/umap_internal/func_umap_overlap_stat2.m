function Flat=func_umap_overlap_stat2(Flat)

% 1) This function solves Small OVLP problems (less than
%    Flat.p.umap.MinNumBuffSyl buffers), we trust 'OK' syllables
%
% 2) The function marks overlapping elements in the is_hidden field on the Tagboard
%
% 3) Short syllables are marked SHRT in ExtClock
%

sel=func_select_mic(Flat,Flat.p.channel_mic_display);
%Flat.Tags(2,:)={'[]'}; Flat.Tags(7,:)={'[]'};

% Put tag for too short syllables
%minS=Flat.p.umap.MinNumBuffSyl*Flat.p.nonoverlap;
%s=Flat.X.data_off(sel)<minS;
%if any(s)
%    Flat.Tags(7,sel(s))={'SHRT'};
%end

% define OK tags
%[Flat,selsOK,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS},'Mark','OK');
[Flat,selsOK,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS});
% compute median durations according to OK tags
%[Flat,mediandurs,QQs]=func_help_umap_mediandurs(Flat,selsOK);
sel=selsOK;
%% if small overlap then try to fix
c=0; 
ovlps=[];
for s=1:length(sel)-1
    i=sel(s); j=sel(s+1);
    if Flat.X.DATindex(i)<Flat.X.DATindex(j) % continue if different files
        continue
    end
    ovlp=Flat.X.indices_all(i)+Flat.X.data_off(i)-Flat.X.indices_all(j); %  with next
    
    if ovlp>0 %&& ovlp<Flat.p.umap.MinNumBuffSyl*Flat.p.nonoverlap
        c=c+1;
        ovlps(c)=ovlp;
            %Flat.Tags{4,i}
            i
    end
    
    
end
    fprintf('%d overlapping syllables, now marked with is_hidden=OVLP\n',c);
    figure(111); hist(ovlps);
    
end