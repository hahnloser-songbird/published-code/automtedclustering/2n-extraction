function Flat=func_umap_help_sim_by_duration(Flat)
%% this function plots the self-similarity distrubtion (as a function of syllable duration)

figure(42); clf;
figure(43); clf;
figure(44);clf;
knN=2;

NbuffsC=Flat.umap.brute_force.NbuffsC;
Is=Flat.umap.brute_force.Is;
nclusts=length(NbuffsC);
dsim=6;
for k=2:nclusts
    durs=diff(NbuffsC{k}(:,Is{k}));
    dursuni=unique(durs);
    CosD=Flat.umap.brute_force.CosD{k}; CosDi=Flat.umap.brute_force.CosDi{k};
    sims=zeros(dsim,length(dursuni));
    for j=dursuni
        whichi=find(durs==j);
        CosDs=CosD(CosDi(whichi(1)):(CosDi(min(end,whichi(end)+1))-1));
        sims(1,j)=j; 
        QQ=quantile(CosDs,[.25 .50 .75]);
        sims(2:4,j)=QQ; 
        h=sort(CosDs,'descend');
        try
        sims(5,j)=mean(h(1:knN));
        end
        sims(dsim,j)=length(CosDs);
    end
    h=find(sims(dsim,:)>sum(sims(dsim,:)/20));
    figure(42);    subplot(nclusts-1,1,k-1);
    plot(sims(1,h),sims(5,h)); ylabel(['syl ' num2str(k)]);
    figure(43);     subplot(nclusts-1,1,k-1);
    plot(sims(1,h),sims(2:4,h)); hold on;  plot(sims(1,h),sims(5,h),'k--'); ylabel(['syl ' num2str(k)]);
    figure(44);     subplot(nclusts-1,1,k-1);
    plot(sims(1,h),sims(dsim,h));ylabel(['syl ' num2str(k)]);
end
figure(42);
xlabel('Syl dur (buffs)');
ylabel('knN sim');
figure(43);
xlabel('Syl dur (buffs)');
ylabel('1-3 Quart sim');
figure(44);
xlabel('Syl dur (buffs)');
ylabel('Num syls');
end
%%
