function      [fcH,Flat]=func_help_umap_init(fcH,Flat)

n=Flat.p.umap.NumPixels;
Maprange=Flat.umap.Maprange;

if isfield(Flat.X,'clust_ID0')
    Flat.umap.X.color=Flat.X.clust_ID0(Flat.umap.X.select);
else
    Flat.umap.X.color=ones(1,length(Flat.umap.X.select));
end


if isfield(Flat.umap.XC,'selectScatterCurrBlob')
    Flat.umap.XC=rmfield(Flat.umap.XC,'selectScatterCurrBlob');
end

Flat.umap.fcH.ScatterCurrBlob=[];
Flat.umap.fcH.ScatterClusteredBlobs=[];
if isfield(fcH,'hf_umapScatterCurrBlob') && ishandle(fcH.hf_umapScatterCurrBlob)
    set(fcH.hf_umapScatterCurrBlob,'Visible','Off','XData',[],'YData',[]);  
 %   Flat.umap.fcH.ScatterClusteredBlobs=[];
    set(fcH.hf_umapScatterClusteredBlobs,'Visible','Off','XData',[],'YData',[]);
end
Flat.umap.XC.ClustNo=1; % currently all blobs are in cluster 1
disp('All clustered blobs erased');

%Flat.X.umapOnOffClust=uint8(zeros(2,Flat.num_Tags));
if isfield(Flat.X,'umapCtrWP')
    Flat.X=rmfield(Flat.X,'umapCtrWP');
end

if isfield(Flat.X,'umapCtrWN')
    Flat.X=rmfield(Flat.X,'umapCtrWN');
end

%%% @Corinna
if isfield(Flat.p.umap.brute_force,'NoiseClusts')
    Flat.p.umap.brute_force=rmfield(Flat.p.umap.brute_force,'NoiseClusts');
end



disp('Flat.X.umapCtrWP reinitialized');

if Flat.umap.fcH.init
    [Flat.umap.X.map_coord,map_index]=func_help_umap_map_coord(Flat.umap.X.map',Maprange,n);
    [map_index_sort,sorted_index]=sort(map_index,'ascend');
    [mpu,ind_mpu,ind_map_index]=unique(map_index_sort);
    multiplicity=zeros(size(map_index));
    d=diff(ind_mpu);
    for i=1:length(d)
        multiplicity(ind_mpu(i):ind_mpu(i+1))=d(i);
    end

    Flat.umap.X.multiplicity=multiplicity(sorted_index);
    if Flat.p.umap.GPUdevice2
        Flat.umap.X.map_index=gpuArray(map_index);
    else
        Flat.umap.X.map_index=map_index;
    end
end

Flat.umap.ContClickCount=0;
if isfield(Flat.umap,'ContClick')
    Flat.umap=rmfield(Flat.umap,'ContClick');
end
if isfield(Flat.umap,'ContClickHist') & strcmp(Flat.v.RMS(1),'@')
    Flat.umap.ContClickHistold=Flat.umap.ContClickHist;
    y=input('Delete UMAP history (no clustering analysis will be possible)? (yes=1) ');
    if y==1
        Flat.umap=rmfield(Flat.umap,'ContClickHist');
    end
end

Flat.umap.fcH.init=0;
end