function [Flat,X,Y,slices]=func_umap_final_statistics1(Flat,onoff)
% this function computes the distribution of slice times in the chosen blobs
% when this distribution is narrow, there is little noise in the data

if nargin<2
    onoff=1; % onset=1   offset=-1
end
H=Flat.umap.ContClickHist;
slices=[];
cH=0;
for i=1:length(H)
    direction=-1+2*(H(i).timeslice>0);
    if direction~=onoff
        continue
    end
    cH=cH+1;
    slice=H(i).slices;
    timeslice=mode(slice);
    %timeslice=H(i).timeslice;
    % p=sum(abs(slice-H(i).timeslice)<2)/length(slice)
    p=sum(slice==sign(timeslice) | slice==sign(timeslice)+direction)/length(slice);
    lags=abs(slice)-abs(timeslice);
    [y,x]=hist(lags,1:101);
    if isempty(slices)
        slices=y(1:100)/sum(y(1:100));
    else
        slices=slices+y(1:100)/sum(y(1:100));
    end
end
slices=100*slices/cH;
X=1000/Flat.scanrate*Flat.p.nonoverlap*x(1:100);
Y=cumsum(slices);
figure(1); clf; loglog(X,Y,'k','linewidth',2); %[y,x]=hist(slice-H(i).timeslice,100); semilogy(x,y);
fprintf('dt=0: %.2f, dt>0: %.2f\n',slices(1),sum(slices(2:end)));
xlabel('Time lag to threshold crossing (ms)'); ylabel('Percent Vocalizations');

% Gold standard
if onoff==1
    doI=1; str='Onset'; %appendixB=0;
else
    doI=2; str='Offset'; %appendixB=Flat.umap.appendixB;
end
[Flat,sels,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS});
lags=zeros(1,length(sels));
for i=1:length(sels)
    z=Flat.X.custom(doI,sels(i))-1+doI;%+appendixB;
    t=0;
    while z>1 && diff(Flat.Z.buffer(z-1:z))==1 && z<length(Flat.Z.buffer)
        t=t+1;
        z=z-onoff;
    end
    lags(i)=t;
end
[y,x]=hist(lags,1:101);
slices=y(1:100)/sum(y(1:100));
slices=slices*100;
hold on; loglog(1000/Flat.scanrate*Flat.p.nonoverlap*x(1:100),cumsum(slices),'r','linewidth',2);
title(str);
axis tight; set(gca,'box','off','fontsize',20);
legend({'2-point','gold standard'},'location','southeast');
Y=[Y;cumsum(slices)];
x=x(1:100);

end