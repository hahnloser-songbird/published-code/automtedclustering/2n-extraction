function [fcH,Flat]=func_umap_select(fcH,Flat,X)%,compute_clustID)
% this function computes the time slice image in fcH.hf_umapIT and the
% blob membership of Flat.umap.X.select


r=Flat.p.umap.RadiusKernel;
theta=Flat.p.umap.theta;
time=Flat.p.umap.time;

if ~isfield(Flat.p.umap,'new_kernel') || Flat.p.umap.new_kernel
    if Flat.p.umap.GPUdevice
        C=gpuArray(zeros(2*r+1));
    else
        C=zeros(2*r+1);
    end
    for i=1:2*r+1
        for j=1:2*r+1
            if (i-r-1)^2+(j-r-1)^2<=r^2
                C(i,j)=1;
            end
        end
    end
    C=C/sum(sum(C));
    Flat.umap.C=C;
end

% convolve time-slice image
%tic
I2=convn(Flat.umap.IMul,Flat.umap.C,'same')-theta>0;
%toc

if Flat.p.umap.GPUdevice
    I2=gather(I2);
end
CC = bwconncomp(I2,4);

IT=zeros(size(I2));
PIX=CC.PixelIdxList;

Lh=cellfun(@length,PIX);
M=min(CC.NumObjects,Flat.p.umap.MaxClust);
Flat.umap.NumClust=M;

[~,clustSize]=sort(Lh,'descend');
PIX=PIX(clustSize(1:M));

for i=1:M
    j=clustSize(i);
  IT(PIX{i})=M+1-i;
end
Flat.umap.PIX=PIX;
Flat.umap.IT=IT;


%% cluster outliers by nearest cluster

set(fcH.hf_umapIT,'CData',IT');
set(fcH.hf_umapTit,'String',... 
    sprintf('r=%.1f || th=%.2f || Nclust=%d || t=%d', r, theta, Flat.p.umap.MaxClust, time), ...
    'fontsize',9);
set(fcH.hf_umapTit,'visible','on');

end



