% first run Flat=func_umap_final_statistics(Flat); on all archives
% then save to Flats
clear birds

%birds{1}.name='tutMeta'; birds{1}.archDir={'ArchivesGS2'};
%birds{1}.arch={'@g17y2xyz'};f

%birds{1}.name='2NPaper'; birds{1}.archDir={'ArchivesGS'}; birds{1}.arch={'@g17y2xyz'};
%birds{1}.name='b8p2male-b10o15female_aligned'; birds{1}.archDir={'ArchivesRich'}; birds{1}.arch={'@2018-08-22xy'};%,'@g17y2xy','@g19o3Trainxy','@g19o10Trainxy'};
%birds{1}.name='tutMeta'; birds{1}.archDir={'ArchivesGS'}; birds{1}.arch={'@g4p5FP'};%,'@g17y2xy','@g19o3Trainxy','@g19o10Trainxy'};

i=1;
birds{i}.name='2NPaper';  birds{i}.archDir={'ArchivesGS'}; birds{i}.arch={'@g17y2xyz'}; i=i+1;
birds{i}.name='2NPaper';  birds{i}.archDir={'ArchivesGS'}; birds{i}.arch={'@2018-08-22xy'}; i=i+1;
birds{i}.name='2NPaper';  birds{i}.archDir={'Archives'}; birds{i}.arch={'@2014-04-29xy'}; i=i+1; % almost perfect bird
birds{i}.name='2NPaper';  birds{i}.archDir={'Archives'}; birds{i}.arch={'#META'}; % almost perfect bird


%%birds{1}.name='copExpBP04_eval';  birds{1}.archDir={'ArchivesRICH'}; birds{1}.arch={'@META'};
%birds(2:4)=birds(1); birds{2}.arch={'@2018-08-22xy'};

fs=16; % fontsize
ca=0;
Ms=zeros(3,length(birds)); Ns=cell(1,length(birds));
slicesAll=struct('X',[],'Yon',[],'Yoff',[],'slices',[]);
for b=1:length(birds)
    for a=1:length(birds{b}.arch)
        
        ca=ca+1;
        name=[birds{b}.name '  ' birds{b}.arch{a}];
        str=[paths.data_path birds{b}.name directoryDelimiter 'ArchiveCollection' directoryDelimiter birds{b}.archDir{a} directoryDelimiter];
        strA=[str birds{b}.arch{a}];
        strD=[str 'Decoupled' directoryDelimiter birds{b}.arch{a}(2:end) '=DAT_data'];
        load(strA); Flat=Flath; clear Flath;
        %h=load(strD);
        %Flat.DAT.data=h.DAT.data;
        
        fprintf('%s: ',Flat.name);
        [Flat,X,Y,slices]=func_umap_final_statistics1(Flat);
        slicesAll(b).X=X;  slicesAll(b).Yon=Y(1,:)'; slicesAll(b).Yoff=Y(2,:)';  slicesAll(b).slices=slices;
        %print(birds{b}.arch{a},'-dsvg');
        Flat=func_umap_final_statistics(Flat,0);
        % pause
        
        Strs={'Precision','Recall'};
        for j=1 % 1=precision, 2=recall
            statsP=[]; statsN=[]; stats2=[]; stats0=[];
            
            stath=Flat.v.umap.StatsP(j,1:end);
            h=find(~isnan(Flat.v.umap.StatsP(j,1:end)) & Flat.v.umap.StatsP(j,1:end)~=0 & ~isnan(Flat.v.umap.Stats2(j,1:end)) & Flat.v.umap.Stats2(j,1:end)~=0 & ~isnan(Flat.v.umap.StatsN(j,1:end)) & Flat.v.umap.StatsN(j,1:end)~=0 );
            %    Flat.v.umap.StatsP(j,h)
            %h=[5 7 2 4 3];
            
            statsP=[statsP Flat.v.umap.StatsP(j,h)];
            statsN=[statsN Flat.v.umap.StatsN(j,h)];
            stats2=[stats2 Flat.v.umap.Stats2(j,h)];
            stats0=[stats0 Flat.v.umap.Stat0(j,:)];
            
            statsP=100-statsP; statsN=100-statsN; stats2=100-stats2;
            
            figure(2*(ca-1)+j); clf;
            for i=1:length(statsP)
                semilogy([statsP(i) stats2(i) statsN(i)],'linewidth',2);
                hold on
            end
            
            if 1
                statsP=(eps+statsP)./(eps+stats2);
                statsN=(eps+statsN)./(eps+stats2);
                stats2=ones(size(stats2));%stats2./(eps+stats2);
            end
            h=statsP>1000 | statsN>1000;
            statsP(h)=[]; statsN(h)=[]; stats2(h)=[];
            mP=mean(statsP); sP=std(statsP);
            mN=mean(statsN); sN=std(statsN);
            m2=mean(stats2); s2=std(stats2);
            
            %semilogy([mP m2 mN],'k','linewidth',2)
            fprintf('Mean error onset=%.2f, both=%.2f, offset=%.2f\n',mP,m2,mN);
            ylabel('Recall error (%)');
            xticks([1 2 3]);
            xticklabels({'On','2N','Off'});
            %title([name '  ' Strs{j}]);
            set(gca,'box','off','fontsize',fs);
            legend(Flat.p.clust_names(h),'location','north');
            print([name ' percentError'],'-dsvg');
            
            figure(2*(ca-1)+1+j); clf; he{j}=errorbar([mP m2 mN],[sP s2 sN]/sqrt(length(statsP)),'k','linewidth',2);
            Ms(:,ca)=[mP m2 mN]; Ns{ca}=Flat.v.umap.StatsNum;
            hold on;
            %if j==1 % precision
            % the red line is the prediction from independent errors
            %    plot([1 3],mP*mN/1e2*[1 1],'r');
            %end
            ylabel('Relative recall error');
            xticks([1 2 3]);
            xticklabels({'On','2N','Off'});
            set(gca,'box','off','fontsize',fs);
            %title([name '  ' Strs{j}]);
            xlim([0.9 3.1]);
            
            print([name ' relativeError'],'-dsvg');
        end
    end
end
j=j+1;
%%
Yon=[slicesAll(:).Yon]; Yoff=[slicesAll(:).Yoff]; 
m=mean(Yon');s=std(Yon')/sqrt(length(birds));
fprintf('Onset: average percent cleanly dissected: %d\n',round(mean(Yon(1,:)),0));
figure(1);clf; 
hold on;  plot(0:99,Yon(:,1),'k','linewidth',1);
plot(0:99,m,'k','linewidth',2);  patch([0:99 99:-1:0],[m-s m(end:-1:1)+s(end:-1:1)],'w','FaceColor',[.7 .7 .7],'EdgeColor',[1 1 1],'FaceAlpha',0.4);
xlabel('Time lag (4 ms bins)'); ylabel('Cumulative percent vocalizations'); title('Onset');
set(gca,'box','off','fontsize',fs);
legend({'g17y2','Mean','SEM'},'Location','Southeast');
axis tight
print('LagdistributionOnset','-dsvg');

m=mean(Yoff');s=std(Yoff')/sqrt(length(birds));
fprintf('Offset: average percent cleanly dissected: %d\n',round(mean(Yoff(1,:)),0));
figure(2);clf; 
hold on;  plot(0:99,Yoff(:,1),'k','linewidth',1);
plot(0:99,m,'k','linewidth',2);
patch([0:99 99:-1:0],[m-s m(end:-1:1)+s(end:-1:1)],'w','FaceColor',[.7 .7 .7],'EdgeColor',[1 1 1],'FaceAlpha',0.4);
xlabel('Time lag (4 ms bins)'); ylabel('Cumulative percent vocalizations'); title('Offset');
set(gca,'box','off','fontsize',fs);
legend({'g17y2','Mean','SEM'},'Location','Southeast');
axis tight
print('LagdistributionOffset','-dsvg');

%%
mMs=mean(Ms,2); sMs=std(Ms,0,2);
figure(2*(ca-1)+1+j); clf; he{j}=errorbar(mMs/mean(Ms(2,:)),sMs/sqrt(size(Ms,2)),'k','linewidth',2);
%Ms(:,i)=[mP m2 mN];
hold on;
%if j==1 % precision
% the red line is the prediction from independent errors
%    plot([1 3],mP*mN/1e2*[1 1],'r');
%end
ylabel('Average relative recall error');
xticks([1 2 3]);
xticklabels({'On','2N','Off'});
set(gca,'box','off','fontsize',fs);
%title([name '  ' Strs{j}]);
axis tight
xlim([0.9 3.1]);
print('AverageRelativeError','-dsvg');

%%
statsum=zeros(1,length(Ns));
for i=1:length(Ns)
h=Ns{i}>0; 
statsum(i)=100*mean(sum2)/mean([mean(sumP),mean(sumN)]);
sumP=Ns{i}(1,find(h(1,:))); sumN=Ns{i}(2,find(h(2,:))); sum2=Ns{i}(3,find(h(3,:)));
%fprintf('%s: P: %d, N: %d, 2: %d\n',birds{i}.arch{1},mean(sumP),mean(sumN),mean(sum2));
fprintf('%s: Percent bins: %.2f\n',birds{i}.arch{1},statsum(i));

end
fprintf('Mean: %.2f\n',mean(statsum));

