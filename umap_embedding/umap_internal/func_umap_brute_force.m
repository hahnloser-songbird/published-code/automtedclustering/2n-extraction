function Flat=func_umap_brute_force(Flat,recompute_NbuffsC,recompute_CosD,verbose)

if nargin<4
    verbose=0;
end
if nargin<3
    recompute_CosD=0;
end
if nargin<2
    recompute_NbuffsC=0;
end



%%
nP=1000; % max number of syllables per clust
%nP=3324;
nDim=400; % number of principal components

do_rank=0; % sort by rank, not by similarity
nAuto=200; % number of syllables to inspect for self-similarity computation

do_PCcoeffs=0; % COs distance of either Flat.X.PCcoeffs or Flat.DAT.data

if ~do_PCcoeffs
    nbuffsL=4;%-Flat.p.umap.ClustWinOffset;
    nbuffsR=6;%Flat.p.umap.buffs_beyond_offset;
    Flat.p.umap.brute_force.nbuffsL=nbuffsL;
    Flat.p.umap.brute_force.nbuffsR=nbuffsR;
end
Flat.p.umap.brute_force.nP=nP;
Flat.p.umap.brute_force.nDim=nDim;
Flat.p.umap.brute_force.doPCcoeffs=do_PCcoeffs;
Flat.p.umap.brute_force.do_rank=do_rank;
Flat.p.umap.brute_force.nAuto=nAuto;


try
    g=gpuDevice;
catch
    g=[];
end
Flat.p.umap.GPUdevice=~isempty(g);

%  max_width OK
[Flat,sels,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS});
nclusts=max(Flat.X.clust_ID(sels));
Flat.umap.brute_force.nclusts=nclusts;

% delete elements in cluster 1
sels1=sels(Flat.X.clust_ID(sels)==1);
if ~isempty(sels1)
    fprintf('Now deleting %d syllables in cluster 1\n',length(sels1));
    Flat=func_remove_elements(Flat,sels1,0,1);
end

if ~isfield(Flat.umap.brute_force,'NbuffsC') | recompute_NbuffsC
    % Extract PC data of OK syllables: Pccoeffs and NormOK (NbuffsC)
    NbuffsC=cell(1,nclusts); selsC=NbuffsC; PCcoeffs=NbuffsC; NormPAll=NbuffsC; NormC=NbuffsC; DursC=NbuffsC; Nk=NbuffsC;
    for k=2:nclusts
        selsCkh=find(Flat.Y.umapCtrWP(1,:)==k);
        [selsCk,NbuffsC{k},PCcoeffs{k}]=func_umap_help_Nbuffs(Flat,selsCkh,nP,0,0);
        selsC{k}=selsCk;
        DursC{k}=diff(NbuffsC{k});
        Nk{k}=size(NbuffsC{k},2); % number of syllables
        NormPAll{k}=sum(PCcoeffs{k}.*PCcoeffs{k});
        
        NormC{k}=zeros(1,Nk{k});
        for i=1:Nk{k}
            NormC{k}(i)=sum(NormPAll{k}(NbuffsC{k}(1,i):NbuffsC{k}(2,i)));
        end
        NormC{k}=sqrt(NormC{k});
    end
    Flat.umap.brute_force.NbuffsC=NbuffsC;
    Flat.umap.brute_force.selsC=selsC;
    Flat.umap.brute_force.DursC=DursC;
    Flat.umap.brute_force.PCcoeffs=PCcoeffs;
else
    NbuffsC=Flat.umap.brute_force.NbuffsC;
    selsC=Flat.umap.brute_force.selsC;
    DursC=Flat.umap.brute_force.DursC;
    PCcoeffs=Flat.umap.brute_force.PCcoeffs;
end

%% Compute self-similarity for each syllable
if do_rank & recompute_CosD
    tgpu=0; tcpu=0;
    CosD=cell(1,nclusts);  CosDi=CosD;
    hbar = waitbar(0,'Please wait...');
    for k=2:nclusts
        nk=Nk{k};
        Isk=1:ceil(nk/nAuto):(nk-1);  nIk=length(Isk); % which syllables to use
        CosD{k}=zeros(nIk,nk-1);
        %  CosD{k}=zeros(1,nIk*(nk-1));
        %  CosDi{k}=zeros(1,nIk);
        P=PCcoeffs{k}; N=NbuffsC{k};
        if Flat.p.umap.GPUdevice
            P=gpuArray(P);
        end
        %     cD=0;
        tstart=now;
        %     for i=1:nk-1 % Process syllable i in cluster k
        for ii=1:nIk
            i=Isk(ii);
            %  waitbar(i/(n-1),hbar,['Cluster ' num2str(k) ', ' datestr((now-tstart)/i*(nk-i),'HH:MM:SS')]); drawnow
            waitbar(ii/nIk,hbar,['Cluster ' num2str(k) ', ' datestr((now-tstart)/ii*(nIk-ii),'HH:MM:SS')]); drawnow
            tic
            A=P(:,N(1,i):N(2,i)); % Ni=min([i+nAuto,nk]);
            B=P(:,N(1,i+1):end);
            %B=P(:,N(1,i+1):N(2,Ni));
            d=(conv2(B,A(end:-1:1,end:-1:1),'valid')); D=zeros(1,nk);
            
            if Flat.p.umap.GPUdevice
                d=gather(d);
            end
            tgpu=tgpu+toc;
            tic
            for j=i+1:nk
                on=N(1,j)-N(1,i+1)+1;
                don=N(2,j)-N(1,j)-(N(2,i)-N(1,i)); %NM(2,j)-NM(1,j)-(N(2,i)-N(1,i));
                h=zeros(1,don+1);
                h(1)=sum(NormPAll{k}(N(1,j):N(1,j)+N(2,i)-N(1,i)));
                for ll=2:length(h)
                    onl=N(1,j)-2+ll;
                    h(ll)=h(ll-1)-NormPAll{k}(onl)+NormPAll{k}(onl+N(2,i)-N(1,i));
                end
                % [D(j),maxi]=max(d(on:on+don));
                [D(j),maxi]=max(d(on:on+don)./sqrt(h));
                D(j)=D(j)/NormC{k}(i);%/sqrt(sum(NormPAll{k}(N(1,j)+maxi-1:N(1,j)+maxi-1+N(2,i)-N(1,i))));
            end
            tcpu=tcpu+toc;
            CosD{k}(ii,i:nk-1)=D(i+1:nk);
            %  CosD{k}(cD+1:cD+nk-i)=D(i+1:nk);
            %  CosDi{k}(ii)=cD+1;
            %  cD=cD+nk-i;
        end
        % CosDi{k}(end)=cD+1;
        %  CosD{k}=CosD{k}(1:cD);
        CosDi{k}=Isk;
    end
    close(hbar);
    fprintf('tGPU=%f, tCPU=%f\n' ,tgpu,tcpu);
    Flat.umap.brute_force.CosD=CosD;
    %   Flat.umap.brute_force.CosDi=CosDi;
    Flat.umap.brute_force.CosDi=CosDi;
end


%% compute self-similarity distribution and plot stat
if do_rank
    knN=2;
    CselfSim=cell(1,nclusts);
    hclust=CselfSim;
    
    figure(41); clf;
    n=ceil(sqrt(double(nclusts-1)));
    
    for k=2:nclusts
        L=length(CosDi{k});
        CselfSim{k}=zeros(1,L);
        for ii=1:L
            %        if j<length(Is{k})
            %        CosDs=CosD{k}(CosDi{k}(j):(CosDi{k}(j+1)-1));
            CosDs=CosD{k}(ii,CosDi{k}(ii):end);
            %       else
            %           CosDs=CosD{k}(CosDi{k}(j):end);
            %       end
            h=sort(CosDs,'descend');
            CselfSim{k}(ii)=mean(h(1:min(end,knN)));
        end
        CselfSim{k}=sort(CselfSim{k},'ascend');
        
        subplot(n,n,double(k-1));
        
        % [y,x]=hist(CosD{k});
        [y,x]=hist(CselfSim{k});
        bar(x,y);hold on;
        hclust{k}=plot(mean(CselfSim{k})*[1 1],[0 max(y)],'r','linewidth',2);
        title(['Clust ' num2str(k) ', knN=' num2str(knN)]);
        drawnow;
    end
    Flat.umap.brute_force.knN=knN;
    Flat.umap.brute_force.CselfSim=CselfSim;
end

%% OMIT
selsM0=find(Flat.Y.umapCtrWP(1,:)==0);
minDurC=cellfun(@min,DursC,'UniformOutput',false); minDurC=min([minDurC{2:end}]);
[selsM0,NbuffsM0,PCcoeffsM]=func_umap_help_Nbuffs(Flat,selsM0,[],minDurC,1);
DursM=diff(NbuffsM0);

%  PCcoeffsM=Flat.Y.PCcoeffs(1:nDim,selsM);
NormMAll=sum(PCcoeffsM.*PCcoeffsM);

Flat.umap.brute_force.NbuffsM0=NbuffsM0;
Flat.umap.brute_force.selsM0=selsM0;

%% Search all omitted regions
hbar = waitbar(0,'Please wait...');
PM=PCcoeffsM; NM=NbuffsM0;
if Flat.p.umap.GPUdevice
    PM=gpuArray(PM);
end
No=size(NM,2); %  number of OMIT regions

Flat.umap.brute_force.No=No;

CosM=cell(1,nclusts); Maxis=CosM; I0s=zeros(nclusts,nP);
Cosm0=cell(nclusts,No);
%Nsyl=sum(cellfun(@length,NbuffsC));

%CosMF=zeros(Nsyl,
for k=2:nclusts
    P=PCcoeffs{k}; N=NbuffsC{k}; nk=size(N,2);
    if Flat.p.umap.GPUdevice
        P=gpuArray(P);
    end
    CosM{k}=zeros(nk,No); Maxis{k}=zeros(nk,No); 
    Cosm0{k}=-2*ones(nk,max(DursM)-minDurC); % is way too big!!
%    CosmF{k}=
    tstart=now;
    for i=1:nk  % loop over syllables
        waitbar(i/nk,hbar,['Process OMIT clust ' num2str(k) ', ' datestr((now-tstart)/i*(nk-i),'HH:MM:SS')]); drawnow
        if i==1
            i0=find(DursM>=DursC{k}(i),1,'first');
        else
            i0=i0-1+find(DursM(i0:end)>=DursC{k}(i),1,'first');
        end
        I0s(k,i)=i0;
        A=PCcoeffs{k}(:,N(1,i):N(2,i));
        B=PM(:,NM(1,i0):end);
        d=(conv2(B,A(end:-1:1,end:-1:1),'valid'));
        D=-2*ones(1,No); maxis=zeros(1,No);
        if Flat.p.umap.GPUdevice
            d=gather(d);
        end
        for j=i0:No
            on=NM(1,j)-NM(1,i0)+1;
            don=NM(2,j)-NM(1,j)-(N(2,i)-N(1,i));
            h=zeros(1,don+1);
            h(1)=(sum(NormMAll(NM(1,j):NM(1,j)+N(2,i)-N(1,i))));
            for ll=2:length(h)
                onl=NM(1,j)-2+ll;
                h(ll)=h(ll-1)-NormMAll(onl)+NormMAll(onl+N(2,i)-N(1,i));
            end
            Cosm0{k,j}(i,1:1+don)=d(on:on+don)./sqrt(h)/NormC{k}(i);
            [D(j),maxi]=max(d(on:on+don)./sqrt(h));
            maxis(j)=maxi;
            D(j)=D(j)/NormC{k}(i);%/sqrt(sum(NormMAll(NM(1,j)+maxi-1:NM(1,j)+maxi-1+N(2,i)-N(1,i))));
        end
        CosM{k}(i,:)=D; Maxis{k}(i,:)=maxis;
    end
end
close(hbar);

Flat.umap.brute_force.Maxis=Maxis;
Flat.umap.brute_force.CosM=CosM;
Flat.umap.brute_force.Cosm0=Cosm0;

end