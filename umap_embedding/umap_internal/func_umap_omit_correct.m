function Flat=func_umap_omit_correct(Flat,verbose)
% this function sorts the OMT (Omitted) syllables by the kNN distance
% in Flat.X.custom

if nargin<2
    verbose=0;
end

con_win=Flat.scanrate/2; % context window in samples

%%
[Flat,sels,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS});
nclust=max(Flat.X.clust_ID(sels));

[Flat,selsOM,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS},'Mark','OMT');
selsnOM=setdiff(sels,selsOM);
[Flat,mediandurs]=func_help_umap_mediandurs(Flat,selsnOM);
%r=randperm(length(selsnOM));
%selsOM=selsnOM(r(1:100));

%% Get Baseline
Nsyls=200; % max num of syllables to compute with
knN=3; % k nearest neighbors
nfft_is=Flat.p.syllable.els;
nOM_per_clust=cell(1,nclust); LnOM=zeros(1,nclust);
SnOMs=cell(1,nclust);
Sim_nOM=cell(1,nclust); Sort_nOM=Sim_nOM;
figure(41); clf; drawnow
n=ceil(sqrt(double(nclust)));  hclust=cell(1,nclust);

for j=2:nclust % all clusters except cluster zero
    nOM_per_clust{j}=selsnOM(Flat.X.clust_ID(selsnOM)==j);
    nOM_per_clust{j}=nOM_per_clust{j}(1:min(Nsyls,end));
    LnOM(j)=length(nOM_per_clust{j});
    % SnOM=func_extract_data(Flat,5,nOM_per_clust{j});
    %   Flat.v.curr_clust=j;
    %   SnOM=func_extract_data(Flat,2,nOM_per_clust{j});
    offs=Flat.p.nfft-(1-Flat.p.umap.ClustWinOffset)*Flat.p.nonoverlap;
    SnOM=zeros(length(nfft_is)*Flat.spec.numbuffs{2}(j),LnOM(j));
    for k=1:LnOM(j)
        s=nOM_per_clust{j}(k);
        [SnOMk,is_truncated]=func_extract_spec2(Flat,Flat.X.DATindex(s),Flat.X.indices_all(s)+offs,Flat.X.indices_all(s)+offs+length(Flat.p.nfft_i)*(Flat.spec.numbuffs{2}(j)-1),0);
        SnOMk=SnOMk(nfft_is,:);
        hh=zeros(size(SnOMk,1)*size(SnOMk,2),1); hh(:)=SnOMk;
        SnOM(:,k)=hh;
    end
    SnOMs{j}=SnOM;
    DS=dist(SnOM);
    %  DS=-SnOM'*SnOM;
    DSsort=zeros(1,size(DS,1));
    for k=1:size(DS,1)
        h=sort(DS(:,k),'ascend');
        DSsort(k)=mean(h(2:knN+1));
    end
    % Sim_nOM{j}=sum(DS)/(size(DS,1)-1);
    [Sim_nOM{j},Sort_nOM{j}]=sort(DSsort,'ascend');
    Sort_nOM{j}=nOM_per_clust{j}(Sort_nOM{j});
    subplot(n,n,double(j));
    hist(Sim_nOM{j}); hold on;
    hclust{j}=plot(mean(Sim_nOM{j})*[1 1],[0 LnOM(j)/4],'r','linewidth',2);
    title(['Clust ' num2str(j)]);
    drawnow;
end
disp('Baseline done');

%% find kNN for each OMIT
if ~verbose
    hbar = waitbar(0,'Please wait...');
end
tstart=now;
for i=1:length(selsOM) % all OMIT intervals
    if ~verbose
        waitbar(i/length(selsOM),hbar,datestr((now-tstart)/i*(length(selsOM)-i),'HH:MM:SS')); drawnow
    end
    s=selsOM(i);
    %    s=4534;
    %  SOM_pc=func_extract_data(Flat,5,s);
    Rs=zeros(1,nclust); DsK=Rs; DsMmin=inf*ones(1,nclust); DssortA=zeros(nclust,knN);
    for j=2:nclust % all clusters
        %   Flat.X.clust_ID(s)=j;
        rcont=min(Nsyls,LnOM(j));
        width_s=Flat.X.data_off(s)/Flat.p.nonoverlap;
        width_samp=Flat.spec.numbuffs{2}(j)-1;
        Nk=floor(width_s-width_samp/2);
        for k=1:Nk
            %  SOM_pc=func_extract_data(Flat,2,s);
            %      SOM_pc=SOM_pc(nfft_is,:);
            offs=Flat.p.nfft-(k-Flat.p.umap.ClustWinOffset)*Flat.p.nonoverlap;
            [SOM_pc2,is_truncated]=func_extract_spec2(Flat,Flat.X.DATindex(s),Flat.X.indices_all(s)+offs,Flat.X.indices_all(s)+offs+length(Flat.p.nfft_i)*width_samp,0);
            SOM_pc2=SOM_pc2(nfft_is,:);
            hh=zeros(size(SOM_pc2,1)*size(SOM_pc2,2),1); hh(:)=SOM_pc2;
            %        if ~isequal(SOM_pc,hh)
            %            disp('nOK');
            %        end
            %       Ds_this=dist(SnOMs{j}',SOM_pc); DsM=mean(Ds_this);
            Ds_this=dist(SnOMs{j}',hh); %DsM=mean(Ds_this);
            % Ds_this=-SnOMs{j}'*hh; %DsM=mean(Ds_this);
            [DsM,Dssort]=sort(Ds_this,'ascend');
            DsM=mean(DsM(1:knN));
            if DsM<DsMmin(j)
                DssortA(j,:)=Sort_nOM{j}(Dssort(1:knN));
                DsK(j)=k;
                DsMmin(j)=DsM;
            end
            
            r=find(DsM<Sim_nOM{j},1,'first');
            if ~isempty(r)
                rcont=min(rcont,r);
            end
        end
        if isempty(rcont)
            Rs(j)=1;
        else
            Rs(j)=rcont/LnOM(j);
        end
    end
    [Flat.X.custom(s),which_clust]=min(Rs(2:end-1));
    % Flat.X.clust_ID(s)=1+which_clust;
    if verbose
        if i>1
            pause
        end
        for j=2:nclust
            set(hclust{j},'XData',DsMmin(j)*[1 1]);
        end
        
        width_samp=Flat.spec.numbuffs{2}(1+which_clust)-1;
        offs=Flat.p.nfft-(DsK(1+which_clust)-Flat.p.umap.ClustWinOffset)*Flat.p.nonoverlap;
        [SOM_pc,is_truncated]=func_extract_spec2(Flat,Flat.X.DATindex(s),Flat.X.indices_all(s)+offs,Flat.X.indices_all(s)+offs+length(Flat.p.nfft_i)*width_samp,0);
        
        figure(42); clf;  h1=subplot('position',[1/(knN+4) 0.05 1/(knN+4) .9]);
        imagesc(SOM_pc(nfft_is,:)); axis xy; axis off
        %h=selsnOM(Flat.X.clust_ID(selsnOM)==1+which_clust);
        %S=func_extract_data_fullspec(Flat,2,h(1:3));
        Flat.v.curr_clust=1+which_clust;
        S=func_extract_data_fullspec(Flat,2,DssortA(1+which_clust,:));
        h2=subplot('position',[3/(knN+4) 0.05 (knN)/(knN+4) .9]);
        imagesc(S(nfft_is,:));
        axis xy; axis off
        
        figure(43); clf;
        [SOM_pc,is_truncated]=func_extract_spec2(Flat,Flat.X.DATindex(s),Flat.X.indices_all(s)+offs-con_win,Flat.X.indices_all(s)+offs+length(Flat.p.nfft_i)*width_samp+con_win,0);
        
        if Flat.X.custom(s)<1
            h1=subplot('position',[.1 1/(knN+4) .8 1/(knN+4)]);
            imagesc(SOM_pc); axis xy; axis off;
            SS=cell(1,knN);
            for j=1:knN
                si=DssortA(1+which_clust,j);
                [SS{j},is_trunc]=func_extract_spec2(Flat,Flat.X.DATindex(si),Flat.X.indices_all(si)+offs-con_win,Flat.X.indices_all(si)+offs+length(Flat.p.nfft_i)*width_samp+con_win,0);
                SS{j}=SS{j}';
                %              if is_trunc
                %                  keyboard
                % %                 disp('truncated');
                %              end
            end
            h2=subplot('position',[.1 3/(knN+4) .8 knN/(knN+4)]);
            imagesc([SS{:}]');
            axis xy; axis off
        end
    end
    Flat.X.clust_ID(s)=1;
    fprintf('Rank %d, in clust: %d\n',round(100*Flat.X.custom(s)),1+which_clust)
end
Flat.v.curr_clust=1;
if ~verbose
    close(hbar);
end
end