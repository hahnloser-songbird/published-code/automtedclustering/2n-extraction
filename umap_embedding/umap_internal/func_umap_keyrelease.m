function func_umap_keyrelease()

Flat = evalin('base', 'Flat');
fcH = evalin('base', 'fcH');
key=get(fcH.hf_umap,'CurrentKey');

%%
if strcmp(key,'shift') && ismember(Flat.v.umap.lastkey,{'rightarrow','leftarrow','uparrow','downarrow','hyphen','equal'})
    [fcH,Flat]=func_help_umap_map_to_I(fcH,Flat);
    [fcH,Flat]=func_umap_select(fcH,Flat,Flat.umap.X);
rand(1)
end
%[Flat,fcH]=func_spec_keyrelease2(Flat,fcH);
Flat.v.umap.evnt_modifier='';

%%
assignin('base', 'Flat', Flat);
assignin('base', 'fcH', fcH);


end
%% EOF
