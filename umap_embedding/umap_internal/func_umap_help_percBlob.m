function [percB,which_in_slice]=func_umap_help_percBlob(Flat)
L=length(Flat.umap.ContClickHist);
percB=zeros(1,L);
which_in_slice=cell(1,L);
for i=1:L
    x=Flat.umap.ContClickHist(i).slices;
    M=mode(x,'all');
    h=find(x==M); x(h)=0;
    which_in_slice{i}=h;
    %   h=find(Flat.umap.ContClickHist(i).slices==Flat.umap.ContClickHist(i).timeslice);
    h=find(x);
    percB(i)=1-length(h)/length(x);
end
end