function Flat=func_umap_brute_forcekN(Flat,do_verbose)
%% find kNN
% Flat.X.custom: 1 = onset buffer in Flat.Z,
%                2 = offset buffer in Flat.Z
%                3 = Similarity Value
if nargin<2
    do_verbose=0;
end
k=Flat.v.curr_clust;

if isfield(Flat.p.umap.brute_force,'knN')
    knN=Flat.p.umap.brute_force.knN;
else
    knN=1;
end
%knN=min(10,knN);
bias_compensate=0;

Flat.p.umap.brute_force.knN=knN;
Flat.p.umap.brute_force.bias_compensate=bias_compensate;
nclusts=Flat.umap.brute_force.nclusts;

if k>nclusts
    disp('Cluster out of bound - aborting');
    return
end


if do_verbose
    MinDurNo=input('Duration at least (which element number in sorted mode) : ');
    fprintf('Min Dur: %d\n',Flat.umap.brute_force.DursC{k}(MinDurNo));
end
DursC=Flat.umap.brute_force.DursC{k};%(MinDurNo:end);
minDurC=min(DursC);

    
%% Remove unused junk syllables and make sure stencil is OK
if isfield(Flat.umap.brute_force,'Clast')
    Flat.v.tagboard0_recompute=1;
    [Flat,sels,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS},'Mark',{Flat.umap.brute_force.Clast});
    if ~isempty(sels)
        fprintf('Now removing Mark=%s syllables\n',Flat.umap.brute_force.Clast);
        Flat=func_remove_elements(Flat,sels,0,1);
        [Flat,OK]=func_umap_Z_test(Flat);
        if ~OK
            disp('Run brute_forcekN again');
            return
        end
    end
else
    [Flat,sels,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS});
    Flat.Tags(4,sels)={'[]'};
end
Rstr=['C' num2str(k)];
Flat.umap.brute_force.Clast=Rstr;



%% compute Cosm from Cosm0
Cosm0=Flat.umap.brute_force.Cosm0;
selsM0=Flat.umap.brute_force.selsM0;
selsM=find(Flat.Z.stencil==0);
%Flat.p.selsMb=selsM;
[selsM,NM,PCcoeffsM]=func_umap_help_Nbuffs(Flat,selsM,[],minDurC);
if isempty(NM)
    Flat.v.curr_clust=nclusts+1;
    return
end
%Flat.p.selsM=selsM;

No=size(NM,2);
N=size(selsM,2);
Cosm=cell(1,N);

c0=1; % index into Cosm0
[s1,s1i]=sort(selsM(1,:),'ascend'); % sort by time
s2=selsM(2,s1i);
[s01,s01i]=sort(selsM0(1,:),'ascend');
%s02=selsM0(2,s01i);

for i=1:N
    i0=c0-1+find(s1(i)>=s01(c0:end),1,'last');
    c0=i0;
    i0=s01i(i0);
    don=1+s1(i)-s01(c0);
    %Cosm{s1i(i)}=Cosm0{k,i0}(:,don:end);
    durm=s2(i)-s1(i)-minDurC;
    try
        Cosm{s1i(i)}=Cosm0{k,i0}(:,don:don+durm);
    catch
        Cosm{s1i(i)}=-1e11*ones(size(Cosm0{k,i0},1),durm+1);
        disp('Cosm problem');
    end
end
eat.Cosm=Cosm;
eat.selsM=selsM;

%% prepare New syllables
L=length(Flat.DAT.filename);
tagvec=cell(length(Flat.p.tagnames),1); tagvec(:)={''}; tagvec{5}=Flat.v.RMS; tagvec{4}=Rstr;
Ons=cell(1,L); Offs=Ons; ClustIDs=Ons; Custs=Ons; Tags=Ons;
Cinsert=zeros(1,L);


%% eat eat.selsM away until nothing is left
to_remove=[];
for i=1:No % all omit regions
    % region too small for even shortes syllable => remove
    if eat.selsM(2,i)-eat.selsM(1,i)<minDurC
        to_remove=[to_remove i];
    end
end
% remove
if ~isempty(to_remove)
    eat.selsM(:,to_remove)=[];
    eat.Cosm=eat.Cosm(to_remove(end)+1:end);
end
ntot=size(eat.selsM,2);

% -------------- #####
% 11111111111111 #####
% ---22222222222 #####
% -------3333333 #####

numFix=0;
cbum=0;
%tic
% while region left & large enough
hbar = waitbar(0,'Please wait...');
tstart=now;
L0=size(eat.selsM,2);
while ~isempty(eat.selsM) %& diff(eat.selsM(1:2,1))>=minDurC
    L1=size(eat.selsM,2);
    waitbar((L0-L1)/L0,hbar,[datestr((now-tstart)/(L0-L1+1)*(L1),'HH:MM:SS')]); drawnow
    
    % if too few syllables
    cbum=cbum+1;
    durM=diff(eat.selsM(1:2,1));
    if durM<minDurC
        eat.Cosm=eat.Cosm(2:end);   eat.selsM=eat.selsM(:,2:end);
        continue
    end
    if any(Flat.Z.stencil(eat.selsM(1,1):eat.selsM(2,1)))
        keyboard
    end
    %     if Flat.p.umap.GPUdevice
    %         CosmG=gpuArray(eat.Cosm{1});
    %     else
    CosmG=eat.Cosm{1};
    %    end
     if do_verbose
         CosmG(1:MinDurNo,:)=-1e11;
     end
%         shorti=find(DursC>DursC(MinDurNo) & DursC<=durM);
%     else
        shorti=find(DursC<=durM);
%    end
    if bias_compensate
        CosmG=CosmG./(sqrt(DursC)'*size(CosmG,2));
    end
    % shorti=1:(find(DursC>durM,1,'first')-1);
    %     if length(shorti)>5
    %         keyboard
    %     end
    d=[1 1+diff(DursC(shorti))]; fd=find(d); fd=[fd length(d)+1];
    %     CosmH=CosmG;
    for i=1:length(fd)-1
        CosmG(fd(i):fd(i+1)-1,max(1,durM-DursC(fd(i))+2):end)=-1e11;
    end
    %     for i=1:length(shorti)%1:length(DursC)
    %         CosmG(shorti(i),max(1,durM-DursC(shorti(i))+2):end)=-1e11;
    %     end
    %     if ~isequal(CosmG,CosmH)
    %         keyboard
    %     end
    %   do_it=1;
    %   while do_it
    %    [cms,whereCms]=sort(CosmG(shorti,:),1,'descend'); % for each buffer sort by similarity
    [cms,whereCms0]=maxk(CosmG(shorti,:),knN,1); % for each buffer sort by similarity
    whereCms=shorti(whereCms0);
    cms=mean(cms(1:min(end,knN),:),1);
    [Cmax,Cmaxi]=max(cms); % max across buffers, Cmaxi(k)=buffer position
    %   inds=whereCms(shorti(1:min(end,knN)),Cmaxi);
    inds=whereCms(1:min(end,knN),Cmaxi);
    sylhdur=DursC(inds);
    if any(sylhdur>durM)
        keyboard
    end
    [maxdur,maxdurpos]=max(sylhdur);
    
    syli=inds(maxdurpos); % longest syllable
    son=eat.selsM(1,1)+Cmaxi-1;  %  selsM is an index into Flat.DAT (1=index into Flat.Y, 2,3=on&offset in buffers)
    dur=DursC(syli);
    
    while any(Flat.Z.stencil(son:son+dur))
        disp('Problem with stencil when creating element');
        dur=dur-1;
        %son=son-1;
    end
    
    Di=Flat.Z.DATindex(son);
    
    onoff(1)=Flat.Z.indices_all(son);
    onoff(2)=onoff(1)+Flat.p.nonoverlap*dur;
    
    Cinsert(Di)=Cinsert(Di)+1;
    Ons{Di}(Cinsert(Di))=onoff(1);
    Offs{Di}(Cinsert(Di))=onoff(2);%+appendix;
    ClustIDs{Di}(Cinsert(Di))=nclusts+1;
    Custs{Di}(:,Cinsert(Di))=[son ; son+dur ; Cmax];
    
    %Flat.Z.stencil(son:son+dur)=-1;
    Tags{Di}(:,Cinsert(Di))=tagvec;
    
    % space at offset
    % 1-10 (dur=9), 11-20 (dur=9), so 20-1=19 > 1+9+9=19
    if eat.selsM(2,1)-eat.selsM(1,1)>=Cmaxi+dur+minDurC
        %   hoff.C=eat.C{1}(:,Cmaxi+dur+1:end);
        hoff.selsM=[eat.selsM(1,1)+Cmaxi+dur  ; eat.selsM(2,1) ; 0 ; 0];%eat.selsM(3,1)+Cmaxi+dur0+1 ; eat.selsM(4,1)];
        hoff.Cosm=CosmG(:,Cmaxi+dur+1:end);
        %  hoff.whereCms=eat.whereCms{1}(:,Cmaxi+dur+1:end);
    else
        hoff=[];
    end
    
    % space at onset
    % Cmaxi=11> dur=9
    if Cmaxi-1>minDurC
        % hon.C=eat.C{1}(:,1:Cmaxi-minDurC);
        hon.selsM=[eat.selsM(1,1) ; eat.selsM(1,1)-2+Cmaxi ; 0 ; 0];
        hon.Cosm=CosmG(:,1:Cmaxi-1-minDurC);
        %      hon.Cosm=CosmG(:,1:Cmaxi-minDurC);
        %       ndurM=diff(hon.selsM(1:2));
        %         for i=1:length(DursC)
        %             hon.Cosm(i,max(1,ndurM-DursC(i)+2):end)=-10;
        %         end
    else
        hon=[];
    end
    eat.Cosm{1}=CosmG;
    % space at both beginning and end
    if ~isempty(hoff) && ~isempty(hon)
        eat.Cosm(3:end+1)=eat.Cosm(2:end); eat.Cosm(1)={hon.Cosm}; eat.Cosm(2)={hoff.Cosm};
        eat.selsM=[hon.selsM hoff.selsM eat.selsM(:,2:end)];
        
    elseif ~isempty(hoff)
        eat.Cosm(1)={hoff.Cosm};   eat.selsM(:,1)=hoff.selsM;
    elseif  ~isempty(hon)
        eat.Cosm(1)={hon.Cosm}; eat.selsM(:,1)=hon.selsM;
    else
        eat.Cosm=eat.Cosm(2:end);  eat.selsM=eat.selsM(:,2:end);
    end
    
    if ~isempty(eat.Cosm) & ~isequal(diff(eat.selsM(1:2,1))-minDurC,size(eat.Cosm{1},2)-1)
        keyboard
    end
end
close(hbar);
tend=now;
%toc
%fprintf('%d/%d fixed regions\n',numFix,ntot);
Flat=func_write_imported_elements_to_Flat(Flat,Ons,Offs,ClustIDs,Custs,[],Tags,[],[]);

Flat.v.all_clust_mode=0;
Flat.v.plot_numbutton=1;
Flat.v.plot_tagboard=1;
Flat.v.curr_clust=nclusts+1;
Flat.v.plot_ordered=8;
Flat.v.run_select=1;
Flat.v.run_select_clust=1;
Flat.v.tagboard0_recompute=1;

marks=setdiff(Flat.v.tagboard0{4},Flat.umap.brute_force.Clast);
[Flat,select]=func_select_tagboard(Flat,[],'activator',Flat.v.RMS,'Mark',marks);
select=select(Flat.X.clust_ID(select)==k);
[h,hi]=sort(Flat.X.data_off(select),'descend');


%% do Mask
if 86400*(tend-tstart)<30
    disp('Now computing mask');
    [Flat,sels]=func_select_tagboard(Flat,[],'activator',Flat.v.RMS);
    sels=sels(Flat.X.clust_ID(sels)==(nclusts+1));
    clusts=Flat.p.umap.brute_force.clusts;
    Flat.DAT.Umap_on_off=cell(1,length(Flat.DAT.filename));
    Flat.DAT.Umap_on_off(:)={[]};
    
    %hbar = waitbar(0,'Please wait...');
    %tstart=now;
    for ii=1:length(sels)
        %     if rem(length(sels),10)==1
        %        % fprintf('%d Percent\n',round(100*ii/length(sels)));
        %        waitbar(i/length(sels),hbar,[datestr((now-tstart)/ii*(length(sels)-ii),'HH:MM:SS')]); drawnow
        %     end
        i=sels(ii);
        loc=Flat.X.custom(1,i);
        j=find(Flat.umap.brute_force.selsM0(1,:)<=loc & Flat.umap.brute_force.selsM0(2,:)>=loc);
        % dur=diff(Flat.X.custom(1:2,i));
        val=-inf(1,nclusts);
        sylsk=cell(1,nclusts);
        for kk=setdiff(clusts,k)
            syls=1:size(Flat.umap.brute_force.NbuffsC{kk},2);
            sylsk{kk}=syls;
            if isempty(j) | isempty(Flat.umap.brute_force.Cosm0{kk,j})
                continue
            end
            vals=Flat.umap.brute_force.Cosm0{kk,j}(syls,min(end,loc-Flat.umap.brute_force.selsM0(1,j)+1));
            [cms,whereCms]=maxk(vals,knN,1); % for each buffer sort by similarity
            val(kk)=mean(cms(1:min(end,Flat.p.umap.brute_force.knN),:),1);
        end
        if all(cellfun(@isempty,sylsk))
            continue
        end
        [max_val,winning_clust]=max(val);
        if max_val<Flat.X.custom(3,i)
            Flat.DAT.Umap_on_off{Flat.X.DATindex(i)}(:,end+1)=Flat.X.indices_all(i)+[0 ; Flat.X.data_off(i)];
        end
    end
else
    if isfield(Flat.DAT,'Umap_on_off')
    Flat.DAT=rmfield(Flat.DAT,'Umap_on_off');
    end
end

%close(hbar);
%%
Flat.umap.brute_force.specID_examples=select(hi(1:min(end,2)));
Flat.v.repeated_put_to=k; % ready for move
Flat.p.display_marks=1;
Flat.v.leftmost_element=max(1,1+sum(Cinsert)-Flat.p.spec_num);
Flat.v.spec_plot_IDs=[];
end

