function Flat=func_umap_final_statistics(Flat,do_verbose)
% set do_single_slice to 1 if you want to analyze only the main time slice within your blob 
if nargin<2
    do_verbose=1;
end

if ~isfield(Flat.Z,'stencil0') || ~(length(Flat.Z.stencil0)==length(Flat.Z.stencil))
    disp('Aborting final stats')
  %  keyboard
    return
end
[Flat,sels,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS});
fprintf('Overlap with original stencil=%.2f perc.\n',100*sum(Flat.Z.stencil==Flat.Z.stencil0)/length(Flat.Z.stencil));

H=Flat.umap.ContClickHist;
Cs=unique([H.ClustNo]);

if isfield(Flat.p.umap,'brute_force') & isfield(Flat.p.umap.brute_force,'clusts')
    clusts=Flat.p.umap.brute_force.clusts;
else
    clusts=Cs;
end
LovP=eps*ones(1,max(clusts)); LovN=LovP; Fs=LovP; Lov2=LovP;

if ~isfield(Flat.Z,'umapCtrWP')
     Flat=func_umap_ZumapCtr(Flat);
end
sel0=find(Flat.Z.custom0(1,:));
% for g3k1
% Flat.Z2.stencil0=0*Flat.Z.stencil0;
% for i=1:length(sel0)
%     Flat.Z2.stencil0(Flat.Z.custom0(1,sel0(i)):Flat.Z.custom0(2,sel0(i)))=Flat.Z.stencil0(Flat.Z.custom0(1,sel0(i)));
% end
% Flat.Z.stencil0=Flat.Z2.stencil0;
Flat.v.umap.StatsP=zeros(2,max(clusts));
Flat.v.umap.StatsN=Flat.v.umap.StatsP;
Flat.v.umap.Stats2=Flat.v.umap.StatsP;
Flat.v.umap.StatsNum=zeros(3,max(clusts));

percB=func_umap_help_percBlob(Flat);
c=0; Flat.v.umap.Stat0=zeros(1,2);
for k=clusts
    %sum(ismember(H(k).Selectonoffset,Flat.umap.X0.select))/length(H(k).Selectonoffset)
    %% get F value
    ii=find([H.ClustNo]==k);
    if length(ii)~=2 % if not two blobs, then drop computation
        f=1;
        disp('Not two blobs');
    else
        s1=H(ii(1)).Selectonoffset; s2=H(ii(2)).Selectonoffset;
        if strcmp(Flat.v.RMS(1),'#')
            s1=find(ismember(Flat.umap.X0.select_old,s1));
            s2=find(ismember(Flat.umap.X0.select_old,s2));
        else
            s1=find(ismember(Flat.umap.X0.select,s1));
            s2=find(ismember(Flat.umap.X0.select,s2));
        end
            x1=Flat.Y.umap(:,s1);  x2=Flat.Y.umap(:,s2);
            l1=size(x1,2); l2=size(x2,2);
            m1=mean(x1,2); m2=mean(x2,2); m=mean([x1 x2],2);
            varx=(sum(sum((x1-m1).^2))+sum(sum((x2-m2).^2)))/(l1+l2-2);
            f=(l1*sum((m1-m).^2)+l2*sum((m2-m).^2))/varx;
            c=c+1;
            Flat.v.umap.Stat0(c,:)=[100*percB(ii(1)) 100*percB(ii(2))];
        if do_verbose
            figure(2);clf; plot(x1(1,:),x1(2,:),'.'); hold on;
            plot(x2(1,:),x2(2,:),'r.');
            fprintf('Percent top slice 1: %.1f, 2: %.1f\n',100*percB(ii(1)),100*percB(ii(2)));
            pause
        end
    end
    Fs(k)=f;
  

    %% compute precision and recall
    selk=sels(Flat.X.clust_ID(sels)==k);
    ovC=0; ovP=0; ovN=0; sumTP=0; sumC=0; ov2=0;
    for i=1:length(sel0)
        sumC=sumC+length(find(Flat.Z.stencil0(Flat.Z.custom0(1,sel0(i)):(Flat.Z.custom0(2,sel0(i))-0*Flat.umap.appendixB))==k));
    end
    for i=1:length(selk)
        j=selk(i);
        ovC=ovC+sum(Flat.Z.stencil0(Flat.X.custom(1,j):(Flat.X.custom(2,j)-0*Flat.umap.appendixB))==k);
        d=diff(Flat.X.custom(1:2,j))-0*Flat.umap.appendixB;
        
        sumTP=sumTP+1+d;
       
        % new start
         zind1=Flat.X.custom(1,j); zind2=Flat.X.custom(2,j);%-Flat.umap.appendixB;
        if size(Flat.Z.umapCtrWP,1)>=k
            ovP=ovP+sum(Flat.Z.umapCtrWP(k,zind1:zind2)==k);
        end
       if size(Flat.Z.umapCtrWN,1)>=k
            ovN=ovN+sum(Flat.Z.umapCtrWN(k,zind1:zind2)==-int8(k));
        end
          ov2=ov2+sum(Flat.Z.umapCtrWP(1,zind1:zind2)==int8(k));
          
        % old start
%         yind1=Flat.Z.Yindex(Flat.X.custom(1,j)); yind2=Flat.Z.Yindex(Flat.X.custom(2,j)-Flat.umap.appendixB);
%         if size(Flat.Y.umapCtrWP,1)>=k
%             ovP=ovP+sum(Flat.Y.umapCtrWP(k,yind1:yind2)==k);
%         end
%         if size(Flat.Y.umapCtrWN,1)>=k
%             ovN=ovN+sum(Flat.Y.umapCtrWN(k,yind1:yind2)==-int8(k));
%         end
%         ov2=ov2+sum(Flat.Y.umapCtrWP(1,yind1:yind2)==int8(k));
    end
     if size(Flat.Z.umapCtrWP,1)>=k
        LovP(k)=eps+length(find(Flat.Z.umapCtrWP(k,:)));
    end
    if size(Flat.Z.umapCtrWN,1)>=k
        LovN(k)=eps+length(find(Flat.Z.umapCtrWN(k,:)));
    end
    Lov2(k)=eps+length(find(Flat.Z.umapCtrWP(1,:)==k));
    
%     if size(Flat.Y.umapCtrWP,1)>=k
%         LovP(k)=length(find(Flat.Y.umapCtrWP(k,:)));
%     end
%     if size(Flat.Y.umapCtrWN,1)>=k
%         LovN(k)=length(find(Flat.Y.umapCtrWN(k,:)));
%     end
%     Lov2(k)=length(find(Flat.Y.umapCtrWP(1,:)==k));
    % old end
    
    fprintf('Cluster %d: f=%.2g precision of stencil0=%.2f, W2=%.2f, WP=%.2f, WN=%.2f percent.\n',k,Fs(k),100*ovC/sumC,100*ov2/Lov2(k),100*ovP/LovP(k),100*ovN/LovN(k));
    fprintf('                   recall of stencil0=%.2f, W2=%.2f, WP=%.2f, WN=%.2f percent.\n\n',100*ovC/sumTP,100*ov2/sumTP,100*ovP/sumTP,100*ovN/sumTP);
   Flat.v.umap.StatsP(:,k)=[100*ovP/LovP(k);100*ovP/sumTP];
   Flat.v.umap.StatsN(:,k)=[100*ovN/LovN(k);100*ovN/sumTP];   
%   Flat.v.umap.Stats2(:,k)=[100*ovC/sumC;100*ovC/sumTP];
   Flat.v.umap.Stats2(:,k)=[100*ov2/Lov2(k);100*ov2/sumTP];
   Flat.v.umap.StatsNum(:,k)=[LovP(k); LovN(k) ; Lov2(k)];
end
end