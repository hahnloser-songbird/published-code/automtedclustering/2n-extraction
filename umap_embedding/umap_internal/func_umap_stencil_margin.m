function Flat=func_umap_stencil_margin(Flat)
% this function adds a margin to the stencil
%
% if ~isfield(Flat.p.umap.brute_force,'stencil_margin')
%     Flat.p.umap.brute_force.stencil_margin=0;
% else
%     disp('Margin can only be added once - press a key to abort');
%     pause
%     return
% end

if isfield(Flat,'umap') & isfield(Flat.umap,'brute_force')
    disp('Currently not allowed while doing brute force searching - aborting');
    return
end
sel=func_select_mic(Flat,Flat.p.channel_mic_display);
sel1=sel(Flat.X.clust_ID(sel)==1);
if ~isempty(sel1)
    disp('Junk cluster 1 must be empty, press a key to remove cluster 1 ');
    pause
    Flat=func_remove_elements(Flat,sel1,0,1);
end
if ~isfield(Flat.p.umap.brute_force,'stencil_margin')
    Flat.p.umap.brute_force.stencil_margin=2;
end
fprintf('Current stencil margin: %d\n',Flat.p.umap.brute_force.stencil_margin);

y=input('Add what margin (enter=abort): ');
if isempty(y)
    disp('Aborting');
    return
else
    Flat.p.umap.brute_force.stencil_margin_tot=Flat.p.umap.brute_force.stencil_margin+y;
    Flat.p.umap.brute_force.stencil_margin=y;
end
Margin=Flat.p.umap.brute_force.stencil_margin;

if isfield(Flat.Z,'custom0')
    Flat=func_umap_ZumapCtr(Flat);
end

ci=0;
Z=Flat.Z;
Z1=Z;
Z.new_index=zeros(1,length(Flat.Z.DATindex));
for i=1:length(Flat.Z.DATindex)-1
    
    dBuff=Z.buffer(i+1)-Z.buffer(i);
    % same file, in syllable
    if Z.DATindex(i)==Z.DATindex(i+1) && dBuff==1
        ci=ci+1;
        Z1.DATindex(ci)=Z.DATindex(i);
        Z1.buffer(ci)=Z.buffer(i);
        Z1.indices_all(ci)=Z.indices_all(i);
        Z1.stencil(ci)=Z.stencil(i);
        if isfield(Z,'stencil0')
            Z1.stencil0(ci)=Z.stencil0(i);
        end
        if isfield(Z,'Yindex')
            Z1.Yindex(ci)=Z.Yindex(i);
        end
        if isfield(Z,'umapCtrWP')
            Z1.umapCtrWP(:,ci)=Z.umapCtrWP(:,i);
            Z1.umapCtrWN(:,ci)=Z.umapCtrWN(:,i);
        end
        Z.new_index(i)=ci;
        continue
    end
    ci=ci+1;
    
    Z1.DATindex(ci)=Z.DATindex(i);
    Z1.buffer(ci)=Z.buffer(i);
    Z1.indices_all(ci)=Z.indices_all(i);
    Z1.stencil(ci)=Z.stencil(i);
    if isfield(Z,'stencil0')
        Z1.stencil0(ci)=Z.stencil0(i);
    end
    if isfield(Z,'Yindex')
        Z1.Yindex(ci)=Z.Yindex(i);
    end
    if isfield(Z,'umapCtrWP')
        Z1.umapCtrWP(:,ci)=Z.umapCtrWP(:,i);
        Z1.umapCtrWN(:,ci)=Z.umapCtrWN(:,i);
    end
    Z.new_index(i)=ci;
    
    % extend end
    j=1;
    while j<=Margin && (j<dBuff || Z.DATindex(i)<Z.DATindex(i+1))  && Z.indices_all(i)+j*Flat.p.nonoverlap<=Flat.DAT.eof(Z.DATindex(i))
        %i+j<=func_numbuffs(Flat.DAT.eof(Z.DATindex(i)),Flat.p.nfft,Flat.p.nonoverlap) %  length(Z.DATindex) && Z.DATindex(i)==Z.DATindex(i+j)
        ci=ci+1;
        j=j+1;
        Z1.DATindex(ci)=Z.DATindex(i);
        Z1.buffer(ci)=Z1.buffer(ci-1)+1;
        Z1.indices_all(ci)=Z1.indices_all(ci-1)+Flat.p.nonoverlap;
        Z1.stencil(ci)=0;
        Z1.stencil0(ci)=0;
        if isfield(Z,'Yindex')
           Z1.Yindex(ci)=Z.Yindex(i);
        end
        Z1.umapCtrWP(:,ci)=0;
        Z1.umapCtrWN(:,ci)=0;
        
    end
    j0=j;
    
    % if new file
    if Z.DATindex(i)<Z.DATindex(i+1)
        j0=0;
        dBuff=Z.buffer(i+1);
    end
    
    % extend start
    for j=(Z.buffer(i+1)-min(Margin,dBuff-j0)):(Z.buffer(i+1)-1)
        ci=ci+1;
        Z1.DATindex(ci)=Z.DATindex(i+1);
        Z1.buffer(ci)=j;
        Z1.indices_all(ci)=double(Z.indices_all(i+1))-Flat.p.nonoverlap*(double(Z.buffer(i+1))-j);
        Z1.stencil(ci)=0;
        Z1.stencil0(ci)=0;
        Z1.umapCtrWP(:,ci)=0;
        Z1.umapCtrWN(:,ci)=0;
        if isfield(Z,'Yindex')
           Z1.Yindex(ci)=Z.Yindex(i+1);
       end
    end
    
end
sel=func_select_mic(Flat,Flat.p.channel_mic_display);
for i=1:length(sel)
    Flat.X.custom(1,sel(i))=Z.new_index(Flat.X.custom(1,sel(i)));
    Flat.X.custom(2,sel(i))=Z.new_index(Flat.X.custom(2,sel(i)));
end
if isfield(Flat.Z,'custom0')
    sel0=find(Flat.Z.custom0(1,:));
    for i=1:length(sel0)
        Z1.custom0(1,sel0(i))=Z.new_index(Flat.Z.custom0(1,sel0(i)));
        Z1.custom0(2,sel0(i))=Z.new_index(Flat.Z.custom0(2,sel0(i)));
    end
end

Flat.Z=Z1;
Flat=func_umap_Z_test(Flat);
disp('done');
end
