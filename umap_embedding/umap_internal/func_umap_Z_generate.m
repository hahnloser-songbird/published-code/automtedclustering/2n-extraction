function Flat=func_umap_Z_generate(Flat,correct_custs)
% generate Flat.Z a completely flat structure in buffer units
% Flat.Z is generated from Flat.Y or Flat.X (if the fomer does not exist)
% Flat.Y and Flat.Z are first generated in func_import_elements_from_DAT_write_to_Flat

if nargin<2
    correct_custs=0;
end
disp('Note: never run this function when you either have dealer or junk syllables present.');
%pause

if isfield(Flat,'Y')
    Y=Flat.Y;
else
    [Flat,sels,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS});
    if isfield(Flat.p,'umap') & isfield(Flat.p.umap,'Ygen')
        fn=Flat.p.umap.Ygen;
    else
        fn={'indices_all','data_off','DATindex','PCcoeffs','umapCtrWP','umapCtrWN','umap'};
    end
    for i=1:length(fn)
        if isfield(Flat.X,fn{i})
            Y.(fn{i})=Flat.X.(fn{i})(:,sels);
        end
    end
end
L=length(Y.DATindex);
Z.DATindex=int32(zeros(1,2*L));
Z.indices_all=int64(zeros(1,2*L));
Z.buffer=int32(zeros(1,2*L));
Z.Yindex=int32(zeros(1,2*L));
Z.stencil=int8(zeros(1,2*L));

Y.Zindex=int32(zeros(1,L));
%Flat.umap.appendixB=0;
if isfield(Flat,'umap') & isfield(Flat.umap,'appendixB')
    aB=Flat.umap.appendixB;
else
    aB=Flat.p.umap.MinNumBuffSyl-Flat.p.umap.MinNumTimeSegments;
    Flat.umap.appendixB=aB;
end

t0=Flat.DAT.timestamp(1);
c=0; stencilc=0;
for i=1:L
    
    % insert needed (new file or large gap)
    if i>1 & (Y.DATindex(i)>Y.DATindex(i-1) | Y.indices_all(i)-Y.indices_all(i-1)>aB*Flat.p.nonoverlap)
        if Z.indices_all(c)+Flat.p.nonoverlap*aB>Flat.DAT.eof(Z.DATindex(c))
            disp('Out of bound!!');
            keyboard
        end
        Z.DATindex(c+1:c+aB)=Z.DATindex(c);
        Z.indices_all(c+1:c+aB)=Z.indices_all(c)+Flat.p.nonoverlap*int64(1:aB);
        Z.buffer(c+1:c+aB)=Z.buffer(c)+int32(1:aB);
        Z.Yindex(c+1:c+aB)=0;
        c=c+aB;
    end
    Z.DATindex(c+1)=Y.DATindex(i);
    Z.indices_all(c+1)=Y.indices_all(i);
    Z.Yindex(c+1)=i;
    %   Z.Xindex(c+1)=i;
    Y.Zindex(i)=c+1;
    %     if ~isequal(Z.indices_all(Y.Zindex(i)),Y.indices_all(i))
    %         keyboard
    %     end
    if isfield(Y,'umapCtrWP')
        Z.stencil(c+1)= Y.umapCtrWP(1,i);
    end
    Bfile=Flat.scanrate/Flat.p.nonoverlap*(Flat.DAT.timestamp(Y.DATindex(i))-t0)*86400;
    Z.buffer(c+1)=round(Bfile+func_numbuffs(Y.indices_all(i),Flat.p.nfft,Flat.p.nonoverlap));
    c=c+1;
end

% insert for last segment
Z.DATindex(c+1:c+aB)=Z.DATindex(c);
Z.indices_all(c+1:c+aB)=Z.indices_all(c)+Flat.p.nonoverlap*int64(1:aB);
Z.buffer(c+1:c+aB)=Z.buffer(c)+int32(1:aB);
Z.Yindex(c+1:c+aB)=0;
c=c+aB;


Z.DATindex=Z.DATindex(1:c);
Z.indices_all=Z.indices_all(1:c);
Z.buffer=Z.buffer(1:c);
Z.Yindex=Z.Yindex(1:c);


%% do stencil
if correct_custs
    on=find([Z.stencil(1) diff(Z.stencil>0)]>0);
    off=find(diff(Z.stencil>0)==-1);
    for i=1:length(off)
        Z.stencil(off(i)+1:off(i)+aB)=Z.stencil(off(i));
        d=diff(Z.buffer(on(i):off(i)));
        if max(d)>1
            keyboard
        end
    end
    Z.stencil=Z.stencil(1:c);
end
Flat.Z=Z;

Flat.Y.Zindex=Y.Zindex;

% correct Flat.X.custom if requested
% in case 'q' on umap fig was run before Z was introduced - add appendix.
if correct_custs & ~isfield(Flat.umap,'Z_doneQ')
    [Flat,sels,board] = func_select_tagboard(Flat,[],'activator',{Flat.v.RMS});
    yi1=Flat.X.custom(1,sels); yi2=Flat.X.custom(2,sels);
    Flat.X.custom(1,sels)=Flat.Y.Zindex(yi1);
    Flat.X.custom(2,sels)=Flat.Y.Zindex(yi2);
    for i=1:length(sels)
        if  any(Flat.Z.stencil(Flat.X.custom(1,sels(i)):Flat.X.custom(2,sels(i)))==0)
            keyboard
        end
        Flat.umap.Z_doneQ=1;
    end
    Flat=func_umap_Z_test(Flat);
end
disp('done')

end