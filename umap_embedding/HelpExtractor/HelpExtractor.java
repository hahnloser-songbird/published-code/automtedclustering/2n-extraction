import java.io.*;
import java.util.ArrayList;

public class HelpExtractor {
	
	private static final String[] KEY_START_TOKEN = {"%^", "case '", "case {"};
	private static final String[] KEY_END_TOKEN = {"^%", "'", "}"};
	private static final String BRIEF_DESCRIPTION_START_TOKEN = "%$";
	private static final String BRIEF_DESCRIPTION_END_TOKEN = "$%";
	private static final String DETAILED_DESCRIPTION_START_TOKEN = "%*";
	private static final String DETAILED_DESCRIPTION_END_TOKEN = "*%";

	public ArrayList<String> readFile(String path) {
		ArrayList<String> lineHolder = new ArrayList<String>();
		try {
			FileInputStream fis = new FileInputStream(path);
			InputStreamReader isr = new InputStreamReader(fis);
			BufferedReader bufReader = new BufferedReader(isr);
			String currentLine = bufReader.readLine();
			while(currentLine != null) {
				lineHolder.add(currentLine);
				currentLine = bufReader.readLine();
			}
		}
		catch (IOException err) {
			System.out.println("Error reading line");
		}
		return lineHolder;
	}	
	
	public ArrayList<HelpEntry> extractHelp(String path) {
		return extractHelp(this.readFile(path));
	}

	public ArrayList<HelpEntry> extractHelp(ArrayList<String> lineHolder) {
		// An ArrayList to holding the extracted help lines
		ArrayList<HelpEntry> helpLines = new ArrayList<HelpEntry>();
		// Change it to the index of the switch statement
		int mainIndex = 0; int previousIndex = 0;
		// Declare variables to be used in extraction
		String currentLine = "", key = "", currentKey = "", modifier = "", briefDescription = "", detailedDescription = "";
		boolean extracting = false;
		while (mainIndex < lineHolder.size()) {
			// Remove extra spaces
			currentLine = lineHolder.get(mainIndex).replaceAll("\\s+", " ");
			// If it has the case statement
			if (currentLine.contains("case")) {
				currentKey = extractKey(currentLine);
				if (currentKey.equals("-1") == false) {
					key = currentKey;
					extracting = true;
				}
			}
			else if (currentLine.contains("if strcmp(modifier,")) {
				modifier = " + " + extractModifier(currentLine);
				extracting = true;
			}
			if (extracting) {
				briefDescription = extractDescription(lineHolder, mainIndex, previousIndex, BRIEF_DESCRIPTION_START_TOKEN,
						BRIEF_DESCRIPTION_END_TOKEN);
				detailedDescription = extractDescription(lineHolder, mainIndex, previousIndex, DETAILED_DESCRIPTION_START_TOKEN, 
						DETAILED_DESCRIPTION_END_TOKEN);
				HelpEntry currentEntry = new HelpEntry(key + modifier, briefDescription, detailedDescription);
				helpLines.add(currentEntry);
				previousIndex = mainIndex;
			}
			extracting = false; modifier = ""; mainIndex++;
		}
		return helpLines;  
	}

	private String extractKey(String codeLine) {
		int beginIndex = 0;
		int offset = 0;
		int endIndex = 0;
		for (int i = 0; i < KEY_START_TOKEN.length; i++) {
			beginIndex = codeLine.indexOf(KEY_START_TOKEN[i]);
			offset = KEY_START_TOKEN[i].length();
			endIndex = codeLine.indexOf(KEY_END_TOKEN[i],beginIndex + offset);
			if (beginIndex != -1 && endIndex != -1) {
				String keyName = codeLine.substring(beginIndex + offset, endIndex);
				return keyName.trim();
			}
		}
		return "-1";
	}

	private String extractModifier(String codeLine) {
		int beginIndex = codeLine.indexOf(KEY_START_TOKEN[0]);
		int offset = KEY_START_TOKEN[0].length();
		int endIndex = codeLine.indexOf(KEY_END_TOKEN[0], beginIndex + offset);
		if (beginIndex == -1) {
			beginIndex = codeLine.indexOf("'");
			offset = 1;
			endIndex = codeLine.indexOf("'", beginIndex + offset);
		}
		String modifier = codeLine.substring(beginIndex + offset, endIndex);
		return modifier.trim();
	}

	private String extractDescription(ArrayList<String> lineHolder, int mainIndex, int previousIndex,
			String startToken, String endToken) {
		String keyFunction = "";
		boolean done = false;
		int subIndex = mainIndex;
		boolean extracting = false;
		// Loop to find the starting position of the help paragraph
		while (!done && subIndex > previousIndex && subIndex < lineHolder.size() - 1) {
			subIndex = subIndex - 1;
			if (lineHolder.get(subIndex).contains(endToken)) {
				extracting = true;
			}
			if (lineHolder.get(subIndex).contains(startToken)) {
				extracting = true;
				done = true;
			}
			if (extracting) {
				keyFunction = lineHolder.get(subIndex) + keyFunction;
			}
		}
		// If no help found, then exit
		if (keyFunction.indexOf(startToken) == -1) return "No Description Given";
		// Else extract that help
		int beginIndex = keyFunction.indexOf(startToken) + 2;
		int endIndex = keyFunction.indexOf(endToken);
		if (endIndex == -1) {
			keyFunction = keyFunction.substring(beginIndex);
		}
		else {
			keyFunction = keyFunction.substring(beginIndex, endIndex);
			keyFunction = keyFunction.replaceAll("%", "").replaceAll("\\s+", " ");
		}
		return keyFunction.trim();
	}

	public void displayData(ArrayList<HelpEntry> lineHolder) {
		for (int i = 0; i < lineHolder.size(); i++) {
			System.out.println(lineHolder.get(i).toString());
		}
	}

	public static void main(String[] args) {
		String path = "/Volumes/Data HD/Work/Programming/Java/FlatClust/data/evalkey_clust.m";
		HelpExtractor helpExtractor = new HelpExtractor();
		ArrayList<String> lineHolder = helpExtractor.readFile(path);
		ArrayList<HelpEntry> helpLines = helpExtractor.extractHelp(lineHolder);
		helpExtractor.displayData(helpLines);
	}
}


