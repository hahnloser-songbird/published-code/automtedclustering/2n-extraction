
public class HelpEntry {
	
	private String mKey;
	private String mBriefDescription;
	private String mDetailedDescription;
	
	public HelpEntry() {
		this.mKey = "";
		this.mBriefDescription = "";
		this.mDetailedDescription = "";
	}
	
	public HelpEntry(String key, String briefDescription, String detailedDescription) {
		this.mKey = key;
		this.mBriefDescription = briefDescription;
		this.mDetailedDescription = detailedDescription;
	}
	
	public String getKey() {
		return this.mKey;
	}
	
	public String getBriefDescription() {
		return this.mBriefDescription;
	}
	
	public String getDetailedDescription() {
		return this.mDetailedDescription;
	}
	
	public String toString() {
		return this.mKey + ": " + this.mBriefDescription + "\n" + this.mDetailedDescription;
	}

}
